/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trminpet;

import audio.Sounds;
import sun.audio.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import trminpet.*;

/**
 *
 * @author Petr Trminek 2016
 */
public class Bomberman {

    static JFrame f;

    public Bomberman(int level) {

        /* Vytvoreni noveho hraciho okna */        
        f = new JFrame();
        f.setTitle("Bomberman");
        f.setSize(480, 507);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        f.setLocationRelativeTo(null);
        /* Pridani hraci plochy */
      
        Board b = new Board(level);
        f.add(b);
        f.setVisible(true);

    }

    /* Metoda na zavreni hraciho okna pri ukonceni hry a vyhlaseni viteze */

    public static void closeMap() {

        f.setVisible(false);
        f.dispose();

    }

}
