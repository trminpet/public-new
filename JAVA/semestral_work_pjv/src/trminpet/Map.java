/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trminpet;

import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;

/**
 *
 * @author Petr Trminek 2016
 */
public class Map {

    /**
     * Deklarace promennych
     *
     */

    private Scanner m;
    private static String map[] = new String[15];
    private Image block, crate, background, fire;

    public Map(int level) {
        /*Nacteni vzhledu bloku*/
        ImageIcon img = new ImageIcon(getClass().getResource("img/block.png"));
        block = img.getImage();

        /*Nacteni vzhledu bedny*/
        img = new ImageIcon(getClass().getResource("img/crate.png"));
        crate = img.getImage();
        
        /*Nacteni vzhledu ohne*/
        img = new ImageIcon(getClass().getResource("img/flame.png"));
        fire = img.getImage();
        
        /*Nacteni pozadi dle urovne*/
        background = setBackground(level);
        
        /*Metody na nacteni lvlu*/
        openFile(level);
        readFile();
        closeFile();

    }

    /*Metoda na vraceni vzhledu bloku*/
    public Image getBlock() {

        return block;

    }

    /*Metoda na vraceni vzhledu bedny*/
    public Image getCrate() {

        return crate;

    }

    /*Metoda na vraceni pozadi*/
    public Image getBackground() {

        return background;
    }

    /*Metoda na vraceni vzhledu ohne*/
    public Image getFire() {

        return fire;
    }

    /*Metoda na vraceni hodnoty na urcene pozici dle x,y pomoci substring*/
    public static String getMap(int x, int y) {
        /*Funkce vyparsuje jeden znak*/
        String index = map[y].substring(x, x + 1);

        return index;
    }

    /*Metoda na nacteni pozadi dle lvlu*/
    public Image setBackground(int level) {
        switch (level) {
            case 1:
                ImageIcon img = new ImageIcon(getClass().getResource("img/ground-grass.png"));
                background = img.getImage();
                break;
            case 2:
                img = new ImageIcon(getClass().getResource("img/ground-ice.png"));
                background = img.getImage();
                break;
            case 3:
                img = new ImageIcon(getClass().getResource("img/ground-sand.png"));
                background = img.getImage();
                break;
            case 4:
                img = new ImageIcon(getClass().getResource("img/ground-mountain.png"));
                background = img.getImage();
                break;
            case 5:
                img = new ImageIcon(getClass().getResource("img/ground-sand.png"));
                background = img.getImage();
                break;
            case 6:
                img = new ImageIcon(getClass().getResource("img/ground-grass.png"));
                background = img.getImage();
                break;

        }
        return background;
    }

    /*Metoda na otevreni lvlu-rozvrzeni*/
    public void openFile(int level) {
        try {

            switch (level) {
                case 1:
                    /*Obsah lvlu se nacte do scanneru*/
                    m = new Scanner(new File(getClass().getResource("levels/level1.txt").getFile()));
                    break;
                case 2:
                    m = new Scanner(new File(getClass().getResource("levels/level2.txt").getFile()));
                    break;
                case 3:
                    m = new Scanner(new File(getClass().getResource("levels/level3.txt").getFile()));
                    break;
                case 4:
                    m = new Scanner(new File(getClass().getResource("levels/level4.txt").getFile()));
                    break;
                case 5:
                    m = new Scanner(new File(getClass().getResource("levels/level5.txt").getFile()));
                    break;
                case 6:
                    m = new Scanner(new File(getClass().getResource("levels/level6.txt").getFile()));
                    break;

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    /*Metoda na nacteni souboru(lvlu) do pole stringu*/
    public void readFile() {
        /*Scanner (m) obsahuje lvl, ktery se pomoci while a for cyklu ulozi do pole Stringu*/
        while (m.hasNext()) {
            for (int i = 0; i < 15; i++) {
                map[i] = m.next();
                        
            }

        };

    }

    /*Metoda na uzavreni souboru*/
    public void closeFile() {
        m.close();

    }

    /*Metoda na zmenu mapy, dle situace - po vybuchu*/
    public void setMap(int x, int y) {

        int counter = 0;
        /*Exploze do prava*/
        while (counter < 4) {
            /*Pokud narazi na blok ukoncuje vybuch*/
            if (getMap(x + counter, y).equals("b")) {
                break;
            }
            char[] changeChar = map[y].toCharArray();
            /*Pokud narazi na bednu zmeni char v stringu na x */
            if (getMap(x + counter, y).equals("c")) {

                changeChar[x + counter] = 'x';
                map[y] = String.valueOf(changeChar);
                break;
            }
            /*Vytvoreni dojmu exploze - zobrazeni ohnu */
            changeChar[x + counter] = 'e';
            map[y] = String.valueOf(changeChar);
            /*Zvyseni rozsahu*/
            counter++;

        }
        
        /*Exploze do leva*/
        counter = 0;
        while (counter < 4) {
            /*Pokud narazi na blok ukoncuje vybuch*/
            if (getMap(x - counter, y).equals("b")) {
                break;
            }
            char[] changeChar = map[y].toCharArray();
            /*Pokud narazi na bednu zmeni char v stringu na x */
            if (getMap(x - counter, y).equals("c")) {

                changeChar[x - counter] = 'x';
                map[y] = String.valueOf(changeChar);
                break;
            }
            /*Vytvoreni dojmu exploze - zobrazeni ohnu */
            changeChar[x - counter] = 'e';
            map[y] = String.valueOf(changeChar);
            /*Zvyseni rozsahu*/
            counter++;

        }
        
        /*Exploze dolu*/
        counter = 1;
        while (counter < 4) {
            /*Pokud narazi na blok ukoncuje vybuch*/
            if (getMap(x, y + counter).equals("b")) {
                break;
            }
            char[] changeChar = map[y + counter].toCharArray();
            /*Pokud narazi na bednu zmeni char v stringu na x */
            if (getMap(x, y + counter).equals("c")) {

                changeChar[x] = 'x';
                map[y + counter] = String.valueOf(changeChar);

                break;
            }
            /*Vytvoreni dojmu exploze - zobrazeni ohnu */
            changeChar[x] = 'e';
            map[y + counter] = String.valueOf(changeChar);
            /*Zvyseni rozsahu*/
            counter++;

        }
        
        /*Exploze nahoru*/
        counter = 1;
        while (counter < 4) {
            /*Pokud narazi na blok ukoncuje vybuch*/
            if (getMap(x, y - counter).equals("b")) {
                break;
            }
            char[] changeChar = map[y - counter].toCharArray();
            /*Pokud narazi na bednu zmeni char v stringu na x */
            if (getMap(x, y - counter).equals("c")) {

                changeChar[x] = 'x';
                map[y - counter] = String.valueOf(changeChar);

                break;
            }
            /*Vytvoreni dojmu exploze - zobrazeni ohnu */
            changeChar[x] = 'e';
            map[y - counter] = String.valueOf(changeChar);
            /*Zvyseni rozsahu*/
            counter++;

        }

        try {
            /*Uspani vlakna na 0.5 sec aby sla videt exploze v podobě plamenu*/
            Thread.sleep(500);

        } catch (InterruptedException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*Zavolani metody na odstraneni exploze*/
        rmExplosion();

    }

    /*Metoda nahradi v poli stringu vsechny exploze (e) za prazdny prostor (x) */
    public void rmExplosion() {
        /*V radku nahrazuje vyskyt e za x */
        for (int y = 0; y < 15; y++) {
            map[y] = map[y].replace("e", "x");

        }

    }

}
