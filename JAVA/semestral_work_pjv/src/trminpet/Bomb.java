/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trminpet;

import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;

/**
 *
 * @author Petr Trminek 2016
 */
public class Bomb extends Thread {

    /*Deklarace promennych*/
    private int xBomb;
    private int yBomb;
    private boolean planted = false;
    private Image BombImg;
    private static Map m;
    private Player p;
    private Board b;

    public Bomb(int tx, int ty) {
        ImageIcon img = new ImageIcon(getClass().getResource("img/bomb.png"));
        BombImg = img.getImage();
        this.xBomb = tx;
        this.yBomb = ty;

    }

    /*Metoda vraci vyhled bomby*/
    public Image getBomb() {
        return BombImg;

    }

    /*Nastaveni pozice bomby*/
    public void setBomb(int dx, int dy) {
        this.xBomb = dx;
        this.yBomb = dy;

    }

    /*Metoda vraci X bomby*/
    public int getxBomb() {

        return xBomb;

    }

    /*Metoda vraci Y bomby*/
    public int getyBomb() {

        return yBomb;
    }

    /*Metoda ridi vybuch*/
    @Override
    public void run() {
        try {
            planted = true;
            Thread.sleep(1500);
            /*Nacteni mapy z aktualni hraci plochy*/
            m = b.getM();
            /*Exploze po 1.5 sec*/
  
            m.setMap(xBomb / 32, yBomb / 32);

            planted = false;
        } catch (InterruptedException ex) {
            Logger.getLogger(Bomb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*Metoda vraci zda je bomba polozena*/
    public boolean isPlanted() {
        return planted;
    }

    /*Metoda urci polozenost bomby*/
    public void setPlanted(boolean planted) {
        this.planted = planted;
    }

    /*Metoda vraci upravenou mapu*/
    public Map getMap() {

        return m;

    }

}
