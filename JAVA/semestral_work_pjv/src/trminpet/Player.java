/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trminpet;

import java.awt.Image;
import static java.lang.Math.random;
import java.util.Random;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Petr Trminek 2016
 */
public class Player extends Thread {

    /*Deklarace promennych*/
    private int tileX, tileY;
    private Image Player1ImgDown;
    private Image Player1ImgUp;
    private Image Player1ImgLeft;
    private Image Player1ImgRight;
    private Image Player2ImgDown;
    private Image Player2ImgUp;
    private Image Player2ImgLeft;
    private Image Player2ImgRight;
    private boolean health;
    private int PlayerType;
    private Map m;
    private Board b;
    private Bomb bomb;
    Random rand = new Random();

    /*Vytvoreni hrace a AI nebo druheho hrace*/
    public Player(int Pl) {

        this.PlayerType = PlayerType;

        switch (Pl) {
            /*Vytvoreni vzhledu hrace*/
            case 1:
                ImageIcon img1 = new ImageIcon(getClass().getResource("img/blueplayerdown1.png"));
                Player1ImgDown = img1.getImage();
                img1 = new ImageIcon(getClass().getResource("img/blueplayerup1.png"));
                Player1ImgUp = img1.getImage();

                img1 = new ImageIcon(getClass().getResource("img/blueplayerleft1.png"));
                Player1ImgLeft = img1.getImage();

                img1 = new ImageIcon(getClass().getResource("img/blueplayerright1.png"));
                Player1ImgRight = img1.getImage();

                break;
            /*Vytvoreni vzhledu pro hrace 2*/
            case 2:
                ImageIcon img2 = new ImageIcon(getClass().getResource("img/redplayerdown1.png"));
                Player2ImgDown = img2.getImage();
                img2 = new ImageIcon(getClass().getResource("img/redplayerup1.png"));
                Player2ImgUp = img2.getImage();

                img2 = new ImageIcon(getClass().getResource("img/redplayerleft1.png"));
                Player2ImgLeft = img2.getImage();

                img2 = new ImageIcon(getClass().getResource("img/redplayerright1.png"));
                Player2ImgRight = img2.getImage();

                break;

        }
        bomb = new Bomb(-100, -100);

        tileX = 1;
        tileY = 1;
        health = true;

    }

    /*Metoda vraci vzhled hrace dle smeru*/
    public Image getPlayer1(int direction) {

        switch (direction) {
            case 1:
                return Player1ImgUp;

            case 2:
                return Player1ImgLeft;

            case 3:
                return Player1ImgRight;

            default:
                return Player1ImgDown;

        }
    }

    /*Metoda vraci vyhled hrace dle smeru*/
    public Image getPlayer2(int direction) {

        switch (direction) {
            case 1:
                return Player2ImgUp;

            case 2:
                return Player2ImgLeft;

            case 3:
                return Player2ImgRight;

            default:
                return Player2ImgDown;

        }
    }

    /*Metoda vraci X hrace*/
    public int gettileX() {
        return tileX;

    }

    /*Metoda vraci Y hrace*/
    public int gettileY() {
        return tileY;

    }

    /*Metoda nastaveni zivotu dle exploze*/
    public void setHealth() {

        this.health = false;

    }

    /*Metoda na posunuti hrace dle objektu*/
    public void move(int dx, int dy) {

        tileX += dx;
        tileY += dy;

    }

    /*Metoda na ziskani zivota hrace*/
    public boolean getHealth() {
        return health;

    }

    /*Metoda pro ovladani naivniho AI*/
    @Override
    public void run() {
        /*Dokud AI zije*/
        while (health) {

            try {
                /*Vygenerovani smeru pohybu*/
                int dx = rand.nextInt(3) - 1;
                int dy = rand.nextInt(3) - 1;
                /*Zpomaleni aby nebehalo sem a tam*/
                Thread.sleep(300);
                /*Kontrola aby se smer generoval pouze v ramci hraci plohy*/
                while (tileX + dx > 13 || tileX + dx < 0 || tileY + dy > 13 || tileY + dy < 0) {
                    dx = rand.nextInt(3) - 1;
                    dy = rand.nextInt(3) - 1;

                }
                /*Pokud je smer validni a na cilove pozici je volno, AI se pohne*/
                if (m.getMap(tileX + dx, tileY + dy).equals("x")) {
                    /*Metoda na urceni vzhledu AI*/
                    AIimg(dx, dy);
                    tileX += dx;
                    tileY += dy;

                    /*Vygenerovani nahody pro bombu*/
                    int chance = rand.nextInt(20);
                    /*Pokud je 1 AI polozi bombu*/

                    if (chance == 1) {
                        /*Metoda pro polozeni bomby*/
                        dropBomb(tileX - dx, tileY - dy);
                        /*Naivni metoda vyhnuti*/
                        avoidBomb();

                    }

                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /*
    Metoda urcuje obraz AI dle pohybu obdoba metody getPlayerOne
    
     */
    public void AIimg(int dx, int dy) {
        if (dx == 1 && dy == 0) {
            b.directionP2 = 3;
        }
        if (dx == -1 && dy == 0) {
            b.directionP2 = 2;
        }
        if (dx == 0 && dy == -1) {
            b.directionP2 = 1;
        }
        if (dx == 0 && dy == 1) {
            b.directionP2 = 4;
        }

    }

    /*Metoda na polozeni bomby*/
    public void dropBomb(int dx, int dy) {

        /*Pokud bomba existuje nelze polozit*/
        if (!bomb.isAlive()) {
            /*Vytvoreni nove bomby dle souradnic AI*/
            bomb = new Bomb(dx * 32, dy * 32);
            /*Predani bomby do boardu kvuli vykresleni bomby*/
            Board.setBombAI(bomb);

            /*Zapnuti bomby - metoda run*/
            bomb.start();
            /*Po explozi se vrati upravena mapa */
            if (bomb.isInterrupted()) {
                m = bomb.getMap();
            }

        }

    }

    /*Uhybani exploze pro AI*/
    public void avoidBomb() {

        /*Zkontroluje se urceny smer zda je moznost skocit a kdyztak skoci*/
        if (m.getMap(tileX + 1, tileY + 0).equals("x")) {

            move(1, 0);
        } else if (m.getMap(tileX - 1, tileY + 0).equals("x")) {

            move(-1, 0);

        } else if (m.getMap(tileX + 0, tileY + 1).equals("x")) {

            move(0, 1);

        } else if (m.getMap(tileX + 0, tileY - 1).equals("x")) {

            move(0, -1);

        } else if (m.getMap(tileX + 1, tileY + 1).equals("x")) {

            move(1, 1);

        } else if (m.getMap(tileX - 1, tileY - 1).equals("x")) {

            move(-1, -1);

        } else if (m.getMap(tileX + 1, tileY - 1).equals("x")) {

            move(1, -1);

        } else if (m.getMap(tileX - 1, tileY + 1).equals("x")) {

            move(-1, 1);

        }

    }

}
