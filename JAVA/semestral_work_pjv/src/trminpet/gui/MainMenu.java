/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trminpet.gui;

import audio.Sounds;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.RadioButton;
import javax.imageio.ImageIO;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import trminpet.*;

/**
 *
 * @author Petr Trminek 2016
 */
public class MainMenu extends JFrame {

    /*Deklarace component do GUI a boolean hodnoty pro praci s AI*/
    JFrame frame = new JFrame("Bomberman");
    JPanel panel = new JPanel();
    JPanel panelMenu = new JPanel();
    JPanel panelLvl = new JPanel();
    JPanel panelAbout = new JPanel();
    Button buttonNG = new Button();
    Button buttonAb = new Button();
    Button buttonEx = new Button();
    Button buttonBa = new Button();
    Button button1LVL = new Button();
    Button button2LVL = new Button();
    Button button3LVL = new Button();
    Button button4LVL = new Button();
    Button button5LVL = new Button();
    Button button6LVL = new Button();
    JLabel background = new JLabel();
    JRadioButton rbAI = new JRadioButton();
    JRadioButton rbPlayer = new JRadioButton();
    ButtonGroup bG = new ButtonGroup();
    CardLayout cl = new CardLayout();
    private static boolean AI = true;

    @SuppressWarnings("LeakingThisInConstructor")
    /*Vytvoreni menu*/
    public MainMenu() {
        /*Metoda na na upravu vzhledu tlacitek*/
        prepareButtons();
        /*Incializace menu*/
        initMenu();
        /*Nastaveni pozadi menu*/
        frame.setContentPane(new JLabel(new ImageIcon(getClass().getResource("img/menu.png"))));
        /*Nastaveni layoutu*/
        frame.setLayout(new FlowLayout());
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setSize(480, 507);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    /*Metoda ktera nastavi obrazek na tlacitka*/
    public void prepareButtons() {

        ImageIcon img;
        /*Nacteni obrazku na tlacitka*/
        img = new ImageIcon(getClass().getResource("img/newgame.png"));
        buttonNG.setIcon(img);
        img = new ImageIcon(getClass().getResource("img/back.png"));

        buttonBa.setIcon(img);
        img = new ImageIcon(getClass().getResource("img/credits.png"));

        buttonAb.setIcon(img);

        img = new ImageIcon(getClass().getResource("img/quitgame.png"));

        buttonEx.setIcon(img);

        img = new ImageIcon(getClass().getResource("img/level1.png"));

        button1LVL.setIcon(img);

        img = new ImageIcon(getClass().getResource("img/level2.png"));

        button2LVL.setIcon(img);
        img = new ImageIcon(getClass().getResource("img/level3.png"));

        button3LVL.setIcon(img);
        img = new ImageIcon(getClass().getResource("img/level4.png"));

        button4LVL.setIcon(img);
        img = new ImageIcon(getClass().getResource("img/level5.png"));

        button5LVL.setIcon(img);

        img = new ImageIcon(getClass().getResource("img/freemode.png"));

        button6LVL.setIcon(img);

        img = new ImageIcon(getClass().getResource("img/vsplayer.png"));

        rbPlayer.setIcon(img);

        img = new ImageIcon(getClass().getResource("img/ai.png"));

        rbAI.setIcon(img);

    }

    /*Metoda nastavi uvodni menu*/
    public void initMenu() {
        /*Nastaveni card layoutu*/
        panel.setLayout(cl);
        /*Nastaveni boxu kvuli zarovnani dle Y*/
        Box box = Box.createVerticalBox();
        /*Vytvoreni uvodniho bodu od ktereho se postupuje dal*/
        box.add(Box.createRigidArea(new Dimension(0, 150)));
        box.add(buttonNG);
        /* Pridani mezery*/
        box.add(Box.createVerticalStrut(10));
        box.add(buttonAb);
        box.add(Box.createVerticalStrut(10));
        box.add(buttonEx);
        box.add(Box.createVerticalStrut(10));
        /*Nastaveni pruhlednosti panelu*/
        box.setOpaque(false);
        panelMenu.add(box);
        panelMenu.revalidate();
        panel.setOpaque(false);
        panelMenu.setOpaque(false);
        panelLvl.setOpaque(false);
        panelAbout.setOpaque(false);
        /*Pridani panelu do cardlayoutu*/
        panel.add(panelMenu, "1");
        panel.add(panelAbout, "2");
        panel.add(panelLvl, "3");
        /*Zobrazeni uvodniho menu*/
        cl.show(panel, "1");
        /*Prepinani layoutu pomoci tlacitek */
        buttonNG.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                initLevel();
                cl.show(panel, "3");
            }
        });

        buttonAb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                initAbout();
                cl.show(panel, "2");
            }
        });
        /*Ukonceni programu*/
        buttonEx.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });
    }

    /*Metoda obstarava menu s lvly*/
    public void initLevel() {
        /*Vytvoreni boxu pro zarovnavani dle Y*/
        Box box = Box.createVerticalBox();
        /*Pridani neviditelneho bloku */
        box.add(Box.createRigidArea(new Dimension(40, 80)));
        /*Incializate radiobuttony*/
        initRB();
        /*Pridani prvku do BOX*/
        box.add(rbAI);
        box.add(rbPlayer);
        box.add(Box.createVerticalStrut(10));
        box.add(button1LVL);
        box.add(Box.createVerticalStrut(10));
        box.add((button2LVL));
        box.add(Box.createVerticalStrut(10));
        box.add(button3LVL);
        box.add(Box.createVerticalStrut(10));
        box.add(button4LVL);
        box.add(Box.createVerticalStrut(10));
        box.add(button5LVL);
        box.add(Box.createVerticalStrut(10));
        box.add(button6LVL);
        box.add(Box.createVerticalStrut(10));
        box.add(buttonBa);
        box.add(Box.createVerticalStrut(10));
        panelLvl.add(box);
        /*Zobrazeni volby pomoci * */
        rbAI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                rbAI.setText("*");
                rbPlayer.setText("");

            }
        });
        /*Zobrazeni volby pomoci * */
        rbPlayer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                rbAI.setText("");
                rbPlayer.setText("*");

            }
        });

        /*Vraceni se na uvodni kartu*/
        buttonBa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cl.show(panel, "1");
            }
        });
        /*Vytvoreni urovne 1*/
        button1LVL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                /*Nastaveni AI nebo Dva Hraci*/
                setAI();
                new Bomberman(1);

            }
        });
        /*Vytvoreni urovne 2*/
        button2LVL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                setAI();
                new Bomberman(2);

            }
        });
        /*Vytvoreni urovne 3*/
        button3LVL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                setAI();
                new Bomberman(3);

            }
        });
        /*Vytvoreni urovne 4*/
        button4LVL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                setAI();
                new Bomberman(4);

            }
        });
        /*Vytvoreni urovne 5*/
        button5LVL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                setAI();
                new Bomberman(5);

            }
        });
        /*Vytvoreni urovne 6*/
        button6LVL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                setAI();
                new Bomberman(6);

            }
        });
    }

    public void initAbout() {
        /*Vytvoreni boxu pro zarovnani dle Y*/
        Box box = Box.createVerticalBox();
        JLabel text = new JLabel(new ImageIcon(getClass().getResource("img/about.png")));
        /*Pridani komponent*/
        box.add(Box.createVerticalStrut(80));
        box.add(text);
        box.add(Box.createVerticalStrut(20));
        box.add(buttonBa);

        panelAbout.add(box);
        /*Vraceni se zpet na uvodni menu*/
        buttonBa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cl.show(panel, "1");
            }
        });

    }

    /*Metoda na nastaveni vlastnosti radio buttonu*/
    public void initRB() {

        rbAI.setOpaque(false);
        rbAI.setContentAreaFilled(false);
        rbAI.setBorderPainted(false);
        rbAI.setBorder(null);
        rbAI.setSelected(true);
        rbAI.setText("*");

        rbPlayer.setOpaque(false);
        rbPlayer.setContentAreaFilled(false);
        rbPlayer.setBorderPainted(false);
        rbPlayer.setBorder(null);
        /*Pridani radiobuttonu do buttongroup aby se vzajemne ovlivnovaly*/
        bG.add(rbAI);
        bG.add(rbPlayer);

    }

    /*Metoda vraci hodnotu promenne AI*/
    public static boolean getAI() {
        return AI;

    }

    /*Metoda nastavi hodnotu AI pri zahajeni hry*/
    public void setAI() {
        /*Zapne se AI*/
        if (rbAI.isSelected()) {
            AI = true;
        }
        /*Vypne se AI*/
        if (rbPlayer.isSelected()) {
            AI = false;
        }

    }

    /*Spusteni programu*/
    public static void main(String[] args) {

        /*Spusteni menu*/
        new MainMenu();
        /*Spusteni hudby*/
        Sounds.playMusic();
    }

}
