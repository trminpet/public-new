/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trminpet.gui;

import javax.swing.JButton;

/**
 *
 * @author Petr Trminek 2016
 */
public class Button extends JButton {
/*Trida na vytvoreni tlacitek se specifickymi vlastnostmi*/
    public Button() {

        setOpaque(false);
        setContentAreaFilled(false);
        setBorderPainted(false);
        setBorder(null);
    }

}
