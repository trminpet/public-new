/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trminpet;

import audio.Sounds;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Graphics;
import static java.awt.PageAttributes.ColorType.COLOR;
import java.awt.event.*;
import static java.awt.image.ImageObserver.WIDTH;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import trminpet.*;
import javax.swing.JOptionPane;
import trminpet.gui.MainMenu;

/**
 *
 * @author Petr Trminek 2016
 */
public class Board extends JPanel implements ActionListener {

    /*Deklarace prommenych*/
    private Timer timer;
    
    private static Map m;
    private Player p;
    private Player p2;
    /*Nastaveni pocatecniho smeru hracu*/
    private int directionP1 = 4;
    static public int directionP2 = 1;
    
    private Bomb b1;
    private static Bomb b2;

    public String winner = "";
    private Bomberman maze;
    private static MainMenu MM;
    private int keyCode;

    public Board(int level) {
        /*Vytvoreni boardu dle specifikaci - lvlu,AI*/
        m = new Map(level);
        p = new Player(1);
        p2 = new Player(2);
        /*Posunuti hrace dva do pravehodolniho rohu*/
        p2.move(12, 12);
        /*Zapnuti AI*/
        if(MM.getAI()) {
            p2.start();
        }

        addKeyListener(new KA());
        setFocusable(true);
        
        timer = new Timer(25, this);
        timer.start();
        
        b1 = new Bomb(-100, -100);
        b2 = new Bomb(-100, -100);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }

    public void paint(Graphics g) {
        super.paint(g);
        /*Vykresleni hraci plochy*/
        g.drawImage(m.getBackground(), 0, 0, null);
        /*Vykresleni objektu*/
        for (int y = 0; y < 15; y++) {
            for (int x = 0; x < 15; x++) {
                if (m.getMap(x, y).equals("b")) {
                    g.drawImage(m.getBlock(), x * 32, y * 32, null);

                }
                if (m.getMap(x, y).equals("c")) {
                    g.drawImage(m.getCrate(), x * 32, y * 32, null);

                }
                /*Vykresleni exploze a zaroven se zde kontroluje vitez*/
                if (m.getMap(x, y).equals("e")) {
                    g.drawImage(m.getFire(), x * 32, y * 32, null);
                    if ((p.gettileX() * 32 == x * 32) && (p.gettileY() * 32 == y * 32)) {
                        /*Pokud je prvni hrac ohni vitezi druhy*/
                        if (p.getHealth()) {
                            p.setHealth();
                            winner = "Red player is the winner!";
                            Winner();
                        }

                    }
                    if ((p2.gettileX() * 32 == x * 32) && (p2.gettileY() * 32 == y * 32)) {
                        p2.setHealth();
                        winner = "Blue play is the winner !";
                        Winner();

                    }
                }

            }

        }
        /*Vykreslovani hracu*/
        g.drawImage(p.getPlayer1(directionP1), p.gettileX() * 32, p.gettileY() * 32, null);
        g.drawImage(p2.getPlayer2(directionP2), p2.gettileX() * 32, p2.gettileY() * 32, null);
        /*Vykreslovani polozenych bomb*/
        if (b1.isPlanted()) {

            g.drawImage(b1.getBomb(), b1.getxBomb(), b1.getyBomb(), null);

        }
        if (b2.isPlanted()) {

            g.drawImage(b2.getBomb(), b2.getxBomb(), b2.getyBomb(), null);

        }

    }

    /*Metoda na ovladani hracu*/
    public class KA extends KeyAdapter {

        public void keyPressed(KeyEvent e) {
            /*Pro hrace dva se otevre pokud je AI=false*/
            keyCode = e.getKeyCode();
            PlayerOneControl();
            if (!MM.getAI()) {
                PlayerTwoControl();

            }

        }
    }

    /*Vraci aktualni mapu*/
    public static Map getM() {
        return m;
    }

    /*Metoda na zobrazeni modalniho okna s vitezem*/
    public void Winner() {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JOptionPane.showMessageDialog(null, winner);
            }
        });
        maze.closeMap();
        if(p2.isAlive()){
        p2.stop();
        }
    }

    /*Metoda na nastaveni bomby od AI pro vykresleni */
    public static void setBombAI(Bomb bomb) {
        b2 = bomb;

    }

    /*Metoda pro ovladani hrace 1*/
    public void PlayerOneControl() {
        /*Pokud je stisknuta W a dalsi policko je X nebo E tak se hrac posune*/
        if (keyCode == KeyEvent.VK_W) {

            if (m.getMap(p.gettileX(), p.gettileY() - 1).equals("x")
                    || m.getMap(p.gettileX(), p.gettileY() - 1).equals("e")) {
                p.move(0, -1);

            }
            /*Nastaveni smeru hrace pro vzhled*/
            directionP1 = 1;
        }
        if (keyCode == KeyEvent.VK_A) {
            if (m.getMap(p.gettileX() - 1, p.gettileY()).equals("x")
                    || m.getMap(p.gettileX() - 1, p.gettileY()).equals("e")) {
                p.move(-1, 0);

            }

            directionP1 = 2;

        }
        if (keyCode == KeyEvent.VK_S) {
            if (m.getMap(p.gettileX(), p.gettileY() + 1).equals("x")
                    || m.getMap(p.gettileX(), p.gettileY() + 1).equals("e")) {
                p.move(0, 1);

            }

            directionP1 = 4;

        }
        if (keyCode == KeyEvent.VK_D) {

            if (m.getMap(p.gettileX() + 1, p.gettileY()).equals("x")
                    || m.getMap(p.gettileX() + 1, p.gettileY()).equals("e")) {
                p.move(1, 0);

            }
            directionP1 = 3;

        }
        /* Polozeni bomby*/
        if (keyCode == KeyEvent.VK_SPACE) {
            /*Bomba nelze polozit pokud jina je polozena*/
            if (!b1.isAlive()) {
                /*Vytvoreni nove bomby*/
                b1 = new Bomb(p.gettileX() * 32, p.gettileY() * 32);
                /*Zapnuti bomby*/
                b1.start();
                /*Ziskani mapy po explozi*/
                if (b1.isInterrupted()) {
                    m = b1.getMap();
                }

            }

        }

    }

    /*Metoda pro pohyb hrace dva*/
    public void PlayerTwoControl() {
        if (keyCode == KeyEvent.VK_UP) {

            if (m.getMap(p2.gettileX(), p2.gettileY() - 1).equals("x")
                    || m.getMap(p2.gettileX(), p.gettileY() - 1).equals("e")) {
                p2.move(0, -1);

            }
            directionP2 = 1;
        }
        if (keyCode == KeyEvent.VK_LEFT) {
            if (m.getMap(p2.gettileX() - 1, p2.gettileY()).equals("x")
                    || m.getMap(p2.gettileX() - 1, p.gettileY()).equals("e")) {
                p2.move(-1, 0);

            }

            directionP2 = 2;

        }
        if (keyCode == KeyEvent.VK_DOWN) {
            if (m.getMap(p2.gettileX(), p2.gettileY() + 1).equals("x")
                    || m.getMap(p2.gettileX(), p2.gettileY() + 1).equals("e")) {
                p2.move(0, 1);

            }

            directionP2 = 4;

        }
        if (keyCode == KeyEvent.VK_RIGHT) {

            if (m.getMap(p2.gettileX() + 1, p2.gettileY()).equals("x")
                    || m.getMap(p2.gettileX() + 1, p2.gettileY()).equals("e")) {
                p2.move(1, 0);

            }
            directionP2 = 3;

        }
        if (keyCode == KeyEvent.VK_ENTER) {

            if (!b2.isAlive()) {
                b2 = new Bomb(p2.gettileX() * 32, p2.gettileY() * 32);

                b2.start();
                if (b2.isInterrupted()) {
                    m = b2.getMap();
                }

            }

        }
    }

}
