/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package audio;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import sun.audio.*;
/**
 *
 * @author Petr Trminek 2016
 */
public class Sounds extends Thread  {

    
    
    
    
 public static void playMusic(){
     
     /*Vytvoreni audia dle oracle dokumentace*/
    AudioPlayer myBackgroundPlayer = AudioPlayer.player;
    ContinuousAudioDataStream myLoop = null;
    AudioStream AS;
    AudioData AD;
    
    try {
        /*Nacteni audio streamu*/
         AS = new AudioStream(new FileInputStream(Sounds.class.getResource("music.wav").getFile()));
         /*Zpracovani zvuku*/
         AD = AS.getData();
         /*Deklarace tridy na nekonecnou smycku*/
         myLoop = new ContinuousAudioDataStream(AD);
    }catch(Exception error){
      //  System.out.println("File Not Found");
        System.out.println(error);
    }
    /*Zapnuti hudby*/
    myBackgroundPlayer.start(myLoop);  
}
 

}

   
