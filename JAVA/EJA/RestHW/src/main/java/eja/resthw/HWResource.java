/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eja.resthw;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author petr
 */
@Path("HW") //localhost:9998/HW vypise anybody a /HW?name=Tom vypise tom pro query param

// @Path("HW/{name}" pro PathParam
public class HWResource {
    //@GET
    // public String hello( @PathParam("name")  String name){ return "Hello " + name + "\n";}
   // public String hello( @QueryParam("name") @DefaultValue("anybody") String name){ return "Hello " + name + "\n";}
    
    //@GET
  //  @Produces(MediaType.APPLICATION_XML) //funguje pomoci anotoce xmlroot
    //@Produces(MediaType.APPLICATION_JSON) //musi se pridat do pom dependency na json moxy
    //public Customer hello(@QueryParam("name") @DefaultValue("anybody") String name){ return new Customer(name,10);}
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response hello(@QueryParam("name") @DefaultValue("anybody") String name){ 
        Customer c = new Customer(name,19);
        //return Response.ok(c).build();
        return Response.status(Response.Status.BAD_REQUEST).build();
}
    
}
