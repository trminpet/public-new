/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eja.customerjsf.backing;

import eja.customerjsf.backing.model.Customer;
import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 *
 * @author petr
 */
public class Facade {

    public static final Facade inst = new Facade();
    
    NavigableMap<Integer, Customer> customers = new ConcurrentSkipListMap<>();
    
    
    private Facade() {
        add(new Customer("Tom"));
        add(new Customer("Bob"));
    }
    
    public void  add(Customer c){
       int id = 1;
       if(!customers.isEmpty()){
               id=customers.lastKey()+1;
               
               }
       c.setId(id);
       customers.put(c.getId(),c);
    }

    
    
    
    List<Customer> allCustomers(){
    return new ArrayList(customers.values());  
    }

  public  Customer findCustomer(int custId) {
        return customers.get(custId);
    }

   public void deleteCustomer(Integer custId) {
     customers.remove(custId);
    }
    
    
}
