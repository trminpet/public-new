/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eja.customerjsf.backing;

import eja.customerjsf.backing.model.Customer;
import java.util.List;
import javax.enterprise.inject.Model;

/**
 *
 * @author petr
 */
@Model
public class AddCustomer {
    private List<Customer> customers;

    private String name;

 
    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    public String addCustomer() {
        Facade.inst.add(new Customer(name));
        return "index?faces-redirect=true";

    }
}
