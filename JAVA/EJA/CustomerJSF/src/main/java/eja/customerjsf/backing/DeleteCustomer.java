/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eja.customerjsf.backing;

import com.sun.net.httpserver.HttpsServer;
import eja.customerjsf.backing.model.Customer;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@Model
public class DeleteCustomer {
    Customer customer;
   private int custId;
    String name = Facade.inst.findCustomer(custId).getName(); 
   

//    public Customer getCustomer() {
//        return customer;
//    }
//    
//    @PostConstruct
//    void init(){
//       FacesContext fc= FacesContext.getCurrentInstance();
//        HttpServletRequest req = (HttpServletRequest)fc.getExternalContext().getRequest();
//        int custId = Integer.parseInt(req.getParameter("custId"));
//       customer= Facade.inst.findCustomer(custId);
//    }

  

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }
    
    
    public String delete(){
    Facade.inst.deleteCustomer(custId);
    return "index?faces-redirect=true";
    }
    
    
}
