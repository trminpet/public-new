/** CVUT FIT PSI 2017 ls
*  UKOL C.1 TCP SERVER pohyb robota
*  Petr Trminek (trminpet)
*/





package tcpserver;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class ClientConnection extends Thread {

    /**
     * Deklarace promennych pro ovladani robota
     *
     *
     */
    final String SERVER_USER = "100 LOGIN\r\n";
    final String SERVER_PASSWORD = "101 PASSWORD\r\n";
    final String SERVER_MOVE = "102 MOVE\r\n";
    final String SERVER_TURN_LEFT = "103 TURN LEFT\r\n";
    final String SERVER_TURN_RIGHT = "104 TURN RIGHT\r\n";
    final String SERVER_PICK_UP = "105 GET MESSAGE\r\n";
    final String SERVER_OK = "200 OK\r\n";
    final String SERVER_LOGIN_FAILED = "300 LOGIN FAILED\r\n";
    final String SERVER_SYNTAX_ERROR = "301 SYNTAX ERROR\r\n";
    final String SERVER_LOGIC_ERROR = "302 LOGIC ERROR\r\n";
    String clientSentence = "";
    String capitalizedSentence = "";
    BufferedReader inFromClient;
    DataOutputStream outToClient;
    Robot robot;
    String MESSAGE = "";
    Socket connectionSocket;

    ClientConnection(Socket connectionSocket) {

        try {
            this.connectionSocket = connectionSocket;
            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(ClientConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metoda pro beh vlakna-robota s nastavenim timeoutu
     */
    public void run() {

        try {
            connectionSocket.setSoTimeout(1000);
            if (authentication()) {
                move();

            } else {
                syntax_error(MESSAGE);

            }
        } catch (Exception ex) {
            try {
                timeoutSocket();
            } catch (IOException ex1) {
                Logger.getLogger(ClientConnection.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }

    public void timeoutSocket() throws IOException {

        outToClient.close();
        inFromClient.close();

    }

    /**
     * Metoda na chybove hlasky
     *
     * @param MESSAGE
     * @throws IOException
     */
    public void syntax_error(String MESSAGE) throws IOException {

        capitalizedSentence = MESSAGE;
        outToClient.writeBytes(capitalizedSentence);
        timeoutSocket();

    }

    /**
     * Metoda pro nacitani zprav pri nabijeni
     *
     * @return String message
     * @throws IOException
     */
    public String inputRecharging() throws IOException {
        String sentence = "";
        int check = 0;
        int count = 0;
        while (true) {
            if (count > 11) {

                return "";
            }
            sentence += (char) inFromClient.read();
            if (sentence.contains("\r\n")) {
                break;
            }
            count++;
        }

        return sentence;

    }

    /**
     * Metoda pro nabijeni, nastaveni timeoutu na 5 sec, prijmuti zpravy s FULL
     * POWER
     *
     * @return
     * @throws IOException
     */
    public Boolean recharging() throws IOException {
        try {
            connectionSocket.setSoTimeout(5000);
            String check = inputRecharging();
            if (check.contains("FULL POWER\r\n")) {

                return true;
            } else {
                syntax_error(SERVER_LOGIC_ERROR);
                return false;
            }

        } catch (SocketException ex) {

            timeoutSocket();
        } finally {
            connectionSocket.setSoTimeout(1000);

        }

        return false;
    }

    /**
     * Metoda pro autentizaci dle zadani
     *
     * @return
     * @throws IOException
     */
    boolean authentication() throws IOException {

        capitalizedSentence = SERVER_USER;
        outToClient.writeBytes(capitalizedSentence);

        clientSentence = readInput();
        if (clientSentence.equals("")) {
            MESSAGE = SERVER_SYNTAX_ERROR;
            return false;

        }
        //validni username=zalozeni noveho robota
        if (validUsername(clientSentence)) {

            robot = new Robot(clientSentence);
            capitalizedSentence = SERVER_PASSWORD;
            outToClient.writeBytes(capitalizedSentence);
            clientSentence = readInput();

            if (validPassword(clientSentence)) {
                //osetreni parsovani stringu na int pro heslo 
                int check = 0;
                try {
                    check = Integer.parseInt(clientSentence.replace("\r\n", ""));
                } catch (Exception e) {
                    check = -1;
                  //nepovedlo se check ==-1 error
                }

                if (check == -1) {

                    MESSAGE = SERVER_SYNTAX_ERROR;
                    return false;
                    //kontrola zda se shoduje vygenerovane heslo z username a heslo
                } else if (check == robot.getPassword()) {
                    
                    capitalizedSentence = SERVER_OK;
                    outToClient.writeBytes(capitalizedSentence);
                    
                    return true;

                } else {
                    
                    MESSAGE = SERVER_LOGIN_FAILED;
                    return false;
                }

            } else {
                

                MESSAGE = SERVER_SYNTAX_ERROR;
                return false;
            }

        } else {
            MESSAGE = SERVER_SYNTAX_ERROR;
            return false;
        }

    }

    boolean validUsername(String username) {

        if (username.length() > 100) {

            return false;
        }

        int n = 0;
      //osetreni specialnich znaku pouze jednou \r\n
        for (int i = 0; i < username.length(); i++) {
            if (username.charAt(i) == '\r' && username.charAt(i + 1) == '\n') {
                n++;

            }

        }

        if (n == 1) {

            return true;
        }
        return false;

    }

    boolean validPassword(String password) {
        if (password.length() < 7) {
            return true;
        }
        return false;
    }
/**
 * Metoda na nacitani vstupu nacita se do \r\n
 * @return
 * @throws IOException 
 */
    String readInput() throws IOException {
        String sentence = "";
        int check = 0;
        int count = 0;

        while (true) {

            if (count > 99 || (sentence.contains("OK") && count > 11)) {

                return "";
            }
            sentence += (char) inFromClient.read();
            if (sentence.contains("\r\n")) {
                break;
            }
            count++;

        }
       //zavolani metody na nabijeni
        if (sentence.contains("RECHARGING\r\n")) {
            if (recharging()) {
                return readInput();
            } else {
                return "";
            }
        }

        return sentence;

    }
/**
 * Metoda na pohyb k cili 0,0
 * @throws IOException 
 */
    void move() throws IOException {
        boolean first = true;
        while (true) {
            if (first) {
                capitalizedSentence = SERVER_MOVE;
                outToClient.writeBytes(capitalizedSentence);
                clientSentence = readInput();

            } else {
                outToClient.writeBytes(capitalizedSentence);
                clientSentence = readInput();

            }
             //nacteni souradnice
            String array[];
            
            if (validCommand(clientSentence)) {
                array = clientSentence.replace("\r\n", " ").split(" ");
            } else {

                capitalizedSentence = SERVER_SYNTAX_ERROR;
                outToClient.writeBytes(capitalizedSentence);
                outToClient.close();
                inFromClient.close();
                break;

            }
          //parsovani souradnic
            try {
                robot.x = Integer.parseInt(array[1]);
                robot.y = Integer.parseInt(array[2]);
                //robot stoji na cili
                if (robot.x == 0 && robot.y == 0) {

                    capitalizedSentence = SERVER_PICK_UP;
                    outToClient.writeBytes(capitalizedSentence);
                    clientSentence = readInput();
                    if (clientSentence.equals("")) {
                        syntax_error(SERVER_SYNTAX_ERROR);
                    } else {
                        capitalizedSentence = SERVER_OK;
                        outToClient.writeBytes(capitalizedSentence);
                        outToClient.close();
                        inFromClient.close();

                    }

                    break;
                }
              //urceni smeru robota
                if (first) {
                    capitalizedSentence = SERVER_MOVE;
                    outToClient.writeBytes(capitalizedSentence);
                    clientSentence = readInput();

                    String array2[];
                    if (validCommand(clientSentence)) {
                        array2 = clientSentence.replace("\r\n", " ").split(" ");
                    } else {

                        capitalizedSentence = SERVER_SYNTAX_ERROR;
                        outToClient.writeBytes(capitalizedSentence);
                        outToClient.close();
                        inFromClient.close();
                        break;

                    }

                    try {
                        robot.x1 = Integer.parseInt(array2[1]);
                        robot.y1 = Integer.parseInt(array2[2]);
                        if (robot.x1 == 0 && robot.y1 == 0) {

                            if (clientSentence.equals("")) {
                                syntax_error(SERVER_SYNTAX_ERROR);
                            } else {
                                capitalizedSentence = SERVER_OK;
                                outToClient.writeBytes(capitalizedSentence);
                                outToClient.close();
                                inFromClient.close();

                            }

                            break;
                        }
                    } catch (Exception e) {
                        capitalizedSentence = SERVER_SYNTAX_ERROR;
                        outToClient.writeBytes(capitalizedSentence);
                        outToClient.close();
                        inFromClient.close();
                        break;
                    }

                    if (robot.x == robot.x1 && robot.y + 1 == robot.y1) {

                        robot.DIRECTION = "UP";

                    }//je otocen dolu
                    else if (robot.x == robot.x1 && robot.y - 1 == robot.y1) {
                        robot.DIRECTION = "DOWN";

                    }//je otocen nahoru
                    else if (robot.x + 1 == robot.x1 && robot.y == robot.y1) {
                        robot.DIRECTION = "RIGHT";

                    } //je otocen doprava
                    else if (robot.x - 1 == robot.x1 && robot.y == robot.y1) {
                        robot.DIRECTION = "LEFT";

                    }//je otocen doleva
                    else {
                        move();
                    }

                    first = false;

                }
                
             //navigace robota vzhledem k ose Y
                if (robot.y == 0) {
                    if (robot.x > 0) {
                        switch (robot.DIRECTION) {
                            case "UP":
                                capitalizedSentence = SERVER_TURN_LEFT;
                                robot.DIRECTION = "LEFT";
                                break;
                            case "DOWN":
                                capitalizedSentence = SERVER_TURN_RIGHT;
                                robot.DIRECTION = "RIGHT";
                                break;
                            case "LEFT":
                                capitalizedSentence = SERVER_MOVE;
                                robot.DIRECTION = "LEFT";
                                break;
                            case "RIGHT":
                                capitalizedSentence = SERVER_MOVE;
                                robot.DIRECTION = "RIGHT";
                                break;

                        }
                    } else if (robot.x < 0) {
                        switch (robot.DIRECTION) {
                            case "UP":
                                capitalizedSentence = SERVER_TURN_RIGHT;
                                robot.DIRECTION = "RIGHT";
                                break;
                            case "DOWN":
                                capitalizedSentence = SERVER_TURN_LEFT;
                                robot.DIRECTION = "RIGHT";
                                break;
                            case "LEFT":
                                capitalizedSentence = SERVER_TURN_RIGHT;
                                robot.DIRECTION = "UP";
                                break;
                            case "RIGHT":
                                capitalizedSentence = SERVER_MOVE;
                                robot.DIRECTION = "RIGHT";
                                break;

                        }
                    }

                } else if (robot.y > 0) {
                    switch (robot.DIRECTION) {
                        case "UP":
                            capitalizedSentence = SERVER_TURN_LEFT;
                            robot.DIRECTION = "LEFT";
                            break;
                        case "DOWN":
                            capitalizedSentence = SERVER_MOVE;
                            robot.DIRECTION = "DOWN";
                            break;
                        case "LEFT":
                            capitalizedSentence = SERVER_TURN_LEFT;
                            robot.DIRECTION = "DOWN";
                            break;
                        case "RIGHT":
                            capitalizedSentence = SERVER_TURN_RIGHT;
                            robot.DIRECTION = "DOWN";
                            break;

                    }
                } else if (robot.y < 0) {
                    switch (robot.DIRECTION) {
                        case "UP":
                            capitalizedSentence = SERVER_MOVE;
                            robot.DIRECTION = "UP";
                            break;
                        case "DOWN":
                            capitalizedSentence = SERVER_TURN_LEFT;
                            robot.DIRECTION = "LEFT";
                            break;
                        case "LEFT":
                            capitalizedSentence = SERVER_TURN_RIGHT;
                            robot.DIRECTION = "UP";
                            break;
                        case "RIGHT":
                            capitalizedSentence = SERVER_TURN_LEFT;
                            robot.DIRECTION = "UP";
                            break;

                    }

                }

            } catch (Exception e) {
                capitalizedSentence = SERVER_SYNTAX_ERROR;
                outToClient.writeBytes(capitalizedSentence);
                outToClient.close();
                inFromClient.close();
                break;
            }
        }

    }

    boolean validMove(String move) {
        if (move.length() > 13) {
            return false;
        }


        return true;
    }

    boolean validCommand(String command) {

        if (command.length() > 13) {
            return false;
        }
        if (command.startsWith("OK ") && command.endsWith("\r\n")) {
            int space = 0;
            for (int i = 0; i < command.length(); i++) {
                if (command.charAt(i) == ' ') {
                    space++;
                }
            }
            if (space != 2) {
                return false;
            } else {
                return true;
            }

        }

        return false;
    }

}

class Robot {
    int x = -1;
    int y = -1;
    int x1 = 0;
    int y1 = 0;
    String DIRECTION = "";
    boolean CHARGING = false;
    String username = "";

    int password;
    public Robot(String username) {

        this.username = username;
        this.password = assciToNum(username);
    }

    int assciToNum(String username) {
        int sum = 0;

        for (int i = 0; i < username.length() - 2; i++) {
            sum += username.charAt(i);

        }

        if (sum != 0) {
            return sum;
        }

        return 0;
    }

    int getPassword() {
        return this.password;
    }

}

public class TCPServer extends Thread {

    ServerSocket welcomeSocket;

    public TCPServer() throws IOException {
        welcomeSocket = new ServerSocket(6789);
    }

    private void runServer() throws IOException {

        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            ClientConnection client = new ClientConnection(connectionSocket);
            client.start();
            
        }

    }

    public static void main(String argv[]) throws Exception {
        TCPServer server = new TCPServer();

        server.runServer();

    }
}
