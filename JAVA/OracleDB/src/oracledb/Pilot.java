/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oracledb;

/**
 *
 * @author petr
 */
public class Pilot {
    /**
     *
     * Deklarace promennych pro vytvoreni pilota
     */

    private String lastName;
    private String surName;
    private int group;
    private int hour;
    
     public Pilot() {
       
    }


    /**
     *
     * Funkce pro nastaveni ci ziskani hodnot od pilota
     */

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

   

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

   
    
    
    
}
