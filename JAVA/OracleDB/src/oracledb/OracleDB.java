/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oracledb;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author petr
 */
public class OracleDB {

    public static void main(String[] args) throws SQLException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        /*          
        *Vytvoreni spojeni s DB a nasledne vytvoreni formulare.
         */
        CreateConn conn = new CreateConn();
        JFrame jf = new JFrame(conn.getConnection());

        /**
         * Funkce ktera, pri zavreni aplikace zavre connection
         */
        final Connection connection = conn.getConnection();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    connection.close();
                    System.out.println("Connection closed...Closing application");
                } catch (SQLException ex) {
                    Logger.getLogger(JFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

    }
}
