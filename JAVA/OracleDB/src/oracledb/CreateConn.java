/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oracledb;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;


/**
 *
 * @author petr
 */
public class CreateConn {

    /**
     * Deklarace promennych pro vstup do DB
     *
     */
    
    private String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
    private String DB_URL = "jdbc:oracle:thin:@oracle.fit.cvut.cz:1521:ORACLE";
    private String DB_USR = "trminpet";
  
    private Connection connection;

    /**
     *
     * Konstruktor na navazani connection
     */
    public CreateConn() throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
       
         /**
         *
         * Nacteni hesla
         */
        
          DB_PASS =  Enc.getPass();
        
        /**
         *
         * Nacteni driveru pro DB
         */
          
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("Driver load failed.Check your driver");
            e.printStackTrace();
            return;
        }
        System.out.println("Oracle JDBC Driver Registered!");
        /**
         *
         * Vytvoreni spojeni s DB
         */
        try {
            connection = DriverManager.getConnection(DB_URL, DB_USR,
                    DB_PASS);

        } catch (SQLException e) {
            System.out.println("Connection Failed!");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("You are now connected to ORACLE database");
        } else {
            System.out.println("Failed to make connection!");
        }

    }

    /**
     * Funkce ktera vraci connection
     *
     * @return connection
     */
    public Connection getConnection() {
        return connection;
    }
}
