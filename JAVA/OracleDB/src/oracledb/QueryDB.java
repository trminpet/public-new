/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oracledb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author petr
 */
public class QueryDB {

    /**
     *
     * Deklarace promennych pro vytvoreni dotazu
     */
    private static ResultSet rs;
    private static ResultSetMetaData rsmd;
    private static String SQLStatement;

    /**
     *
     * Funkce pro ziskani max ID z tabulky pro inkrementaci(key violation
     * osetreni)
     *
     * @param connection predani connection z formu
     */
    public static int getMaxIdFromTable(Connection connection) throws SQLException {
        /**
         *
         * Dotaz na MAX(PILOT_KEY=ID)
         */
        SQLStatement = "SELECT MAX(PILOT_KEY) FROM PILOT";
        rs = connection.createStatement().executeQuery(SQLStatement);
        int id = 0;
        while (rs.next()) {
            id = rs.getInt(1);
        }
        rs.close();
        return id;
    }

    /**
     *
     * Funkce pro select z tabulky, vraci result set, ktery se pak interpretuje
     * ve formu
     *
     *
     * @param connection predani connection z formu
     */
    public static ResultSet selectFromDB(Connection connection) throws SQLException {
        SQLStatement = "SELECT * FROM PILOT ORDER BY PILOT_KEY";
        rs = connection.createStatement().executeQuery(SQLStatement);
        
        return rs;
        

    }

    /**
     *
     * Funkce pro insert do DB
     *
     *
     * @param connection predani connection z formu
     * @param p predani pilota, ze ktereho se ziskaji dalsi hodnoty
     */
    public static void insertIntoDB(Connection connection, Pilot p) throws SQLException {

        /**
         * Ziskani max ID + 1
         */
        int id = getMaxIdFromTable(connection) + 1;
        /**
         * PreparedStatement
         */
        SQLStatement = "INSERT INTO PILOT (PILOT_KEY,JMENO,PRIJMENI,LETOVA_TRIDA,NALETANE_HODINY) VALUES (?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(SQLStatement)) {

            /**
             * Dosazovani dat z pilota do pripraveno dotazu
             */
            pstmt.setInt(1, id);
            pstmt.setString(2, p.getSurName());
            pstmt.setString(3, p.getLastName());
            pstmt.setInt(4, p.getGroup());
            pstmt.setInt(5, p.getHour());
            /**
             * Vykonani dotazu, nasledny commit DB a uzavreni statementu.
             */
            pstmt.executeUpdate();
            connection.commit();
            pstmt.close();

        } catch (Exception e) {
            Logger.getLogger(JFrame.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     *
     * Funkce pro ziskani metadat z tabulky
     *
     *
     * @param rs predani rs pro navraceni resultsetmetadata
     *
     */
    public static ResultSetMetaData getInfoDB(ResultSet rs) throws SQLException {

        rsmd = rs.getMetaData();
        return rsmd;

    }
}
