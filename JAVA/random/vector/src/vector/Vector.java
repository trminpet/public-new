/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vector;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author petr
 */
public class Vector {

    private int dimension;
    private int[] part;
    Random rnd = new Random();

    public Vector(int dimension) {
        this.dimension = dimension;
        part = new int[dimension];
        fillVector(dimension);
    }

    public void fillVector(int dimension) {
        for (int i = 0; i < dimension; i++) {
            part[i] = rnd.nextInt(10 + 1 + 10) - 10;
        }

    }
    public int maxPart(){
    
    int max=part[0];
        for (int i = 0; i < part.length; i++) {
            if(max<part[i]){
            max=part[i];
            }
        }
        return max;
        
    }
    public void switchPart(int first,int second){
    int tmp = part[first];
    part[first]=part[second];
    part[second]=tmp;
    
    
    }

    public int getDimension() {
        return dimension;
    }

    public int[] getPart() {
        return part;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vector other = (Vector) obj;
        if (this.dimension != other.dimension) {
            return false;
        }
        if (!Arrays.equals(this.part, other.part)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Vector:" + Arrays.toString(part);
    }

}
