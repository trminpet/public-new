/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cv3;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Objects;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author petr
 *
 *
 * class idNumberHelper(){ private String day; private String month; private
 * String year; SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
 *
 *
 * public IdNumberHelper(String s){
 *
 * try(){ Date d = sdf.parse(s); int day =d.getDay(); int month =d.getMonth();
 * int year = d.getYear(); this.day=(new Integer(day)).toString();
 * this.month=(new Integer(month)).toString(); this.year=(new
 * Integer(year)).toString(); *
 *
 * }
 *
 *
 * }
 *
 *
 *
 * }
 *
 */
public  class NationalIdentificationNumber {

    private final String rc;
    private final String suffix;

    public  NationalIdentificationNumber(String rc) {

        

        this.rc = getPrefix(rc);

        this.suffix = getSuffixFromRc(rc);
    }

    public String getPrefix(String rc) {
        String prefix = rc.substring(0, 6);
        return prefix;
    }

    public String getSuffixFromRc(String rc) {
        String suffix = rc.substring(6, rc.length());
        return suffix;
    }

    public String getRC() {
        return rc.toString() + "/" + suffix.toString();
    }

    public String getDay(String rc) {

        return rc.substring(4, 6);
    }

    public String getMonth(String rc) {

        return rc.substring(2, 4);
    }

    public String getYear(String rc) {

        return rc.substring(0, 2);
    }

    public  boolean validationDate(String rc) {
        Date date = null;
        String inputDate = new StringBuilder().append(getDay(rc)).append("-").append(getMonth(rc)).append("-").append(getYear(rc)).toString();;
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yy");
            formatter.setLenient(false);
            date = formatter.parse(inputDate);
           
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

    }

    public  boolean validationRC() {
        String a = new StringBuilder().append(rc).append(suffix).toString();
        System.out.println(a);
        int even = 0;
        int odd = 0;
        for (int i = 0; i < a.length(); i++) {
            int k = Character.getNumericValue(a.charAt(i));
            if (i % 2 == 0) {
                even += k;
            } else {
                odd += k;
            }
            System.out.println(k);
        }
        System.out.println(even + " " + odd);
        int res = even - odd;
        System.out.println(res);
        if (res % 11 == 0) {
            System.out.println("Spravne");
            return true;
        } else {
            
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.rc);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NationalIdentificationNumber other = (NationalIdentificationNumber) obj;
        if (!Objects.equals(this.rc, other.rc)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NationalIdentificationNumber{" + "rc=" + rc + '}';
    }

}
