/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vector2;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author petr
 */
public class VectorService implements IVectorService{

    @Override
    public Vector createVectorBySize(int size) {
        Vector<Integer> v = new Vector();
        for (int i = 0; i < size; i++) {
            v.add(new Integer(i));
        }
        return v ;
        
    }

    /**
     *
     * @param v
     */
    @Override
    public void setVectorByRandomValue(Vector<Integer> v ) {
        Random r = new Random();
      int size = v.size();
        for (int i = 0; i < size; i++) {
             int randomNumber = r.nextInt(10 + 1 + 10) - 10;
            v.set(i,new Integer(randomNumber));
           Integer x = v.get(i);
            System.out.println("hodnota prvku " + i + " = " + x.toString()+ " randomValue = " + randomNumber);
        }
 
        
    }

    @Override
    public String toString(Vector<Integer> v) {
        String tmp = "Muj string [ ";
        Iterator<Integer> iter = v.iterator();
        while (iter.hasNext()) {
        tmp += iter.next().toString();
        if(iter.hasNext())
        tmp +=" | ";
            
        }
        tmp += " ]";
        
    return tmp;    
    }

    @Override
    public Boolean equals(Vector<Integer> v1, Vector<Integer> v2) {
        if(v1==null && v2==null) return true;
        if(v1!=null && v2!=null){
          if(v1.size()==v2.size()){
              boolean equals = true;
              for (int i = 0; i < v1.size(); i++) {
                  int vv1,vv2;
                  vv1 = v1.get(i);
                  vv2= v2.get(i);
                  if(vv2!=vv1) equals= false;
                  }
              return equals;
          }
          else
              return false;
        }
        else
         return false;
    }

    @Override
    public void switchVector(Vector<Integer> v, int a, int b) {
        int tmp = v.get(a);
        v.set(a, v.get(b));
        v.set(b,tmp);
    }

    @Override
    public int skalarVector(Vector<Integer> v1, Vector<Integer> v2) {
        if(v1.size() == 10 && v2.size()==10){
        int sk = v1.get(0)*v2.get(0) + v1.get(1)*v2.get(1);
        return sk;
        
        }
        else
        return 0;
    }

    @Override
    public int biggestVector(Vector<Integer> v1) {
        Collections.sort(v1);
        return v1.get(v1.size()-1);
    }

    @Override
    public void vectorSum(Vector<Integer> v1, Vector<Integer> v2) {
        int a = v1.get(1)*v2.get(2)-v1.get(2)*v2.get(1);
        int b = v1.get(2)*v2.get(0)-v1.get(0)*v2.get(2);
        int c = v1.get(0)*v2.get(1)-v1.get(1)*v2.get(0);
        System.out.printf("Vectorovy soucin je: [ %d, %d, %d ]\n",a,b,c);
    }

    @Override
    public int freqVal(Vector<Integer> v1) {
      Integer [] arr = new Integer[v1.size()];
      v1.toArray(arr);
      HashMap<Integer,Integer> hm=new HashMap<Integer,Integer>();
    int max=1,temp=0;
    for(int i=0;i<arr.length;i++)
        {
            if(hm.get(arr[i])!=null)
            {int count=hm.get(arr[i]);
            count=count+1;
            hm.put(arr[i],count);
            if(count>max)
                {max=count;
                 temp=arr[i];}
            }
            else
            {hm.put(arr[i],1);}
        }
        return temp;
      
    }

   
       
    }

    

   
    
