/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vector2;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author petr
 */
public interface IVectorService {

    /**
     *
     * @param size
     * @return
     */
    public Vector <Integer> createVectorBySize(int size);
    public void setVectorByRandomValue(Vector<Integer> v);
    public String toString(Vector<Integer>v);
    public Boolean equals(Vector<Integer> v1,Vector<Integer> v2 );
    public void switchVector(Vector<Integer>v ,int a, int b);
    public int skalarVector(Vector<Integer> v1,Vector<Integer> v2);
    public int biggestVector(Vector<Integer>v1);
    public  void vectorSum(Vector <Integer> v1,Vector <Integer> v2 );
    public int freqVal(Vector <Integer> v1);
    
}
