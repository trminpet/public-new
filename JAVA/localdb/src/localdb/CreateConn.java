/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package localdb;
import java.sql.*;
/**
 *
 * @author petr
 */
public class CreateConn {
    private final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private final String DB_URL = "jdbc:derby:students;create=true";
    private final Connection connection;
    private boolean DB_TABLE_CREATED;

    public CreateConn() throws ClassNotFoundException, SQLException {
        Class.forName(DB_DRIVER);
         connection = DriverManager.getConnection(DB_URL);
         
         ResultSet rs = connection.getMetaData().getTables(null, null, null, new String[]{"TABLE"});
         DB_TABLE_CREATED=false;
         while(rs.next()){
         DB_TABLE_CREATED=true;
         
         }if(DB_TABLE_CREATED){
        System.out.println("Connected..");
         }
         else{
            
        connection.createStatement().execute("create table student(id int ,firstName varchar(20),lastName varchar(30))"); 
        System.out.println("Table created...");
            System.out.println("Connected..");
         }
        
        
         
       
    }

    public String getDB_DRIVER() {
        return DB_DRIVER;
    }

    public String getDB_URL() {
        return DB_URL;
    }

    public Connection getConnection() {
        return connection;
    }
    
    
    
    
    
    
}
