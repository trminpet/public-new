/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author petr
 */
public class Commands {
    
    
        private   Statement stmt = null;
      
        private  final String DB_TABLE ="STUDENT";
    
    
   public  void insertCustomer(Connect cn,int id, String lastName, String surName)
    {
        try
        {
            stmt = cn.getConn().createStatement();
            stmt.execute("insert into " + DB_TABLE + " values (" +
                    id + ",'" + lastName + "','" + surName +"')");
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
    }
    
   
   public  void selectCustomer(Connect cn)
    {
        try
        {
            stmt = cn.getConn().createStatement();
            ResultSet results = stmt.executeQuery("select * from " + DB_TABLE);
            ResultSetMetaData rsmd = results.getMetaData();
            int numberCols = rsmd.getColumnCount();
            for (int i=1; i<=numberCols; i++)
            {
                //print Column Names
                System.out.print(rsmd.getColumnLabel(i)+"\t\t");  
            }

            System.out.println("\n-------------------------------------------------");

            while(results.next())
            {
                int id = results.getInt(1);
                String lastName = results.getString(2);
                String surName = results.getString(3);
                System.out.println(id + "\t\t" + lastName + "\t\t" + surName);
            }
            results.close();
            stmt.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }

    }

    public  Statement getStmt() {
        return stmt;
    }
    
}
