/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.*;


/**
 *
 * @author petr
 */


public class Connect {
    private static final String DB_URL = "jdbc:derby://localhost:1527/Students";
    private static final String DB_USER = "app";
    private static final String DB_PASS = "app";
    private static final String DB_TABLE ="STUDENT";
    private  Connection con = null;
  
    public Connect() {
        
       
    }

    public Connection getConn() {
        return con;
    }
   
       
    
     public void createConnection(){
       try{ 
           System.out.println("Connecting to DB: "+DB_URL);
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        }catch(ClassNotFoundException e){
            System.out.println(e);
        }
        
        try{
        con = DriverManager.getConnection(DB_URL,DB_USER,DB_PASS);
            System.out.println("Connected to DB.");
        
     
    }
        catch(SQLException e){
            System.out.println("Connection failed.");
            System.err.println(e);
        }                 
    
        
        
    }
     
    public void closeConnection() throws SQLException{
      
         try
        {
           
            if (con != null)
            {
                System.out.println("Connection closed");
                DriverManager.getConnection(DB_URL + ";shutdown=true");
                con.close();
            }           
        }
        catch (SQLException sqlExcept)
        {
            
        }

    
    }
}
    

