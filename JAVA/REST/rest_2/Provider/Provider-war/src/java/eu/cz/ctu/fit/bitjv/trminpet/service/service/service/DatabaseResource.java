/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bitjv.trminpet.service.service.service;

import eu.cz.ctu.fit.bitjv.trminpet.entities.Cestujici;
import eu.cz.ctu.fit.bitjv.trminpet.entities.Linka;
import eu.cz.ctu.fit.bitjv.trminpet.entities.Pilot;
import eu.cz.ctu.fit.bitjv.trminpet.service.DatabaseService;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.DELETE;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service Trida pro praci s REST na serveru
 *
 * @author petr
 */
@Path("db")
public class DatabaseResource {

    @Context
    private UriInfo context;
    @EJB
    private DatabaseService service;

   

    /**
     * Vytvoreni noveho datasource z ejb
     */
    public DatabaseResource() {
    }

    private DatabaseService getService() {
        if (service == null) {
            try {
                InitialContext ic = new InitialContext();

                service = (DatabaseService) ic.lookup("java:global/Provider/Provider-ejb/DatabaseServiceBean");
            } catch (NamingException ex) {
                Logger.getLogger(DatabaseResource.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return service;
    }

    /**
     * Metoda na ziskani list pilotu
     *
     *
     * @return Response
     */
    @GET
    @Path("/getListPilot")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListPilot() {

        List<Pilot> list = getService().getPilots();
        if (list == null) {
            System.out.println("error");
        }
        GenericEntity<List<Pilot>> entity = new GenericEntity<List<Pilot>>(list) {
        };
        Response response = Response.ok(entity).build();

        return response;

    }

    /**
     * Metoda na ziskani list cestujici
     *
     *
     * @return Response
     */
    @GET
    @Path("/getListCestujici")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListCestujici() {

        List<Cestujici> list = getService().getCestujici();

        GenericEntity<List<Cestujici>> entity = new GenericEntity<List<Cestujici>>(list) {
        };
        Response response = Response.ok(entity).build();
        return response;

    }

    /**
     * Metoda na ziskani list linka
     *
     *
     * @return Response
     */
    @GET
    @Path("/getListLinka")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListLinka() {

        List<Linka> list = getService().getLinka();

        GenericEntity<List<Linka>> entity = new GenericEntity<List<Linka>>(list) {
        };
        Response response = Response.ok(entity).build();
        return response;

    }

    /**
     * Metoda na vytvoreni linky
     *
     *
     * @return Response
     */
    @POST
    @Path("/createLinka")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createLinka(Linka linka) {
        getService().createLinka(linka);
        return Response.ok().build();
    }

    /**
     * Metoda na vytvoreni pilota
     *
     *
     * @return Response
     */
    @POST
    @Path("/createPilot")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPilot(Pilot pilot) {
        getService().createPilot(pilot);
        return Response.ok().build();
    }

    /**
     * Metoda na vytvoreni cestujici
     *
     *
     * @return Response
     */
    @POST
    @Path("/createCestujici")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCestujici(Cestujici cestujici) {
        getService().createCestujici(cestujici);
        return Response.ok().build();
    }

    /**
     * Metoda na upraveni linky
     *
     *
     * @return Response
     */
    @PUT
    @Path("/updateLinka/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateLinka(Linka linka) {

        getService().updateLinka(linka);
        return Response.ok().entity(linka).build();
    }

    /**
     * Metoda na upraveni pilota
     *
     *
     * @return Response
     */
    @PUT
    @Path("/updatePilot/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePilot(Pilot pilot) {

        getService().updatePilot(pilot);
        return Response.ok().entity(pilot).build();
    }

    /**
     * Metoda na upraveni cestujici
     *
     *
     * @return Response
     */
    @PUT
    @Path("/updateCestujici/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCestujici(Cestujici cestujici) {

        getService().updateCestujici(cestujici);
        return Response.ok().entity(cestujici).build();
    }

    /**
     * Metoda na odstraneni cestujici
     *
     * @return Response
     */
    @PUT
    @Path("/deleteCestujici/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCestujici(Cestujici cestujici) {

        getService().deleteCestujici(cestujici);
        return Response.ok().entity(cestujici).build();
    }

    /**
     * Metoda na odstraneni pilota
     *
     * @return Response
     */
    @PUT
    @Path("/deletePilot/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePilot(Pilot pilot) {

        getService().deletePilot(pilot);
        return Response.ok().entity(pilot).build();
    }

    /**
     * Metoda na odstraneni linky
     *
     * @return Response
     */
    @PUT
    @Path("/deleteLinka/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteLinka(Linka linka) {

        getService().deleteLinka(linka);
        return Response.ok().entity(linka).build();
    }

}
