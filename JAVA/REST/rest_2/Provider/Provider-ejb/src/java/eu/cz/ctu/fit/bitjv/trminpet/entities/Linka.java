/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bitjv.trminpet.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author petr
 */
@Entity
@Table(name = "LINKA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Linka.findAll", query = "SELECT l FROM Linka l"),
    @NamedQuery(name = "Linka.findByLinkaKey", query = "SELECT l FROM Linka l WHERE l.linkaKey = :linkaKey"),
    @NamedQuery(name = "Linka.findByOdletovaDestinace", query = "SELECT l FROM Linka l WHERE l.odletovaDestinace = :odletovaDestinace"),
    @NamedQuery(name = "Linka.findByCilovaDestinace", query = "SELECT l FROM Linka l WHERE l.cilovaDestinace = :cilovaDestinace"),
    @NamedQuery(name = "Linka.findByVzdalenost", query = "SELECT l FROM Linka l WHERE l.vzdalenost = :vzdalenost")})
public class Linka implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "LINKA_KEY")
    private BigDecimal linkaKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ODLETOVA_DESTINACE")
    private String odletovaDestinace;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CILOVA_DESTINACE")
    private String cilovaDestinace;
    @Column(name = "VZDALENOST")
    private BigInteger vzdalenost;

    public Linka() {
    }

    public Linka(BigDecimal linkaKey) {
        this.linkaKey = linkaKey;
    }

    public Linka(BigDecimal linkaKey, String odletovaDestinace, String cilovaDestinace) {
        this.linkaKey = linkaKey;
        this.odletovaDestinace = odletovaDestinace;
        this.cilovaDestinace = cilovaDestinace;
    }

    public BigDecimal getLinkaKey() {
        return linkaKey;
    }

    public void setLinkaKey(BigDecimal linkaKey) {
        this.linkaKey = linkaKey;
    }

    public String getOdletovaDestinace() {
        return odletovaDestinace;
    }

    public void setOdletovaDestinace(String odletovaDestinace) {
        this.odletovaDestinace = odletovaDestinace;
    }

    public String getCilovaDestinace() {
        return cilovaDestinace;
    }

    public void setCilovaDestinace(String cilovaDestinace) {
        this.cilovaDestinace = cilovaDestinace;
    }

    public BigInteger getVzdalenost() {
        return vzdalenost;
    }

    public void setVzdalenost(BigInteger vzdalenost) {
        this.vzdalenost = vzdalenost;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (linkaKey != null ? linkaKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Linka)) {
            return false;
        }
        Linka other = (Linka) object;
        if ((this.linkaKey == null && other.linkaKey != null) || (this.linkaKey != null && !this.linkaKey.equals(other.linkaKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pokus.Linka[ linkaKey=" + linkaKey + " ]";
    }
    
}
