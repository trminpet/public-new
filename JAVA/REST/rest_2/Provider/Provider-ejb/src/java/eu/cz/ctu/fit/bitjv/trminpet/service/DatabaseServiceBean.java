/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bitjv.trminpet.service;

import eu.cz.ctu.fit.bitjv.trminpet.entities.Cestujici;
import eu.cz.ctu.fit.bitjv.trminpet.entities.Linka;
import eu.cz.ctu.fit.bitjv.trminpet.entities.Pilot;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * JAVA BEAN Trida ktera se stara o CRUD operace a persistenci dat
 * @author petr
 */
@Stateless

public class DatabaseServiceBean implements DatabaseService {

    @PersistenceContext(unitName = "Provider-ejbPU")
    private EntityManager em;

    @Override
    public List<Pilot> getPilots() {
        PilotFacade pf = new PilotFacade(Pilot.class);
        pf.setEntityManager(em);
        return pf.findAll();
    }

    @Override
    public void updatePilot(Pilot pilot) {
        PilotFacade pf = new PilotFacade(Pilot.class);
        pf.setEntityManager(em);
        pf.edit(pilot);
    }

    @Override
    public void deletePilot(Pilot pilot) {
        PilotFacade pf = new PilotFacade(Pilot.class);
        pf.setEntityManager(em);
        pf.remove(pilot);
    }

    @Override
    public List<Cestujici> getCestujici() {
        CestujiciFacade cf = new CestujiciFacade(Cestujici.class);
        cf.setEntityManager(em);
        return cf.findAll();
    }

    @Override
    public void updateCestujici(Cestujici cestujici) {
        CestujiciFacade cf = new CestujiciFacade(Cestujici.class);
        cf.setEntityManager(em);
        cf.edit(cestujici);
    }

    @Override
    public void deleteCestujici(Cestujici cestujici) {
        CestujiciFacade cf = new CestujiciFacade(Cestujici.class);
        cf.setEntityManager(em);
        cf.remove(cestujici);
    }

    @Override
    public List<Linka> getLinka() {
         LinkaFacade lf = new LinkaFacade(Linka.class);
        lf.setEntityManager(em);
        return lf.findAll();
    }

    @Override
    public void updateLinka(Linka linka) {
          LinkaFacade lf = new LinkaFacade(Linka.class);
        lf.setEntityManager(em);
        lf.edit(linka);
    }

    @Override
    public void deleteLinka(Linka linka) {
        LinkaFacade lf = new LinkaFacade(Linka.class);
        lf.setEntityManager(em);
        lf.remove(linka);
    }

    @Override
    public void createPilot(Pilot pilot) {
      PilotFacade pf = new PilotFacade(Pilot.class);
        pf.setEntityManager(em);
        pf.create(pilot);
    }

    @Override
    public void createCestujici(Cestujici cestujici) {
     CestujiciFacade cf = new CestujiciFacade(Cestujici.class);
        cf.setEntityManager(em);
        cf.create(cestujici);    
    }

    @Override
    public void createLinka(Linka linka) {
        LinkaFacade lf = new LinkaFacade(Linka.class);
        lf.setEntityManager(em);
        lf.create(linka);
    }

}
