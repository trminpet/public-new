/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bittjv.trminpet.client;

import eu.cz.ctu.fit.bitjv.trminpet.entit.Cestujici;
import eu.cz.ctu.fit.bitjv.trminpet.entit.Linka;
import eu.cz.ctu.fit.bitjv.trminpet.entit.Pilot;
import java.util.List;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 * Jersey REST client generated for REST resource:DatabaseResource [db]<br>
 * USAGE:
 * <pre>
 *        RestClient client = new RestClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 * Trida pro praci s REST sluzbami
 * @author petr
 */
public class RestClient {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:27232/Provider-war/webresources";

    public RestClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("db");
    }

    public Response createPilot(Object requestEntity) throws ClientErrorException {
        return webTarget.path("createPilot").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public Response createCestujici(Object requestEntity) throws ClientErrorException {
        return webTarget.path("createCestujici").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public Response deleteCestujici(Object requestEntity) throws ClientErrorException {
        return webTarget.path("deleteCestujici").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public <T> T getListCestujici(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("getListCestujici");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get((GenericType<T>) new GenericType<List<Cestujici>>(){});
    }

    public <T> T getListLinka(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("getListLinka");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get((GenericType<T>) new GenericType<List<Linka>>(){});
    }

    public Response updatePilot(Object requestEntity) throws ClientErrorException {
        return webTarget.path("updatePilot").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public Response updateCestujici(Object requestEntity) throws ClientErrorException {
        return webTarget.path("updateCestujici").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public Response deletePilot(Object requestEntity) throws ClientErrorException {
        return webTarget.path("deletePilot").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public Response createLinka(Object requestEntity) throws ClientErrorException {
        return webTarget.path("createLinka").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public <T> T getListPilot(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("getListPilot");
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get((GenericType<T>) new GenericType<List<Pilot>>(){});
    }

    public Response deleteLinka(Object requestEntity) throws ClientErrorException {
        return webTarget.path("deleteLinka").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public Response updateLinka(Object requestEntity) throws ClientErrorException {
        return webTarget.path("updateLinka").request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), Response.class);
    }

    public void close() {
        client.close();
    }
    
}
