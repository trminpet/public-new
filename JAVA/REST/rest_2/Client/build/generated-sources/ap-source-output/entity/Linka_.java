package entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-24T18:13:16")
@StaticMetamodel(Linka.class)
public class Linka_ { 

    public static volatile SingularAttribute<Linka, String> cilovaDestinace;
    public static volatile SingularAttribute<Linka, BigDecimal> linkaKey;
    public static volatile SingularAttribute<Linka, BigInteger> vzdalenost;
    public static volatile SingularAttribute<Linka, String> odletovaDestinace;

}