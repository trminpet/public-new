Na konci semestru (druhý lednový týden) odevzdat 3 vrstvou semestrální (či více vrstvou) aplikaci kde:

Datová vrstva komunikuje se schematem z BI-DBS (min. Dvěma entitami), všechny CRUD operace s využitím ORM a pomocí volání jmenné služby JNDI registrované na aplikačním serveru
Vrstva obchodní logiky zajistí komunikaci s datovou vrstvou:
pomocí vyhledání datového zdroje v seznamu registrovaných zdrojů JNDI, pomocí Entity manageru, JPA controlleru a Persistence Unit
Zprostředuje REST WebService (případně SOA) rozhraní
Vrstva presenční logiky umožní CRUD operace nad zmíněnými dvěma (či více) entitami.
