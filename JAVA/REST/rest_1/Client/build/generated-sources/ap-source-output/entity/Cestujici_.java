package entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-24T18:13:16")
@StaticMetamodel(Cestujici.class)
public class Cestujici_ { 

    public static volatile SingularAttribute<Cestujici, String> cilovaDestinace;
    public static volatile SingularAttribute<Cestujici, BigDecimal> cestujiciKey;
    public static volatile SingularAttribute<Cestujici, String> narodnost;
    public static volatile SingularAttribute<Cestujici, String> prijmeni;
    public static volatile SingularAttribute<Cestujici, BigInteger> zavazadla;
    public static volatile SingularAttribute<Cestujici, String> jmeno;
    public static volatile SingularAttribute<Cestujici, Date> datumNarozeni;

}