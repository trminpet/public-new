/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bittjv.trminpet.client;

import java.text.ParseException;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;

/**
 *
 * @author petr trida pro GUI
 */
public class ClientView extends javax.swing.JFrame {

    /**
     * Deklarace MVC
     *
     */
    RestClient client;
    ClientController controller;

    /**
     * Init sluzeb a vlastnosti formulare
     *
     */
    public ClientView() {
        initComponents();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
        client = new RestClient();
        controller = new ClientController(this, client);
        this.TFCestujiciKey.setVisible(false);
        this.TFLinkaKey.setVisible(false);
        this.TFPilotKey.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TABLECestujici = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        TFCestujiciNarod = new javax.swing.JTextField();
        TFCestujiciJmeno = new javax.swing.JTextField();
        TFCestujiciPrijmeni = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        TFCestujiciDatum = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        TFCestujiciZavazadla = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        TFCestujiciCil = new javax.swing.JTextField();
        BTCestujiciSelect = new javax.swing.JButton();
        BTCestujiciDelete = new javax.swing.JButton();
        BTCestujiciUpdate = new javax.swing.JButton();
        BtCestujiciInsert = new javax.swing.JButton();
        TFCestujiciKey = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TABLEPilot = new javax.swing.JTable();
        TFPilotJmeno = new javax.swing.JTextField();
        TFPilotPrijmeni = new javax.swing.JTextField();
        TFPilotLetovaTrida = new javax.swing.JTextField();
        TFPilotNaletano = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        BTPilotDelete = new javax.swing.JButton();
        BTPilotInsert = new javax.swing.JButton();
        BTPilotSelect = new javax.swing.JButton();
        BTPilotUpdate = new javax.swing.JToggleButton();
        TFPilotKey = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        TABLELinka = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        TFLinkaOdlet = new javax.swing.JTextField();
        TFLinkaPrilet = new javax.swing.JTextField();
        TFLinkaVzdalenost = new javax.swing.JTextField();
        BTLinkaSelect = new javax.swing.JButton();
        BTLinkaDelete = new javax.swing.JButton();
        BTLinkaUpdate = new javax.swing.JButton();
        BTLinkaInsert = new javax.swing.JButton();
        TFLinkaKey = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setBackground(new java.awt.Color(160, 87, 87));

        TABLECestujici.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Národnost:", "Jméno:", "Příjmení:", "Datum narození:", "Počet zavazadel:", "Cílová destinace:", "Key"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, true, true, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TABLECestujici.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TABLECestujiciMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(TABLECestujici);

        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel5.setText("Národnost:");

        jLabel6.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel6.setText("Jméno:");

        jLabel7.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel7.setText("Příjmení:");

        jLabel8.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel8.setText("Datum narození:");

        TFCestujiciDatum.setText("yyyy-MM-dd");

        jLabel9.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel9.setText("Počet zavazadel:");

        TFCestujiciZavazadla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TFCestujiciZavazadlaActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel10.setText("Cílová destinace:");

        BTCestujiciSelect.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTCestujiciSelect.setText("Vybrat");
        BTCestujiciSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTCestujiciSelectActionPerformed(evt);
            }
        });

        BTCestujiciDelete.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTCestujiciDelete.setText("Vymazat");
        BTCestujiciDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTCestujiciDeleteActionPerformed(evt);
            }
        });

        BTCestujiciUpdate.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTCestujiciUpdate.setText("Upravit");
        BTCestujiciUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTCestujiciUpdateActionPerformed(evt);
            }
        });

        BtCestujiciInsert.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BtCestujiciInsert.setText("Vložit");
        BtCestujiciInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtCestujiciInsertActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Noto Sans", 0, 18)); // NOI18N
        jLabel14.setText("Cestujici");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGap(58, 58, 58)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel6))
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel8Layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(TFCestujiciNarod, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel8Layout.createSequentialGroup()
                                                .addGap(69, 69, 69)
                                                .addComponent(TFCestujiciJmeno))))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel7)
                                            .addComponent(jLabel8)
                                            .addComponent(jLabel9)
                                            .addComponent(jLabel10))
                                        .addGap(29, 29, 29)
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(TFCestujiciDatum, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(TFCestujiciZavazadla, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(TFCestujiciCil)
                                            .addComponent(TFCestujiciPrijmeni)))))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(BTCestujiciDelete)
                                .addGap(18, 18, 18)
                                .addComponent(BTCestujiciUpdate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(BtCestujiciInsert)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 113, Short.MAX_VALUE)))
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(TFCestujiciKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(173, 173, 173)
                        .addComponent(BTCestujiciSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(79, 79, 79))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(TFCestujiciNarod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(TFCestujiciJmeno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(TFCestujiciPrijmeni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(TFCestujiciDatum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(TFCestujiciZavazadla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(TFCestujiciCil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BTCestujiciDelete)
                            .addComponent(BTCestujiciUpdate)
                            .addComponent(BtCestujiciInsert)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BTCestujiciSelect)
                    .addComponent(TFCestujiciKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cestujici", jPanel8);

        TABLEPilot.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Jméno", "Příjmení", "Letová třída", "Nalétáno hodin", "Key"
            }
        ));
        TABLEPilot.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TABLEPilotMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TABLEPilot);
        if (TABLEPilot.getColumnModel().getColumnCount() > 0) {
            TABLEPilot.getColumnModel().getColumn(4).setPreferredWidth(0);
        }

        TFPilotJmeno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TFPilotJmenoActionPerformed(evt);
            }
        });

        TFPilotPrijmeni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TFPilotPrijmeniActionPerformed(evt);
            }
        });

        TFPilotLetovaTrida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TFPilotLetovaTridaActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel1.setText("Jméno:");

        jLabel2.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel2.setText("Příjmení:");

        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel3.setText("Letová třída:");

        jLabel4.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel4.setText("Nalétáno hodin:");

        BTPilotDelete.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTPilotDelete.setText("Vymazat");
        BTPilotDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTPilotDeleteActionPerformed(evt);
            }
        });

        BTPilotInsert.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTPilotInsert.setText("Vložit");
        BTPilotInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTPilotInsertActionPerformed(evt);
            }
        });

        BTPilotSelect.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTPilotSelect.setText("Vybrat");
        BTPilotSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTPilotSelectActionPerformed(evt);
            }
        });

        BTPilotUpdate.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTPilotUpdate.setText("Upravit");
        BTPilotUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTPilotUpdateActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        jLabel16.setText("Pilot");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addGroup(jPanel6Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel1))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(TFPilotPrijmeni)
                                    .addComponent(TFPilotJmeno)
                                    .addComponent(TFPilotNaletano)
                                    .addComponent(TFPilotLetovaTrida)))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addContainerGap(248, Short.MAX_VALUE)
                        .addComponent(BTPilotDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTPilotUpdate)
                        .addGap(13, 13, 13)
                        .addComponent(BTPilotInsert)))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(TFPilotKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(211, 211, 211)
                        .addComponent(BTPilotSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 559, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BTPilotSelect)
                    .addComponent(TFPilotKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel16)
                .addGap(113, 113, 113)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TFPilotJmeno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(27, 27, 27)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TFPilotPrijmeni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TFPilotLetovaTrida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TFPilotNaletano, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(30, 30, 30)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BTPilotDelete)
                    .addComponent(BTPilotUpdate)
                    .addComponent(BTPilotInsert))
                .addGap(112, 112, 112))
        );

        jLabel1.getAccessibleContext().setAccessibleName("");

        jTabbedPane1.addTab("Pilot", jPanel6);

        TABLELinka.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Odletová destinace:", "Příletová destinace:", "Vzdálenost:", "Key"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TABLELinka.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TABLELinkaMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(TABLELinka);

        jLabel11.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel11.setText("Odletová destinace:");

        jLabel12.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel12.setText("Příletová destinace:");

        jLabel13.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel13.setText("Vzdálenost:");

        TFLinkaOdlet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TFLinkaOdletActionPerformed(evt);
            }
        });

        BTLinkaSelect.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTLinkaSelect.setText("Vybrat");
        BTLinkaSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTLinkaSelectActionPerformed(evt);
            }
        });

        BTLinkaDelete.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTLinkaDelete.setText("Vymazat");
        BTLinkaDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTLinkaDeleteActionPerformed(evt);
            }
        });

        BTLinkaUpdate.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTLinkaUpdate.setText("Upravit");
        BTLinkaUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTLinkaUpdateActionPerformed(evt);
            }
        });

        BTLinkaInsert.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        BTLinkaInsert.setText("Vložit");
        BTLinkaInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTLinkaInsertActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Noto Sans", 0, 18)); // NOI18N
        jLabel15.setText("Linka");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(59, 59, 59)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(TFLinkaOdlet, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(BTLinkaDelete)
                                .addGap(18, 18, 18)
                                .addComponent(BTLinkaUpdate)
                                .addGap(18, 18, 18)
                                .addComponent(BTLinkaInsert))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGap(76, 76, 76)
                                        .addComponent(jLabel13))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                                        .addGap(59, 59, 59)
                                        .addComponent(jLabel12)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(TFLinkaVzdalenost, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(TFLinkaPrilet, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(14, 14, 14)))
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addComponent(TFLinkaKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(BTLinkaSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(87, 87, 87))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(TFLinkaOdlet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(43, 43, 43)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(TFLinkaPrilet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(TFLinkaVzdalenost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BTLinkaInsert)
                            .addComponent(BTLinkaUpdate)
                            .addComponent(BTLinkaDelete))
                        .addGap(155, 155, 155))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(TFLinkaKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BTLinkaSelect))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jTabbedPane1.addTab("Linka", jPanel7);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("Cestujici");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TFPilotJmenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TFPilotJmenoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFPilotJmenoActionPerformed

    private void TFPilotPrijmeniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TFPilotPrijmeniActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFPilotPrijmeniActionPerformed

    private void TFPilotLetovaTridaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TFPilotLetovaTridaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFPilotLetovaTridaActionPerformed

    private void TFCestujiciZavazadlaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TFCestujiciZavazadlaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFCestujiciZavazadlaActionPerformed

    private void TFLinkaOdletActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TFLinkaOdletActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFLinkaOdletActionPerformed

    /*
    Zavolani metody na SELECT pro PILOTA
     */
    private void BTPilotSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTPilotSelectActionPerformed

        controller.setTablePilot();

    }//GEN-LAST:event_BTPilotSelectActionPerformed
    /*
    Zavolani metody na DELETE pro PILOTA
     */
    private void BTPilotDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTPilotDeleteActionPerformed
        controller.deletePilot(this.TFPilotJmeno.getText(), this.TFPilotPrijmeni.getText(), this.TFPilotLetovaTrida.getText(), this.TFPilotNaletano.getText(), this.TFPilotKey.getText());
    }//GEN-LAST:event_BTPilotDeleteActionPerformed
    /*
    Zavolani metody na INSERT pro PILOTA
     */
    private void BTPilotInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTPilotInsertActionPerformed
        controller.insertPilot(this.TFPilotJmeno.getText(), this.TFPilotPrijmeni.getText(), this.TFPilotLetovaTrida.getText(), this.TFPilotNaletano.getText());
    }//GEN-LAST:event_BTPilotInsertActionPerformed
    /*
    Zavolani metody na SELECT pro CESTUJICI
     */
    private void BTCestujiciSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTCestujiciSelectActionPerformed
        controller.setTableCestujici();
    }//GEN-LAST:event_BTCestujiciSelectActionPerformed
    /*
    Zavolani metody na SELECT pro LINKA
     */
    private void BTLinkaSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTLinkaSelectActionPerformed
        controller.setTableLinka();
    }//GEN-LAST:event_BTLinkaSelectActionPerformed
    /*
    Zavolani metody na INSERT pro LINKA
     */
    private void BTLinkaInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTLinkaInsertActionPerformed
        controller.insertLinka(this.TFLinkaOdlet.getText(), this.TFLinkaPrilet.getText(), this.TFLinkaVzdalenost.getText());
    }//GEN-LAST:event_BTLinkaInsertActionPerformed
    /*
    Zavolani metody na DELETE pro CESTUJICI
     */
    private void BTCestujiciDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTCestujiciDeleteActionPerformed
        try {
            controller.deleteCestujici(TFCestujiciNarod.getText(), TFCestujiciJmeno.getText(), TFCestujiciPrijmeni.getText(), TFCestujiciDatum.getText(), TFCestujiciZavazadla.getText(), TFCestujiciCil.getText(), TFCestujiciKey.getText());
        } catch (ParseException ex) {
            Logger.getLogger(ClientView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BTCestujiciDeleteActionPerformed
    /*
    Zavolani metody na UPDATE pro PILOTA
     */
    private void BTPilotUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTPilotUpdateActionPerformed
        controller.updatePilot(this.TFPilotJmeno.getText(), this.TFPilotPrijmeni.getText(), this.TFPilotLetovaTrida.getText(), this.TFPilotNaletano.getText(), this.TFPilotKey.getText());
    }//GEN-LAST:event_BTPilotUpdateActionPerformed
    /*
    Zavolani metody na UPDATE pro CESTUJICI
     */
    private void BTCestujiciUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTCestujiciUpdateActionPerformed
        try {
            controller.updateCestujici(TFCestujiciNarod.getText(), TFCestujiciJmeno.getText(), TFCestujiciPrijmeni.getText(), TFCestujiciDatum.getText(), TFCestujiciZavazadla.getText(), TFCestujiciCil.getText(), TFCestujiciKey.getText());
        } catch (ParseException ex) {
            Logger.getLogger(ClientView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BTCestujiciUpdateActionPerformed
    /*
    Zavolani metody na INSERT pro CESTUJICI
     */
    private void BtCestujiciInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtCestujiciInsertActionPerformed
        try {
            controller.insertCestujici(TFCestujiciNarod.getText(), TFCestujiciJmeno.getText(), TFCestujiciPrijmeni.getText(), TFCestujiciDatum.getText(), TFCestujiciZavazadla.getText(), TFCestujiciCil.getText());
        } catch (ParseException ex) {
            Logger.getLogger(ClientView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BtCestujiciInsertActionPerformed
    /*
    Zavolani metody na DELETE pro LINKA
     */
    private void BTLinkaDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTLinkaDeleteActionPerformed
        controller.deleteLinka(this.TFLinkaOdlet.getText(), this.TFLinkaPrilet.getText(), this.TFLinkaVzdalenost.getText(), TFLinkaKey.getText());
    }//GEN-LAST:event_BTLinkaDeleteActionPerformed
    /*
    Zavolani metody na UPDATE pro LINKA
     */
    private void BTLinkaUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTLinkaUpdateActionPerformed

        controller.updateLinka(this.TFLinkaOdlet.getText(), this.TFLinkaPrilet.getText(), this.TFLinkaVzdalenost.getText(), TFLinkaKey.getText());
    }//GEN-LAST:event_BTLinkaUpdateActionPerformed
    /*
    Metoda, ktera vyplni TextField Pilota dle prokliku v tabulce
     */
    private void TABLEPilotMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TABLEPilotMouseClicked

        int row = TABLEPilot.getSelectedRow();

        TFPilotJmeno.setText((String) TABLEPilot.getModel().getValueAt(row, 0));
        TFPilotPrijmeni.setText((String) TABLEPilot.getModel().getValueAt(row, 1));
        TFPilotLetovaTrida.setText((String) TABLEPilot.getModel().getValueAt(row, 2).toString());
        TFPilotNaletano.setText((String) TABLEPilot.getModel().getValueAt(row, 3).toString());
        TFPilotKey.setText((String) TABLEPilot.getModel().getValueAt(row, 4).toString());
    }//GEN-LAST:event_TABLEPilotMouseClicked
    /*
    Metoda, ktera vyplni TextField Cestujici dle prokliku v tabulce
     */
    private void TABLECestujiciMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TABLECestujiciMouseClicked
        int row = TABLECestujici.getSelectedRow();

        TFCestujiciNarod.setText((String) TABLECestujici.getModel().getValueAt(row, 0));
        TFCestujiciJmeno.setText((String) TABLECestujici.getModel().getValueAt(row, 1));
        TFCestujiciPrijmeni.setText((String) TABLECestujici.getModel().getValueAt(row, 2));
        TFCestujiciDatum.setText((String) TABLECestujici.getModel().getValueAt(row, 3));
        TFCestujiciZavazadla.setText((String) TABLECestujici.getModel().getValueAt(row, 4).toString());
        TFCestujiciCil.setText((String) TABLECestujici.getModel().getValueAt(row, 5));
        TFCestujiciKey.setText((String) TABLECestujici.getModel().getValueAt(row, 6).toString());

    }//GEN-LAST:event_TABLECestujiciMouseClicked
    /*
    Metoda, ktera vyplni TextField Linky dle prokliku v tabulce
     */
    private void TABLELinkaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TABLELinkaMouseClicked
        int row = TABLELinka.getSelectedRow();

        TFLinkaOdlet.setText((String) TABLELinka.getModel().getValueAt(row, 0));
        TFLinkaPrilet.setText((String) TABLELinka.getModel().getValueAt(row, 1));
        TFLinkaVzdalenost.setText((String) TABLELinka.getModel().getValueAt(row, 2).toString());
        TFLinkaKey.setText((String) TABLELinka.getModel().getValueAt(row, 3).toString());

    }//GEN-LAST:event_TABLELinkaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClientView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClientView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClientView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClientView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClientView().setVisible(true);
            }
        });
    }

    /*
   SET,GET metody pro controller
     */
    public JTable getTABLECestujici() {
        return TABLECestujici;
    }

    public void setTABLECestujici(JTable TABLECestujici) {
        this.TABLECestujici = TABLECestujici;
    }

    public JTable getTABLELinka() {
        return TABLELinka;
    }

    public void setTABLELinka(JTable TABLELinka) {
        this.TABLELinka = TABLELinka;
    }

    public JTable getTABLEPilot() {
        return TABLEPilot;
    }

    public void setTABLEPilot(JTable TABLEPilot) {
        this.TABLEPilot = TABLEPilot;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTCestujiciDelete;
    private javax.swing.JButton BTCestujiciSelect;
    private javax.swing.JButton BTCestujiciUpdate;
    private javax.swing.JButton BTLinkaDelete;
    private javax.swing.JButton BTLinkaInsert;
    private javax.swing.JButton BTLinkaSelect;
    private javax.swing.JButton BTLinkaUpdate;
    private javax.swing.JButton BTPilotDelete;
    private javax.swing.JButton BTPilotInsert;
    private javax.swing.JButton BTPilotSelect;
    private javax.swing.JToggleButton BTPilotUpdate;
    private javax.swing.JButton BtCestujiciInsert;
    private javax.swing.JTable TABLECestujici;
    private javax.swing.JTable TABLELinka;
    private javax.swing.JTable TABLEPilot;
    private javax.swing.JTextField TFCestujiciCil;
    private javax.swing.JTextField TFCestujiciDatum;
    private javax.swing.JTextField TFCestujiciJmeno;
    private javax.swing.JTextField TFCestujiciKey;
    private javax.swing.JTextField TFCestujiciNarod;
    private javax.swing.JTextField TFCestujiciPrijmeni;
    private javax.swing.JTextField TFCestujiciZavazadla;
    private javax.swing.JTextField TFLinkaKey;
    private javax.swing.JTextField TFLinkaOdlet;
    private javax.swing.JTextField TFLinkaPrilet;
    private javax.swing.JTextField TFLinkaVzdalenost;
    private javax.swing.JTextField TFPilotJmeno;
    private javax.swing.JTextField TFPilotKey;
    private javax.swing.JTextField TFPilotLetovaTrida;
    private javax.swing.JTextField TFPilotNaletano;
    private javax.swing.JTextField TFPilotPrijmeni;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}
