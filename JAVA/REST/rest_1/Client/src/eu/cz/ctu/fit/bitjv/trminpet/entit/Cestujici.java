/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bitjv.trminpet.entit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


/**
 *
 * @author petr
 * Trida cestujici pro prenos do databaze
 */

public class Cestujici implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private BigDecimal cestujiciKey;
 
    private String narodnost;
   
    private String jmeno;
  
   
    private String prijmeni;
 
  
    private Date datumNarozeni;

    private BigInteger zavazadla;
 
    private String cilovaDestinace;

    public Cestujici() {
    }

    public Cestujici(BigDecimal cestujiciKey) {
        this.cestujiciKey = cestujiciKey;
    }

    public Cestujici(BigDecimal cestujiciKey, String jmeno, String prijmeni, Date datumNarozeni) {
        this.cestujiciKey = cestujiciKey;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.datumNarozeni = datumNarozeni;
    }

    public BigDecimal getCestujiciKey() {
        return cestujiciKey;
    }

    public void setCestujiciKey(BigDecimal cestujiciKey) {
        this.cestujiciKey = cestujiciKey;
    }

    public String getNarodnost() {
        return narodnost;
    }

    public void setNarodnost(String narodnost) {
        this.narodnost = narodnost;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public Date getDatumNarozeni() {
        return datumNarozeni;
    }

    public void setDatumNarozeni(Date datumNarozeni) {
        this.datumNarozeni = datumNarozeni;
    }

    public BigInteger getZavazadla() {
        return zavazadla;
    }

    public void setZavazadla(BigInteger zavazadla) {
        this.zavazadla = zavazadla;
    }

    public String getCilovaDestinace() {
        return cilovaDestinace;
    }

    public void setCilovaDestinace(String cilovaDestinace) {
        this.cilovaDestinace = cilovaDestinace;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cestujiciKey != null ? cestujiciKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cestujici)) {
            return false;
        }
        Cestujici other = (Cestujici) object;
        if ((this.cestujiciKey == null && other.cestujiciKey != null) || (this.cestujiciKey != null && !this.cestujiciKey.equals(other.cestujiciKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Cestujici[ cestujiciKey=" + cestujiciKey + " ]";
    }
    
}
