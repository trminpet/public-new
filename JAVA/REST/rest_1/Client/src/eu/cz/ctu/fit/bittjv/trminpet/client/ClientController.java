/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bittjv.trminpet.client;

import eu.cz.ctu.fit.bitjv.trminpet.entit.Cestujici;
import eu.cz.ctu.fit.bitjv.trminpet.entit.Linka;
import eu.cz.ctu.fit.bitjv.trminpet.entit.Pilot;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.table.DefaultTableModel;
import javax.ws.rs.core.Response;

/**
 *
 * @author petr Trida pro controller view
 */
public class ClientController {

    List<Pilot> lsp;
    List<Linka> lsl;
    List<Cestujici> lsc;
    private ClientView view;
    private RestClient model;

    public ClientController(ClientView view, RestClient model) {
        this.view = view;
        this.model = model;

    }

    /*
    Metoda, ktera vyplni tabulky pro PILOT ze selectu 
     */
    public void setTablePilot() {

        lsp = model.getListPilot(List.class);
        DefaultTableModel modeltable = (DefaultTableModel) view.getTABLEPilot().getModel();
        int rowCount = modeltable.getRowCount();

        if (rowCount > 0) {
            clearRow(modeltable, rowCount);

        }

        for (Pilot pilot : lsp) {
            modeltable.addRow(new Object[]{pilot.getJmeno(), pilot.getPrijmeni(), pilot.getLetovaTrida(), pilot.getNaletaneHodiny(), pilot.getPilotKey()});
        }

    }

    /*
    Metoda, ktera vyplni tabulky pro CESTUJICI ze selectu 
     */

    public void setTableCestujici() {

        lsc = model.getListCestujici(List.class);
        DefaultTableModel modeltable = (DefaultTableModel) view.getTABLECestujici().getModel();
        int rowCount = modeltable.getRowCount();

        if (rowCount > 0) {
            clearRow(modeltable, rowCount);

        }

        for (Cestujici cestujici : lsc) {
            Format formatter = new SimpleDateFormat("yyyy-MM-dd");
            String datum = formatter.format(cestujici.getDatumNarozeni());
            modeltable.addRow(new Object[]{cestujici.getNarodnost(), cestujici.getJmeno(), cestujici.getPrijmeni(), datum, cestujici.getZavazadla(),
                cestujici.getCilovaDestinace(), cestujici.getCestujiciKey()
            });
        }

    }

    /*
    Metoda, ktera vyplni tabulky pro LINKA ze selectu 
     */
    public void setTableLinka() {

        lsl = model.getListLinka(List.class);
        DefaultTableModel modeltable = (DefaultTableModel) view.getTABLELinka().getModel();
        int rowCount = modeltable.getRowCount();

        if (rowCount > 0) {
            clearRow(modeltable, rowCount);

        }

        for (Linka linka : lsl) {
            modeltable.addRow(new Object[]{linka.getOdletovaDestinace(), linka.getCilovaDestinace(), linka.getVzdalenost(), linka.getLinkaKey()});
        }

    }

    /*
    Metoda, ktera pripravi objekt LINKA na vlozeni do DB, zavolani rest sluzby 
     */
    public void insertLinka(String odlet, String prilet, String vzdalenost) {

        Linka linka = new Linka() {
        };
        try {
            linka.setVzdalenost(new BigInteger(vzdalenost));
            linka.setCilovaDestinace(prilet);
            linka.setOdletovaDestinace(odlet);

            linka.setLinkaKey(BigDecimal.valueOf(100));

            Response res = model.createLinka(linka);
            System.out.println("Uspesne pridano!");
            setTableLinka();
        } catch (Exception e) {
            System.out.println("Nespravny format cisla pro vzdalenost!");
        }

    }

    /*
    Metoda, ktera pripravi objekt PILOT na vlozeni do DB, zavolani rest sluzby 
     */
    public void insertPilot(String jmeno, String prijmeni, String trida, String naletano) {

        try {
            Pilot pilot = new Pilot();
            pilot.setJmeno(jmeno);
            pilot.setPrijmeni(prijmeni);
            pilot.setNaletaneHodiny(new BigInteger(naletano));
            pilot.setLetovaTrida(new BigInteger(trida));
            pilot.setPilotKey(BigDecimal.ZERO);

            Response res = model.createPilot(pilot);
            System.out.println("Uspesne pridano!");
            setTablePilot();
        } catch (Exception e) {
            System.out.println("Nespravny format cisla pro naletane hodiny nebo letova trida!");

        }
    }

    /*
    Metoda, ktera pripravi objekt CESTUJICI na vlozeni do DB, zavolani rest sluzby 
     */
    public void insertCestujici(String narodnost, String jmeno, String prijmeni, String datum, String zavazadla, String cil) throws ParseException {

        try {
            Cestujici cestujici = new Cestujici();
            cestujici.setNarodnost(narodnost);
            cestujici.setJmeno(jmeno);
            cestujici.setPrijmeni(prijmeni);
            cestujici.setCilovaDestinace(cil);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = df.parse(datum);

            cestujici.setDatumNarozeni(startDate);
            cestujici.setZavazadla(new BigInteger(zavazadla));
            cestujici.setCestujiciKey(BigDecimal.valueOf(100));
            Response res = model.createCestujici(cestujici);
            System.out.println("Uspesne pridano!");
            setTableCestujici();
        } catch (Exception e) {
            System.out.println("Spatny format datumu (yyyy-MM-dd) nebo spatne zadane cislo zavazadla");

        }
    }

    /*
    Metoda, ktera pripravi objekt PILOT na smazani z DB, zavolani rest sluzby 
     */
    public void deletePilot(String jmeno, String prijmeni, String trida, String naletano, String key) {
        try {
            Pilot pilot = new Pilot();
            pilot.setJmeno(jmeno);
            pilot.setPrijmeni(prijmeni);
            pilot.setNaletaneHodiny(new BigInteger(naletano));
            pilot.setLetovaTrida(new BigInteger(trida));
            pilot.setPilotKey(new BigDecimal(key));

            model.deletePilot(pilot);
            System.out.println("Zaznam odstranen!");
            setTablePilot();
        } catch (Exception e) {
            System.out.println("Nespravny format cisla pro naletane hodiny nebo letova trida!");

        }

    }

    /*
    Metoda, ktera pripravi objekt PILOT na upraveni v DB, zavolani rest sluzby 
     */
    void updatePilot(String jmeno, String prijmeni, String trida, String naletano, String key) {
        try {

            Pilot pilot = new Pilot();
            pilot.setJmeno(jmeno);
            pilot.setPrijmeni(prijmeni);
            pilot.setNaletaneHodiny(new BigInteger(naletano));
            pilot.setLetovaTrida(new BigInteger(trida));
            pilot.setPilotKey(new BigDecimal(key));

            Response res = model.updatePilot(pilot);
            System.out.println("Zaznam upraven!");
            setTablePilot();
        } catch (Exception e) {
            System.out.println("Nespravny format cisla pro naletane hodiny nebo letova trida!");

        }
    }

    /*
    Metoda, ktera pripravi objekt CESTUJICI na upraveni v DB, zavolani rest sluzby 
     */

    void updateCestujici(String narodnost, String jmeno, String prijmeni, String datum, String zavazadla, String cil, String key) throws ParseException {

        try {
            Cestujici cestujici = new Cestujici();

            cestujici.setNarodnost(narodnost);
            cestujici.setJmeno(jmeno);
            cestujici.setPrijmeni(prijmeni);
            cestujici.setCilovaDestinace(cil);
            DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
            Date startDate = df.parse(datum);

            cestujici.setDatumNarozeni(startDate);
            cestujici.setZavazadla(new BigInteger(zavazadla));
            cestujici.setCestujiciKey(new BigDecimal(key));
            Response res = model.updateCestujici(cestujici);
            System.out.println("Zaznam upraven!");
            setTableCestujici();
        } catch (Exception e) {
            System.out.println("Spatny format datumu (yyyy-MM-dd) nebo spatne zadane cislo zavazadla");

        }

    }

    /*
    Metoda, ktera pripravi objekt CESTUJICI na smazani z DB, zavolani rest sluzby 
     */

    public void deleteCestujici(String narodnost, String jmeno, String prijmeni, String datum, String zavazadla, String cil, String key) throws ParseException {

        try {
            Cestujici cestujici = new Cestujici();
            cestujici.setNarodnost(narodnost);
            cestujici.setJmeno(jmeno);
            cestujici.setPrijmeni(prijmeni);
            cestujici.setCilovaDestinace(cil);
            DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
            Date startDate = df.parse(datum);

            cestujici.setDatumNarozeni(startDate);
            cestujici.setZavazadla(new BigInteger(zavazadla));
            cestujici.setCestujiciKey(new BigDecimal(key));
            Response res = model.deleteCestujici(cestujici);
            System.out.println("Zaznam odstranen!");
            setTableCestujici();
        } catch (Exception e) {
            System.out.println("Spatny format datumu (yyyy-MM-dd) nebo spatne zadane cislo zavazadla");

        }
    }

    /*
    Metoda, ktera pripravi objekt LINKA na upraveni v DB, zavolani rest sluzby 
     */

    void updateLinka(String odlet, String prilet, String vzdalenost, String key) {
        try {
            Linka linka = new Linka() {
            };
            linka.setCilovaDestinace(prilet);
            linka.setOdletovaDestinace(odlet);
            linka.setVzdalenost(new BigInteger(vzdalenost));
            linka.setLinkaKey(new BigDecimal(key));

            Response res = model.updateLinka(linka);
            System.out.println("Zaznam upraven!");
            setTableLinka();
        } catch (Exception e) {
            System.out.println("Nespravny format cisla pro vzdalenost!");
        }
    }

    /*
    Metoda, ktera pripravi objekt LINKA na smazani z DB, zavolani rest sluzby 
     */
    void deleteLinka(String odlet, String prilet, String vzdalenost, String key) {

        try {
            Linka linka = new Linka() {
            };
            linka.setCilovaDestinace(prilet);
            linka.setOdletovaDestinace(odlet);
            linka.setVzdalenost(new BigInteger(vzdalenost));
            linka.setLinkaKey(new BigDecimal(key));

            Response res = model.deleteLinka(linka);
            System.out.println("Zaznam odstranen!");
            setTableLinka();
        } catch (Exception e) {

            System.out.println("Nespravny format cisla pro vzdalenost!");
        }
    }

    /*
    Metoda, ktera pripravi tabulky, smaze vsechny radky
     */

    public void clearRow(DefaultTableModel model, int rowCount) {
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

    }
}
