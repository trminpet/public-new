package eu.cz.ctu.fit.bitjv.trminpet.entities;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-24T18:24:34")
@StaticMetamodel(Pilot.class)
public class Pilot_ { 

    public static volatile SingularAttribute<Pilot, BigInteger> naletaneHodiny;
    public static volatile SingularAttribute<Pilot, BigDecimal> pilotKey;
    public static volatile SingularAttribute<Pilot, String> prijmeni;
    public static volatile SingularAttribute<Pilot, String> jmeno;
    public static volatile SingularAttribute<Pilot, BigInteger> letovaTrida;

}