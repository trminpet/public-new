/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bitjv.trminpet.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author petr
 */
@Entity
@Table(name = "PILOT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pilot.findAll", query = "SELECT p FROM Pilot p"),
    @NamedQuery(name = "Pilot.findByPilotKey", query = "SELECT p FROM Pilot p WHERE p.pilotKey = :pilotKey"),
    @NamedQuery(name = "Pilot.findByJmeno", query = "SELECT p FROM Pilot p WHERE p.jmeno = :jmeno"),
    @NamedQuery(name = "Pilot.findByPrijmeni", query = "SELECT p FROM Pilot p WHERE p.prijmeni = :prijmeni"),
    @NamedQuery(name = "Pilot.findByLetovaTrida", query = "SELECT p FROM Pilot p WHERE p.letovaTrida = :letovaTrida"),
    @NamedQuery(name = "Pilot.findByNaletaneHodiny", query = "SELECT p FROM Pilot p WHERE p.naletaneHodiny = :naletaneHodiny")})
public class Pilot implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PILOT_KEY")
    private BigDecimal pilotKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "JMENO")
    private String jmeno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "PRIJMENI")
    private String prijmeni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LETOVA_TRIDA")
    private BigInteger letovaTrida;
    @Column(name = "NALETANE_HODINY")
    private BigInteger naletaneHodiny;

    public Pilot() {
    }

    public Pilot(BigDecimal pilotKey) {
        this.pilotKey = pilotKey;
    }

    public Pilot(BigDecimal pilotKey, String jmeno, String prijmeni, BigInteger letovaTrida) {
        this.pilotKey = pilotKey;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.letovaTrida = letovaTrida;
    }

    public BigDecimal getPilotKey() {
        return pilotKey;
    }

    public void setPilotKey(BigDecimal pilotKey) {
        this.pilotKey = pilotKey;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public BigInteger getLetovaTrida() {
        return letovaTrida;
    }

    public void setLetovaTrida(BigInteger letovaTrida) {
        this.letovaTrida = letovaTrida;
    }

    public BigInteger getNaletaneHodiny() {
        return naletaneHodiny;
    }

    public void setNaletaneHodiny(BigInteger naletaneHodiny) {
        this.naletaneHodiny = naletaneHodiny;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pilotKey != null ? pilotKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pilot)) {
            return false;
        }
        Pilot other = (Pilot) object;
        if ((this.pilotKey == null && other.pilotKey != null) || (this.pilotKey != null && !this.pilotKey.equals(other.pilotKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pokus.Pilot[ pilotKey=" + pilotKey + " ]";
    }
    
}
