/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bitjv.trminpet.service;

import eu.cz.ctu.fit.bitjv.trminpet.entities.Cestujici;
import eu.cz.ctu.fit.bitjv.trminpet.entities.Linka;
import eu.cz.ctu.fit.bitjv.trminpet.entities.Pilot;
import java.util.List;

/**
 * Rozhrani pro definici metod pouzitych v service
 *
 * @author petr
 */
public interface DatabaseService {

    public List<Pilot> getPilots();

    public void updatePilot(Pilot pilot);

    public void deletePilot(Pilot pilot);

    public void createPilot(Pilot pilot);

    public List<Cestujici> getCestujici();

    public void updateCestujici(Cestujici cestujici);

    public void deleteCestujici(Cestujici cestujici);

    public void createCestujici(Cestujici cestujici);

    public List<Linka> getLinka();

    public void updateLinka(Linka linka);

    public void deleteLinka(Linka linka);

    public void createLinka(Linka linka);

}
