/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bitjv.trminpet.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author petr
 */
@Entity
@Table(name = "CESTUJICI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cestujici.findAll", query = "SELECT c FROM Cestujici c"),
    @NamedQuery(name = "Cestujici.findByCestujiciKey", query = "SELECT c FROM Cestujici c WHERE c.cestujiciKey = :cestujiciKey"),
    @NamedQuery(name = "Cestujici.findByNarodnost", query = "SELECT c FROM Cestujici c WHERE c.narodnost = :narodnost"),
    @NamedQuery(name = "Cestujici.findByJmeno", query = "SELECT c FROM Cestujici c WHERE c.jmeno = :jmeno"),
    @NamedQuery(name = "Cestujici.findByPrijmeni", query = "SELECT c FROM Cestujici c WHERE c.prijmeni = :prijmeni"),
    @NamedQuery(name = "Cestujici.findByDatumNarozeni", query = "SELECT c FROM Cestujici c WHERE c.datumNarozeni = :datumNarozeni"),
    @NamedQuery(name = "Cestujici.findByZavazadla", query = "SELECT c FROM Cestujici c WHERE c.zavazadla = :zavazadla"),
    @NamedQuery(name = "Cestujici.findByCilovaDestinace", query = "SELECT c FROM Cestujici c WHERE c.cilovaDestinace = :cilovaDestinace")})

public class Cestujici implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CESTUJICI_KEY")
    private BigDecimal cestujiciKey;
    @Size(max = 3)
    @Column(name = "NARODNOST")
    private String narodnost;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "JMENO")
    private String jmeno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "PRIJMENI")
    private String prijmeni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_NAROZENI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumNarozeni;
    @Column(name = "ZAVAZADLA")
    private BigInteger zavazadla;
    @Size(max = 50)
    @Column(name = "CILOVA_DESTINACE")
    private String cilovaDestinace;

    public Cestujici() {
    }

    public Cestujici(BigDecimal cestujiciKey) {
        this.cestujiciKey = cestujiciKey;
    }

    public Cestujici(BigDecimal cestujiciKey, String jmeno, String prijmeni, Date datumNarozeni) {
        this.cestujiciKey = cestujiciKey;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.datumNarozeni = datumNarozeni;
    }

    public BigDecimal getCestujiciKey() {
        return cestujiciKey;
    }

    public void setCestujiciKey(BigDecimal cestujiciKey) {
        this.cestujiciKey = cestujiciKey;
    }

    public String getNarodnost() {
        return narodnost;
    }

    public void setNarodnost(String narodnost) {
        this.narodnost = narodnost;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public Date getDatumNarozeni() {
        return datumNarozeni;
    }

    public void setDatumNarozeni(Date datumNarozeni) {
        this.datumNarozeni = datumNarozeni;
    }

    public BigInteger getZavazadla() {
        return zavazadla;
    }

    public void setZavazadla(BigInteger zavazadla) {
        this.zavazadla = zavazadla;
    }

    public String getCilovaDestinace() {
        return cilovaDestinace;
    }

    public void setCilovaDestinace(String cilovaDestinace) {
        this.cilovaDestinace = cilovaDestinace;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cestujiciKey != null ? cestujiciKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cestujici)) {
            return false;
        }
        Cestujici other = (Cestujici) object;
        if ((this.cestujiciKey == null && other.cestujiciKey != null) || (this.cestujiciKey != null && !this.cestujiciKey.equals(other.cestujiciKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pokus.Cestujici[ cestujiciKey=" + cestujiciKey + " ]";
    }
    
}
