/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.ctu.fit.bitjv.trminpet.service;


import eu.cz.ctu.fit.bitjv.trminpet.entities.Cestujici;


import javax.persistence.EntityManager;

/**
 * Trida ktera deni metody z abstractfacade
 * @author Petr
 */
public class CestujiciFacade extends AbstractFacade<Cestujici>{

    public CestujiciFacade(Class<Cestujici> entityClass) {
        super(entityClass);
    }

   
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
    return em;
    }

    @Override
    public void setEntityManager(EntityManager em) {
    this.em=em;
    }
    
}
