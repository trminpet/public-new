/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orm;

import controlers.CustomerJpaController;
import controlers.MicroMarketJpaController;
import java.util.List;
import javax.persistence.Persistence;
import orm.dto.Customer;
import orm.dto.MicroMarket;

/**
 *
 * @author petr
 */
public class ORM {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        CustomerJpaController cjc = new CustomerJpaController(Persistence.createEntityManagerFactory("ORMPU"));
        List<Customer> lcs = cjc.findCustomerEntities();
        for(Customer c:lcs){
            System.out.println(c.toString());
        
        }
        MicroMarketJpaController mjc = new MicroMarketJpaController(Persistence.createEntityManagerFactory("ORMPU"));
        List<MicroMarket> mj = mjc.findMicroMarketEntities();
        for(MicroMarket m:mj){
            System.out.println(m.toString());
        
        } 
    }
    
}
