/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;

/**
 *
 * @author petr
 */
public class CreateConnection {
     private final String DB_DRIVER = "org.apache.derby.jdbc.ClientDriver";
   private static String DB_URL = "jdbc:derby://localhost:1527/sample;create=true;user=app;password=app";
    private  Connection connection;
 private List<CustomerDTO> ls = new ArrayList<CustomerDTO>();
    public CreateConnection() throws ClassNotFoundException, SQLException {
         Class.forName(DB_DRIVER);
         connection = DriverManager.getConnection(DB_URL);
         ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM CUSTOMER");
    
        while(rs.next()){
           CustomerDTO cs = new CustomerDTO();
           cs.setId(rs.getLong(1));
           cs.setDiscount_code(rs.getString(2));
           cs.setZip(rs.getString(3));
           cs.setName(rs.getString(4));
           cs.setAdress(rs.getString(5));
           cs.setCity(rs.getString(7));
           cs.setPhone(rs.getString(9));
           ls.add(cs);
           
        }
       // addCustomer(c, connection);
        write(ls);
        rs.close();
        
    }

      public void write(List<CustomerDTO> ls){
      
      for(CustomerDTO customer:ls){
        System.out.println(customer.toString());
    
      
      }
      }
      public void addCustomer(CustomerDTO c,Connection conn) throws SQLException{
          PreparedStatement ps = conn.prepareStatement("INSERT INTO APP.CUSTOMER (CUSTOMER_ID, DISCOUNT_CODE, ZIP, NAME, ADDRESSLINE1, ADDRESSLINE2, CITY, STATE, PHONE, FAX, EMAIL, CREDIT_LIMIT) VALUES (?, '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', ?)");
          ps.setLong(1, c.getId());
          ps.setString(2, c.getDiscount_code());
          ps.setString(3, c.getZip());
          ps.setString(4, c.getName());
          ps.setString(5, c.getAdress());
          ps.setString(6, c.getAdress());
          ps.setString(7, c.getCity());
          ps.setString(8, "LA");
          ps.setString(9, c.getPhone());
          ps.setString(10, "fax fax");
          ps.setString(11, "email");
          ps.setString(12, "100");
      }
      
      
      
      
      
    
    public Connection getConnection(){
    
    return connection;
    } 
    
        
    }
    
   
   
    
    

