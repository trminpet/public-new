#include <iostream>
#include <cassert>
#include <set>
#include <vector>
#include <map>
using namespace std;
 
#include "queue.cpp"
 


struct Integer{
    Integer(){}
    Integer(int x){m_Data=x;}
    int m_Data;
    friend ostream & operator <<(ostream & os, const Integer & q){
                   
        return os << q.m_Data;
                
                
                
                }
    bool operator < (const Integer & i)const { return m_Data > i.m_Data;}
    

};


void main1() {
	Queue<int> positive, negative;
 
	int num;
	cout << "Zadejte radu celych cisel zakoncenou nulou:" << endl;
	cin >> num;
 
	while(num)
	{
		if(num > 0)
			positive.push(num);
		else
			negative.push(num);
		cin >> num;
	}
 
	cout << "kladna cisla:" << endl;
	while (!positive.isEmpty()) {
		cout << positive.front() << ' ';
		positive.pop();
	}
	cout << endl;
 
	cout << "zaporna cisla:" << endl;
	while (!negative.isEmpty()) {
		cout << negative.front() << ' ';
		negative.pop();
	}
	cout << endl;
}
 
void main2()
{
	Queue<int> q;
 
	// Stav:
	assert(q.isEmpty() == true);
 
	for (int i = 0; i < 10; i++)
		q.push(i);
	// Stav: 0 1 2 3 4 5 6 7 8 9
 
	assert(q.isEmpty() == false);
	assert(q.front() == 0);
 
	for (int i = 0; i < 5; i++)
	{
		assert(q.front() == i);
		q.pop();
	}
	// Stav: 5 6 7 8 9
 
	assert(q.front() == 5);
 
	for (int i = 20; i < 25; i++)
		q.push(i);
	// Stav: 5 6 7 8 9 20 21 22 23 24
 
	assert(q.front() == 5);
 
 
	for(int i = 5; i <= 9; i++)
	{
		assert(q.front() == i);
		q.pop();
	}
	for(int i = 20; i <= 24; i++)
	{
		assert(q.front() == i);
		q.pop();
	}
	// Stav:
	assert(q.isEmpty() == true);
        try{
        cout << q.front() << endl;
        }
        catch(int x){
            cout << "vyjimka " << x << endl;
        }
        catch (const string &x){
         cout << "vyjimka " << x << endl;
        
        }
         catch (...){
         cout << "vyjimka "<< endl;
        
        }
        
	q.push(0);
	// Stav: 0
	assert(q.isEmpty() == false);
        
	// Schvalne nechame jeden prvek ve fronte, at vidime, ze pracuje destruktor (Spustte pres valgrind a hledejte memleaky).
}
template <class T>
void printFirst(const T & f){

   typename T::TItem * tmp = f.m_Head;
    cout << tmp->m_Val << endl;
}
int main(void)
{
    
//    Queue<Integer> q;
//    for (int i = 0; i < 10; i++) {
//        q.push(Integer(i));
//       
//
//    }
//    cout << q << endl;
//    printFirst(q);
    
//    set <int> S;
//    for (int i = 0; i < 10; i++) {
//        S.insert(i);
//
//    }
//    S.insert(5);
//    for (auto s :S) {
//        cout << s << endl;
//
//    }

//
//     set <int> S;
//    for (int i = 0; i < 10; i++) {
//       // cout << S.insert(Integer(i)).second;
//        S.insert(i);
//
//    }
//     cout << endl;
//     cout << S.count(1) << "  "<< S.count(15) << endl;
//    cout << S.insert(5).second<< endl;
//    
//    cout << (S.find(1) == S.end())<< endl;
//    
//    S.erase(2);
//    S.erase(S.find(3));
////    for (auto s :S) {
////        cout << s << endl;
////
////    }
//    
//    for(auto it = S.begin() ; it!=S.end();  ){
//        if(*it == 6){
//           it= S.erase(it);
//        }else{
//        cout << "X:" << *it << endl;
//        ++it;
//        }
//    
//    }
        
//    vector <int> v;
//    auto it = find(v.begin(),v.end(),5); //linearni pro set lepsi pouzivat ten jejich find
//    
//    
//    
//    
    
    
    
    
   map<string,int> mapa; 
    
   mapa["aa"] = 245;
   mapa["axa"] = 24;
   mapa["axq"] = 25;
   cout << mapa.find("aa")->second << endl;
   cout << mapa.count("axa") << endl;
   for (const auto & x : mapa) {
       cout << x.first << " " << x.second << endl;
    }

    
    
    
    
    
    
    

    
    
    
	//main1();
	//cout << "------------------------" << endl;
//	main2();
}