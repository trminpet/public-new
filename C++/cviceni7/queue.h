#ifndef QUEUE_H_
#define QUEUE_H_
#include <iostream>
 

template <class value_type>
class Queue
{
	public:
		Queue(void);
		~Queue(void);
		bool isEmpty(void) const;
		void push(const value_type & val);
		void pop(void);
		value_type front(void) const;
                friend std::ostream & operator <<(std::ostream & os, const Queue & q){
                    os<<"Fronta:";
                    TItem * tmp = q.m_Head;
                    while(tmp){
                        os <<" "<< tmp->m_Val;
                        tmp=tmp->m_Next;
                    
                    }
                    return os;
                
                
                
                }
 
	//private:
		struct TItem
		{
			TItem(const value_type & val);
			value_type m_Val;
			TItem *m_Next;
		};
 
		int m_Size;
		TItem *m_Head, *m_Tail;
};
 
#endif /* QUEUE_H_ */
