#include "queue.h"
#include <cstdlib>
#include <string>
 
/* 
 * I struktura muze mit konstruktor. Zmena oproti cviku, kdy jsme vytvorili strukturu a pak ji nainicializovali rucne.
 * Plne jmeno Queue::TItem::TItem, protoze trida TItem je definovana uvnitr tridy Queue.
 */
template <class value_type>
Queue<value_type>::TItem::TItem(const value_type & val):m_Val(val)
{
	//m_Val = val;
	m_Next = NULL;
}
 
 template <class value_type>
Queue<value_type>::Queue(void)
{
	m_Size = 0;
	m_Head = m_Tail = NULL;
}
 
/*
 * Samozrejme nesmime zapomenout na destruktor. Trida si po sobe uklidi.
 *
 * (pozn.: protoze implementujeme destruktor, pravdepodobne budeme potrebovat naimplementovat i copy-constructor a operator=, aby fungovaly spravne operace prirazovani jednoho objektu do druheho. To ukazeme pozdeji, jedna se o tzv pravidlo "Rule of three".)
 * */
 template <class value_type>
Queue<value_type>::~Queue(void)
{
	TItem* toDel;
 
	while(m_Head)
	{
		toDel = m_Head;
		m_Head = m_Head->m_Next;
		delete toDel;
	}
}
 template <class value_type>
bool Queue<value_type>::isEmpty(void) const
{
	return m_Size == 0;
}
 template <class value_type>
void Queue<value_type>::push(const value_type & val)
{
	/*
	 * Pridani prvku - 2 pripady:
	 * 1. Spojak je prazdny. V tom pripade musime nastavit jak m_Head, tak m_Tail aby ukazovaly na novy prvek
	 * 2. Uz tam neco je. V tom pripade pridame na konec a ten posuneme na novy vlozeny prvek
	 */
 
	if(m_Tail == NULL)
	{
		m_Head = m_Tail = new TItem(val); // Zmena oproti cviku: Ukazka, ze samozrejme i struktura muze mit konstruktor
	}
	else
	{
		m_Tail->m_Next = new TItem(val);
		m_Tail = m_Tail->m_Next;
	}
 
	/* Kdyby TItem melo pouze defaultni konstruktor, pak (to jsem ukazoval na cviku): */
	/*
	m_Tail->m_Next = NULL;
	m_Tail->m_Val = val;
	*/
 
	m_Size += 1;
}
 template <class value_type>
void Queue<value_type>::pop(void)
{
	/*
	 * Odstraneni prvniho prvku ze spojaku
	 *  - zapamatujeme si prvni prvek, budeme ho chtit za chvili smazat.
	 *  - posuneme head ukazatel na nasledujici prvek (nove tedy bude ukazovat bud na druhy prvek, nebo na NULL)
	 *  - a smazeme byvaly prvni prvek, na ten ukazujeme pomoci toDel
	 *
	 *  Pokud jsme smazali vse, nastavime m_Tail taky na NULL, at nam to neukazuje nekam do pryc
	 */
	TItem* toDel = m_Head;
	m_Head = m_Head -> m_Next;
	delete toDel;
 
	if(m_Head == NULL)
		m_Tail = NULL;
 
	m_Size -= 1;
}
 template <class value_type>
value_type Queue<value_type>::front(void) const
{
    if(!m_Head)
        throw /*EmptyClassException(); std::string(*/"Prazdna fronta";
	return m_Head->m_Val;
}