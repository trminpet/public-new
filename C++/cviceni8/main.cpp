/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: petr
 *
 * Created on April 11, 2017, 4:28 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
class Zamestanec {
public:

    Zamestanec(const string & po, const string & jm, int p) : m_Popis(po), m_Jmeno(jm), m_Plat(p) {
    };

    string getJmeno()const {
        return m_Jmeno;
    };

    virtual string getPopis()const {
        return m_Popis;
    };

    virtual int getPlat()const {
        return m_Plat;
    };
   virtual void print(ostream & os) {
        os << getJmeno() << " " << getPopis() << " " << getPlat();

    }
    friend ostream & operator<<(ostream & os, const Zamestanec & zam) {
        zam.print(os);
        return os;

    }

   
private:
protected:
    string m_Popis;
    string m_Jmeno;
    int m_Plat;
};

class Reditel : public Zamestanec {
public:

    Reditel(const string & po, const string & jm, int p, const string & ben) : Zamestanec(po, jm, p), m_Benefity(ben) {
    };

    string getBenefity()const {
        return m_Benefity;
    };

    string getPopis()const {
        return m_Popis + "(reditel)";
    };

    int getPlat()const {
        return 3 * m_Plat;
    };

    friend ostream & operator<<(ostream & os, const Reditel & zam) {
        zam.print(os);
        return os;

    }

    virtual void print(ostream & os) const  {
        os << getJmeno() << " " << getPopis() << " " << getPlat() << " " << m_Benefity;

    }
private:
protected:
    string m_Benefity;

};

int main(int argc, char** argv) {
    Zamestanec z("udrzbar", "Jerry", 16000);
    Reditel r("reditel", "Tom", 124000, "auto");
    //    cout << z.getJmeno() << " " << z.getPopis() << " " << z.getPlat() << endl;
    //    cout << r.getJmeno() << " " << r.getPopis() << " " << r.getPlat() << " " <<  r.getBenefity() << endl;
    //    z = r;
    //     cout << z.getJmeno() << " " << z.getPopis() << " " << z.getPlat() << endl;
    cout << z << endl;
    cout << r << endl;

    Zamestanec * pole [5];
    pole[0] = new Zamestanec("p0", "j0", 0);
    pole[1] = new Zamestanec("p1", "j1", 1000);
    pole[2] = new Zamestanec("p2", "j2", 2000);
    pole[3] = new Reditel("p3", "j3", 3000, "auto");
    pole[4] = new Reditel("p4", "j4", 4000, "motorko");


    for (int i = 0; i < 5; i++) {
        cout << *pole[i] << endl;

    }

    return 0;
}

