#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
using namespace std;

class CDate {
public:
    //---------------------------------------------------------------------------------------------

    CDate(int y,
            int m,
            int d)
    : m_Year(y),
    m_Month(m),
    m_Day(d) {
    }
    //---------------------------------------------------------------------------------------------

    int Compare(const CDate & x) const {
        if (m_Year != x . m_Year)
            return m_Year - x . m_Year;
        if (m_Month != x . m_Month)
            return m_Month - x . m_Month;
        return m_Day - x . m_Day;
    }
    //---------------------------------------------------------------------------------------------

    int Year(void) const {
        return m_Year;
    }
    //---------------------------------------------------------------------------------------------

    int Month(void) const {
        return m_Month;
    }
    //---------------------------------------------------------------------------------------------

    int Day(void) const {
        return m_Day;
    }
    //---------------------------------------------------------------------------------------------

    friend ostream & operator<<(ostream & os,
            const CDate & x) {
        char oldFill = os . fill();
        return os << setfill('0') << setw(4) << x . m_Year << "-"
                << setw(2) << (int) x . m_Month << "-"
                << setw(2) << (int) x . m_Day
                << setfill(oldFill);
    }
    //---------------------------------------------------------------------------------------------
private:
    int16_t m_Year;
    int8_t m_Month;
    int8_t m_Day;
};
#endif /* __PROGTEST__ */
unsigned int ID_INSERT = 1;

string toLower(const string & tmp) {
    string a = tmp;
    transform(a.begin(), a.end(), a.begin(), ::tolower);
    return a;
}

//Prevzata metoda na odstraneni white space z proseminare c.8 PA1

string removeWhiteSpace(string str) {
    string src, dest = "";
    unsigned int pos = 0, posFixed = 0;
    bool inSpaces;
    unsigned int end = str.length();
    inSpaces = true;
    src = str;
    while (pos < end) {
        if (inSpaces) {

            if (src[pos] != ' ') {
                dest += src[pos];
                inSpaces = false;
                posFixed++;
            }

        } else {

            dest += src[pos];
            posFixed++;
            if (src[pos] == ' ')
                inSpaces = true;
        }
        pos++;
    }


    if (inSpaces && dest.length() < str.length()) {
        posFixed--;
    }
    dest = dest.substr(0, posFixed);
    if (dest[dest.length() - 1] == ' ')
        dest = dest.substr(0, posFixed - 1);


    return dest;
}

class CInvoice {
public:

    CInvoice(const CDate & date,
            const string & seller,
            const string & buyer,
            unsigned int amount,
            double VAT) : date(date) {

        this->seller = seller;
        this->buyer = buyer;
        this->amount = amount;
        this->vat = VAT;


    };

    CDate Date(void) const {
        return date;
    };

    string Buyer(void) const {
        return buyer;
    }

    string Seller(void) const {
        return seller;
    }

    int Amount(void) const {
        return amount;
    };

    double VAT(void) const {
        return vat;
    };

    void setRealSeller(const string & tmp) {
        this->seller = tmp;
    };

    void setRealBuyer(const string & tmp) {
        this->buyer = tmp;
    };
    unsigned int ID;
    //pretizeni operatu < pro insert/find

    bool operator<(const CInvoice &tmp) const {
        if (date.Year() == tmp.date.Year()) {
            if (date.Month() == tmp.date.Month()) {
                if (date.Day() == tmp.date.Day()) {

                    if (removeWhiteSpace(toLower(buyer)) == removeWhiteSpace(toLower(tmp.buyer))) {
                        if (removeWhiteSpace(toLower(seller)) == removeWhiteSpace(toLower(tmp.seller))) {
                            if (amount == tmp.amount) {
                                if (vat == tmp.vat) {
                                    return false;
                                }
                                return vat < tmp.vat;
                            }
                            return amount < tmp.amount;
                        }
                        return removeWhiteSpace(toLower(seller)) < removeWhiteSpace(toLower(tmp.seller));
                    }
                    return removeWhiteSpace(toLower(buyer)) < removeWhiteSpace(toLower(tmp.buyer));
                }
                return date.Day() < tmp.date.Day();
            }
            return date.Month() < tmp.date.Month();
        }
        return date.Year() < tmp.date.Year();
    }
private:
    CDate date;
    string buyer;
    string seller;
    int amount;
    double vat;

};
//Trida pro uchovani originalnich informaci

class CCompany {
public:

    CCompany(const string & n, const string & alt) : name(n), alterName(alt) {
    }

    CCompany(const string &alt) : alterName(alt) {
        name = "";
    }

    bool operator<(const CCompany &tmp) const {
        return alterName < tmp.alterName;
    }



    string name, alterName;







};

class CSortOpt {
public:
    static const int BY_DATE = 0;
    static const int BY_BUYER = 1;
    static const int BY_SELLER = 2;
    static const int BY_AMOUNT = 3;
    static const int BY_VAT = 4;

    CSortOpt(void) {
    };

    CSortOpt & AddKey(int key,
            bool ascending = true) {

        inputOrder.push_back(std::make_pair(key, ascending));

        return *this;


    }



    vector<pair<int, bool> > inputOrder;
private:
    bool order;
    int sortBy;


};

bool compare_by_date(const CInvoice& a, const CInvoice& b, bool order) {
    if (a.Date().Year() == b.Date().Year()) {
        if (a.Date().Month() == b.Date().Month()) {
            if (a.Date().Day() == b.Date().Day()) {
                return false;
            }
            return !(a.Date().Day() < b.Date().Day())^order;
        }
        return !(a.Date().Month() < b.Date().Month())^order;
    }
    return !(a.Date().Year() < b.Date().Year())^order;

}

bool compare_by_buyer(const CInvoice& a, const CInvoice& b, bool order) {
    if (removeWhiteSpace(toLower(a.Buyer())) == removeWhiteSpace(toLower(b.Buyer())))
        return a.ID < b.ID;
    return !(toLower(a.Buyer()) < toLower(b.Buyer()))^order;

}

bool compare_by_seller(const CInvoice& a, const CInvoice& b, bool order) {
    if (removeWhiteSpace(toLower(a.Seller())) == removeWhiteSpace(toLower(b.Seller())))
        return a.ID < b.ID;
    return !(toLower(a.Seller()) < toLower(b.Seller()))^order;

}

bool compare_by_amount(const CInvoice& a, const CInvoice& b, bool order) {
    if (a.Amount() == b.Amount())
        return a.ID < b.ID;
    return !(a.Amount() < b.Amount())^order;

}

bool compare_by_vat(const CInvoice& a, const CInvoice& b, bool order) {
    if (a.VAT() == b.VAT())
        return a.ID < b.ID;
    return !(a.VAT() < b.VAT())^order;

}

class CVATRegister {
public:

    CVATRegister(void) {

    };

    bool RegisterCompany(const string & name) {
        string alterName = removeWhiteSpace(toLower(name));

        if (companies.size() == 0) {
            companies.insert(CCompany(name, alterName));

        } else {
            if (!companies.insert(CCompany(name, alterName)).second)
                return false;

        }
        return true;
    };
    //pridani addIssued

    bool AddIssued(const CInvoice & x) {
        //kontrola zda firmy jsou validni
        if (removeWhiteSpace(toLower(x.Buyer())) == removeWhiteSpace(toLower(x.Seller()))) return false;
        set<CCompany>::iterator iterSeller;
        set<CCompany>::iterator iterBuyer;
        string sell = removeWhiteSpace(toLower(x.Seller()));
        string buy = removeWhiteSpace(toLower(x.Buyer()));

        iterSeller = companies.find(CCompany(sell));
        iterBuyer = companies.find(CCompany(buy));
        map<CInvoice, bool>::iterator iterFind;
        if (iterBuyer == companies.end()) return false;
        if (iterSeller == companies.end()) return false;
        //vlozeni prvku do datovych struktur
        if (released.size() == 0) {
            CInvoice tmp = x;
            //pridani ID a nastaveni spravneho jmena
            tmp.ID = ID_INSERT++;
            tmp.setRealBuyer(iterBuyer->name);
            tmp.setRealSeller(iterSeller->name);
            released.insert(CInvoice(x.Date(), iterSeller->name, iterBuyer->name, x.Amount(), x.VAT()));
            iterFind = pairing.find(tmp);
            if (iterFind == pairing.end()) {
                pairing.insert(std::pair<CInvoice, bool>(tmp, false));
            } else {
                  if(iterFind->second==false)
                iterFind->second = true;

            }
        } else {
            //cinvoice jiz existuje 
            if (!released.insert(x).second) {

                return false;
            }
            //pridani zaznamu
            CInvoice tmp = x;
            tmp.ID = ID_INSERT++;
            tmp.setRealBuyer(iterBuyer->name);
            tmp.setRealSeller(iterSeller->name);

            iterFind = pairing.find(tmp);
            if (iterFind == pairing.end()) {
                pairing.insert(std::pair<CInvoice, bool>(tmp, false));
            } else {
                //sparovani addIssued a addAccepted
                 if(iterFind->second==false)
                iterFind->second = true;

            }

        }

        return true;

    };

    bool AddAccepted(const CInvoice & x) {
        //kontrola existence firem
        if (removeWhiteSpace(toLower(x.Buyer())) == removeWhiteSpace(toLower(x.Seller()))) return false;
        set<CCompany>::iterator iterSeller;
        set<CCompany>::iterator iterBuyer;
        map<CInvoice, bool>::iterator iterFind;
        string sell = removeWhiteSpace(toLower(x.Seller()));
        string buy = removeWhiteSpace(toLower(x.Buyer()));
        //cout << sell << " " << buy <<endl;
        iterSeller = companies.find(CCompany(sell));
        iterBuyer = companies.find(CCompany(buy));
        if (iterBuyer == companies.end()) return false;
        if (iterSeller == companies.end()) return false;
        //vlozeni noveho prvku
        if (received.size() == 0) {
            received.insert(CInvoice(x.Date(), iterSeller->name, iterBuyer->name, x.Amount(), x.VAT()));
            CInvoice tmp = x;
            //pridani ID
            tmp.ID = ID_INSERT++;
            tmp.setRealBuyer(iterBuyer->name);
            tmp.setRealSeller(iterSeller->name);

            iterFind = pairing.find(tmp);

            if (iterFind == pairing.end()) {
                pairing.insert(std::pair<CInvoice, bool>(tmp, false));
            } else { //sparovani addAccepted a addIssued
                 if(iterFind->second==false)
                iterFind->second = true;

            }

        } else {
            if (!received.insert(x).second) {

                return false;
            }
            CInvoice tmp = x;
            tmp.ID = ID_INSERT++;
            tmp.setRealBuyer(iterBuyer->name);
            tmp.setRealSeller(iterSeller->name);
            iterFind = pairing.find(tmp);
            if (iterFind == pairing.end()) {
                pairing.insert(std::pair<CInvoice, bool>(tmp, false));
            } else {
                //sparovani addAccepted a addIssued
                 if(iterFind->second==false)
                iterFind->second = true;

            }
        }

        return true;

    };
    //Odstraneni z registru

    bool DelIssued(const CInvoice & x) {

        if (released.size() == 0) {
            return false;
        } else {
            //nalezeni faktury
            set<CInvoice>::iterator del;
            del = released.find(CInvoice(x.Date(), removeWhiteSpace(toLower(x.Seller())), removeWhiteSpace(toLower(x.Buyer())), x.Amount(), x.VAT()));
            if (del != released.end()) {
                released.erase(del);
                //nalezeni faktury v pairing a zmena parametru
                map<CInvoice, bool>::iterator it;
                it = pairing.find((CInvoice(x.Date(), removeWhiteSpace(toLower(x.Seller())), removeWhiteSpace(toLower(x.Buyer())), x.Amount(), x.VAT())));
                 it->second=!it->second;
            } else
                return false;
        }
        return true;

    }
    //odstraneni faktury

    bool DelAccepted(const CInvoice & x) {

        if (received.size() == 0) {
            return false;
        } else {
            //nalezeni faktury v registru
            set<CInvoice>::iterator del;
            del = received.find(CInvoice(x.Date(), removeWhiteSpace(toLower(x.Seller())), removeWhiteSpace(toLower(x.Buyer())), x.Amount(), x.VAT()));
            if (del != received.end()) {

                received.erase(del);
                //nalezeni faktury v pairing a zmena parametru
                map<CInvoice, bool>::iterator it;
                it = pairing.find((CInvoice(x.Date(), removeWhiteSpace(toLower(x.Seller())), removeWhiteSpace(toLower(x.Buyer())), x.Amount(), x.VAT())));
               
                    it->second=!it->second;
                
            } else
                return false;
        }
        return true;

    };
    //metoda na hledani unmatched faktur

    list<CInvoice> Unmatched(const string & company,
            const CSortOpt & sortBy) const {


        //vyhledani faktur ktera nejsou sparovane
        list<CInvoice> c;
         map<CInvoice, bool>::const_iterator it = pairing.begin();
        for(; it != pairing.end() ; ++it) {
            if (!(it->second) && ((removeWhiteSpace(toLower(company)) == removeWhiteSpace(toLower((it->first.Buyer())))) || (removeWhiteSpace(toLower(company)) == removeWhiteSpace(toLower((it->first.Seller())))))) {
                // cout <<f.first.Date()<< " " <<  f.first.Seller() << " " << f.first.Buyer() << " "<< f.first.Amount()<< " " << f.first.VAT()<<endl;
                c.push_back(it->first);

            }
        }
        //defalutni razeni
        if (sortBy.inputOrder.empty() || sortBy.inputOrder.size() == 0) {
            c.sort([](const CInvoice & a, const CInvoice & b) {
                return a.ID < b.ID; });

            return c;
        }




        //razeni dle kriterie a pomoci lambda funkce
        c.sort([&sortBy](const CInvoice & a, const CInvoice & b) {
            
            //pole priorit
            for(int pos = 0 ; pos < (int)sortBy.inputOrder.size();pos++) {

              
                switch (sortBy.inputOrder[pos].first) {

                    //razeni dle amount
                    case sortBy.BY_AMOUNT: if (a.Amount() == b.Amount()) {

                            break;
                        }

                        return compare_by_amount(a, b, sortBy.inputOrder[pos].second);
                   //razeni dle buyer
                    case sortBy.BY_BUYER:
                        if (removeWhiteSpace(toLower(a.Buyer())) == removeWhiteSpace(toLower(b.Buyer()))) {

                            break;
                        }
                        return compare_by_buyer(a, b, sortBy.inputOrder[pos].second);
                    //razeni dle date
                    case sortBy.BY_DATE: if (a.Date().Year() == b.Date().Year()) {
                            if (a.Date().Month() == b.Date().Month()) {
                                if (a.Date().Day() == b.Date().Day()) {
                                    break;

                                }


                                return (bool) !((a.Date().Day() < b.Date().Day()) ^ sortBy.inputOrder[pos].second);
                            }
                            return (bool) !((a.Date().Month() < b.Date().Month()) ^ sortBy.inputOrder[pos].second);
                        }
                        return (bool) !((a.Date().Year() < b.Date().Year()) ^ sortBy.inputOrder[pos].second);
                   //razeni dle seller
                    case sortBy.BY_SELLER: if (removeWhiteSpace(toLower(a.Seller())) == removeWhiteSpace(toLower(b.Seller()))) {

                            break;
                        }
                        return compare_by_seller(a, b, sortBy.inputOrder[pos].second);
                     //razeni dle VAT
                    case sortBy.BY_VAT:
                        if (a.VAT() == b.VAT()) {

                            break;
                        }

                        return compare_by_vat(a, b, sortBy.inputOrder[pos].second);

                    default : return a.ID < b.ID; break;
                }
               


            }
            //serazeni dle ID
            return a.ID < b.ID;

        });
        return c;


    };

  

    void print() {
        set<CCompany>::iterator iter;
        for (iter = companies.begin(); iter != companies.end(); ++iter) {
            cout << iter->alterName << "x" << endl;
        }




    };

    void printReleased() {
       
       map<CInvoice, bool>::iterator it;
       for(it = pairing.begin(); it != pairing.end() ; ++it)
       {
           cout << it->first.Seller() << " " << it->first.Buyer() << " " << it->first.Amount() <<  " " << it->first.VAT() << " " << it->second << endl;
       
       
       }

    };
    //private:
    set<CCompany> companies;
    set<CInvoice> released;
    set<CInvoice> received;
    map<CInvoice, bool> pairing;


};

#ifndef __PROGTEST__

bool equalLists(const list<CInvoice> & a, const list<CInvoice> & b) {

    //    cout << a.size() << " " << b.size() << endl;
      if (a.size() != b.size()) return false;
    std::list<CInvoice>::const_iterator ita = a.begin();
    std::list<CInvoice>::const_iterator itb = b.begin();

    for (; ita != a.end(); ++ita, ++itb) {
         //cout << "S: " << ita->Date() << " " << ita->Seller() << " " << ita->Buyer() << " " << ita->Amount() << " " << ita->VAT() << endl;
        //  cout << "R: " << itb->Date() << " " << itb->Seller() << " " << itb->Buyer() << " " << itb->Amount() << " " << itb->VAT() << endl;
        if (ita->Amount() != itb->Amount() || ita->Buyer() != itb->Buyer() || ita->Seller() != itb->Seller() || ita->Date().Day() != itb->Date().Day() ||
                ita->Date().Month() != itb->Date().Month() || ita->Date().Year() != itb->Date().Year() || ita->VAT() != itb->VAT())return false ;

    }

    return true;
}

int main(void) {
 CVATRegister r;
    assert(r . RegisterCompany("first Company"));
    assert(r . RegisterCompany(""));
    return 0;
    assert(r . RegisterCompany("Second     Company"));
    assert(r . RegisterCompany("ThirdCompany, Ltd."));
    assert(r . RegisterCompany("Third Company, Ltd."));
    assert(!r . RegisterCompany("Third Company, Ltd."));
    assert(!r . RegisterCompany(" Third  Company,  Ltd.  "));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 100, 20)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 2), "FirSt Company", "Second Company ", 200, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 100, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 300, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "Second Company ", "First Company", 300, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "Third  Company,  Ltd.", "  Second    COMPANY ", 400, 34)));
    assert(!r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 300, 30)));
    assert(!r . AddIssued(CInvoice(CDate(2000, 1, 4), "First Company", "First   Company", 200, 30)));
    assert(!r . AddIssued(CInvoice(CDate(2000, 1, 4), "Another Company", "First   Company", 200, 30)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, false) . AddKey(CSortOpt::BY_DATE, false)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
    }));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_DATE, true) . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000)
    }));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_VAT, true) . AddKey(CSortOpt::BY_AMOUNT, true) . AddKey(CSortOpt::BY_DATE, true) . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
    }));
    assert(equalLists(r . Unmatched("First Company", CSortOpt()),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
    }));
    assert(equalLists(r . Unmatched("second company", CSortOpt() . AddKey(CSortOpt::BY_DATE, false)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Third Company, Ltd.", "Second     Company", 400, 34.000000)
    }));
    assert(equalLists(r . Unmatched("last company", CSortOpt() . AddKey(CSortOpt::BY_VAT, true)),
            list<CInvoice>{
    }));
    assert(r . AddAccepted(CInvoice(CDate(2000, 1, 2), "First Company", "Second Company ", 200, 30)));
    assert(r . AddAccepted(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
    assert(r . AddAccepted(CInvoice(CDate(2000, 1, 1), "Second company ", "First Company", 300, 32)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true) . AddKey(CSortOpt::BY_DATE, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
    }));
    assert(!r . DelIssued(CInvoice(CDate(2001, 1, 1), "First Company", "Second Company ", 200, 30)));
    assert(!r . DelIssued(CInvoice(CDate(2000, 1, 1), "A First Company", "Second Company ", 200, 30)));
    assert(!r . DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Hand", 200, 30)));
    assert(!r . DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company", 1200, 30)));
    assert(!r . DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company", 200, 130)));
    assert(r . DelIssued(CInvoice(CDate(2000, 1, 2), "First Company", "Second Company", 200, 30)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true) . AddKey(CSortOpt::BY_DATE, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
    }));
    assert(r . DelAccepted(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true) . AddKey(CSortOpt::BY_DATE, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
    }));
    assert(r . DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true) . AddKey(CSortOpt::BY_DATE, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
    }));
    return 0;
    
}
#endif /* __PROGTEST__ */