/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: petr
 *
 * Created on April 25, 2017, 4:23 PM
 */

#include <cstdlib>
#include <iostream>
#include <memory>
#include <map>
#include <set>
#include <queue>
#include <list>
#include <tuple>

using namespace std;

/*
 * 
 */

ostream & operator << (ostream & os, const list <string> & l){
    for (auto & p : l) {
        os << p << ", ";
    }
    return os;


}

class NoRouteException{};

class CTrain {
public:
    //CTrain();
    //~CTrain();

    CTrain& Add(const std::string& c1, const std::string& c2) {

        m_Map[c1].insert(c2);
        m_Map[c2].insert(c1);
        return *this;


    }

    CTrain& Del(const std::string& c1, const std::string& c2) {

        m_Map[c1].erase(c2);
        m_Map[c2].erase(c1);
        return *this;
    }

    /* std::list<std::string> */
    list<string> Route(const std::string& from, const std::string& to) const {

        queue < pair <string,int > >  q;
        set <string> visited;
        map<string, string> pred; //kam odkud
        
        if(!m_Map.count(from) || !m_Map.count(to)) throw NoRouteException();

        q.push(make_pair(from,0));
        visited.insert(from);
        while (!q.empty()) {

            string current = q.front().first;
            int distance = q.front().second;
            cout << distance << "|" << current << endl;
            if(current == to){
            //rekonstrukce cesty
                list <string> c ;
                c.push_back(current);
                while(current!=from){
                  string s =   pred.at(current);
                  c.push_front(s);
                  current = s;
                
                }
                
    
                return c;
            }
            q.pop();

            for (const string & it : m_Map.at(current)) {
               
                if(!visited.count(it)){
                q.push(make_pair(it,distance+1));
                visited.insert(it);
                pred.insert(make_pair(it,current));
                }
            }


        }
        throw NoRouteException();

    }
    

private:
    map<string, set < string > > m_Map;
};

void FillRoutes(CTrain& c1, const char * pole[], int len) {
    for (int i = 1; i < len; ++i) {
        c1.Add(pole[i - 1], pole[i]);
        c1.Add(pole[i], pole[i - 1]);
    }
}

set<int> foo(){

    return {1,5,7};

}
int main() {
    
    std::tuple <int,string,double,string> t;
    std::get<0>(t) = 1;
    get<1>(t) = "aaa";
    get<2>(t) = 1.123;
    get<3>(t) = "avzx";
    
    //inicializer list
    std::set<int> sa{1,4,7,50};
    std::set<int> s=foo();
    
    for (auto x : s) {
        cout << s<<endl;
    }

    //Pretizeni 
    &Ctrain operator=(const Ctrain & x) =delete; nelze vytvaret = nepujde zkompilovat
    
    return 9;
    
    
    
    
    
    
    
    
    
    
    
    
    
    const char* r14[] = {"Pardubice", "Hradec Kralove", "Stara Paka", "Semily", "Zelezny Brod", "Turnov", "Liberec"};
    const char* r15[] = {"Usti nad Labem", "Decin", "Benesov n. Pl.", "Ceska Lipa", "Mimon", "Liberec"};
    const char* r21[] = {"Praha", "Neratovice", "Vsetaty", "Mlada Boleslav", "Bakov nad Jizerou", "Mnichovo Hradiste", "Turnov", "Zelezny Brod", "Tanvald"};
    const char* r22[] = {"Kolin", "Podebrady", "Nymburk", "Mlada Boleslav", "Bakov nad Jizerou", "Bela pod Bezdezem", "Doksy", "Stare Splavy", "Ceska Lipa", "Novy Bor", "Rumburk"};
    const char* r23[] = {"Kolin", "Lysa nad Labem", "Stara Boleslav", "Vsetaty", "Melnik", "Steti", "Litomerice", "Usti nad Labem"};

    CTrain t1;

    FillRoutes(t1, r14, sizeof (r14) / sizeof (*r14));
    FillRoutes(t1, r15, sizeof (r15) / sizeof (*r15));
    FillRoutes(t1, r21, sizeof (r21) / sizeof (*r21));
    FillRoutes(t1, r22, sizeof (r22) / sizeof (*r22));
    FillRoutes(t1, r23, sizeof (r23) / sizeof (*r23));

    //t1.Del("Mlada Boleslav", "Bakov nad Jizerou");


    std::cout << t1.Route("Ceska Lipa", "Mlada Boleslav") << std::endl;
    std::cout << t1.Route("Praha", "Mlada Boleslav") << std::endl;
    std::cout << t1.Route("Praha", "Ceska Lipa") << std::endl;

    std::cout << t1.Route("Praha", "Liberec") << std::endl;
    std::cout << t1.Route("Praha", "Praha") << std::endl;
    try{
    std::cout << t1.Route("Brno", "Brno") << std::endl;
    }
    catch(const NoRouteException &){
        cout << "Neexistuje cesta";
    }
    return 0;
}