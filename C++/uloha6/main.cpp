#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <list>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <stack>
#include <queue>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <functional>
using namespace std;
#endif /* __PROGTEST__ */

class OrderingDoesNotExistException {
};

class DuplicateMatchException {
};
//Funkce na sort podle druhe hodnoty v pair
bool mySort(const pair<string, int> &a, const pair<string, int> &b) {
    return a.second < b.second;
}

/*Prevzeti tridy Graph a nasledna uprava a celeho algoritmu BFS z http://www.geeksforgeeks.org/breadth-first-traversal-for-a-graph/
*  Petr Trminek 21.4.2017
 */
//Trida pro graf
class CRoaster {
   
public:
    // Constructor
    CRoaster(int c){
    this->count = c;
    results = new list<int>[c];
    } 
    //pridani zapasu
    void addResult(int a, int b){
    results[a].push_back(b); 
    } 
    //BFS algo
    int BFS(int s){
    
    
    int res = 0;
    bool *visited = new bool[count];
    for (int i = 0; i < count; i++)
        visited[i] = false;

    // Vytvoreni fronty pro BFS
    list<int> queue;

    // Oznaceni nastivenych uzlu
    visited[s] = true;

    queue.push_back(s);

   
    list<int>::iterator i;

    while (!queue.empty()) {
        
        s = queue.front();
        
        queue.pop_front();
        res++;

        //Prochazeni matice pomoci BFS
        for (i = results[s].begin(); i != results[s].end(); ++i) {
            if (!visited[*i]) {
                visited[*i] = true;
                queue.push_back(*i);
            }
        }
    }
    delete [] visited;
    return res;
    
    }

    ~CRoaster() {

        delete []results;
    }
private:
     int count; //pocet ucastniku
    list<int> *results; // zapasy ucastniku
};


template <typename _M>
class CContest {
public:
    // default constructor

    CContest() {
        index = 0;
    }
    // destructor

    ~CContest() {
    }

    // AddMatch ( contestant1, contestant2, result )

    CContest & AddMatch(const string & a, const string &b, _M result) {
        if (matches.size() == 0) {
            //pridani zapasu
            matches.insert(Match(a, b, result));
            //ulozeni jmena a jeho ciselne hodnoty pro BFS
            names.insert(std::pair<string, int>(a, index));
            index++;
            names.insert(std::pair<string, int>(b, index));

        } else if (matches.size() >=1) {
            auto iterA = matches.begin();
            auto iterB = matches.begin();
            //kontrola zda zapas jiz nebyl zadan v opacnem poradi
            iterA = matches.find(Match(a, b, result));
            iterB = matches.find(Match(b, a, result)); 
            if(iterA!=matches.end() || iterB!=matches.end())
                throw DuplicateMatchException();
            
            //vlozeni zapasu a nasledna kontrola pro vlozeni jmena
            matches.insert(Match(a, b, result)).second;
            auto iter = names.begin();
            iter = names.find(a);
            if (iter == names.end()) {
                index++;
                names.insert(std::pair<string, int>(a, index));
            }
            iter = names.find(b);
            if (iter == names.end()) {
                index++;
                names.insert(std::pair<string, int>(b, index));
            }



        } else {

            throw DuplicateMatchException();
        }



        return *this;
    }
    // IsOrdered ( comparator )

    template <typename _FN>
    bool IsOrdered(const _FN & comparator) const {

        if (names.size() == 0) return false;

        CRoaster g(names.size());

        vector<pair<string, int > > array;

     //vlozeni dat do CRoaster(graf pro bfs)
        int res = 0;
        auto itA = names.begin();
        auto itB = names.begin();

        for (auto iter = matches.begin(); iter != matches.end(); iter++) {
            itA = names.find(iter->m_contA);
            itB = names.find(iter->m_contB);
            
            if (itA != names.end() && itB != names.end()) {
                res = comparator(iter->m_result);
               
                //A porazil B
                if (res >= 1) g.addResult(itA->second, itB->second);
                //B porazil A
                else if (res <= -1) g.addResult(itB->second, itA->second);
                //Remiza
                else {
                    g.addResult(itA->second, itB->second);
                    g.addResult(itB->second, itA->second);
                }
               
        }

    }


    //ulozeni vysledku BFS
    for (auto iter = names.begin(); iter != names.end(); iter++) {
        //  cout << "Index:" << iter->second << " ";
        array.push_back(std::make_pair(iter->first, g.BFS(iter->second)));
        // cout << " size:" << size << endl;


    }
    //urceni zda splnuje BFS zadane podminky
    sort(array.begin(), array.end(), mySort);
    for (int i = 0; i < (int) array.size(); i++) {
        if (array[i].second != i + 1)
            return false;

    }


    return true;

}
// Results ( comparator )

template <typename _FN>
list<string>Results(const _FN & comparator) const {

    list<string> c;
    if (names.size() == 0) return c;

    CRoaster g(names.size());
    vector<pair<string, int > > array;



  //vlozeni dat do CRoaster(graf pro bfs)
        int res = 0;
        auto itA = names.begin();
        auto itB = names.begin();

        for (auto iter = matches.begin(); iter != matches.end(); iter++) {
            itA = names.find(iter->m_contA);
            itB = names.find(iter->m_contB);
            
            if (itA != names.end() && itB != names.end()) {
                res = comparator(iter->m_result);
               
                //A porazil B
                if (res >= 1) g.addResult(itA->second, itB->second);
                //B porazil A
                else if (res <= -1) g.addResult(itB->second, itA->second);
                //Remiza
                else {
                    g.addResult(itA->second, itB->second);
                    g.addResult(itB->second, itA->second);
                }
               
        }

    }


    //ulozeni vysledku BFS
    for (auto iter = names.begin(); iter != names.end(); iter++) {
        //  cout << "Index:" << iter->second << " ";
        array.push_back(std::make_pair(iter->first, g.BFS(iter->second)));
        // cout << " size:" << size << endl;


    }
    sort(array.begin(), array.end(), mySort);
    //pridani serazanych vysledku do listu
    bool checker = false;
    
    for (int i = 0; i < (int) array.size(); i++) {

        c.push_front(array[i].first);

        if (array[i].second != i + 1)
            checker = true;

    }
    //nebylo serazeno
    if (checker) throw OrderingDoesNotExistException();

    return c;
}
private:
  
//Trida pro ulozeni zapasu
class Match {
public:

    Match(const string & a, const string & b, _M result) : m_contA(a), m_contB(b), m_result(result) {
    };

    bool operator<(const Match & b)const {

        
        if (m_contA == b.m_contA ) {
            if (m_contB == b.m_contB) {
                return false;
            }
            return m_contB < b.m_contB;
        }
        return m_contA < b.m_contA;
    }


    string m_contA;
    string m_contB;
    _M m_result;

};
set<Match> matches;
int index = 0;
map<string, int> names;

};

#ifndef __PROGTEST__

struct CMatch {
public:

    CMatch(int a,
            int b)
    : m_A(a),
    m_B(b) {
    }




    int m_A;
    int m_B;

};

class HigherScoreThreshold {
public:

    HigherScoreThreshold(int diffAtLeast)
    : m_DiffAtLeast(diffAtLeast) {
    }

    int operator()(const CMatch & x) const {
        return ( x . m_A > x . m_B + m_DiffAtLeast) - (x . m_B > x . m_A + m_DiffAtLeast);
    }
private:
    int m_DiffAtLeast;
};

int HigherScore(const CMatch & x) {
    return ( x . m_A > x . m_B) - (x . m_B > x . m_A);
}

int main(void) {
    CContest<CMatch>  x1;
    x1 . AddMatch ( "C++", "C++", CMatch ( 10, 3 ) );
   
   // assert ( ! x1 . IsOrdered ( HigherScore ) );
   // return 0;
    try {
        list<string> res = x1.Results(HigherScore);
        assert("aaaaa" == NULL);
    } catch (OrderingDoesNotExistException ex) {

    }
//        CContest<CMatch> x;
//    
//     
//      x . AddMatch ( "C++", "Pascal", CMatch ( 10, 3 ) )
//        . AddMatch ( "C++", "Java", CMatch ( 8, 1 ) )
//        . AddMatch ( "Pascal", "Basic", CMatch ( 40, 0 ) )
//        . AddMatch ( "Java", "PHP", CMatch ( 6, 2 ) )
//        . AddMatch ( "Java", "Pascal", CMatch ( 7, 3 ) )
//        . AddMatch ( "PHP", "Basic", CMatch ( 10, 0 ) );
//     
//        
//      
//      assert ( ! x . IsOrdered ( HigherScore ) );
//     
//      try
//      {
//        list<string> res = x . Results ( HigherScore );
//        assert ( "Exception missing!" == NULL );
//      }
//      catch ( const OrderingDoesNotExistException & e )
//      {
//      }
//      catch ( ... )
//      {
//        assert ( "Invalid exception thrown!" == NULL );
//      }
//    
//      x . AddMatch ( "PHP", "Pascal", CMatch ( 3, 6 ) ); 
//    
//      assert ( x . IsOrdered ( HigherScore ) );
//      try
//      {
//        list<string> res = x . Results ( HigherScore );
//        assert ( ( res == list<string>{ "C++", "Java", "Pascal", "PHP", "Basic" } ) );
//      }
//      catch ( ... )
//      {
//        assert ( "Unexpected exception!" == NULL );
//      }
//    
//      
//      assert ( ! x . IsOrdered ( HigherScoreThreshold ( 3 ) ) );
//      try
//      {
//        list<string> res = x . Results ( HigherScoreThreshold ( 3 ) );
//        assert ( "Exception missing!" == NULL );
//      }
//      catch ( const OrderingDoesNotExistException & e )
//      {
//      }
//      catch ( ... )
//      {
//        assert ( "Invalid exception thrown!" == NULL );
//      }
//      
//      assert ( x . IsOrdered ( [] ( const CMatch & x )
//      {
//        return ( x . m_A < x . m_B ) - ( x . m_B < x . m_A ); 
//      } ) );
//      try
//      {
//        list<string> res = x . Results ( [] ( const CMatch & x )
//        {
//          return ( x . m_A < x . m_B ) - ( x . m_B < x . m_A ); 
//        } );
//        assert ( ( res == list<string>{ "Basic", "PHP", "Pascal", "Java", "C++" } ) );
//      }
//      catch ( ... )
//      {
//        assert ( "Unexpected exception!" == NULL );
//      }
//
//    CContest<bool> y;
//
//    y . AddMatch("Python", "PHP", true)
//            . AddMatch("PHP", "Perl", true)
//            . AddMatch("Perl", "Bash", true)
//            . AddMatch("Bash", "JavaScript", true)
//            . AddMatch("JavaScript", "VBScript", true);
//
//    assert(y . IsOrdered([] (bool v) {
//        return v ? 10 : -10;
//    }));
//    
//    try {
//        list<string> res = y . Results([] (bool v) {
//            return v ? 10 : -10;
//        });
//        assert((res == list<string>{"Python", "PHP", "Perl", "Bash", "JavaScript", "VBScript"}));
//    } catch (...) {
//        assert("Unexpected exception!" == NULL);
//    }
//
//    y . AddMatch("PHP", "JavaScript", false);
//    assert(!y . IsOrdered([] (bool v) {
//        return v ? 10 : -10;
//    }));
//    try {
//        list<string> res = y . Results([] (bool v) {
//            return v ? 10 : -10;
//        });
//        assert("Exception missing!" == NULL);
//    } catch (const OrderingDoesNotExistException & e) {
//    } catch (...) {
//        assert("Invalid exception thrown!" == NULL);
//    }
//
//    try {
//        y . AddMatch("PHP", "JavaScript", false);
//        assert("Exception missing!" == NULL);
//    } catch (const DuplicateMatchException & e) {
//    } catch (...) {
//        assert("Invalid exception thrown!" == NULL);
//    }
//
//    try {
//        y . AddMatch("JavaScript", "PHP", true);
//        assert("Exception missing!" == NULL);
//    } catch (const DuplicateMatchException & e) {
//    } catch (...) {
//        assert("Invalid exception thrown!" == NULL);
//    }
//
//    return 0;
}
#endif /* __PROGTEST__ */