#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cmath>
#include <cfloat>
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <complex>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */

ios_base & dummy_polynomial_manipulator(ios_base & x) {
    return x;
}

ios_base & (* (polynomial_variable(const string & varName))) (ios_base & x) {
    return dummy_polynomial_manipulator;
}

class CPolynomial {
public:
    // default constructor

    CPolynomial() {
        line.push_back(0);
        degree = 0;



    }

    CPolynomial(const vector<double>& tmp, unsigned int x) {
        line = tmp;
        degree = x;
    }
    // Degree (), returns unsigned value

    unsigned int Degree() const {

        unsigned int check = 0;
        for (unsigned int i = 0; i < line.size(); i++) {
            if (line[i] >= 0.00000001 || line[i]<=0.000000001) {
                check = i;

            }

        }


        return check;
    }
   


    // operator +

    CPolynomial operator+(const CPolynomial & b) const {
        unsigned int size = 0;

        vector<double> tmp;
        if (line.size() <= b.line.size()) {
            size = b.line.size();


        } else {
            size = line.size();

        }

        for (unsigned int i = 0; i < size; i++) {
            if (i < line.size() && i < b.line.size())
                tmp.push_back(line[i] + b.line[i]);
            else if (i >= line.size())
                tmp.push_back(b.line[i]);
            else if (i >= b.line.size())
                tmp.push_back(line[i]);


        }
        return CPolynomial(tmp, size - 1);



    }
    // operator -

    CPolynomial operator-(const CPolynomial & b) const {
        unsigned int size = 0;

        vector<double> tmp;
        if (line.size() <= b.line.size()) {
            size = b.line.size();


        } else {
            size = line.size();

        }


        for (unsigned int i = 0; i < size; i++) {
            if (i < line.size() && i < b.line.size())
                tmp.push_back(line[i] - b.line[i]);
            else if (i >= line.size())
                tmp.push_back(-b.line[i]);
            else if (i >= b.line.size())
                tmp.push_back(-line[i]);

        }
        return CPolynomial(tmp, size - 1);



    }
    // operator * (polynomial, double)

    /*
     http://www.geeksforgeeks.org/multiply-two-polynomials-2/ Inspirace 
     * 26.3.2017 Trminek
     */

    CPolynomial operator*(const CPolynomial b) const {
        vector<double> tmp;
        unsigned int size = line.size();

        unsigned int sizeB = b.line.size();



        tmp.resize(size + sizeB - 1, 0);

        for (unsigned int i = 0; i < size; i++) {

            for (unsigned int j = 0; j < sizeB; j++)
                tmp[i + j] += line[i] * b.line[j];
        }
        unsigned int d1 = const_cast<CPolynomial *> (this)->Degree();
        unsigned int d2 = const_cast<CPolynomial *> (&b)->Degree();
        return CPolynomial(tmp, d1 + d2);
    }

    CPolynomial operator*( double b) const {
        if (b == 0) {
            return CPolynomial();
        }


        unsigned int size = 0;
        vector<double> tmp;
        for (unsigned int i = 0; i < line.size(); i++) {
            tmp.push_back(line[i] * b);
        }
        size = const_cast<CPolynomial *> (this)->Degree();
        return CPolynomial(tmp, size);
    }
    // operator ==

    bool operator==( CPolynomial const & b) const {
        unsigned int d1 = const_cast<CPolynomial *> (this)->Degree();
        unsigned int d2 = const_cast<CPolynomial *> (&b)->Degree();

        if (d1 != d2)
            return false;
        for (unsigned int i = 0; i < line.size(); i++) {
            if (line[i] != b.line[i])
                return false;
        }
        return true;


    }

    // operator !=

    bool operator!=(CPolynomial const & b) const {
        unsigned int d1 = const_cast<CPolynomial *> (this)->Degree();
        unsigned int d2 = const_cast<CPolynomial *> (&b)->Degree();
        if (d1 == d2)
            return true;
        for (unsigned int i = 0; i < line.size(); i++) {
            if (line[i] != b.line[i])
                return true;
        }
        return false;


    }
    // operator []

    double& operator[](unsigned int x) {
        if (x == 0) {
            return line[x];
        }
        if (x > degree) {
            for (unsigned int i = degree; i < x; i++) {
                line.push_back(0);
            }
            degree = x;
        }
        return line[x];
    }

    const double& operator[](unsigned int x) const {
        return line[x];
    }

    // operator ()

    double operator()(double x) const {
        double res = 0;
        for (unsigned int i = 0; i < line.size(); i++) {
            if (i != 0) {
                res += line[i] * pow(x, (double)i);

            } else {
                res += line[i];
            }
        }
        return res;

    }
    // operator <<
    friend ostream & operator<<(ostream & os, const CPolynomial & b) ;




private:
    vector<double> line;
    unsigned int degree;

};

ostream & operator<<(ostream & os, const CPolynomial & b) {
    int size = b.line.size() - 1;


    bool print = true;
    for (int i = size; i >= 0; --i) {
        if (b.line[i] == 0 && i != 0)continue;
        if (i == 0) {
            if (b.line[i] > 0) {
                os << " + " << b.line[i];


            } else if (b.line[i] < 0) {
                os << " - " << -b.line[i];

            } else if (print) {
                os << b.line[i];

            }

        } else {
            print = false;
            if (b.line[i] == 1) {
                if (i == size) {
                    os << "x^" << i;


                } else {
                    os << " + x^" << i;


                }

            } else if (b.line[i] == -1) {
                if (i == size) {
                    os << "- x^" << i;



                } else {
                    os << " - x^" << i;


                }

            } else {
                if (size == i) {
                    if (b.line[i] <= 0) {
                        os << "- " << -b.line[i] << "*x^" << i;


                    } else if (b.line[i] >= 0) {
                        os << b.line[i] << "*x^" << i;


                    }


                } else {
                    if (b.line[i] <= 0) {
                        os << " - " << -b.line[i] << "*x^" << i;


                    } else if (b.line[i] >= 0) {
                        os << " + " << b.line[i] << "*x^" << i;


                    }
                }
            }

        }
    }

    return os;

}

#ifndef __PROGTEST__

bool smallDiff(double a,
        double b) {

    return a == b;
}

bool dumpMatch(const CPolynomial & x,
        const vector<double> & ref) {

    return true;
}

int main(void) {
    CPolynomial a, b, c;
    ostringstream out;
    a[0] = -10;
    a[1] = 3.5;
    a[3] = 1;
    
    assert(smallDiff(a(2), 5));
    out . str("");
    out << a;
    assert(out . str() == "x^3 + 3.5*x^1 - 10");
    a = a * -2;
    assert(a . Degree() == 3
            && dumpMatch(a, vector<double>{20.0, -7.0, -0.0, -2.0}));

    out . str("");
    out << a;
    assert(out . str() == "- 2*x^3 - 7*x^1 + 20");
    out . str("");
    out << b;
    assert(out . str() == "0");
    b[5] = -1;
    out . str("");
    out << b;
    assert(out . str() == "- x^5");
    c = a + b;
    assert(c . Degree() == 5
            && dumpMatch(c, vector<double>{20.0, -7.0, 0.0, -2.0, 0.0, -1.0}));

    out . str("");
    out << c;
    assert(out . str() == "- x^5 - 2*x^3 - 7*x^1 + 20");
    c = a - b;

    assert(c . Degree() == 5
            && dumpMatch(c, vector<double>{20.0, -7.0, -0.0, -2.0, -0.0, 1.0}));


    out . str("");
    out << c;
    assert(out . str() == "x^5 - 2*x^3 - 7*x^1 + 20");
    c = a * b;
    assert(c . Degree() == 8
            && dumpMatch(c, vector<double>{0.0, -0.0, 0.0, -0.0, -0.0, -20.0, 7.0, 0.0, 2.0}));

    out . str("");
    out << c;
    assert(out . str() == "2*x^8 + 7*x^6 - 20*x^5");
    assert(a != b);
    b[5] = 0;
    assert(!(a == b));
    a = a * 0;
    assert(a . Degree() == 0
            && dumpMatch(a, vector<double>{0.0}));

    assert(a == b);
  

    return 0;
}
#endif /* __PROGTEST__ */