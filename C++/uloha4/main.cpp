#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <sstream>
using namespace std;
#endif /* __PROGTEST__ */

class CBank {
public:
    // default constructor

    CBank() {
        cbank = new CBankReal();


    };
    // copy constructor

    CBank(const CBank & tmp) {

        cbank = tmp.cbank;
        cbank->ref_cnt += 1;



    }
    // destructor

    ~CBank() {
     
        if (--cbank->ref_cnt == 0) {
           
            delete cbank;
        }
    }
    // operator =

    CBank & operator=(const CBank & tmp) {

        if (&tmp == this) return *this;

        //detach
        if (--cbank -> ref_cnt == 0)
            delete cbank;

        //attach
        cbank = tmp.cbank;
        cbank -> ref_cnt += 1;

        return *this;


    }
  //Metoda na zalozeni noveho uctu
    bool NewAccount(const char * accID,
            int initialBalance) {

        if (!cbank->accountExists(accID))
            return false;
   //Vznika nova kopie
        if (cbank -> ref_cnt > 1) {
           
            cbank -> ref_cnt -= 1;
            //Vytvoreni nove kopie
            cbank =  new CBankReal(*cbank);
        }

        if (!cbank -> NewAccount(accID, initialBalance))
            return false;

        return true;


    }
 //Metoda na transakci
    bool Transaction(const char * debAccID,
            const char * credAccID,
            int amount,
            const char * signature) {
        if (amount < 0 || !strcmp(debAccID, credAccID)) return false;

        int posDeb = cbank->accountExists(debAccID);
        if (posDeb == -1) return false;
        int posCred = cbank->accountExists(credAccID);
        if (posCred == -1) return false;
  //Vznik nove kopie
        if (cbank -> ref_cnt > 1) {
            cbank -> ref_cnt -= 1;
            cbank = new CBankReal(*cbank);
        }

        if (!cbank -> Transaction(debAccID, credAccID, amount, signature))
            return false;
        return true;

    }
//Metoda na trim uctu 
    bool TrimAccount(const char * accID) {


        int pos = cbank->accountExists(accID);
        if (pos == -1) return false;
        //vznik nove kopie
        if (cbank -> ref_cnt > 1) {
            cbank -> ref_cnt -= 1;
            cbank = new CBankReal(*cbank);
        }

        if (!cbank -> TrimAccount(accID))
            return false;
        return true;

    }
  //Pomocna trida transakce k drzeni informaci o pohybu na uctu
    class CTransaction {
    public:

        CTransaction() {
        }

        CTransaction(const char * deb, const char * cred, int am, const char * sign) {

        //Informace o transakci

            debAccID = new char [strlen(deb) + 1];
            credAccID = new char [strlen(cred) + 1];
            signature = new char [strlen(sign) + 1];
            strcpy(debAccID, deb);
            strcpy(credAccID, cred);
            strcpy(signature, sign);
            amount = am;

        }


        char * debAccID;
        char * credAccID;
        char * signature;
        int amount;



    };
  //Pomocna trida na vedeni uctu
    class CAccount {
    public:

        CAccount() {
        }

        CAccount(const char * a, int init) {
            accID = new char [strlen(a) + 1];
            strcpy(accID, a);
            initialBalance = init;
            currentBalance = init;
            size = 0;
            alloc = 10;
            //Ucet ma transakce
            m_Transaction = new CTransaction[alloc];
        }

        ~CAccount() {

        }
       //Vraceni soucasneho stavu
        int Balance() {
            return currentBalance;
        }

        char * accID;
        int initialBalance;
        int currentBalance;
        int size;
        int alloc;
        CTransaction *m_Transaction;

    };

    // Account ( accID )
    //Metoda ktera vraci ucet dle ID
    CAccount Account(const char * accID) {
        int pos = cbank->accountExists(accID);
        if (pos == -1)
            throw 0;

        return cbank->m_Account[pos];
    }
    //Pretizeni vypisu
    friend ostream & operator<<(ostream & os, const CAccount & x);
    //Trida zastresujici realne instance
    class CBankReal {
    public: 
        int size;
        int alloc;
        int ref_cnt;
        CAccount *m_Account;


        CBankReal() {
            size = 0;
            alloc = 10;
            //Pocitani referenci
            ref_cnt = 1;
            m_Account = new CAccount[alloc];


        };
     //Copy konstruktor
        CBankReal(const CBankReal & tmp) {
            //Nastaveni nove reference
            ref_cnt = 1;
       //Prekopirovani obsahu
            size = tmp.size;
            alloc = tmp.alloc;
               m_Account = new CAccount[alloc];
            for (int i = 0; i < size; i++) {
                m_Account[i].size = tmp.m_Account[i].size;
                m_Account[i].alloc = tmp.m_Account[i].alloc;
                m_Account[i].m_Transaction = new CTransaction[ m_Account[i].alloc];
                if (m_Account[i].size > 0) {
                    for (int j = 0; j < m_Account[i].size; j++) {
                        m_Account[i].m_Transaction[j].debAccID = new char [strlen(tmp.m_Account[i].m_Transaction[j].debAccID) + 1];
                        strcpy(m_Account[i].m_Transaction[j].debAccID, tmp.m_Account[i].m_Transaction[j].debAccID);

                        m_Account[i].m_Transaction[j].credAccID = new char [strlen(tmp.m_Account[i].m_Transaction[j].credAccID) + 1];
                        strcpy(m_Account[i].m_Transaction[j].credAccID, tmp.m_Account[i].m_Transaction[j].credAccID);

                        m_Account[i].m_Transaction[j].signature = new char [strlen(tmp.m_Account[i].m_Transaction[j].signature) + 1];
                        strcpy(m_Account[i].m_Transaction[j].signature, tmp.m_Account[i].m_Transaction[j].signature);

                        m_Account[i].m_Transaction[j].amount = tmp.m_Account[i].m_Transaction[j].amount;
                    }


                }
                m_Account[i].accID = new char [strlen(tmp.m_Account[i].accID) + 1];
                strcpy(m_Account[i].accID, tmp.m_Account[i].accID);

                m_Account[i].currentBalance = tmp.m_Account[i].currentBalance;
                m_Account[i].initialBalance = tmp.m_Account[i].initialBalance;

            }
         
        }
        
    //Deskturokor
        ~CBankReal() {
            for (int i = 0; i < size; i++) {
                if (m_Account[i].size > 0) {
                    for (int j = 0; j < m_Account[i].size; j++) {
                        delete [] m_Account[i].m_Transaction[j].credAccID;
                        delete [] m_Account[i].m_Transaction[j].debAccID;
                        delete [] m_Account[i].m_Transaction[j].signature;
                    }
                }

                delete [] m_Account[i].accID;
                delete [] m_Account[i].m_Transaction;
            }

            delete [] m_Account;

        }
//Realna funkce new account pridani do pole novy ucet
        bool NewAccount(const char * accID,
                int initialBalance) {
            if (accountExists(accID) != -1) {
                return false;
            }
            if (size >= alloc)
                realloc();
            m_Account[size++] = CAccount(accID, initialBalance);
            return true;
        }
//Realna funkce transaction
        bool Transaction(const char * debAccID,
                const char * credAccID,
                int amount,
                const char * signature) {
            if (amount < 0 || !strcmp(debAccID, credAccID)) return false;

            int posDeb = accountExists(debAccID);
            if (posDeb == -1) return false;
            int posCred = accountExists(credAccID);
            if (posCred == -1) return false;
            // cout << posDeb << " " << posCred;


            m_Account[posDeb].currentBalance -= amount;
            m_Account[posCred].currentBalance += amount;
             //realloc pole s transakcemi
            if (m_Account[posDeb].size >= m_Account[posDeb].alloc) {
                CTransaction * tmp = new CTransaction[m_Account[posDeb].alloc *= 2];
                for (int i = 0; i < m_Account[posDeb].size; i++) {
                    tmp[i] = m_Account[posDeb].m_Transaction[i];
                }
                delete [] m_Account[posDeb].m_Transaction;
                m_Account[posDeb].m_Transaction = tmp;



            }

            if (m_Account[posCred].size >= m_Account[posCred].alloc) {
                CTransaction * tmp = new CTransaction[m_Account[posCred].alloc *= 2];
                for (int i = 0; i < m_Account[posCred].size; i++) {
                    tmp[i] = m_Account[posCred].m_Transaction[i];
                }
                delete [] m_Account[posCred].m_Transaction;
                m_Account[posCred].m_Transaction = tmp;


            }

             //Pridani nove transakce do pole
            CTransaction transOne(debAccID, credAccID, amount, signature);
            CTransaction transTwo(debAccID, credAccID, amount, signature);

            m_Account[posDeb].m_Transaction[m_Account[posDeb].size++] = transOne;

            m_Account[posCred].m_Transaction[m_Account[posCred].size++] = transTwo;
            return true;




        }
//Realna funkce trim, odstrani vsechny zaznamy transakci
        bool TrimAccount(const char * accID) {


            int pos = accountExists(accID);
            if (pos == -1) return false;
            m_Account[pos].initialBalance = m_Account[pos].currentBalance;

            if (m_Account[pos].size > 0) {
                for (int j = 0; j < m_Account[pos].size; j++) {
                    delete [] m_Account[pos].m_Transaction[j].credAccID;
                    delete [] m_Account[pos].m_Transaction[j].debAccID;
                    delete [] m_Account[pos].m_Transaction[j].signature;

                }
            }
            delete [] m_Account[pos].m_Transaction;
            m_Account[pos].size = 0;
            m_Account[pos].alloc = 10;
            CTransaction * tmp = new CTransaction[alloc];
            m_Account[pos].m_Transaction = tmp;
            return true;
        }
        //Funkce na zjisteni zda ucet existuje a kdyz ano vraci pozici
        int accountExists(const char * id) {
            if (size == 0)return -1;
            for (int i = 0; i < size; i++) {
                if (!strcmp(id, m_Account[i].accID))
                    return i;

            }
            return -1;



        }
   //Funcke na realloc po account
        void realloc() {

            CAccount * tmp = new CAccount[alloc *= 2];
            for (int i = 0; i < size; i++) {
                tmp[i] = m_Account[i];
                tmp[i].m_Transaction = m_Account[i].m_Transaction;
            }
            delete [] m_Account;
            m_Account = tmp;






        }

        void print() {

            for (int i = 0; i < size; i++) {
                cout << "ID:" << m_Account[i].accID << " current:" << m_Account[i].currentBalance << " initial:" << m_Account[i].initialBalance << endl;

            }

        }

    };
   

    CBankReal * cbank;



};
//Vypis dle specifikace
ostream & operator<<(ostream & os, const CBank::CAccount & x) {

    os << x.accID << ":\n   " << x.initialBalance << "\n";
  //  cout << x.accID << ":\n   " << x.initialBalance << "\n";

    for (int i = 0; i < x.size; i++) {


        if (!strcmp(x.m_Transaction[i].debAccID, x.accID)) {
            os << " - " << x.m_Transaction[i].amount << ", to: " << x.m_Transaction[i].credAccID << ", sign: " << x.m_Transaction[i].signature << "\n";
    //        cout << " - " << x.m_Transaction[i].amount << ", to: " << x.m_Transaction[i].credAccID << ", sign: " << x.m_Transaction[i].signature << "\n";
        } else {
            os << " + " << x.m_Transaction[i].amount << ", from: " << x.m_Transaction[i].debAccID << ", sign: " << x.m_Transaction[i].signature << "\n";
      //      cout << " + " << x.m_Transaction[i].amount << ", from: " << x.m_Transaction[i].debAccID << ", sign: " << x.m_Transaction[i].signature << "\n";
        }
    }
    os << " = " << x.currentBalance << "\n";
   // cout << " = " << x.currentBalance << "\n";
    return os;

}

#ifndef __PROGTEST__

int main(void) {
    ostringstream os;
    char accCpy[100], debCpy[100], credCpy[100], signCpy[100];


    CBank x0;
    assert(x0 . NewAccount("123456", 1000));

    assert(x0 . NewAccount("987654", -500));

    
    assert(x0 . Transaction("123456", "987654", 300, "XAbG5uKz6E="));
    assert(x0 . Transaction("123456", "987654", 2890, "AbG5uKz6E="));
    assert(x0 . NewAccount("111111", 5000));
    assert(x0 . Transaction("111111", "987654", 290, "Okh6e+8rAiuT5="));

    assert(x0 . Account("123456"). Balance() == -2190);
    assert(x0 . Account("987654"). Balance() == 2980);
    assert(x0 . Account("111111"). Balance() == 4710);
    os . str("");
    os << x0 . Account("123456");
    assert(!strcmp(os . str() . c_str(), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n"));
    os . str("");
    os << x0 . Account("987654");
    assert(!strcmp(os . str() . c_str(), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 290, from: 111111, sign: Okh6e+8rAiuT5=\n = 2980\n"));
    os . str("");
    os << x0 . Account("111111");
    assert(!strcmp(os . str() . c_str(), "111111:\n   5000\n - 290, to: 987654, sign: Okh6e+8rAiuT5=\n = 4710\n"));
    assert(x0 . TrimAccount("987654"));
    assert(x0 . Transaction("111111", "987654", 123, "asdf78wrnASDT3W"));
    os . str("");
    os << x0 . Account("987654");
    assert(!strcmp(os . str() . c_str(), "987654:\n   2980\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 3103\n"));
    
    CBank x2;
    strncpy(accCpy, "123456", sizeof ( accCpy));
    assert(x2 . NewAccount(accCpy, 1000));
    strncpy(accCpy, "987654", sizeof ( accCpy));
    assert(x2 . NewAccount(accCpy, -500));
    strncpy(debCpy, "123456", sizeof ( debCpy));
    strncpy(credCpy, "987654", sizeof ( credCpy));
    strncpy(signCpy, "XAbG5uKz6E=", sizeof ( signCpy));
    assert(x2 . Transaction(debCpy, credCpy, 300, signCpy));
    strncpy(debCpy, "123456", sizeof ( debCpy));
    strncpy(credCpy, "987654", sizeof ( credCpy));
    strncpy(signCpy, "AbG5uKz6E=", sizeof ( signCpy));
    assert(x2 . Transaction(debCpy, credCpy, 2890, signCpy));
    strncpy(accCpy, "111111", sizeof ( accCpy));
    assert(x2 . NewAccount(accCpy, 5000));
    strncpy(debCpy, "111111", sizeof ( debCpy));
    strncpy(credCpy, "987654", sizeof ( credCpy));
    strncpy(signCpy, "Okh6e+8rAiuT5=", sizeof ( signCpy));
    assert(x2 . Transaction(debCpy, credCpy, 2890, signCpy));
    assert(x2 . Account("123456"). Balance() == -2190);
    assert(x2 . Account("987654"). Balance() == 5580);
    assert(x2 . Account("111111"). Balance() == 2110);
    os . str("");
    os << x2 . Account("123456");
    assert(!strcmp(os . str() . c_str(), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n"));
    os . str("");
    os << x2 . Account("987654");
    assert(!strcmp(os . str() . c_str(), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n = 5580\n"));
    os . str("");
    os << x2 . Account("111111");
    assert(!strcmp(os . str() . c_str(), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n = 2110\n"));
    assert(x2 . TrimAccount("987654"));
    strncpy(debCpy, "111111", sizeof ( debCpy));
    strncpy(credCpy, "987654", sizeof ( credCpy));
    strncpy(signCpy, "asdf78wrnASDT3W", sizeof ( signCpy));
    assert(x2 . Transaction(debCpy, credCpy, 123, signCpy));
    os . str("");
    os << x2 . Account("987654");
    assert(!strcmp(os . str() . c_str(), "987654:\n   5580\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n"));

    CBank x4;
    assert(x4 . NewAccount("123456", 1000));
    assert(x4 . NewAccount("987654", -500));
    assert(!x4 . NewAccount("123456", 3000));
    assert(!x4 . Transaction("123456", "666", 100, "123nr6dfqkwbv5"));
    assert(!x4 . Transaction("666", "123456", 100, "34dGD74JsdfKGH"));
    assert(!x4 . Transaction("123456", "123456", 100, "Juaw7Jasdkjb5"));
    try {
        x4 . Account("666"). Balance();
        assert("Missing exception !!" == NULL);
    } catch (...) {
    }
    try {
        os << x4 . Account("666"). Balance();
        assert("Missing exception !!" == NULL);
    } catch (...) {
    }
    assert(!x4 . TrimAccount("666"));
return 0;
   CBank x6;
  assert ( x6 . NewAccount ( "123456", 1000 ) );
  assert ( x6 . NewAccount ( "987654", -500 ) );
  assert ( x6 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
  assert ( x6 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
  assert ( x6 . NewAccount ( "111111", 5000 ) );
  assert ( x6 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
  CBank x7 ( x6 );
  
  assert ( x6 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
  assert ( x7 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
  assert ( x6 . NewAccount ( "99999999", 7000 ) );
  assert ( x6 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
  assert ( x6 . TrimAccount ( "111111" ) );
  assert ( x6 . Transaction ( "123456", "111111", 221, "Q23wr234ER==" ) );
  os . str ( "" );
  os << x6 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n + 221, from: 123456, sign: Q23wr234ER==\n = -1581\n" ) );
  os . str ( "" );
  os << x6 . Account ( "99999999" );
  assert ( ! strcmp ( os . str () . c_str (), "99999999:\n   7000\n + 3789, from: 111111, sign: aher5asdVsAD\n = 10789\n" ) );
  os . str ( "" );
  os << x6 . Account ( "987654" );
  assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );
  os . str ( "" );
  os << x7 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );
  try
  {
    os << x7 . Account ( "99999999" ). Balance ( );
    assert ( "Missing exception !!" == NULL );
  }
  catch ( ... )
  {
  }
  os . str ( "" );
  os << x7 . Account ( "987654" );
  assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 789, from: 111111, sign: SGDFTYE3sdfsd3W\n = 6369\n" ) );
  
  CBank x8;
  CBank x9;
  assert ( x8 . NewAccount ( "123456", 1000 ) );
  assert ( x8 . NewAccount ( "987654", -500 ) );
  assert ( x8 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
  assert ( x8 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
  assert ( x8 . NewAccount ( "111111", 5000 ) );
  assert ( x8 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
  x9 = x8;
  assert ( x8 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
  assert ( x9 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
  assert ( x8 . NewAccount ( "99999999", 7000 ) );
  assert ( x8 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
  assert ( x8 . TrimAccount ( "111111" ) );
  os . str ( "" );
  os << x8 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n = -1802\n" ) );
  os . str ( "" );
  os << x9 . Account ( "111111" );
  assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );





    return 0;


}
#endif /* __PROGTEST__ */