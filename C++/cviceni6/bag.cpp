#include "bag.h"

Bag::Bag() {
    m_BagData = new BagData();
}

Bag::BagData::BagData() {
	m_Cnt = 0;
	m_Size = 5;
	m_Data = new int[m_Size];
        m_RefCnt = 1;
}
Bag::~Bag() {
    if(--m_BagData->m_RefCnt==0)
        delete m_BagData;
}
Bag::BagData::~BagData() {
	delete [] m_Data;
}
Bag::BagData::BagData(const BagData& b) {
	m_Cnt = b.m_Cnt;
	m_Size = b.m_Size;
	// m_Data = b.m_Data;
	m_Data = new int[m_Size];
	for ( int i = 0; i < m_Cnt; ++i )
		m_Data[i] = b.m_Data[i];
        m_RefCnt=1;
}
Bag & Bag::operator=(const Bag & b) {
	if ( this == &b )
		return *this;
/*	delete [] m_Data;

	m_Cnt = b.m_Cnt;
	m_Size = b.m_Size;
	m_Data = new int[m_Size];
	for ( int i = 0; i < m_Cnt; ++i )
		m_Data[i] = b.m_Data[i];*/
	Bag tmp( b );
	swap( m_Cnt, tmp.m_Cnt );
	swap( m_Size, tmp.m_Size );
	swap( m_Data, tmp.m_Data );
	return *this;
}
void Bag::vloz( int x ) {

    if(m_BagData ->m_RefCnt > 1){
        m_BagData->m_RefCnt-=1;
        m_BagData = new BagData(m_BagData);
    }
    m_BagData->vloz(x);
}

void Bag::BagData::vloz( int x ) {

	// realokace -> lepsi do vlastni metody
	if ( m_Cnt == m_Size ) {
		int * tmp = new int[m_Size *= 2];
		for ( int i = 0; i < m_Cnt; ++i )
			tmp[i] = m_Data[i];
		delete [] m_Data;
		m_Data = tmp;
	}

	m_Data[m_Cnt++] = x;
}
bool Bag::odeber( int x ) {
	// sesypavani. Netrvame ale na poradi, rychlejsi by bylo prohodit mazany s poslednim a zmensit delku
	for ( int i = 0; i < m_Cnt; ++i ) {
		if ( x == m_Data[i] ) {
			m_Cnt--;
			for ( int j = i; j < m_Cnt; ++j )
				m_Data[j] = m_Data[j+1];
			return true;
		}
	}
	return false;
}
bool Bag::BagData::odeber( int x ) {
	// sesypavani. Netrvame ale na poradi, rychlejsi by bylo prohodit mazany s poslednim a zmensit delku
	for ( int i = 0; i < m_Cnt; ++i ) {
		if ( x == m_Data[i] ) {
			m_Cnt--;
			for ( int j = i; j < m_Cnt; ++j )
				m_Data[j] = m_Data[j+1];
			return true;
		}
	}
	return false;
}

bool Bag::jeTam( int x ) const {
	for ( int i = 0; i < m_Cnt; ++i )
		if ( x == m_Data[i] )
			return true;
	return false;
}

ostream & operator<<( ostream & os, const Bag & b )
{
	os << "Bag:";
	for ( int i = 0; i < b.m_Cnt; ++i )
		os << " " << b.m_Data[i];
	return os;
}
