// #pragma once

#ifndef BAG_H
#define BAG_H

#include <iostream>
using namespace std;

class Bag {
public:
    Bag();
    ~Bag();
    Bag(const Bag & b);
    void vloz(int x);
    bool odeber(int x);
    bool jeTam(int x) const;
    Bag & operator=(const Bag & b);
    friend ostream & operator<<(ostream & os, const Bag & b);
    BagData m_BagData;
private:

    struct BagData {
        void vloz(int x);
        bool odeber(int x);
        bool jeTam(int x) const;
        BagData();
        BagData(const BagData & b);
        ~BagData();



        int * m_Data;
        int m_Cnt, m_Size;
        int m_RefCnt;


    };

};

#endif /*BAG_H*/
