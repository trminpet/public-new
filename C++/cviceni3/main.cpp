/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: petr
 *
 * Created on March 14, 2017, 4:37 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

/*
 * 
 */
class Complex {
private:
    double m_re, m_im;

public:

    Complex(double re, double im = 0) : m_re(re), m_im(im) {
    }

    Complex add(const Complex & x) const {
        return Complex(x.m_re + m_re, x.m_im + m_im);
    }

    Complex sub(const Complex & x) const {
        return Complex(x.m_re - m_re, x.m_im - m_im);
    }

    double getIm()const {
        return m_im;
    }

    double getRe()const {
        return m_re;
    }

  //  Complex operator+(const Complex &x) const {

    //    return add(x);
   // }
    
    
    

    Complex operator-(const Complex &x) const {

        return sub(x);
    }

    /*
     bool operator==(const Complex &x) const {
         return m_re == x.m_re && m_im == x.m_im;
     }
     bool operator !=(const Complex &x) const {
         return !(x==*this);
     }
     */
    bool operator<(const Complex &x) const {
        if (x.m_re == m_re) return m_im < x.m_im;
        return m_re < x.m_re;
    }

    bool operator==(const Complex &x) const {
        return !(*this < x) && !(x < *this);
    }

    bool operator!=(const Complex &x) const {
        return !(x == *this);
    }
    friend ostream & operator << (ostream& os, const Complex &x);
    friend Complex operator+(const Complex &x,const Complex &y)  {

        return x.add(y);
    }
    Complex& operator ++(){
        m_re++;
        return *this;
    }
    Complex operator ++(int){
        Complex ret = *this;
        m_re++;
        
        return ret;
    }

};

    ostream & operator << (ostream& os, const Complex &x){
       return os << x.getRe() << " + " << x.getIm() << "i";
    
    };

int main(int argc, char** argv) {

    Complex a(1, 2), b(2, 1);
    Complex c = a + b;

   /* 
    cout << (a + b - a == b) << endl;
    cout << (a != b) << endl;
    cout << (a < b) << endl;
    cout << (a < c) << endl;
    cout << (b < c) << endl;
    cout << (b < a) << endl;
    cout << a << endl;
   
    cout << (3.14 + a) << endl;
   */
    
    cout << ++a << " " << a << endl;
     
     
    return 0;
}

