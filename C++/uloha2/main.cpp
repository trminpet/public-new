#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream> 
#include <iomanip> 
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */
unsigned int position = 0;
string idHelp = "";
string nameHelp = "";
string addrHelp = "";

class CVATRegister {
public:

    CVATRegister(void) {
    }

    ~CVATRegister(void) {
    }

    /*Funkce na zalozeni nove spolecnosti
     */
    bool NewCompany(const string & name,
            const string & addr,
            const string & taxID) {
        if (databaseID.empty() && databaseName.empty()) {
            Company c(toLower(name), toLower(addr), taxID);
            databaseID.push_back(c);
            databaseName.push_back(c);
            return true;
        }

        string nameTmp = toLower(name);
        string addrTmp = toLower(addr);
        string complex = nameTmp + "//#" + addrTmp;
        //Hledani firmy zda jiz existuje

        unsigned int posID = 0;
        bool ins = false;
        int rc = 0;
        while (posID < databaseID.size()) {
            rc = taxID.compare(databaseID[posID].taxID);

            if (rc == 0) {
                return false;
            }
            if (rc < 0) {
                ins = true;
                break;
            }
            posID++;



        }

        unsigned int posName = 0;

        bool inse = false;
        while (posName < databaseName.size()) {

            rc = complex.compare(databaseName[posName].id);
            if (rc == 0) {
                return false;
            }
            if (rc < 0) {
                inse = true;
                break;
            }
            posName++;

        }


        Company c(toLower(name), toLower(addr), taxID);
        if (posName >= databaseName.size())
            databaseName.push_back(c);
        else if (inse) {
            databaseName.insert(databaseName.begin() + posName, c);
        }

        if (posID >= databaseID.size())
            databaseID.push_back(c);
        else if (ins) {
            databaseID.insert(databaseID.begin() + posID, c);
        }




        return true;

    }

    /*Funcke na se sorteni database
     */
    void sortDB() {

        sort(databaseName.begin(), databaseName.end(), [](Company a, Company b) {

            return a.id < b.id;
        });

    }

    void print() {

        for (unsigned int i = 0; i < databaseID.size(); i++) {
            cout << databaseID[i].name << " " << databaseID[i].addr << " " << databaseID[i].taxID << " " << databaseID[i].sum << endl;

        }
        cout << "/////////////////////////////////////////////////" << endl;
        for (unsigned int i = 0; i < databaseName.size(); i++) {
            cout << databaseName[i].name << " " << databaseName[i].addr << " " << databaseName[i].taxID << " " << databaseName[i].sum << " " << databaseName[i].id << endl;

        }
        cout << "/////////////////////////////////////////////////" << endl;





    }

    /*Funkce na zruseni spolecnosti v obou vektorech
     */
    bool CancelCompany(const string & name,
            const string & addr) {
        if (!findCompany(toLower(name), toLower(addr))) return false;

        databaseName.erase(databaseName.begin() + position);
        findCompany(idHelp);
        databaseID.erase((databaseID.begin() + position));
        return true;
    }

    bool CancelCompany(const string & taxID) {
        if (!findCompany(taxID)) return false;
        databaseID.erase(databaseID.begin() + position);

        findCompany(nameHelp, addrHelp);
        databaseName.erase((databaseName.begin() + position));
        return true;
    }

    /*Funkce na pridani hodnoty pokud firma existuje 
     */

    bool Invoice(const string & taxID,
            unsigned int amount) {
        int res = binarySearch(taxID);
        if (res == -1)
            return false;
        else {
            databaseID[res].sum += amount;

            res = binarySearch(nameHelp, addrHelp);

            databaseName[res].sum += amount;
            invoices.push_back(amount);
            count++;
            return true;
        }


    }

    bool Invoice(const string & name,
            const string & addr,
            unsigned int amount) {
        int res = binarySearch(name, addr);

        if (res == -1)
            return false;
        else {
            databaseName[res].sum += amount;
            res = binarySearch(idHelp);

            databaseID[res].sum += amount;

            invoices.push_back(amount);

            count++;

            return true;
        }

        return true;
    }

    /*Funkce na zjisteni celkoveho prijmu firmy
     */
    bool Audit(const string & name,
            const string & addr,
            unsigned int & sumIncome) const {
        int res = const_cast<CVATRegister *> (this)->binarySearch(name, addr);
        if (res == -1)
            return false;
        else {

            sumIncome = databaseName[res].sum;
            return true;
        }

    }

    bool Audit(const string & taxID,
            unsigned int & sumIncome)const {
        int res = const_cast<CVATRegister *> (this)->binarySearch(taxID);
        if (res == -1)
            return false;
        else {
            sumIncome = databaseID[res].sum;
            return true;
        }
    }

    /*Funkce na median
     */

    unsigned int MedianInvoice(void) const {
        if (!count)return 0;

        return const_cast<CVATRegister *> (this)->median();
    }
private:

    class Company {
    public:

        Company(string name, string addr, string taxID)
        : name(name), addr(addr), taxID(taxID), sum(0) {
            id = name + "//#" + addr;
        }

        unsigned int getSum() {
            return this->sum;
        }

        void addIncome(unsigned int inc) {
            this->sum += inc;
        }
        // private:
        string name;
        string addr;
        string taxID;
        string id;
        unsigned int sum;


    };
    vector<Company> databaseID;
    vector<Company> databaseName;
    vector <unsigned int> invoices;
    unsigned int count = 0;

    /*Funkce na hledani firmy ve vectoru dle retezce jmeno+adresa
     */

    bool findCompany(string name,
            string addr) {
        position = 0;
        idHelp = "";
        nameHelp = "";
        addrHelp = "";
        string complex = name + "//#" + addr;
        if (databaseName.empty()) return false;
        position = 0;
        for (auto &company : databaseName) {


            if (company.id == complex) {
                idHelp = company.taxID;
                return true;
            }
            position++;
        }
        return false;


    }

    /*Funkce na hledani firmy dle taxid
     */
    bool findCompany(string taxID) {
        position = 0;
        idHelp = "";
        nameHelp = "";
        addrHelp = "";
        if (databaseID.empty()) return false;
        for (auto &company : databaseID) {
            if (company.taxID == taxID) {
                nameHelp = company.name;
                addrHelp = company.addr;
                return true;
            }

            position++;
        }
        return false;



    }

    /*Funkce na prevedeni na mala pismena
     */
    string toLower(const string & tmp) {
        string a = tmp;
        transform(a.begin(), a.end(), a.begin(), ::tolower);
        return a;
    }

    unsigned int median() {
        sort(invoices.begin(), invoices.begin() + count);
        unsigned int size = count;

        unsigned int median = 0;
        if (size % 2 == 0) {
            median = invoices[size / 2 - 1] >= invoices[size / 2] ? invoices[size / 2 - 1] : invoices[size / 2];
        } else {
            median = invoices[size / 2];
        }

        return median;

    }

        /*Binarni vyhledavani ve vectoru
     */
    int binarySearch(const string & name,
            const string & addr) {

        unsigned int low = 0;
        unsigned int high = databaseName.size();

        string complex = toLower(name) + "//#" + toLower(addr);


        unsigned int mid = 0;
        while (low < high) {
            mid = (low + high) / 2;
            if (complex == databaseName[mid].id) {
                idHelp = databaseName[mid].taxID;

                return mid;
            }

            if (complex > databaseName[mid].id)
                low = mid + 1;
            else
                high = mid;

        }


        return -1;
    }

    int binarySearch(const string & taxID) {

        unsigned int low = 0;
        unsigned int high = databaseID.size();



        unsigned int mid = 0;
        while (low < high) {
            mid = (low + high) / 2;
            if (taxID == databaseID[mid].taxID) {
                nameHelp = databaseID[mid].name;
                addrHelp = databaseID[mid].addr;
                return mid;
            }

            if (taxID > databaseID[mid].taxID)
                low = mid + 1;
            else
                high = mid;

        }


        return -1;
    }


};

#ifndef __PROGTEST__

int main(void) {
    unsigned int sumIncome;



    CVATRegister b1;
    assert(b1 . NewCompany("ACME", "Kolejni", "666/666/666"));
    assert(b1 . NewCompany("ACME", "Thakurova", "666/666"));
    assert(b1 . NewCompany("Dummy", "Xhakurova", "12345as6"));
    assert(b1 . NewCompany("Dumadsmy", "Taakurova", "14"));
    assert(b1 . NewCompany("Dumamy", "Taakurova", "a"));
    assert(b1 . NewCompany("Duddsammy", "Taakurova", "1qewq4"));
    assert(b1 . NewCompany("Dumewqmy", "Taakurova", "1lkdsaj4"));
    assert(b1 . NewCompany("Dummdsay", "Taakurova", "1dlksaas4"));
    assert(b1 . NewCompany("Dummasy", "Taakurova", "141122"));
    assert(!b1 . NewCompany("Dumewqmy", "Taakurova", "14cxz"));
    assert(b1 . NewCompany("Dumcxzmy", "Taakurova", "14djsah"));
    assert(b1 . NewCompany("Dumasdmy", "Taakurova", "14dsa"));
    assert(b1 . NewCompany("Dumqwmy", "Taakurova", "1"));


    CVATRegister b2;
    assert(b2 . NewCompany("ACME", "Kolejni", "abcdef"));
    assert(b2 . NewCompany("Dummy", "Kolejni", "123456"));
    assert(!b2 . NewCompany("AcMe", "kOlEjNi", "1234"));
    assert(b2 . NewCompany("Dummy", "Thakurova", "ABCDEF"));
    assert(b2 . MedianInvoice() == 0);
    assert(b2 . Invoice("ABCDEF", 1000));
    assert(b2 . MedianInvoice() == 1000);
    assert(b2 . Invoice("abcdef", 2000));
    assert(b2 . MedianInvoice() == 2000);
    assert(b2 . Invoice("aCMe", "kOlEjNi", 3000));
    assert(b2 . MedianInvoice() == 2000);
    assert(!b2 . Invoice("1234567", 100));
    assert(!b2 . Invoice("ACE", "Kolejni", 100));
    assert(!b2 . Invoice("ACME", "Thakurova", 100));
    assert(!b2 . Audit("1234567", sumIncome));
    assert(!b2 . Audit("ACE", "Kolejni", sumIncome));
    assert(!b2 . Audit("ACME", "Thakurova", sumIncome));
    assert(!b2 . CancelCompany("1234567"));
    assert(!b2 . CancelCompany("ACE", "Kolejni"));
    assert(!b2 . CancelCompany("ACME", "Thakurova"));
    assert(b2 . CancelCompany("abcdef"));
    assert(b2 . MedianInvoice() == 2000);
    assert(!b2 . CancelCompany("abcdef"));
    assert(b2 . NewCompany("ACME", "Kolejni", "abcdef"));
    assert(b2 . CancelCompany("ACME", "Kolejni"));
    assert(!b2 . CancelCompany("ACME", "Kolejni"));

    return 1;


}
#endif /* __PROGTEST__ */