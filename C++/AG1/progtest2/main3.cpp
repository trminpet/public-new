#include <iostream>

using namespace std;

class CNode {
public:

    CNode() {

    }

    CNode(unsigned int v) : m_value(v) {
        m_left = NULL;
        m_right = NULL;
        m_height = 1;

    }

    ~CNode(void) {


    }




    int m_value;
    int m_height;
    CNode *m_left;
    CNode *m_right;


};

class CTree {

public:
    CTree(void) {
        m_root = nullptr;
        m_size = 0;
    }

    ~CTree() {
        freeNode(m_root);
    }

    void add(int value) {


        if (m_size == 0) {
            m_root = new CNode(value);

        } else {
            m_root = insert(value, m_root);

        }
        m_size++;

    }

    //removing values by replacing it by zero
    void remove(unsigned int value) {

    }

    void display(CNode *node, int level) {


        int i;
        if (node != NULL) {
            display(node->m_right, level + 1);
            cout << endl;
            if (node == m_root)
                cout << "Root -> ";
            for (i = 0; i < level && node != m_root; i++)
                cout << "        ";
            cout << node->m_value << " " << node->m_height;
            display(node->m_left, level + 1);
        }


    }



    unsigned int getMedian(int min, int max) {



        if (m_size == 0) return 0;






        //kontrola validity
        if (min > max) return 0;

        //kontrola zda se element nachazi v poli
        int check = select(m_root, m_size)->m_value;
        if (check < min) return 0;



        int minRank = rankMin(min);


        int maxRank = rankMax(max);





        if (minRank > maxRank || minRank == 0 || maxRank == 0) return 0;

        if(maxRank>m_size)
            maxRank=m_size;

        int diff = maxRank - minRank;

        int pos = (diff / 2);

        if (minRank == maxRank) return select(m_root, minRank)->m_value;

        return 0;

        return select(m_root, minRank + pos)->m_value;


        return 0;


    }




private:
    CNode *m_root;
    int m_size;

    int getHeight(CNode *node) {
        int heightTMP = 0;

        if (node != NULL) {

            int height_left = height(node->m_left);
            int height_right = height(node->m_right);



            int max = height_left>height_right?height_left:height_right;
            heightTMP = max + 1;

        }
        return heightTMP;


    }
    int height(CNode * node){
        if(node == NULL)
            return 0;
        return node->m_height;
    }




    /*
     * Method for selecting node by index
     * Implement by https://en.wikipedia.org/wiki/Order_statistic_tree
     *
     */

    CNode *select(CNode *node, int index) {

        //bottom of tree
        if (!node) return NULL;
        //starting index
        int l = 1;

        //size of left subtree gives us rank
        if (node->m_left != NULL)
            l = node->m_left->m_height + 1;


        if (l == index) {
            return node;
        }
            //moving left
        else if (index < l) {
            return select(node->m_left, index);
        }

            //moving right
        else {
            return select(node->m_right, index - l);
        }
    }

    /*
     * Method for finding minimum rank by value
     * Implement by https://en.wikipedia.org/wiki/Order_statistic_tree
     *
     */

    int rankMin(int value) {
        //default rank is 1 lowest node
        int r           = 1;
        //if we dont find minimum or valid value returns 0 -> not found
        int min         = 0;
        //distance between nodes - value
        int minDistance = 2000000000;

        CNode *tmp = m_root;

        while (tmp != NULL) {

            //distance between node and value
            int distance = abs(tmp->m_value - value);



            //if distance is <= we found better candidate to be the minimum
            if (distance <= minDistance) {

                //if we foundbetter distance
                minDistance = distance;
                //min rank we need is r + size of left subtree

                if (tmp->m_left != NULL)
                    min = r + tmp->m_left->m_height;
                else
                    min = r;

            }

            //moving to left
            if (value < tmp->m_value) {
                tmp = tmp->m_left;

            }
                //moving to right
            else if (value > tmp->m_value) {

                //adds size of the left subtree to overal rank - value will not be there
                //  cout << getHeightLeft(tmp->m_left) <<endl;

                if(tmp->m_left!=NULL)
                    r += 1+ tmp->m_left->m_height;
                else if(tmp->m_left==NULL)
                    r+=1;

                tmp = tmp->m_right;


            } else {
                //we found exact match if left tree is empty returns rank + 1
                //else returns rank +   sub tree left  height

                if(tmp->m_left==NULL) return r;
                return r + tmp->m_left->m_height;
            }


        }


        return min;

    }

    /*
     * Method for returning max possible rank
     */
    int rankMax(int value) {
        //default rank
        int r           = 1;
        //if we dont find maximum or valid value returns 0 -> not found
        int max         = 0;
        //distance between nodes - value
        int minDistance = 2000000000;
        CNode *tmp = m_root;

        while (tmp != NULL) {


            //pro maximum <=
            int distance = abs(tmp->m_value - value);


            if (distance <= minDistance) {
                //only valid values can be for maximum we need <=
                if (tmp->m_value <= value) {


                    //if we foundbetter distance
                    minDistance = distance;
                    //max rank we need is r + size of left subtree

                    if (tmp->m_left != NULL)
                        max = r + tmp->m_left->m_height;
                    else
                        max = r;

                }

            }


            if (value < tmp->m_value) {

                tmp = tmp->m_left;


            } else if (value > tmp->m_value) {

                //adds size of the left subtree to overal rank - value will not be there

                if(tmp->m_left!=NULL)
                    r += 1+ tmp->m_left->m_height;
                else if(tmp->m_left==NULL)
                    r+=1;
                tmp = tmp->m_right;

            } else {
                //we found exact match if left tree is empty returns rank + 1
                //else returns rank +   sub tree left  heigh

                if(tmp->m_left==NULL) return r;
                return r + tmp->m_left->m_height;
            }


        }


        return max;

    }


/*
 * Method for counting diffrenece betwen left and right side -> signum
 */
    int difference(CNode *node) {

      if(node==NULL)
         return 0;
          return height(node->m_left) - height(node->m_right);
    }


    /*
     * Method for rr rotation
     *
     */
    CNode *rr_rotation(CNode *node) {
        CNode *tmp;
        tmp = node->m_right;
        node->m_right = tmp->m_left;
        tmp->m_left   = node;

        // Update heights
     //   node->m_height = getHeight(node);

       // tmp->m_height = getHeight(tmp);


        return tmp;
    }

/*Method for ll rotation
 *
 */
    CNode *ll_rotation(CNode *node) {
        CNode *tmp;
        tmp = node->m_left;
        node->m_left = tmp->m_right;
        tmp->m_right = node;

     //   node->m_height=getHeight(tmp);
     //   tmp->m_height=getHeight(node);


        return tmp;
    }


/*Method for lr rotation
 *
 */
    CNode *lr_rotation(CNode *node) {
        CNode *tmp;
        tmp = node->m_left;
        node->m_left = rr_rotation(tmp);
        return ll_rotation(node);
    }

/*Method for rl rotation
 *
 */
    CNode *rl_rotation(CNode *node) {
        CNode *tmp;
        tmp = node->m_right;
        node->m_right = ll_rotation(tmp);
        return rr_rotation(node);
    }

    /*
     * Balacning tree check for signum then rotation
     */
    CNode *balance(CNode *node) {

        int signum = difference(node);
        cout << difference(node)<<endl;
        if (signum > 1) {
            cout << difference(node)<<endl;
            if (difference(node->m_left) > 0)
                node = ll_rotation(node);
            else
                node = lr_rotation(node);

        } else if (signum < -1) {
            cout << difference(node)<<endl;
            if (difference(node->m_right) > 0)
                node = rl_rotation(node);
            else
                node = rr_rotation(node);
        }


        return node;
    }

    /*
     * Method for inserting value into tree
     */
    CNode *insert(int value, CNode *node) {

        //Position where value is supposed to be
        if (!node) {
            node = new CNode(value);

            return node;
        }
        //moving left
        if (value < node->m_value) {

            node->m_left = insert(value, node->m_left);

        }
            //moving right
        else if (value > node->m_value) {
            node->m_right = insert(value, node->m_right);

        }

        node->m_height = getHeight(node);
        cout << node->m_height<<endl;

        balance(node);


        return node;


    }




    /*
     * Method for dealloc tree
     */
    void freeNode(CNode *node) {
        if (node == NULL)
            return;
        freeNode(node->m_left);
        freeNode(node->m_right);

        delete node;
    }


};


int main() {

    int command            = 0;
    int value              = 0;
    int rangeMin, rangeMax = 0;

    CTree tree;


    while (true) {
        value = 0;
        cin >> command;

        //End
        if (command == 4) {
            //   con.printFull();
            // cout << endl;

            return 0;
        }

        //Add value
        if (command == 1) {
            cin >> value;
            tree.add(value);
        }
            //Remove add
        else if (command == 2) {
            cin >> value;
            tree.remove(value);
        }
            //Median
        else if (command == 3) {
            cin >> rangeMin;
            cin >> rangeMax;


            int median = tree.getMedian(rangeMin, rangeMax);


            if (median == 0)
                cout << "notfound" << endl;
            else {

                cout << median << endl;
            }


        }


    }

    return 0;
}