#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <string>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <list>
#include <queue>
#include <cassert>
#include <cctype>
#include <set>

using namespace std;

class CPCB {
public:

    CPCB(void) {
    }

    ~CPCB(void) {
       
    }
    list<string> Short(const map<string , int > & a);

    void Add(const string & a, const string & b);
    void print();
private:
    map < string, set < string > > m_Map;
};

list<string> CPCB::Short(const map<string , int > & a) {
}

void CPCB::Add(const string& a, const string& b) {
    m_Map[a].insert(b);
    m_Map[b].insert(a);




}

void CPCB::print() {
    for (auto elem : m_Map) {
        cout << elem.first << endl;
    }



}

int main(void) {
    CPCB x;
    x.Add("A", "B");
    x.Add("B", "C");
    x.Add("X", "Y");
    x.Add("Y", "C");
    x.Add("Q", "Z");

    x.print();
     // zkrat - ruzne napeti
  assert( x.Short( map { {"A", 10}, {"C", 15} } ) == list {} ); 
  // projde - stejne napeti - vratim seznam (v jakemkoliv poradi)
  assert( x.Short( map { {"X", 10}, {"A", 10} } ) == list {"A", "B", "C", "Y", "X"} ); 
  // projde - stejne napeti u X a A a ruzne napeti na Q, ktere s ostatnimi neni spojene - vratim seznam (v jakemkoliv poradi)
  assert( x.Short( map { {"X", 10}, {"A", 10}, {"Q", 15} } ) == list {"A", "B", "C", "Y", "X"} ); 
    return 0;
    return 0;

    
}