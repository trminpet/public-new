#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
using namespace std;

class CDate {
public:
    //---------------------------------------------------------------------------------------------

    CDate(int y,
            int m,
            int d)
    : m_Year(y),
    m_Month(m),
    m_Day(d) {
    }
    //---------------------------------------------------------------------------------------------

    int Compare(const CDate & x) const {
        if (m_Year != x . m_Year)
            return m_Year - x . m_Year;
        if (m_Month != x . m_Month)
            return m_Month - x . m_Month;
        return m_Day - x . m_Day;
    }
    //---------------------------------------------------------------------------------------------

    int Year(void) const {
        return m_Year;
    }
    //---------------------------------------------------------------------------------------------

    int Month(void) const {
        return m_Month;
    }
    //---------------------------------------------------------------------------------------------

    int Day(void) const {
        return m_Day;
    }
    //---------------------------------------------------------------------------------------------

    friend ostream & operator<<(ostream & os,
            const CDate & x) {
        char oldFill = os . fill();
        return os << setfill('0') << setw(4) << x . m_Year << "-"
                << setw(2) << (int) x . m_Month << "-"
                << setw(2) << (int) x . m_Day
                << setfill(oldFill);
    }
    //---------------------------------------------------------------------------------------------
private:
    int16_t m_Year;
    int8_t m_Month;
    int8_t m_Day;
};
#endif /* __PROGTEST__ */
unsigned int ID_INSERT = 1;
const int ISSUED = 1;
const int ACCEPTED = 2;
const int DELETED = 3;
const int PAIRED = 4;


string toLower(const string & tmp) {
    string a = tmp;
    transform(a.begin(), a.end(), a.begin(), ::tolower);
    return a;
}

//Prevzata metoda na odstraneni white space z http://stackoverflow.com/questions/35301432/remove-extra-white-spaces-in-c 15.4.2017

string removeWhiteSpace(const string &input) {
    string output;
  
    unique_copy(input.begin(), input.end(), back_insert_iterator<string>(output),
            [](char a, char b) {
                return isspace(a) && isspace(b);
            });

            
            if(output.at(0) == ' ') output=output.substr(1,output.size());
            if(output.at(output.size()-1) == ' ') output.erase(output.size()-1);
           
    return output;
}

class CInvoice {
public:

     
    CInvoice(const CDate & date,
            const string & seller,
            const string & buyer,
            unsigned int amount,
            double VAT) : date(date) {

        this->seller = seller;
        this->buyer = buyer;
        this->amount = amount;
        this->vat = VAT;
        


    };
   

    CDate Date(void) const {
        return date;
    };

    string Buyer(void) const {
        return buyer;
    }

    string Seller(void) const {
        return seller;
    }

    int Amount(void) const {
        return amount;
    };

    double VAT(void) const {
        return vat;
    };
    string AlterBuyer() const {
        return buyerAlter;
    }
    string AlterSeller() const {
       
        return sellerAlter;
    }
    int State()const{
        return state;
    }
    void setState( const int tmp) const{
        
       const_cast<CInvoice*>(this)->helpSetState(tmp);
    };
    void helpSetState(const int tmp){
    
        this->state=tmp;
    };

    void setRealSeller(const string & tmp) {
        this->seller = tmp;
    };

    void setRealBuyer(const string & tmp) {
        this->buyer = tmp;
    };
    
    void setAlterSeller(const string & tmp){
        this->sellerAlter=tmp;
    }
     void setAlterBuyer(const string & tmp){
        this->buyerAlter=tmp;
    }
    unsigned int ID;
    //pretizeni operatu < pro insert/find

    bool operator<(const CInvoice &tmp) const {
        if (date.Year() == tmp.date.Year()) {
            if (date.Month() == tmp.date.Month()) {
                if (date.Day() == tmp.date.Day()) {

                    if (buyerAlter == tmp.buyerAlter) {
                        if (sellerAlter == tmp.sellerAlter) {
                            if (amount == tmp.amount) {
                                if (vat == tmp.vat) {
                                    return false;
                                }
                                return vat < tmp.vat;
                            }
                            return amount < tmp.amount;
                        }
                        return sellerAlter < tmp.sellerAlter;
                    }
                    return buyerAlter < tmp.buyerAlter;
                }
                return date.Day() < tmp.date.Day();
            }
            return date.Month() < tmp.date.Month();
        }
        return date.Year() < tmp.date.Year();
    }
private:
    CDate date;
    string buyer;
    string seller;
    int amount;
    double vat;
    string sellerAlter;
    string buyerAlter;
    int state;


};
//Trida pro uchovani originalnich informaci

class CCompany {
public:

    CCompany(const string & n, const string & alt) : name(n), alterName(alt) {
    }

    CCompany(const string &alt) : alterName(alt) {
        name = "";
    }

    bool operator<(const CCompany &tmp) const {
        return alterName < tmp.alterName;
    }



    string name, alterName;







};

class CSortOpt {
public:
    static const int BY_DATE = 0;
    static const int BY_BUYER = 1;
    static const int BY_SELLER = 2;
    static const int BY_AMOUNT = 3;
    static const int BY_VAT = 4;

    CSortOpt(void) {
    };

    CSortOpt & AddKey(int key,
            bool ascending = true) {
        
        if(!(key>5 || key <0))
        inputOrder.push_back(std::make_pair(key, ascending));

        return *this;


    }



    vector<pair<int, bool> > inputOrder;
private:
    bool order;
    int sortBy;


};



bool compare_by_buyer(const CInvoice& a, const CInvoice& b, bool order) {
    if (toLower(a.Buyer()) == toLower(b.Buyer()))
        return a.ID < b.ID;
    return !(toLower(a.Buyer()) < toLower(b.Buyer()))^order;

}

bool compare_by_seller(const CInvoice& a, const CInvoice& b, bool order) {
    if (toLower(a.Seller()) == toLower(b.Seller()))
        return a.ID < b.ID;
    return !(toLower(a.Seller()) < toLower(b.Seller()))^order;

}

bool compare_by_amount(const CInvoice& a, const CInvoice& b, bool order) {
    if (a.Amount() == b.Amount())
        return a.ID < b.ID;
    return !(a.Amount() < b.Amount())^order;

}

bool compare_by_vat(const CInvoice& a, const CInvoice& b, bool order) {
    if (a.VAT() == b.VAT())
        return a.ID < b.ID;
    return !(a.VAT() < b.VAT())^order;

}

class CVATRegister {
public:

    CVATRegister(void) {

    };

    bool RegisterCompany(const string & name) {
        string alterName = removeWhiteSpace(toLower(name));

       
        set<CInvoice> invoices;
        if(c.size()==0){
            c.insert(make_pair(CCompany(name,alterName),invoices));
        }
        else{
            if(!c.insert(make_pair(CCompany(name,alterName),invoices)).second)
                return false;
        
        }
        return true;
    };
    //pridani addIssued

    bool AddIssued(const CInvoice & x) {
        //kontrola zda firmy jsou validni
   
         if (removeWhiteSpace(toLower(x.Buyer())) == removeWhiteSpace(toLower(x.Seller()))) return false;
        map<CCompany,set<CInvoice> >::iterator iterSeller;
        map<CCompany,set<CInvoice> >::iterator iterBuyer;
        iterSeller = c.find(removeWhiteSpace(toLower(x.Seller())));
        iterBuyer = c.find(removeWhiteSpace(toLower(x.Buyer())));
     
        if(iterSeller==c.end()) return false;
        if(iterBuyer==c.end()) return false;
          CInvoice tmp = x;
            //pridani ID a nastaveni spravneho jmena
            tmp.ID = ID_INSERT++;
            tmp.setRealBuyer(iterBuyer->first.name);
            tmp.setRealSeller(iterSeller->first.name);
            tmp.setAlterBuyer(iterBuyer->first.alterName);
            tmp.setAlterSeller(iterSeller->first.alterName);
           
        if(iterSeller->second.size()==0){
            
            tmp.setState(ISSUED);
            iterSeller->second.insert(tmp);
            
        
        }
        else{
             set<CInvoice>::iterator iter = iterSeller->second.find(tmp);
             //nalezeni faktury
             if(iter!=iterSeller->second.end())
             {
                 //prace se stavem
                 switch(iter->State()){
                     case ACCEPTED: iter->setState(PAIRED);break;
                     case ISSUED: return false;
                     case DELETED: return false;
                     case PAIRED: return false;
                 
                 
                 }
             
             
             }
             else{
                 tmp.setState(ISSUED);
             iterSeller->second.insert(tmp);
             }
            
            
        }
            
        if(iterBuyer->second.size()==0){
        
       
           tmp.setState(ISSUED);
            iterBuyer->second.insert(tmp);
        }
        else{
        set<CInvoice>::iterator iter = iterBuyer->second.find(tmp);
             //nalezeni faktury
             if(iter!=iterBuyer->second.end())
             {
                 //prace se stavem faktury
                 switch(iter->State()){
                     case ACCEPTED: iter->setState(PAIRED);break;
                     case ISSUED: return false;
                     case DELETED: return false;
                     case PAIRED: return false;
                 
                 
                 }
             
             
             }
             else{
                 tmp.setState(ISSUED);
             iterBuyer->second.insert(tmp);
             }
        
        }


        return true;

    };

    bool AddAccepted(const CInvoice & x) {
        
        
        
        
         if (removeWhiteSpace(toLower(x.Buyer())) == removeWhiteSpace(toLower(x.Seller()))) return false;
        map<CCompany,set<CInvoice> >::iterator iterSeller;
        map<CCompany,set<CInvoice> >::iterator iterBuyer;
        iterSeller = c.find(removeWhiteSpace(toLower(x.Seller())));
        iterBuyer = c.find(removeWhiteSpace(toLower(x.Buyer())));
      
        if(iterSeller==c.end()) return false;
        if(iterBuyer==c.end()) return false;
          CInvoice tmp = x;
            //pridani ID a nastaveni spravneho jmena
            tmp.ID = ID_INSERT++;
            tmp.setRealBuyer(iterBuyer->first.name);
            tmp.setRealSeller(iterSeller->first.name);
            tmp.setAlterBuyer(iterBuyer->first.alterName);
            tmp.setAlterSeller(iterSeller->first.alterName);
            
        if(iterSeller->second.size()==0){
            tmp.setState(ACCEPTED);
            iterSeller->second.insert(tmp);
        
        }
        else{
            //nalezeni faktury a zjisteni stavu
            set<CInvoice>::iterator iter = iterSeller->second.find(tmp);
             //je tam
             if(iter!=iterSeller->second.end())
             {
                 //prace se stavem faktury
                 switch(iter->State()){
                     case ACCEPTED: return false;
                     case ISSUED: iter->setState(PAIRED);break;
                     case DELETED: return false;
                     case PAIRED: return false;
                 
                 
                 }
             
             
             }
             else{
                 tmp.setState(ACCEPTED);
             iterSeller->second.insert(tmp);
             }
        
        }
        if(iterBuyer->second.size()==0){
        
             tmp.setState(ACCEPTED);
           
            iterBuyer->second.insert(tmp);
        }
        else{
        set<CInvoice>::iterator iter = iterBuyer->second.find(tmp);
             //nalezeni faktury
             if(iter!=iterBuyer->second.end())
             {
                 //prace se stavem faktury
                 switch(iter->State()){
                     case ACCEPTED: return false;
                     case ISSUED: iter->setState(PAIRED);break;
                     case DELETED: return false;
                     case PAIRED: return false;
                 
                 
                 }
             
             
             }
             else{
                 tmp.setState(ACCEPTED);
             iterBuyer->second.insert(tmp);
             }
        
        }
        


        return true;
    };
    //Odstraneni z registru

    bool DelIssued(const CInvoice & x) {
        //nalezeni firmy
        if (removeWhiteSpace(toLower(x.Buyer())) == removeWhiteSpace(toLower(x.Seller()))) return false;
        map<CCompany,set<CInvoice> >::iterator iterSeller;
        map<CCompany,set<CInvoice> >::iterator iterBuyer;
      
       iterSeller = c.find(removeWhiteSpace(toLower(x.Seller())));
        iterBuyer = c.find(removeWhiteSpace(toLower(x.Buyer())));
       
        if(iterSeller==c.end()) return false;
        if(iterBuyer==c.end()) return false;
 
        if (iterSeller->second.size() == 0 || iterBuyer->second.size()==0) {
          
            return false;
        } else {
            //nalezeni faktury
              CInvoice tmp = x;
            
          
            tmp.setRealBuyer(iterBuyer->first.name);
            tmp.setRealSeller(iterSeller->first.name);
            tmp.setAlterBuyer(iterBuyer->first.alterName);
            tmp.setAlterSeller(iterSeller->first.alterName);
       
            auto del = iterSeller->second.find(tmp);
           
            if (del != iterSeller->second.end()) {
            //rozhodnuti dle stavu   
               switch(del->State()){
                     case ACCEPTED: return false;
                     case ISSUED: del->setState(DELETED); iterSeller->second.erase(del);break;
                    // case DELETED: return false;
                     case PAIRED: del->setState(ACCEPTED); break;
                 
                 
                 }
                
                
            } else
                return false;
            del= iterBuyer->second.find(tmp);
            if (del != iterBuyer->second.end()) {
             switch(del->State()){
                     case ACCEPTED: return false;
                     case ISSUED: del->setState(DELETED); iterBuyer->second.erase(del);break;
                    // case DELETED: return false;
                     case PAIRED: del->setState(ACCEPTED); break;
                 
                 
                 }
                
                
            } else
                return false;
        }
        return true;

    }
    //odstraneni faktury

    bool DelAccepted(const CInvoice & x) {
           //nalezeni firmy
         if (removeWhiteSpace(toLower(x.Buyer())) == removeWhiteSpace(toLower(x.Seller()))) return false;
        map<CCompany,set<CInvoice> >::iterator iterSeller;
        map<CCompany,set<CInvoice> >::iterator iterBuyer;
        iterSeller = c.find(removeWhiteSpace(toLower(x.Seller())));
        iterBuyer = c.find(removeWhiteSpace(toLower(x.Buyer())));
        
        if(iterSeller==c.end()) return false;
        if(iterBuyer==c.end()) return false;
        

        if (iterSeller->second.size() == 0 || iterBuyer->second.size()==0) {
            return false;
        } else {
  
            //hledani faktury
              CInvoice tmp = x;
           
          
            tmp.setRealBuyer(iterBuyer->first.name);
            tmp.setRealSeller(iterSeller->first.name);
            tmp.setAlterBuyer(iterBuyer->first.alterName);
            tmp.setAlterSeller(iterSeller->first.alterName);
            set<CInvoice>::iterator del;
            del = iterSeller->second.find(tmp);
            //rozhodnuti dle stavu
            if (del != iterSeller->second.end()) {
                 switch(del->State()){
                     case ACCEPTED: del->setState(DELETED); iterSeller->second.erase(del);break;
                     case ISSUED: return false; 
                    // case DELETED: return false;
                     case PAIRED: del->setState(ISSUED); break;
                 
                 
                 }
                
                
            } else
                return false;
            del= iterBuyer->second.find(tmp);
            if (del != iterBuyer->second.end()) {
                  //rozhodnuti dle stavu
               switch(del->State()){
                     case ACCEPTED: del->setState(DELETED); iterBuyer->second.erase(del);break;
                    case ISSUED: return false;
                    // case DELETED: return false;
                     case PAIRED: del->setState(ISSUED); break;
                 
                 
                 }
                
                
            } else
                return false;
        }
        return true;

    };
    //metoda na hledani unmatched faktur

    list<CInvoice> Unmatched(const string & company,
            const CSortOpt & sortBy) const  {


        //vyhledani faktur ktera nejsou sparovane
        list<CInvoice> res;
         string comp =removeWhiteSpace(toLower(company));
        map<CCompany,set<CInvoice> >::const_iterator iter = c.find(comp);
        
        if(iter==c.end())
            return res;
         
        
       for(auto f : iter->second) {
           if(f.State()==ISSUED || f.State()==ACCEPTED)
               res.push_back(f);
} 
        //defalutni razeni
        if (sortBy.inputOrder.empty() || sortBy.inputOrder.size() == 0) {
            res.sort([](const CInvoice & a, const CInvoice & b) {
                return a.ID < b.ID; });

            return res;
        }




        //razeni dle kriterie a pomoci lambda funkce
        res.sort([&sortBy](const CInvoice & a, const CInvoice & b) {

            //pole priorit
            for (int pos = 0; pos < (int) sortBy.inputOrder.size(); pos++) {


                switch (sortBy.inputOrder[pos].first) {

                        //razeni dle amount
                    case sortBy.BY_AMOUNT: if (a.Amount() == b.Amount()) {

                            break;
                        }

                        return compare_by_amount(a, b, sortBy.inputOrder[pos].second);
                        break;
                        //razeni dle buyer
                    case sortBy.BY_BUYER:
                        if (toLower(a.Buyer()) == toLower(b.Buyer())) {

                            break;
                        }
                        return compare_by_buyer(a, b, sortBy.inputOrder[pos].second);
                         break;
                        //razeni dle date
                    case sortBy.BY_DATE: if (a.Date().Year() == b.Date().Year()) {
                            if (a.Date().Month() == b.Date().Month()) {
                                if (a.Date().Day() == b.Date().Day()) {
                                    break;

                                }


                                return (bool) !((a.Date().Day() < b.Date().Day()) ^ sortBy.inputOrder[pos].second);
                                 break;
                            }
                            return (bool) !((a.Date().Month() < b.Date().Month()) ^ sortBy.inputOrder[pos].second);
                             break;
                        }
                        return (bool) !((a.Date().Year() < b.Date().Year()) ^ sortBy.inputOrder[pos].second);
                         break;
                        //razeni dle seller
                    case sortBy.BY_SELLER: if (toLower(a.Seller()) == toLower(b.Seller())) {

                            break;
                        }
                        return compare_by_seller(a, b, sortBy.inputOrder[pos].second);
                         break;
                        //razeni dle VAT
                    case sortBy.BY_VAT:
                        if (a.VAT() == b.VAT()) {

                            break;
                        }

                        return compare_by_vat(a, b, sortBy.inputOrder[pos].second);
                         break;

                    default: return a.ID < b.ID;
                        break;
                }



            }
            //serazeni dle ID
            return a.ID < b.ID;

        });
        return res;


    };

    void print() {
        map<CCompany, set<CInvoice> > ::iterator iter;
        for (iter = c.begin(); iter != c.end(); ++iter) {
            cout << iter->first.alterName << "x" << endl;
        }




    };

    void printReleased(string company) {
       

    };
    //private:
    map<CCompany, set<CInvoice> > c;
   
    


};

#ifndef __PROGTEST__

bool equalLists(const list<CInvoice> & a, const list<CInvoice> & b) {

       
    if (a.size() != b.size()) return false;
    std::list<CInvoice>::const_iterator ita = a.begin();
    std::list<CInvoice>::const_iterator itb = b.begin();

    for (; ita != a.end(); ++ita, ++itb) {
       
        if (ita->Amount() != itb->Amount() || ita->Buyer() != itb->Buyer() || ita->Seller() != itb->Seller() || ita->Date().Day() != itb->Date().Day() ||
                ita->Date().Month() != itb->Date().Month() || ita->Date().Year() != itb->Date().Year() || ita->VAT() != itb->VAT())return false;

    }

    return true;
}

int main(void) {
   CVATRegister r;
    assert(r . RegisterCompany("first Company"));
  
    assert(r . RegisterCompany("Second     Company"));
    assert(r . RegisterCompany("ThirdCompany, Ltd."));
    assert(r . RegisterCompany("Third Company, Ltd."));
    assert(!r . RegisterCompany("Third Company, Ltd."));
    assert(!r . RegisterCompany(" Third  Company,  Ltd.  "));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 100, 20)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 2), "FirSt Company", "Second Company ", 200, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 100, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 300, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "Second Company ", "First Company", 300, 30)));
    assert(r . AddIssued(CInvoice(CDate(2000, 1, 1), "Third  Company,  Ltd.", "  Second    COMPANY ", 400, 34)));
    assert(!r . AddIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company ", 300, 30)));
    assert(!r . AddIssued(CInvoice(CDate(2000, 1, 4), "First Company", "First   Company", 200, 30)));
    assert(!r . AddIssued(CInvoice(CDate(2000, 1, 4), "Another Company", "First   Company", 200, 30)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, false) . AddKey(CSortOpt::BY_DATE, false)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
    }));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_DATE, true) . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000)
    }));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_VAT, true) . AddKey(CSortOpt::BY_AMOUNT, true) . AddKey(CSortOpt::BY_DATE, true) . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
    }));
    assert(equalLists(r . Unmatched("First Company", CSortOpt()),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000)
    }));
    assert(equalLists(r . Unmatched("second company", CSortOpt() . AddKey(CSortOpt::BY_DATE, false)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Third Company, Ltd.", "Second     Company", 400, 34.000000)
    }));
    assert(equalLists(r . Unmatched("last company", CSortOpt() . AddKey(CSortOpt::BY_VAT, true)),
            list<CInvoice>{
    }));
    assert(r . AddAccepted(CInvoice(CDate(2000, 1, 2), "First Company", "Second Company ", 200, 30)));
    assert(r . AddAccepted(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
    assert(r . AddAccepted(CInvoice(CDate(2000, 1, 1), "Second company ", "First Company", 300, 32)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true) . AddKey(CSortOpt::BY_DATE, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
    }));
    assert(!r . DelIssued(CInvoice(CDate(2001, 1, 1), "First Company", "Second Company ", 200, 30)));
    assert(!r . DelIssued(CInvoice(CDate(2000, 1, 1), "A First Company", "Second Company ", 200, 30)));
    assert(!r . DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Hand", 200, 30)));
    assert(!r . DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company", 1200, 30)));
    assert(!r . DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", "Second Company", 200, 130)));
    assert(r . DelIssued(CInvoice(CDate(2000, 1, 2), "First Company", "Second Company", 200, 30)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true) . AddKey(CSortOpt::BY_DATE, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
    }));
    assert(r . DelAccepted(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true) . AddKey(CSortOpt::BY_DATE, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Third Company, Ltd.", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
    }));
    assert(r . DelIssued(CInvoice(CDate(2000, 1, 1), "First Company", " Third  Company,  Ltd.   ", 200, 30)));
    assert(equalLists(r . Unmatched("First Company", CSortOpt() . AddKey(CSortOpt::BY_SELLER, true) . AddKey(CSortOpt::BY_BUYER, true) . AddKey(CSortOpt::BY_DATE, true)),
            list<CInvoice>{
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 20.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 100, 30.000000),
        CInvoice(CDate(2000, 1, 1), "first Company", "Second     Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 2), "first Company", "Second     Company", 200, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 30.000000),
        CInvoice(CDate(2000, 1, 1), "Second     Company", "first Company", 300, 32.000000)
    }));
    return 0;
    

}
#endif /* __PROGTEST__ */