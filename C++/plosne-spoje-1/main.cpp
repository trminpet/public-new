#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <string>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <list>
#include <queue>
#include <cassert>
#include <cctype>
#include <set>
#include <queue>

using namespace std;

class CPCB {
public:

    CPCB(void){};

    ~CPCB(void) {
        
    }
    bool Add(const string & a, const string &b);
    bool Del(const string & a, const string & b);
    void print(const string & a)const;

    list<string> Short(const string & from,
            const string & to);


private:
    multimap<string, string> m_pEdges;
    map<string,set<string> > m_Graph;
};

list<string> CPCB::Short(const string & from,
        const string & to) {
    std::list<string> ret;
  if (m_Graph.find(from) == m_Graph.end() || m_Graph.find(to) == m_Graph.end())
      return ret;
		
			queue<std::pair<string, int>> q;	// fronta se vzdalenosti
			set<string> visited;				// byl jsem tam?
			map<string, string> pred; // kam -> odkud
			
			q.push(make_pair(from, 0));
			visited.insert(from);

			while(q.empty() == false)
			{	
				// T current; current = q.front().first /* nebude fungovat s T, ktery nema bezparam. konstruktor */
				string current = q.front().first;
				int dest = q.front().second;
				q.pop();
				
				
				if (current == to)
				{
					
					string cnode = to;
					
					while(cnode != from)
					{
						ret.push_front(cnode);
						cnode = pred.at(cnode);
					}
					ret.push_front(cnode);
					
					return ret;
				}
				
				for(auto i = m_Graph.at(current).begin() ; i != m_Graph.at(current).end() ; ++i )
				/* nebo for (const T& i : m_Map.at(current), pote uz ale nedereferencuji i (c++11) */
				{
					if (visited.count(*i) == 0)
					{
						visited.insert(*i);
						q.push(make_pair(*i, dest + 1));
						// pred[*i] = current; /* Nebude fungovat s typem T, ktery nema bezparam. konstruktor, protoze pred[*i] nejdriv vytvori prvek pomoci bezparam. konstruktoru */
						pred.insert(make_pair(*i, current));
					}
				}
			}
                        return ret;

}

bool CPCB::Add(const string& a, const string& b){
    m_Graph[a].insert(b);
    m_Graph[b].insert(a);
    return true;


}
bool CPCB::Del(const string& a, const string& b){
    m_Graph[a].erase(b);
    m_Graph[b].erase(a);
    return true;
}
void CPCB::print(const string &a) const{
    for (auto elem : m_Graph.at(a)) {
        cout << elem<<endl;
    }


}

int main(void) {

    CPCB c;
    list<string> a;
    c.Add("a","b");
    c.Add("a","c");
    
    c.Add("b","e");
    c.Add("e","g");
    //c.Del("b","e");
    c.Add("g","h");
    a = c.Short("a","h");
    for (auto elem : a) {
        cout<< elem << endl;
    }

    


}
