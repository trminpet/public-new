#include "CCardBonus.h"
#include "CCard.h"
#include "../Ship/CShip.h"
#include <ncurses.h>

CCardBonus::CCardBonus(string desc, int energy, int type) : CCard(desc, energy), m_Type(type) {
}

CCardBonus::CCardBonus(const CCardBonus &orig) {
}

CCardBonus::~CCardBonus() {
}

/**
 * Function for applying card. Returns false when ship does not have  enough resources to use card. True  card has been applied
 * Modifies properties of ships
 *  
 * @param  a,(CShip &), b(CShip &)
 * @return bool
 */
bool CCardBonus::applyCard(CShip &a, CShip &b) {

    switch (m_Type) {

        case ADD_VALUE_AE:
            if (a.getEnergy() < m_EnergyPrice) return false;
            a.setAttackEng(a.getAttackEng() + ADD_VALUE);
            break;
        case ADD_VALUE_DE:
            if (a.getEnergy() < m_EnergyPrice) return false;
            a.setDeffendEng(a.getDeffendEng() + ADD_VALUE);
            break;
        case ADD_HALFVALUE_AEDE:

            if (a.getEnergy() < m_EnergyPrice) return false;
            a.setDeffendEng(a.getDeffendEng() + ADD_VALUE / 2);
            a.setAttackEng(a.getAttackEng() + ADD_VALUE / 2);
            break;

        case EMP:
            if (a.getEnergy() < m_EnergyPrice) return false;
            b.setStunned(true);
            break;

        case DOUBLE_AGENT:
            if (a.getEnergy() < m_EnergyPrice) return false;
            a.setAttackPoints(b.getAttackPoints() + a.getAttackPoints());
            b.setAttackPoints(0);
            break;
        case SUPPORT:

            if (a.getEnergy() < m_EnergyPrice) return false;
            a.setAttackPoints(a.getAttackPoints() + 2 * ADD_VALUE);
            a.setDeffendEng(a.getDeffendPoints() + 2 * ADD_VALUE);
            break;
        case OVERLOAD:
            if (a.getEnergy() < m_EnergyPrice) return false;
            a.setEnergy(a.ENERGY);
            break;


    }
    a.setEnergy(a.getEnergy() - m_EnergyPrice);
    CCard::applyCard(a, b);
    return true;


}

/**
 * Method for printing properties of card
 *  
 * @param  x(int)
 * @return
 */
void CCardBonus::print(int x) const {
    CCard::print(x);
    mvprintw(19, x, "%s", m_Description.c_str());
    mvprintw(20, x, "%s %d", "Energy:", m_EnergyPrice);

}

/**
 * Function for AI. Returns true/false depends on playable
 * 
 *  
 * @param  a,(CShip &)
 * @return bool
 */
bool CCardBonus::isPlayable(const CShip &a) const {
    if (a.getEnergy() < m_EnergyPrice) return false;
    return true;

}

