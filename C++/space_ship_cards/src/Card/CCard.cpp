#include "../Player/CPlayer.h"
#include "../File/CFileIO.h"
#include "../Card/CCard.h"
#include "CCardAttack.h"
#include "CCardBonus.h"
#include "CCardDeffense.h"
#include "../Ship/CShip.h"
#include <iostream>
#include <string>
#include <ncurses.h>

using namespace std;


CCard::CCard() {}

CCard::CCard(string desc, int energy) : m_Description(desc), m_EnergyPrice(energy) {
}

CCard::CCard(const CCard &orig) {

}

/**
 * Destructor for dynamically allocated cards
 *  
 * @param  
 * @return int
 */
CCard::~CCard() {

    for (int i = 0; i < (int) m_Deck.size(); i++) {
        delete m_Deck[i];

    }


}

/**
 * Function for printing last played card in game. In subclasses also applies its properties
 *  
 * @param  
 * @return bool
 */
bool CCard::applyCard(CShip &a, CShip &b) {
    mvprintw(16, 0, "%s",
             "                                                                                                                 ");
    mvprintw(16, (65 - m_Description.length()) / 2, "LAST PLAYED CARD %s", m_Description.c_str());
    return true;


}

/**
 * Method for loading cards properties from file 
 *  
 * @param  
 * @return void
 */
void CCard::loadCards() {

    CFileIO         f;
    vector <string> c;

    m_DeckParse = f.readFile(m_CARDS_FILE);
    for (int i = 2; i < (int) m_DeckParse.size() - 1; i++) {
        c = f.parseFile(m_DeckParse.at(i));
        makeCard(c);

    }
    m_DeckParse.clear();
    m_DeckParse = f.readFile(m_BONUS_FILE);
    for (int i = 2; i < (int) m_DeckParse.size() - 1; i++) {
        c = f.parseFile(m_DeckParse.at(i));
        makeCard(c);
    }


}

/**
 * Method for creating card with properties and type
 *  
 * @param  tmp (const vector<string> &
 * @return
 */
void CCard::makeCard(const vector <string> &tmp) {

    m_Card = NULL;

    string typeOfCard = tmp.at(0);
    string desc       = tmp.at(1);
    int    energy     = stoi(tmp.at(2));
    int    type, mp, dp, a, d, id;
    if (typeOfCard == "B") {
        type = stoi(tmp.at(3));
        id   = stoi(tmp.at(4));
    } else {
        mp = stoi(tmp.at(3));
        dp = stoi(tmp.at(4));
        a  = stoi(tmp.at(5));
        d  = stoi(tmp.at(6));
        id = stoi(tmp.at(7));
    }


    if (typeOfCard == "A") {
        m_Card = new CCardAttack(desc, energy, a, mp);


    } else if (typeOfCard == "D") {
        m_Card = new CCardDeffense(desc, energy, d, dp);


    } else if (typeOfCard == "B") {
        m_Card = new CCardBonus(desc, energy, type);


    }
    m_Card->m_ID = id;
    m_Deck.push_back(m_Card);


}

/**
 * Method for printing card properties and position in deck
 *  
 * @param  x (int)
 * @return 
 */
void CCard::print(int x) const {

    mvprintw(18, x, "%d%s", (x / 13) + 1, "______________");
    mvprintw(19, x, "%s", "             ");
    mvprintw(20, x, "%s", "             ");
    mvprintw(21, x, "%s", "             ");
    mvprintw(22, x, "%s", "             ");


}

/**
 * Function getting Card from deck at pos
 *  
 * @param  pos (int)
 * @return CCard *
 */

CCard *CCard::getCard(int pos) const {
    return m_Deck[pos];
}

/**
 * Function for AI. Provides information about properties of ship and card
 *  
 * @param  a (CShip &)
 * @return bool
 */
bool CCard::isPlayable(const CShip &a) const {

    return true;
}

/**
 * Function getting Cards ID in deck
 *  
 * @param  
 * @return int
 */
int CCard::getID() const {
    return m_ID;
}









