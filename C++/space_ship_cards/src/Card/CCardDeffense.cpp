#include "CCardDeffense.h"
#include "CCard.h"
#include "../Ship/CShip.h"
#include <ncurses.h>

CCardDeffense::CCardDeffense(string desc, int energy, int deff, int dp) : CCard(desc, energy), m_DEFF(deff), m_DP(dp) {
}

CCardDeffense::CCardDeffense(const CCardDeffense &orig) {
}

CCardDeffense::~CCardDeffense() {
}

/**
 * Function for applying card. Returns false when ship does not have  enough resources to use card. True  card has been applied
 * Modifies properties of ships
 *  
 * @param  a,(CShip &), b(CShip &)
 * @return bool
 */
bool CCardDeffense::applyCard(CShip &a, CShip &b) {

    if (a.getEnergy() < m_EnergyPrice || a.getDeffendPoints() < m_DP) return false;

    CCard::applyCard(a, b);
    a.setEnergy(a.getEnergy() - m_EnergyPrice);
    a.setDeffendPoints(a.getDeffendEng() - m_DP);
    a.setHP(a.getHP() + m_DEFF);
    return true;

}

/**
 * Method for printing properties of card
 *  
 * @param  x(int)
 * @return
 */
void CCardDeffense::print(int x) const {
    CCard::print(x);
    mvprintw(19, x, "%s", m_Description.c_str());
    mvprintw(20, x, "%s %d", "Energy:", m_EnergyPrice);
    mvprintw(21, x, "%s %d", "DP:", m_DP);
    mvprintw(22, x, "%s %d", "DEFF:", m_DEFF);

}

/**
 * Function for AI. Returns true/false depends on playable
 * 
 *  
 * @param  a,(CShip &)
 * @return bool
 */

bool CCardDeffense::isPlayable(const CShip &a) const {
    if (a.getEnergy() < m_EnergyPrice || a.getDeffendPoints() < m_DP) return false;
    return true;

}

