#ifndef CCARDBONUS_H
#define CCARDBONUS_H

#include <string>
#include "CCard.h"
#include "../Ship/CShip.h"

using namespace std;

/**
 * CCardBonus is  subclass. Tts super class is CCard
 * 
 *
 */
class CCardBonus : public CCard {
public:
    CCardBonus(string desc, int energy, int type);

    CCardBonus(const CCardBonus &orig);

    virtual ~CCardBonus();

    virtual bool applyCard(CShip &a, CShip &b);

    virtual bool isPlayable(const CShip &a) const;

    virtual void print(int x) const;

private:
    int              m_Type;
    int              ADD_VALUE          = 4;
    static const int ADD_VALUE_AE       = 0;
    static const int ADD_VALUE_DE       = 1;
    static const int ADD_HALFVALUE_AEDE = 2;
    static const int EMP                = 3;
    static const int DOUBLE_AGENT       = 4;
    static const int SUPPORT            = 5;
    static const int OVERLOAD           = 6;

};

#endif /* CCARDBONUS_H */

