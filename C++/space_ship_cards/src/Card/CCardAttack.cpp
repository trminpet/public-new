#include "CCardAttack.h"
#include "CCard.h"
#include "../Ship/CShip.h"
#include <ncurses.h>

CCardAttack::CCardAttack(string desc, int energy, int dmg, int mp) : CCard(desc, energy), m_DMG(dmg), m_MP(mp) {
}

CCardAttack::CCardAttack(const CCardAttack &orig) {
}

CCardAttack::~CCardAttack() {

}

/**
 * Function for applying card. Returns false when ship does not have  enough resources to use card. True  card has been applied
 * Modifies properties of ships
 *  
 * @param  a(CShip &), b(CShip &)
 * @return bool
 */
bool CCardAttack::applyCard(CShip &a, CShip &b) {
    if (a.getEnergy() < m_EnergyPrice || a.getAttackPoints() < m_MP) return false;

    CCard::applyCard(a, b);
    a.setEnergy(a.getEnergy() - m_EnergyPrice);
    a.setAttackPoints(a.getAttackPoints() - m_MP);
    b.setHP(b.getHP() - m_DMG);

    return true;


}

/**
 * Method for printing properties of card
 *  
 * @param  x(int)
 * @return
 */
void CCardAttack::print(int x) const {
    CCard::print(x);

    mvprintw(19, x, "%s", m_Description.c_str());
    mvprintw(20, x, "%s %d", "Energy:", m_EnergyPrice);
    mvprintw(21, x, "%s %d", "MP:", m_MP);
    mvprintw(22, x, "%s %d", "DMG:", m_DMG);

}

/**
 * Function for AI. Returns true/false depends on playable
 * 
 *  
 * @param  a(CShip &)
 * @return bool
 */

bool CCardAttack::isPlayable(const CShip &a) const {
    if (a.getEnergy() < m_EnergyPrice || a.getAttackPoints() < m_MP) return false;
    return true;

}

