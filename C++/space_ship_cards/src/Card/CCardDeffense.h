#ifndef CCARDDEFFENSE_H
#define CCARDDEFFENSE_H

#include <string>
#include "CCard.h"
#include "../Ship/CShip.h"

using namespace std;

/**
 * CCardDeffense is  subclass. Tts super class is CCard
 * 
 *
 */
class CCardDeffense : public CCard {
public:
    CCardDeffense(string desc, int energy, int deff, int dp);

    CCardDeffense(const CCardDeffense &orig);

    virtual ~CCardDeffense();

    virtual bool applyCard(CShip &a, CShip &b);

    virtual void print(int x) const;

    virtual bool isPlayable(const CShip &a) const;


private:
    int m_DEFF;
    int m_DP;

};

#endif /* CCARDDEFFENSE_H */

