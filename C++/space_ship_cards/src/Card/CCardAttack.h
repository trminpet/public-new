#ifndef CCARDATTACK_H
#define CCARDATTACK_H

#include <string>
#include "CCard.h"
#include "../Ship/CShip.h"

using namespace std;

/**
 * CCardAttack is  subclass. Tts super class is CCard
 * 
 *
 */

class CCardAttack : public CCard {
public:
    CCardAttack(string desc, int energy, int dmg, int mp);

    CCardAttack(const CCardAttack &orig);

    virtual ~CCardAttack();

    virtual bool applyCard(CShip &a, CShip &b);

    virtual bool isPlayable(const CShip &a)const;

    virtual void print(int x)const;

private:
    int m_DMG;
    int m_MP;

};

#endif /* CCARDATTACK_H */

