#ifndef CCARD_H
#define CCARD_H

#include <string>
#include <vector>
#include <iostream>
#include "../Ship/CShip.h"

using namespace std;

/**
 * CCard class is super class for three sub class (Attack,Deffense and Bonus) Card. It provides common properties and methods
 * 
 *
 */
class CCard {
public:
    CCard();

    CCard(string desc, int energy);

    CCard(const CCard &orig);

    virtual ~CCard();

    virtual bool applyCard(CShip &a, CShip &b);

    void loadCards();

    void makeCard(const vector <string> &tmp);

    CCard *getCard(int pos)const;

    virtual void print(int x)const;

    void printDeck() const;

    virtual bool isPlayable(const CShip &a)const;

    int getID() const;

protected:
    string m_Description;
    int    m_EnergyPrice, m_ID;


private:


    CCard *m_Card;
    vector <string> m_DeckParse;
    vector<CCard *> m_Deck;
    const string    m_CARDS_FILE = "src/cards/cards.txt";
    const string    m_BONUS_FILE = "src/cards/bonus.txt";


};

#endif /* CCARD_H */
