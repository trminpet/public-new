#ifndef CCONSOLE_H
#define CCONSOLE_H

#include <ncurses.h>

#include "../Player/CPlayer.h"

/**
 * CConsole class provides GUI and input for user. 
 * Also operates with loading,saving player
 * @author Petr Trminek <trminpet>
 */
class CConsole {
public:
    CConsole();

    ~CConsole();

    int menu();

    void newGame();

    void showWinner(int winner) const;

    void exitGame(bool s) const;

    void refreshScreen() const;

    void init();

    int inputCard();

    int inputSpace();

    void clearCards() const;

    void emptyFile() ;

    void printText(int y, int type);

    int          xMax, yMax;
    const string m_SwitchPlayers   = "Press 2xSPACE to switch players or q to save and quit";
    const string m_EnoughResources = "NOT ENOUGH RESOURCES";
    const string m_PlayerOne       = "PLAYER ONE IS PLAYING";
    const string m_PlayerTwo       = "PLAYER TWO IS PLAYING";
    const int    KEY_CONFIRM       = ' ';
    const int    KEY_QUIT          = 'q';
    const int    KEY_SKIP          = 'x';

private:

    void loadGame();



    void printName()const;

    void clearText()const;

    void printUI() ;

    void printShipA() const;

    void printShipB() const;

    void exitGameSupp() const;

    WINDOW *win, *playerX, *playerY;


};

#endif /* CCONSOLE_H */

