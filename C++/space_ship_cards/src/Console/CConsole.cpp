#include "CConsole.h"
#include <string>
#include <ncurses.h>

using namespace std;

CConsole::CConsole() {
}

CConsole::~CConsole() {
    delwin(win);
    delwin(playerX);
    delwin(playerY);
}


/**
 * Method for showing winner graphicly . 
 * @param  winner (int)
 * @return void
 */
void CConsole::showWinner(int winner) const {
    clearText();
    string mesg;
    string mesgEnd = "Press q to exit";
    if (winner == 1)
        mesg = "The winner is player one";
    else
        mesg = "The winner is player two";
    mvprintw(yMax / 2, (xMax - mesg.length()) / 2, "%s", mesg.c_str());
    mvprintw((yMax + 4) / 2, (xMax - mesgEnd.length()) / 2, "%s", mesgEnd.c_str());
    refresh();

    while (true) {
        int c = getch();
        if (c == KEY_QUIT)
            exitGame(true);
    }


}

/**
 * Method for printing game name in menu 
 * @param  
 * @return void
 */
void CConsole::printName() const {
    clearText();
    clear();
    box(win, 0, 0);

    printw("                  _______  _______  _______  _______  _______ \n");
    printw("                 (  ____ \\(  ____ )(  ___  )(  ____ \\(  ____ \\\n");
    printw("                 | (    \\/| (    )|| (   ) || (    \\/| (    \\/\n");
    printw("                 | (_____ | (____)|| (___) || |      | (__    \n");
    printw("                 (_____  )|  _____)|  ___  || |      |  __)   \n");
    printw("                       ) || (      | (   ) || |      | (      \n");
    printw("                 /\\____) || )      | )   ( || (____/\\| (____/\\\n");
    printw("                 \\_______)|/       |/     \\|(_______/(_______/\n");
    printw("                  _______  _______  _______  ______   _______\n");
    printw("                 (  ____ \\(  ___  )(  ____ )(  __  \\ (  ____ \\\n");
    printw("                 | (    \\/| (   ) || (    )|| (  \\  )| (    /\n");
    printw("                 | |      | (___) || (____)|| |   ) || (_____ \n");
    printw("                 | |      |  ___  ||     __)| |   | |(_____  )\n");
    printw("                 | |      | (   ) || (\\ (   | |   ) |      ) |\n");
    printw("                 | (____/\\| )   ( || ) \\ \\__| (__/  )/\\____) |\n");
    printw("                 (_______/|/     \\||/   \\__/(______/ \\_______)\n");
    refresh();
    move(0, 0);
}

/**
 * Method for clearing console window 
 * @param  
 * @return void
 */
void CConsole::clearText() const {
    system("clear");
}

/**
 * Method for proper clean ncurses
 * @param  
 * @return void
 */
void CConsole::exitGameSupp() const  {

    clear();
    endwin();

}

/**
 * Method for init console with ncurses 
 * @param  
 * @return void
 */
void CConsole::init() {
    initscr();

    noecho();
    cbreak();
    curs_set(0);

    getmaxyx(stdscr, yMax, xMax);
    win = newwin(7, xMax - 12, yMax - 7, 5);

    wrefresh(win);
    keypad(win, true);


}

/**
 * Method for printing UI (names of ship properties)
 * used source code from https://www.youtube.com/watch?v=3YiPdibiQHA&t=449s
 * Date 03.5.2017 Petr Trminek
 * @param  
 * @return void
 */

void CConsole::printUI()  {
    clearText();
    playerX = newwin(9, 15, 0, 0);
    string choices[7] = {"HP", "ENERGY", "ATTACK ENG", "MP", "DEFFEND ENG", "DP", "STUNNED"};

    for (int i = 0; i < 7; i++) {

        mvwprintw(playerX, i, 0, choices[i].c_str());

    }
    playerY = newwin(9, 15, 0, xMax - 11);

    for (int i = 0; i < 7; i++) {

        mvwprintw(playerY, i, 0, choices[i].c_str());

    }

    wrefresh(playerX);
    wrefresh(playerY);
    printShipA();
    printShipB();
    refresh();

}

/**
 * Method for printing UI (names of ship properties)
 * used source code from https://www.youtube.com/watch?v=3YiPdibiQHA&t=449s
 * Date 03.5.2017 Petr Trminek
 * @param  
 * @return void
 */

void CConsole::newGame()  {
    printUI();

}

/**
 * Method for exiting the game (menu and during the game)
 * @param  s (bool)
 * @return void
 */

void CConsole::exitGame(bool s) const {
    if (s) {
        exitGameSupp();
        throw 0;
    }
    int response = 0;
    mvprintw(yMax - 8, 6, "Do you really want to quit? [y/n]");
    refresh();
    response = getch();

    if (response == 'y') {

        exitGameSupp();
        throw 0;
    }
    clrtoeol();
    move(0, 0);
    refresh();


}

/**
 * Method for printing menu of the game
 * used source code from https://www.youtube.com/watch?v=3YiPdibiQHA&t=449s
 * @param  s (bool)
 * @return void
 */
int CConsole::menu() {
    printName();
    string choices[4] = {"NEW GAME TWO PLAYERS", "NEW GAME AI", "LOAD GAME", "EXIT GAME"};
    int    choice     = 0, highlight = 0;
    bool   show       = true;
    while (true) {
        for (int i = 0; i < 4 && show; i++) {
            if (i == highlight)
                wattron(win, A_REVERSE);

            mvwprintw(win, i + 1, 1, choices[i].c_str());

            wattroff(win, A_REVERSE);

        }
        choice = wgetch(win);
        switch (choice) {

            case KEY_UP:
                highlight--;
                if (highlight == -1)
                    highlight = 0;
                break;
            case KEY_DOWN:
                highlight++;
                if (highlight == 4)
                    highlight = 3;
                break;

            default:
                break;


        }
        if (choice == KEY_CONFIRM)
            break;


    }

    return highlight;

}

/**
 * Method for refreshing the console screen 
 * @param  
 * @return void
 */
void CConsole::refreshScreen() const {
    refresh();

}

/**
 * Method for printing ship A
 * @param  
 * @return void
 */
void CConsole::printShipA() const {

    mvprintw(8, 10, "-----");
    mvprintw(9, 10, "| x  \\");
    mvprintw(10, 10, "|-------->____Y___");
    mvprintw(11, 10, "0__________________\\");

}

/**
 * Method for printing ship B
 * @param  
 * @return void
 */
void CConsole::printShipB() const {

    mvprintw(8, xMax - 30, "               -----");
    mvprintw(9, xMax - 30, "              /  x |");
    mvprintw(10, xMax - 30, " ___Y____<---------|");
    mvprintw(11, xMax - 30, "/__________________0");

}

/**
 * Method for reading input from user. Returns card position or skip turn
 * @param  
 * @return int
 */
int CConsole::inputCard() {
    int input = 0;

    while (true) {

        input = getchar();

        switch (input) {
            case '1':
                return 0;
            case '2':
                return 1;
            case '3':
                return 2;
            case '4':
                return 3;
            case '5':
                return 4;
            case '6':
                return 5;
            case 'x':
                return KEY_SKIP;
            default:
                break;

        }

    }

}

/**
 * Method for reading input from user. Returns confirm or quit game
 * @param  
 * @return int
 */
int CConsole::inputSpace() {
    int input = 0;
    while (true) {
        input = getchar();
        if (input == KEY_CONFIRM)
            return KEY_CONFIRM;
        if (input == KEY_QUIT)
            return KEY_QUIT;

    }


}

/**
 * Method for clearing part of console where are cards to prevent nonsense 
 * @param  
 * @return 
 */
void CConsole::clearCards() const {

    mvprintw(19, 0, "%s",
             "                                                                                                                        ");
    mvprintw(20, 0, "%s",
             "                                                                                                                        ");
    mvprintw(21, 0, "%s",
             "                                                                                                                        ");
    mvprintw(22, 0, "%s",
             "                                                                                                                        ");
    refreshScreen();


}

/**
 * Method for printing text based on position and type
 * @param  y (int), type (int)
 * @return int
 */
void CConsole::printText(int y, int type) {

    mvprintw(y, 0, "%s", "                                                                                           ");
    string msg;
    switch (type) {
        case 0:
            msg = m_EnoughResources;
            break;
        case 1:
            msg = m_PlayerOne;
            break;
        case 2:
            msg = m_PlayerTwo;
            break;
        case 3:
            msg = m_SwitchPlayers;
            break;
        default:
            msg = " ";
            break;

    }
    mvprintw(y, (xMax - msg.length()) / 2, "%s", msg.c_str());
    refreshScreen();


}

/**
 * Method for information about empty file while loading game
 * 
 * @param  
 * @return int
 */
void CConsole::emptyFile() {
    mvprintw(yMax - 8, 6, "None saved game");
    refreshScreen();


}