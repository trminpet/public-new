#include "../Player/CPlayer.h"
#include "../Deck/CDeck.h"
#include "../Card/CCard.h"
#include "CFileIO.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>


using namespace std;

CFileIO::CFileIO() {}

/**
 * Method which reads file by lines and saves it into vector
 * @param file (const string &)
 * @throws string
 * @return vector <string> &
 */
vector <string> &CFileIO::readFile(const string &file) {

    m_content.clear();
    m_f.open(file);
    if (m_f.is_open()) {
        while (getline(m_f, m_line)) {
            m_content.push_back(m_line);
        }
        m_f.close();
    } else throw 1;

    return m_content;


}

/**
 * Method which parse string by separator ' ' from vector into characters
 * @param file (const string &)
 * @return vector <string> &
 */
vector <string> CFileIO::parseFile(const string &tmp) {

    string          res;
    vector <string> c;
    istringstream   iss(tmp);
    while (getline(iss, res, ' ')) {
        c.push_back(res);
    }
    return c;


}

/**
 * Method for saving CPlayer instance into file.
 * @param orig(CPlayer & ), file (const string &), 
 * @return void
 */
void CFileIO::writeFile(CPlayer &orig, const string &file) {


    CShip &ship = orig.getShip();
    CDeck &deck = orig.getDeck();
    m_off.open(file);
    m_off << orig.getAI() << endl;
    m_off << orig.m_Turn << endl;
    m_off << ship.getHP() << endl;
    m_off << ship.getEnergy() << endl;
    m_off << ship.getAttackEng() << endl;
    m_off << ship.getAttackPoints() << endl;
    m_off << ship.getDeffendEng() << endl;
    m_off << ship.getDeffendPoints() << endl;
    m_off << ship.getStunned() << endl;
    for (int i = 0; i < 6; i++) {
        m_off << deck.getCard(i)->getID() << endl;
    }

    m_off.close();


}

