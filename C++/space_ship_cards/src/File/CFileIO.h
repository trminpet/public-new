#ifndef CFILEIO_H
#define CFILEIO_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include "../Player/CPlayer.h"

using namespace std;

/**
 * Class which provides support for I/O operation. Methods cooperates with CPlayer and CCard class
 *
 */
class CFileIO {
public:
    CFileIO();

    vector <string> &readFile(const string &file);

    vector <string> parseFile(const string &tmp);

    void writeFile(CPlayer &orig, const string &file);

private:

    fstream         m_f;
    ofstream        m_off;
    string          m_line;
    vector <string> m_content;


};

#endif /* CFILEIO_H */

