# README - Install #

# Card Space Cards - Assignment #

Program the RPG card engine

## Your engine ##


* Inserts the definition of cards (their type, attributes, effects), package (list of cards in the package)
* Allows you to create and play the card game itself
* It implements single-player variants against the computer and also for two players (exchange through the confirmation window to prevent the opponent from seeing cards or vice versa seeing the opponents all the time)
* Allows you to save and download the games you have played


## Where can polymorphism be used? (Recommended) ##


* Types of cards: offensive, defensive, special, ...
* Action Effects: Power of Attack, Defense Force, Get X Cards, Throw X Cards
* Player Control: Local Player, Artificial Intelligence (Different Kinds), Network Player
* User Interface: console, ncurses, SDL, OpenGL (various variants), ...

# Installation and resolution #

* The application is installed using makefile
* The application is made for terminal with size 80x24

# Control #
Menu navigation - arrows (UP, DOWN), 
Confirmation - SPACE, 
Card selection - 1 to 6, 
Skip turn - x, 
Save and end game - q

# README - Instalace #

# Karetní engine Space Cards - Zadání #

Naprogramujte engine pro hraní karetního RPG

## Váš engine ##


* Ze souboru nahraje definici karet (jejich typ, atributy, efekty), balíčku (seznam karet v balíčku)
* Umožní vytvořit a hrát samotnou karetní hru
* Implementuje varianty pro jednoho hráče proti počítači a také pro dva hráče (výměna přes potvrzovací okno, aby soupeř neviděl karty, nebo naopak soupeři si celou dobu do karet vidí)
* Umožňuje ukládat a načítat rozehrané hry


## Kde lze využít polymorfismus? (doporučené) ##


* Typy karet: útočné, obranné, speciální, ...
* Efekty akcí: síla útoku, síla obrany, vem si X karet, zahoď X karet
* Ovládání hráče: lokální hráč, umělá inteligence (různé druhy), síťový hráč
* Uživatelské rozhraní: konzole, ncurses, SDL, OpenGL (různé varianty), ...

# Instalace a rozlišení #

* Aplikace se nainstaluje pomoci makefile
* Aplikace je dělana na terminal s veliksoti okna 80x24

# Ovládání #
Pohyb v menu - šipky (UP,DOWN), 
Potvrzení - SPACE, 
Výběr karty - 1 až 6, 
Vynechání kola - x, 
Uložení a ukončení hry - q 

