#include "CGame.h"
#include "../Console/CConsole.h"
#include "../File/CFileIO.h"
#include <string>
#include <iostream>
#include <cstdlib>


using namespace std;

CGame::CGame() {
}

/**
 * Method for running game. This is main function of main game. It controls the development of game.
 * It reacts on played cards.
 *  
 * @param  
 * @return void
 */
void CGame::runGame() {
    m_Console.newGame();
    printShips();
    bool AI = m_Y.getAI();
    CShip &shipX = m_X.getShip();
    CShip &shipY = m_Y.getShip();


    while (true) {

        if (shipX.getHP() <= 0 || shipY.getHP() <= 0)
            endGame();
        if (m_X.m_Turn)
            m_X.showDeck(0);
        else if (m_Y.m_Turn && !AI)
            m_Y.showDeck(0);
        else
            m_Console.clearCards();

        if (m_X.m_Turn) {

            m_Console.printText(14, 1);

            while (true) {
                int c = m_Console.inputCard();
                if (c == m_Console.KEY_SKIP)
                    break;

                if (m_X.playCard(c, shipX, shipY)) {
                    break;
                } else {
                    m_Console.printText(14, 0);
                }
            }
        } else {
            m_Console.printText(14, 2);

            while (true) {
                int c;
                if (AI)
                    c = m_Y.playAI(shipX, shipY);

                else
                    c = m_Console.inputCard();
                if (c == m_Console.KEY_SKIP)
                    break;
                if (m_Y.playCard(c, shipY, shipX)){

                    break;
                } else {

                    m_Console.printText(14, 0);
                }
            }
        }

        printShips();
        m_Console.printText(14, 3);

        while (true) {
            int c = m_Console.inputSpace();
            if (c == m_Console.KEY_QUIT) {
                savePlayers(m_X, m_Y);
                m_Console.exitGame(true);
            }
            if (c == m_Console.KEY_CONFIRM) {
                m_Console.clearCards();
                if (m_X.m_Turn) {
                    shipX.afterTurn();
                } else {
                    shipY.afterTurn();
                }
                printShips();
                c = m_Console.inputSpace();
                if (c == m_Console.KEY_CONFIRM)
                    break;
            }
        }
        endOfRound();
    }

}

/**
 * Method for end of the round. This method will evaluate whose turn is in next round
 * based on m_Turn and Stunned. 
 * It reacts on played cards.
 *  
 * @param  
 * @return void
 */
void CGame::endOfRound() {
    CShip &shipX = m_X.getShip();
    CShip &shipY = m_Y.getShip();
    if (m_X.m_Turn && shipY.getStunned()) {

        m_X.m_Turn = true;
        m_Y.m_Turn = false;
        shipY.setStunned(false);
    } else if (m_Y.m_Turn && shipX.getStunned()) {
        m_Y.m_Turn = true;
        m_X.m_Turn = false;
        shipX.setStunned(false);

    } else {
        m_X.m_Turn = !m_X.m_Turn;
        m_Y.m_Turn = !m_Y.m_Turn;


    }
}

/**
 * Method for end game. Its called when one of players HP is <=0 
 * @param  
 * @return void
 */
void CGame::endGame() {
    CShip &shipX = m_X.getShip();
    if (shipX.getHP() <= 0)
        m_Console.showWinner(2);

    else
        m_Console.showWinner(1);


}

/**
 * Method for printing ships properties 
 * @param  
 * @return void
 */
void CGame::printShips() {

    m_X.showShip(13);
    m_Y.showShip(65);
    m_Console.refreshScreen();

}

/**
 * Method initialisation the game. It shows menu and choice. Choice will start another function
 * @param  
 * @return void
 */
void CGame::initGame() {
    m_Console.init();

    while (1) {
        int choice = m_Console.menu();

        switch (choice) {
            case 0:
                m_X.m_Turn = true;
                m_Y.m_Turn = false;
                runGame();
                break;
            case 1:
                m_Y.setAI(true);
                m_X.m_Turn = true;
                m_Y.m_Turn = false;
                runGame();
                break;
            case 2:
                try {
                    loadPlayer(m_X, m_PlayerOne);
                    loadPlayer(m_Y, m_PlayerTwo);
                    runGame();
                } catch (int e) {

                }
                break;

            case 3:
                m_Console.exitGame(false);
                break;
            default:
                break;

        }

    }


}

/**
 * Method for loading players previously saved. 
 * @param  tmp (CPlayer &), file(const string &)
 * @return void
 */
void CGame::loadPlayer(CPlayer &tmp, const string &file) {
    vector <string> info = m_File.readFile(file);
    if (info.size() == 0)
        throw 0;
    CShip &ship = tmp.getShip();
    CDeck &deck = tmp.getDeck();

    tmp.setAI(stoi(info[0]));
    tmp.m_Turn = stoi(info[1]);
    ship.setHP(stoi(info[2]));
    ship.setHP(stoi(info[2]));
    ship.setEnergy(stoi(info[3]));
    ship.setAttackEng(stoi(info[4]));
    ship.setAttackPoints(stoi(info[5]));
    ship.setDeffendEng(stoi(info[6]));
    ship.setDeffendPoints(stoi(info[7]));
    ship.setStunned(stoi(info[8]));

    for (int i = 9; i < 15; i++) {
        deck.addToDeck(i - 9, stoi(info[i]));
    }


}

/**
 * Method for saving players 
 * @param  x (CPlayer &), y(CPlayer &)
 * @return void
 */
void CGame::savePlayers(CPlayer &x, CPlayer &y) {

    m_File.writeFile(x, m_PlayerOne);
    m_File.writeFile(y, m_PlayerTwo);

}

