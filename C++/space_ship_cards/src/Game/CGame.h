#ifndef CGAME_H
#define CGAME_H

#include "../Player/CPlayer.h"
#include "../Console/CConsole.h"
#include "../File/CFileIO.h"
#include "string.h"

using namespace std;

/**
 * Main class in program. It provides interface for game flow-control as init,run and end game
 * Also operates with loading,saving player
 *
 */

class CGame {
public:
    CGame();

    void initGame();

private:
    void endGame();

    void endOfRound();

    void runGame();

    void printShips()  ;

    void loadPlayer(CPlayer &tmp, const string &file);

    void savePlayers(CPlayer &x, CPlayer &y);

    /**
     *Private variable for players
     */
    CPlayer      m_X, m_Y;
    CConsole     m_Console;
    CFileIO      m_File;
    const string m_PlayerOne = "src/save/playerOne.txt";
    const string m_PlayerTwo = "src/save/playerTwo.txt";


};

#endif /* CGAME_H */

