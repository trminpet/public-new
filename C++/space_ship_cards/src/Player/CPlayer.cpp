#include "CPlayer.h"
#include "../Ship/CShip.h"
#include "../Deck/CDeck.h"
#include "../Card/CCardAttack.h"
#include "../Card/CCardDeffense.h"
#include "../Card/CCardBonus.h"
#include <iostream>

using namespace std;

CPlayer::CPlayer() {
    m_Deck.makeDeck();
    m_AI = false;
}

CPlayer::CPlayer(const CPlayer &orig) {
}

CPlayer::~CPlayer() {
}

/**
 * Method for printing deck 
 * @param  x (int)
 * @return 
 */
void CPlayer::showDeck(int x)  {

    m_Deck.print(x);
}

/**
 * Method for setting AI 
 * @param  s (bool)
 * @return 
 */
void CPlayer::setAI(bool s) {
    m_AI = s;

}

/**
 * Function for getting AI 
 * @param  
 * @return bool
 */
bool CPlayer::getAI() {
    return m_AI;

}

/**
 * Method for printing ship 
 * @param  x (int)
 * @return 
 */
void CPlayer::showShip(int x) {

    m_Ship.print(x);

}

/**
 * Function for playing card from deck  
 * @param  pos (int), a (CShip &), b (CShip &)
 * @return bool
 */
bool CPlayer::playCard(int pos, CShip &a, CShip &b) {
    if (m_Deck.playCard(pos, a, b))
        return true;
    else
        return false;

}

/**
 * Function for AI  
 * @param  a (CShip &), b (CShip &)
 * @return int
 */
int CPlayer::playAI(CShip &a, CShip &b) {


    vector < CCard * > tmp;
    tmp.resize(6);
    int      counter = 0;
    for (int i       = 0; i < 6; i++) {
        CCard *card = m_Deck.getCard(i);
        if (card->isPlayable(b)) {
            tmp.insert(tmp.begin() + i, card);
            counter++;
        }

    }
    if (!counter) return 66;
    int attack   = -1;
    int deffense = -1;
    int bonus    = -1;

    for (int i = 0; i < 6; i++) {
        if (tmp[i] != NULL) {


            if (dynamic_cast<CCardAttack *>(tmp[i]) != NULL) {
                attack = i;


            } else if (dynamic_cast<CCardDeffense *>(tmp[i]) != NULL) {

                deffense = i;

            } else {
                bonus = i;

            }


        }

    }
    if (a.getHP() - 50 <= 0 && attack != -1)
        return attack;
    if (b.getHP() - 10 <= 0 && deffense != -1)
        return deffense;

    if (bonus == -1 && attack == -1 && deffense == -1)
        return 66;

    vector<int> arrayChoice;
    if (attack != -1)
        arrayChoice.push_back(attack);
    if (deffense != -1)
        arrayChoice.push_back(deffense);
    if (bonus != -1)
        arrayChoice.push_back(bonus);


    return arrayChoice[rand() % arrayChoice.size()];


}

/**
 * Function for getting ship   
 * @param  
 * @return CShip &
 */
CShip &CPlayer::getShip() {
    return m_Ship;
}

/**
 * Function for getting deck   
 * @param  
 * @return CDeck &
 */
CDeck &CPlayer::getDeck() {
    return m_Deck;
}

