#ifndef CPLAYER_H
#define CPLAYER_H

#include "../Ship/CShip.h"
#include "../Deck/CDeck.h"


/**
 * CPlayer stores all information about player, deck and ship.
 * Provides method for playing the game
 * 
 *
 */
class CPlayer {
public:
    CPlayer();

    CPlayer(const CPlayer &orig);

    virtual ~CPlayer();

    void showDeck(int x) ;

    void showShip(int x);

    void setAI(bool s);

    bool getAI();

    bool playCard(int pos, CShip &a, CShip &b);

    int playAI(CShip &a, CShip &b);

    CShip &getShip();

    CDeck &getDeck();

    bool m_Turn;


private:
    bool  m_AI;
    CShip m_Ship;
    CDeck m_Deck;


};

#endif /* CPLAYER_H */

