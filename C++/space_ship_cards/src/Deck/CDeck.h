#ifndef CDECK_H
#define CDECK_H

#include <vector>
#include "../Card/CCard.h"
/**
 * CDeck class controls deck of cards and provides method/fucntion for manipulation with deck 
 * Also operates with loading,saving player
 * @author Petr Trminek <trminpet>
 */
using namespace std;

class CDeck {
public:
    CDeck();

    void addToDeck(int pos);

    void addToDeck(int pos, int id);

    void makeDeck();

    CCard *getCard(int pos);

    void print(int x) ;

    bool playCard(int pos, CShip &a, CShip &b);

    void loadDeck(const vector<int> &source);

private:
    int             m_array[6];
    CCard           c;
    int numGenerator();
    vector<CCard *> m_Deck;
    int             m_Size = 6;


};

#endif /* CDECK_H */

