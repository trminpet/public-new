#include "CDeck.h"
#include "../Card/CCard.h"
#include <stdlib.h>     
#include <time.h>  
#include <vector>
#include <random>

/**
 * Fuction  for random number generator 
 *  
 * @param  
 * @return int
 */
int CDeck::numGenerator() {

    return rand() % 15;

}

/**
 * Constructor loadsCard into deck
 *  
 * @param  
 * @return void
 */
CDeck::CDeck() {

    c.loadCards();

}

/**
 * Method for replacing card into the deck
 *    
 * @param pos(int) 
 * @return void
 */
void CDeck::addToDeck(int pos) {

    m_Deck.at(pos) = c.getCard(numGenerator());


}

/**
 * Method for insert card into the deck when loading player
 *    
 * @param pos(int), id (int) 
 * @return void
 */
void CDeck::addToDeck(int pos, int id) {

    m_Deck.at(pos) = c.getCard(id);


}

/**
 * Method for creating deck (6xcards) 
 *    
 * @param  
 * @return void
 */
void CDeck::makeDeck() {

    for (int i = 0; i < m_Size; i++) {

        m_array[i] = rand() % 15;
        m_Deck.push_back(c.getCard(m_array[i]));
    }

}

/**
 * Method for creating deck (6xcards) when loading players
 *    
 * @param  
 * @return void
 */
void CDeck::loadDeck(const vector<int> &source) {
    for (int i = 0; i < m_Size; i++) {

        m_array[i] = source[i];

        m_Deck.push_back(c.getCard(m_array[i]));
    }


}

/**
 * Function for playing card and replacing playable card. Returns bool depending on playable
 *    
 * @param  pos (int), a (CShip &) , b (CShip &)
 * @return bool
 */
bool CDeck::playCard(int pos, CShip &a, CShip &b) {

    if (!m_Deck.at(pos)->applyCard(a, b))
        return false;
    addToDeck(pos);
    return true;


}

/**
 * Method for printing cards in deck. x+=13 move in terminal
 *    
 * @param  x (int)
 * @return bool
 */
void CDeck::print(int x)   {
    for (int i = 0; i < m_Size; i++, x += 13) {
        m_Deck[i]->print(x);

    }


}

/**
 * Function getter card from deck on pos 
 *    
 * @param  pos (int)
 * @return CCard *
 */
CCard *CDeck::getCard(int pos)  {

    return m_Deck.at(pos);

}

