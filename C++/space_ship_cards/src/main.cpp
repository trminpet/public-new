#include <cstdlib>
#include "Game/CGame.h"
#include "File/CFileIO.h"
#include "Card/CCard.h"
#include "Deck/CDeck.h"
#include "Player/CPlayer.h"
#include "Console/CConsole.h"
#include <fstream>
#include <iostream>
#include <string>
#include <ncurses.h>

using namespace std;

/**
 * Main function  in program. Creates game and init
 * 
 * @author Petr Trminek <trminpet>
 */
int main(int argc, char **argv) {

    srand(time(NULL));

    CGame game;
    try {
        game.initGame();
    }
    catch (int e) {
        return 0;
    }

    return 0;
}

