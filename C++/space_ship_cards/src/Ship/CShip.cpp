#include "CShip.h"
#include <iostream>
#include <string>
#include <ncurses.h>

using namespace std;

CShip::CShip() {
    m_HP            = HP;
    m_Energy        = ENERGY;
    m_AttackEng     = 5;
    m_DeffendEng    = 5;
    m_AttackPoints  = 1;
    m_DeffendPoints = 1;
    m_Stunned       = false;
}

CShip::~CShip() {
}

/**
 * Function for getting property m_Stunned 
 *  
 * @param  
 * @return bool
 */

bool CShip::getStunned() const {
    return m_Stunned;
}

/**
 * Method for setting property m_Stunned 
 *  
 * @param  t (bool)
 * @return void
 */
void CShip::setStunned(bool t) {
    m_Stunned = t;
}

/**
 * Function for getting property m_AttackEng 
 *  
 * @param  
 * @return int
 */
int CShip::getAttackEng() const {
    return m_AttackEng;
}

/**
 * Function for setting property m_AttackEng 
 *  
 * @param  AttackEng (int)
 * @return void
 */
void CShip::setAttackEng(int AttackEng) {
    m_AttackEng = AttackEng;
}

/**
 * Function for getting property m_AttackPoints
 *  
 * @param  
 * @return int
 */
int CShip::getAttackPoints() const {
    return m_AttackPoints;
}

/**
 * Method for setting property m_AttackPoints 
 *  
 * @param  AttackVal (int)
 * @return void
 */
void CShip::setAttackPoints(int AttackVal) {
    m_AttackPoints = AttackVal;
}

/**
 * Function for getting property m_DeffendEng 
 *  
 * @param  
 * @return int
 */
int CShip::getDeffendEng() const {
    return m_DeffendEng;
}

/**
 * Method for setting property m_DeffendEng  
 *  
 * @param  DeffendEng (int)
 * @return void
 */
void CShip::setDeffendEng(int DeffendEng) {
    m_DeffendEng = DeffendEng;
}

/**
 * Function for getting property m_DeffendPoints
 *  
 * @param  
 * @return int
 */
int CShip::getDeffendPoints() const {
    return m_DeffendPoints;
}

/**
 * Method for setting property m_DeffendPoints 
 *  
 * @param  Deffendval (int)
 * @return void
 */
void CShip::setDeffendPoints(int DeffendVal) {
    m_DeffendPoints = DeffendVal;
}

/**
 * Function for getting property m_Energy 
 *  
 * @param  
 * @return int
 */
int CShip::getEnergy() const {
    return m_Energy;
}

/**
 * Method for setting property m_Energy
 *  
 * @param  Energy (int)
 * @return void
 */
void CShip::setEnergy(int Energy) {
    m_Energy = Energy;
}

/**
 * Function for getting property m_HP
 *  
 * @param  
 * @return int
 */
int CShip::getHP() const {
    return m_HP;
}

/**
 * Method for setting property m_HP 
 *  
 * @param  HP (int)
 * @return void
 */
void CShip::setHP(int HP) {

    m_HP     = HP;
    if (m_HP >= 120)
        m_HP = 120;
}

/**
 * Method for increasing properties after each turn
 *  
 * @param  
 * @return void
 */
void CShip::afterTurn() {

    m_AttackPoints += m_AttackEng;
    m_DeffendPoints += m_DeffendEng;
    if (m_Energy + 40 <= ENERGY)
        m_Energy += 40;
    else
        m_Energy = ENERGY;

}

/**
 * Method for printing ships properties
 *  
 * @param  
 * @return void
 */
void CShip::print(int x) const {
    mvprintw(0, x, "%s", "    ");
    mvprintw(0, x, "%s", (to_string(m_HP)).c_str());
    mvprintw(1, x, "%s", "    ");
    mvprintw(1, x, "%s", (to_string(m_Energy)).c_str());
    mvprintw(2, x, "%s", "    ");
    mvprintw(2, x, "%s", (to_string(m_AttackEng)).c_str());
    mvprintw(3, x, "%s", "    ");
    mvprintw(3, x, "%s", (to_string(m_AttackPoints)).c_str());
    mvprintw(4, x, "%s", "    ");
    mvprintw(4, x, "%s", (to_string(m_DeffendEng)).c_str());
    mvprintw(5, x, "%s", "    ");
    mvprintw(5, x, "%s", (to_string(m_DeffendPoints)).c_str());
    mvprintw(6, x, "%s", "    ");
    mvprintw(6, x, "%s", (to_string(m_Stunned)).c_str());


}
