#ifndef CSHIP_H
#define CSHIP_H

/**
 * CShip class provides information about ship properties 
 * Also operates with loading,saving player
 *
 */
class CShip {
public:
    CShip();

    CShip(const CShip &orig);

    virtual ~CShip();

    bool getStunned() const;

    void setStunned(bool t);

    int getAttackEng() const;

    void setAttackEng(int AttackEng);

    int getAttackPoints() const;

    void setAttackPoints(int AttackVal);

    int getDeffendEng() const;

    void setDeffendEng(int DeffendEng);

    int getDeffendPoints() const;

    void setDeffendPoints(int DeffendVal);

    int getEnergy() const;

    void setEnergy(int Energy);

    int getHP() const;

    void setHP(int HP);

    void print(int x) const;

    const int ENERGY = 120;

    void afterTurn();

private:

    const int HP = 100;
    int       m_Energy;
    int       m_HP;
    int       m_AttackEng;
    int       m_DeffendEng;
    int       m_AttackPoints;
    int       m_DeffendPoints;
    bool       m_Stunned;


};

#endif /* CSHIP_H */

