/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: petr
 *
 * Created on April 18, 2017, 4:38 PM
 */

#include <cstdlib>
#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

using namespace std;



/*
 * 
 */

class Tvar {
public:
    Tvar(int x1,int y1, int x2, int y2){
        m_minX=min(x1,x2);
        m_maxX=max(x1,x2);
        m_minY=min(y1,y2);
        m_maxY=max(y1,y2);
    
    }
    virtual Tvar * Clone() const{
      return  new Tvar(*this);
    }
    virtual ~Tvar(){}
  virtual  ostream & Print (ostream & os) const{
        return os << "[" << m_minX << "," << m_minY << "], [" << m_maxX << "," << m_maxY << "]" << endl;
        
    
    
    }
   friend  ostream & operator << (ostream & os, const Tvar & t){
        return t.Print(os);
    }
protected:
    int m_minX,m_maxX,m_minY,m_maxY;
};
class Obdelnik : public Tvar{
    string nazev;
public:Obdelnik (int x1, int x2, int y1, int y2,string nazev):Tvar(x1,x2,y1,y2){
    this->nazev=nazev;


}
virtual Obdelnik * Clone() const{
      return  new Obdelnik(*this);
    }
virtual ostream & Print (ostream & os) const{
         os << "Obdelnik: " << nazev << " " ;
         return Tvar::Print(os);
    
    
    }









};
class Ctverec : public Tvar{
public: Ctverec (int x1, int x2, int y1, int y2):Tvar(x1,x2,y1,y2){
    if(m_maxX - m_minX != m_maxY - m_minY) 
        throw "tohle neni ctverec";

}
virtual Ctverec * Clone() const{
      return  new Ctverec(*this);
    }

virtual ostream & Print (ostream & os) const{
         os << "Ctverec: " ;
         return Tvar::Print(os);
    
    
    }

};



int main(int argc, char** argv) {

    vector<Tvar*> tvary;
    tvary.push_back(new Ctverec (0,0,1,1));
    tvary.push_back(new  Obdelnik (0,0,1,2,"Obdelnik"));
    
    
    for (unsigned int i = 0; i < 2; i++) {
        tvary.push_back(tvary[i]->Clone());

    }

   
    for (unsigned int i = 0; i <tvary.size(); i++) {
       // cout << tvary[i] << endl;
        tvary[i]->Print(cout);

    }
    for (unsigned int i = 0; i < tvary.size(); i++) {
        delete  tvary[i];

    }


    
    
    
    
    
    
    
    return 0;
}

