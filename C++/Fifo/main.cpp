/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: petr
 *
 * Created on March 7, 2017, 4:29 PM
 */

#include <cstdlib>
#include <iostream>
#include <algorithm>

using namespace std;

class CFifo_array {
public:

    CFifo_array() {
        count = 0;
        size = 10;
        array = new int[size];
        first = 0;
        last = 0;

    };

    ~CFifo_array() {
        delete [] array;

    };

    void push(const int x) {
        if (count == size) realloc();

        array[last] = x;
        last = (last + 1) % size;
        count++;
    };

    void pop() {
        if (!empty()) {
            first = (first + 1) % size;
            count--;
        }
    };

    int front() const {
        return array[first];
    };

    bool empty() const {
        return !count;

    };
private:
    int size;
    int * array;
    int first, last;
    int count;

    void realloc() {
        int * tmp = new int [size *= 2];
        for (int i = 0; i < count; i++) {
            tmp[i] = array[first];
            first = (first + 1) % count;

        }
        delete [] array;
        array = tmp;
        first = 0;
        last = count;

    };


};

class CFifo_link {
public:

    CFifo_link() {
        first = NULL;
        last = NULL;


    };

    ~CFifo_link() {
        TElem * tmp ;
        while(!empty()){
            pop();
        
        }
       
    };

    void push(const int x) {
        if (empty()) {
            first = last = new TElem(x, NULL);
        } else {
            last->next = new TElem(x, NULL);
            last = last->next;
        }

    };

    void pop() {

        if(!empty()){
        TElem * tmp = first;
        
        first = first->next;
        delete  tmp;
        }
    };

    int front() const {
        return first->value;
    };

    bool empty() const {
        return first==NULL?true:false;
            

    };
private:

    struct TElem {

        TElem(int val, TElem * nex) : value(val), next(nex) {
        }
        int value;
        TElem * next;
    };
    TElem * first;
    TElem * last;



};
typedef CFifo_link Queue;

int main(int argc, char** argv) {
    Queue q;
    for (int i = 0; i < 100; i++) {
        q.push(i % 5);

    }
    for (int i = 0; i < 100; i++) {
        std::cout << q.front() << std::endl;
        q.pop();

    }



    return 0;
}

//binarni and a vyuzit masky
//nactu jeden byte a pak 8xmasku abych nacetl bit