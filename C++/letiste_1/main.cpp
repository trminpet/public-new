#ifndef __PROGTEST__
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <string>
#include <assert.h>
using namespace std;
#endif /* __PROGTEST__ */





class CAirport
{
    public:
                            CAirport    ();
                            CAirport    (const CAirport&    src);   //todo
                            ~CAirport   ();                         //todo
        bool                Add         (const string&      a,
                                         const string&      b);     //todo
        friend ostream&  operator<<      (ostream&           os,
                                         const CAirport&    src);
    private:
        static const int MAX_AIRPORTS = 500;
        struct TAirport;
        struct TDest
        {
                        TDest(): m_Next(NULL), m_Name(NULL){};
            TDest*      m_Next;
            TAirport*   m_Name;
        };
 
        struct TAirport
        {
                        TAirport(const string& name): m_Name(name), m_Dest(NULL), m_Ref(0) {};
            string      m_Name;
            TDest*      m_Dest;
            TDest*      m_Last;
            int         m_Ref;
        };
 
        TAirport *  m_List[MAX_AIRPORTS];
        int         m_Nr;
        friend class CTester;
        TAirport* findPort  (const string&  name);
        TAirport* addToArr  (const string&  name);
        bool checkLine(TAirport * a, TAirport* b);
        void addLineEnd       (CAirport::TAirport*    a,
                                 CAirport::TAirport*    b);
        void addLineBeg       (CAirport::TAirport*    a,
                                 CAirport::TAirport*    b);
};
 
CAirport::CAirport  (): m_Nr(0)
{
    for (int i = 0; i < MAX_AIRPORTS; i++)
        m_List[i] = NULL;
}
CAirport::CAirport(const CAirport& src){
  m_Nr = 0;
    for (int i = 0; i < MAX_AIRPORTS; i++)
        m_List[i] = NULL;
 
    for (int i = 0; i < src.m_Nr; i++)
        addToArr(src.m_List[i]->m_Name);
 
    CAirport::TDest* it;
    for (int i = 0; i < m_Nr; ++i)
    {
        it = src.m_List[i]->m_Dest;
        while(it != NULL)
        {
            addLineEnd(m_List[i], findPort(it->m_Name->m_Name));
            it = it->m_Next;
        }
    }




}
CAirport::~CAirport(){
    CAirport::TDest* it;
    for (int i = 0; i < m_Nr; i++) {
        if(m_List[i]==NULL)
            continue;
        while(m_List[i]->m_Dest!=NULL){
            it=m_List[i]->m_Dest;
            m_List[i]->m_Dest=m_List[i]->m_Dest->m_Next;
            delete it;
        
        }
        delete m_List[i];

    }



}
CAirport::TAirport* CAirport::findPort(const string& name){
    for (int i = 0; i < m_Nr; i++) {
        if(m_List[i]->m_Name==name)
            return m_List[i];

    }
    return NULL;

}

CAirport::TAirport* CAirport::addToArr(const string& name){
    TAirport * tmp  = new CAirport::TAirport(name);
    m_List[m_Nr++]=tmp;
    return tmp;


}
bool  CAirport::checkLine(TAirport* a, TAirport* b){


CAirport::TDest* it1 = a->m_Dest;
    while(it1 != NULL)
    {
        if(it1->m_Name->m_Name == b->m_Name)
            return true;
        it1 = it1->m_Next;
    }
    return false;



}
bool CAirport::Add(const string& a, const string& b){
    if(a==b) return false;
    TAirport * m_A = findPort(a);
    if(m_A==NULL)
        m_A =addToArr(a);
    TAirport * m_B = findPort(b);
    if(m_B==NULL)
        m_B=addToArr(b);
    
    bool exists = checkLine(m_A,m_B);
    
     if(exists == true)
        return false;
 
    addLineBeg(m_A, m_B);
    addLineBeg(m_B, m_A);
    return true;
    

}
void CAirport::addLineBeg       (CAirport::TAirport*    a,
                                 CAirport::TAirport*    b)
{
    CAirport::TDest* tmp = new CAirport::TDest();
    tmp->m_Name = b;
    b->m_Ref++;
    if(a->m_Dest == NULL)
    {
        a->m_Dest = a->m_Last = tmp;
        return;
    }
    tmp->m_Next = a->m_Dest;
    a->m_Dest   = tmp;
}
 
void CAirport::addLineEnd       (CAirport::TAirport*    a,
                                 CAirport::TAirport*    b)
{
    CAirport::TDest* tmp = new CAirport::TDest();
    tmp->m_Name = b;
    b->m_Ref++;
    if(a->m_Dest == NULL)
    {
        a->m_Dest = a->m_Last = tmp;
        return;
    }
    a->m_Last->m_Next   = tmp;
    a->m_Last           = tmp;
}
 
ostream& operator<< (ostream&           os,
                     const CAirport&    src)
{
    for(int i = 0; i < src.m_Nr; i++)
    {
        os << src.m_List[i]->m_Name << "[" << src.m_List[i]->m_Ref << "]" << ":";
        CAirport::TDest* tmp = src.m_List[i]->m_Dest;
        while(tmp != NULL)
        {
            os << tmp-> m_Name -> m_Name << "[" << tmp -> m_Name->m_Ref << "], ";
            tmp = tmp->m_Next;
        }
        os << endl;
    }
    return os;
}
//
//int main(void){
//
//// CAirport f0;
////    assert(f0.Add("Prague", "Berlin") == true);
////    assert(f0.Add("Prague", "Berlin") == false);
////    assert(f0.Add("Prague", "London") == true);
////    assert(f0.Add("London", "Berlin") == true);
////    assert(f0.Add("Berlin", "London") == false);
////    assert(f0.Add("New York", "Oslo") == true);
////    assert(f0.Add("London", "London") == false);
//// 
////    cout << f0 << endl;
////    /*
////        Prague[2]:London[2], Berlin[2], 
////        Berlin[2]:London[2], Prague[2], 
////        London[2]:Berlin[2], Prague[2], 
////        New York[1]:Oslo[1], 
////        Oslo[1]:New York[1],
////    */
//// 
////    CAirport f1 (f0);
////    assert(f1.Add("Paris", "Wien") == true);
//// 
////    cout << f0 << endl;
////    /*
////        Prague[2]:London[2], Berlin[2], 
////        Berlin[2]:London[2], Prague[2], 
////        London[2]:Berlin[2], Prague[2], 
////        New York[1]:Oslo[1], 
////        Oslo[1]:New York[1],
////    */
////    cout << f1 << endl;
////    /*
////        Prague[2]:London[2], Berlin[2], 
////        Berlin[2]:London[2], Prague[2], 
////        London[2]:Berlin[2], Prague[2], 
////        New York[1]:Oslo[1], 
////        Oslo[1]:New York[1], 
////        Paris[1]:Wien[1], 
////        Wien[1]:Paris[1], 
////    */
//}
class CArray{

public:

CArray( int len ){

m_D = new int[ m_L = len ];

for( int i = 0; i < m_L; i++ ) m_D[i] = 0;

}

CArray & operator = ( const CArray & src ){

delete [] m_D;

m_D = new int[ m_L = src . m_L ];

for( int i = 0; i < m_L; i ++ ) m_D[i] = src . m_D[i];

return *this;

}



int & operator [] ( int i ){ return m_D[i]; }

private:

int * m_D;

int m_L;

};

int main ( void ){

CArray a(69), b = a;

a[19] = 1;

cout << b[19];

return 0;

}