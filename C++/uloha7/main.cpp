#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST_ */
const string MODEL = "Galaxy Note S7";
const string COMPANY = "Samsung";

/*Natrida CItem
 */
class CItem {
public:

    CItem() {
    };

    CItem(int w) : m_weight(w) {
    };

    virtual ~CItem() {
    };

    virtual void print(ostream & os) const {
    };
    //Deklarace virtualni metod
    //Metoda Clone prevzata ze cviceni PA2(9.cviceni) Trminek 23.4.2017
    virtual CItem * Clone() const = 0;
    //Metoda na urceni zda je predmet nebezpecny ci nikoliv
    virtual bool isDanger()const = 0;
    //Metoda na porovnani dvou objektu

    virtual bool compare(const CItem * tmp)const {
        return true;
    };
    //Vaha objektu
    int m_weight;

};

/*Podtrida CKnife dedi od CItem
 */
class CKnife : public CItem {
public:
    // CKnife ( bladeLength );

    CKnife(int l) : CItem(100), m_bladeLenght(l) {
    };
    // CKnife(const CKnife & tmp):CItem(tmp){};

    virtual void print(ostream & os) const {
        //os << 1;
        os << "-Knife, blade: " << m_bladeLenght << " cm\n";
        //cout << "-Knife, blade: "<<m_bladeLenght<<" cm\n";
    };

    virtual CKnife * Clone() const {
        return new CKnife(*this);
    };

    virtual bool isDanger()const {
        return m_bladeLenght > 7;
    }

    virtual bool compare(const CKnife * tmp)const {
        return m_bladeLenght == tmp->m_bladeLenght;
    }


    int m_bladeLenght;
};

/*Podtrida CClothes dedi od CItem
 */
class CClothes : public CItem {
public:
    // CClothes ( desc );

    CClothes(const string & desc) : CItem(500), m_description(desc) {
    };

    virtual void print(ostream & os) const {
        os << "-Clothes (" << m_description << ")\n";
        // cout <<"-Clothes ("<<m_description<<")\n";
    };

    virtual CClothes * Clone() const {
        return new CClothes(*this);
    };

    virtual bool isDanger()const {
        return false;
    }

    virtual bool compare(const CClothes * tmp)const {
        return m_description == tmp->m_description;
    }

    string m_description;
};

/*Podtrida CShoes dedi od CItem
 */
class CShoes : public CItem {
public:
    // CShoes ();

    CShoes() : CItem(750) {
    };

    virtual void print(ostream & os) const {
        os << "-Shoes\n";
        // cout <<"-Shoes\n";
    };

    virtual CShoes * Clone() const {
        return new CShoes(*this);
    }

    virtual bool isDanger()const {
        return false;
    }

    virtual bool compare(const CShoes * tmp)const {
        return true;
    }

};

/*Podtrida CMobile dedi od CItem
 */
class CMobile : public CItem {
public:
    // CMobile ( manufacturer, model );

    CMobile(const string & manufacturer, const string & model) : CItem(150), m_manufactured(manufacturer), m_model(model) {
    };

    virtual void print(ostream & os) const {

        os << "-Mobile " << m_model << " by: " << m_manufactured << "\n";
        // cout <<"-Mobile " << m_model << " by: "<<m_manufactured<<"\n";
    };

    virtual CMobile * Clone() const {
        return new CMobile(*this);
    }

    virtual bool isDanger()const {
        return m_manufactured == COMPANY && m_model == MODEL;
    }

    virtual bool compare(const CMobile * tmp)const {
        return m_manufactured == tmp->m_manufactured && m_model == tmp->m_model;
    }

    string m_manufactured, m_model;
};

/*Nadtrida pro zavazadla, stara se o spravu pameti, pretezovani operatoru a copy konstruktor
 */
class CLuggage {
public:

    CLuggage(int w) : m_totalWeight(w), m_alloc(10), m_content(new const CItem * [m_alloc]) {
        m_count = 0;
        m_danger = false;
    };

    CLuggage(const CLuggage & tmp) {
        //zkopirovani obsahu
        m_totalWeight = tmp.m_totalWeight;
        m_alloc = tmp.m_alloc;
        m_count = tmp.m_count;
        m_content = new const CItem * [m_alloc];
        m_danger = tmp.m_danger;

        for (int i = 0; i < m_count; i++) {
            m_content[i] = tmp.m_content[i]->Clone();
        }

    };

    ~CLuggage() {
        for (int i = 0; i < m_count; i++) {
            delete m_content[i];

        }
        delete [] m_content;


    };

    CLuggage & operator=(const CLuggage & tmp) {

        if (&tmp == this) return *this;
        //smazani puvodniho obsahu
        for (int i = 0; i < m_count; i++) {
            delete m_content[i];

        }
        delete [] m_content;
        //zkopirovano obsahu
        m_totalWeight = tmp.m_totalWeight;
        m_alloc = tmp.m_alloc;
        m_count = tmp.m_count;
        m_content = new const CItem * [m_alloc];
        m_danger = tmp.m_danger;

        for (int i = 0; i < m_count; i++) {
            m_content[i] = tmp.m_content[i]->Clone();
        }

        return *this;


    }
    //realloc pole pro CItem

    void realloc() {

        const CItem ** tmp = new const CItem *[m_alloc *= 2];

        for (int i = 0; i < m_count; i++) {
            tmp[i] = m_content[i];

        }
        delete []m_content;
        m_content = tmp;



    };
    int m_totalWeight, m_count, m_alloc;

    const CItem ** m_content;
    bool m_danger;


};
//Podtrida CSuitcase dedi od CLuggage

class CSuitcase : public CLuggage {
public:
    // CSuitcase ( w, h, d )

    CSuitcase(int w, int h, int d) : CLuggage(2000), m_width(w), m_height(h), m_depth(d) {
    };

    // Add pridani po pole, vyuziti metody Clone

    CSuitcase & Add(const CItem & tmp) {
        if (m_count == m_alloc - 1)
            realloc();
        m_content[m_count] = tmp.Clone();
        //pridani hmotnosti CItem do celkove
        m_totalWeight += tmp.m_weight;
        m_count++;
        //kontrola nebezpecneho itemu
        if (!m_danger)
            m_danger = tmp.isDanger();
        return *this;

    }


    // Weight

    int Weight() const {
        return m_totalWeight;
    }
    // Count

    int Count() const {
        return m_count;
    }
    // Danger

    bool Danger() const {
        return m_danger;
    }
    // IdenticalContents

    bool IdenticalContents(const CLuggage & tmp) const {
        // cout << m_count << " " << tmp.m_count<<endl;
        if (&tmp == this) return true;
        if (m_count != tmp.m_count) return false;
        int counter = 0;
        //prochazeni pole pro hledani shod
        for (int i = 0; i < m_count; i++) {
            for (int j = 0; j < m_count; j++) {
                if (m_content[i]->compare(tmp.m_content[j])) {
                    counter++;
                    break;
                }
            }


        }
        // cout << counter;
        if (counter == m_count)return true;

        return false;




    }
    // operator <<

    friend ostream & operator<<(ostream & os,
            const CSuitcase & tmp) {
        os << "Suitcase " << tmp.m_width << "x" << tmp.m_height << "x" << tmp.m_depth << "\n";
        //  cout << "Suitcase " << tmp.m_width << "x" << tmp.m_height << "x" << tmp.m_depth << "\n";
        int size = tmp.m_count;
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                os << "\\";
                // cout << "\\";

            } else {
                os << "+";
                // cout << "+";

            }
            tmp.m_content[i]->print(os);
        }
        return os;
    }

    int m_width, m_height, m_depth;

};

/*Podtrida Cbackpack dedi od natridy CLugagge
 */
class CBackpack : public CLuggage {
public:

    CBackpack() : CLuggage(1000) {
    };


    // Add

    CBackpack & Add(const CItem & tmp) {
        if (m_count == m_alloc - 1)
            realloc();

        m_content[m_count] = tmp.Clone();
        //pridani hmotnosti k celkove hmotnosti
        m_totalWeight += tmp.m_weight;
        m_count++;
        //kontrola nebezpecnoho itemu
        if (!m_danger)
            m_danger = tmp.isDanger();
        return *this;

    }


    // Weight

    int Weight() const {
        return m_totalWeight;
    }
    // Count

    int Count() const {
        return m_count;
    }
    // Danger

    bool Danger() const {
        return m_danger;
    }
    // IdenticalContents

    bool IdenticalContents(const CLuggage & tmp) const {
        // cout << m_count << " " << tmp.m_count<<endl;
        if (&tmp == this) return true;
        if (m_count != tmp.m_count) return false;
        int counter = 0;
        //prochazeni pole na hledani shod
        for (int i = 0; i < m_count; i++) {
            for (int j = 0; j < m_count; j++) {
                if (m_content[i]->compare(tmp.m_content[j])) {
                    counter++;
                    break;
                }
            }


        }
        //  cout << counter;
        if (counter == m_count)return true;

        return false;

    }
    // operator <<

    friend ostream & operator<<(ostream & os,
            const CBackpack & tmp) {

        os << "Backpack\n";

        int size = tmp.m_count;
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                os << "\\";
                //   cout << "\\\\";
            } else {
                os << "+";
                // cout << "+";

            }
            tmp.m_content[i]->print(os);
        }

        return os;


    }
};

#ifndef __PROGTEST__

int main(void) {

    CSuitcase x(1, 2, 3);
    CBackpack y;
    ostringstream os;
    x . Add(CKnife(7));
    x . Add(CClothes("red T-shirt"));
    x . Add(CClothes("black hat"));
    x . Add(CShoes());
    x . Add(CClothes("green pants"));
    x . Add(CClothes("blue jeans"));
    x . Add(CMobile("Samsung", "J3"));
    x . Add(CMobile("Tamtung", "Galaxy Note S7"));
    os . str("");
    os << x;
    assert(os . str() == "Suitcase 1x2x3\n"
            "+-Knife, blade: 7 cm\n"
            "+-Clothes (red T-shirt)\n"
            "+-Clothes (black hat)\n"
            "+-Shoes\n"
            "+-Clothes (green pants)\n"
            "+-Clothes (blue jeans)\n"
            "+-Mobile J3 by: Samsung\n"
            "\\-Mobile Galaxy Note S7 by: Tamtung\n");
    assert(x . Count() == 8);
    assert(x . Weight() == 5150);
    assert(!x . Danger());
    x . Add(CKnife(8));
    os . str("");
    os << x;
    assert(os . str() == "Suitcase 1x2x3\n"
            "+-Knife, blade: 7 cm\n"
            "+-Clothes (red T-shirt)\n"
            "+-Clothes (black hat)\n"
            "+-Shoes\n"
            "+-Clothes (green pants)\n"
            "+-Clothes (blue jeans)\n"
            "+-Mobile J3 by: Samsung\n"
            "+-Mobile Galaxy Note S7 by: Tamtung\n"
            "\\-Knife, blade: 8 cm\n");
    assert(x . Count() == 9);
    assert(x . Weight() == 5250);
    assert(x . Danger());
    y . Add(CKnife(7))
            . Add(CClothes("red T-shirt"))
            . Add(CShoes());
    y . Add(CMobile("Samsung", "Galaxy Note S7"));
    y . Add(CShoes());
    y . Add(CClothes("blue jeans"));
    y . Add(CClothes("black hat"));
    y . Add(CClothes("green pants"));
    os . str("");
    os << y;
    assert(os . str() == "Backpack\n"
            "+-Knife, blade: 7 cm\n"
            "+-Clothes (red T-shirt)\n"
            "+-Shoes\n"
            "+-Mobile Galaxy Note S7 by: Samsung\n"
            "+-Shoes\n"
            "+-Clothes (blue jeans)\n"
            "+-Clothes (black hat)\n"
            "\\-Clothes (green pants)\n");
    assert(y . Count() == 8);
    assert(y . Weight() == 4750);
    assert(y . Danger());
    y . Add(CMobile("Samsung", "J3"));
    y . Add(CMobile("Tamtung", "Galaxy Note S7"));
    y . Add(CKnife(8));
    os . str("");
    os << y;
    assert(os . str() == "Backpack\n"
            "+-Knife, blade: 7 cm\n"
            "+-Clothes (red T-shirt)\n"
            "+-Shoes\n"
            "+-Mobile Galaxy Note S7 by: Samsung\n"
            "+-Shoes\n"
            "+-Clothes (blue jeans)\n"
            "+-Clothes (black hat)\n"
            "+-Clothes (green pants)\n"
            "+-Mobile J3 by: Samsung\n"
            "+-Mobile Galaxy Note S7 by: Tamtung\n"
            "\\-Knife, blade: 8 cm\n");
    assert(y . Count() == 11);
    assert(y . Weight() == 5150);
    assert(y . Danger());
    assert(!x . IdenticalContents(y));
    assert(!y . IdenticalContents(x));
    x . Add(CMobile("Samsung", "Galaxy Note S7"));
    assert(!x . IdenticalContents(y));
    assert(!y . IdenticalContents(x));
    x . Add(CShoes());
    assert(x . IdenticalContents(y));
    assert(y . IdenticalContents(x));
    assert(y . IdenticalContents(y));


    assert(x . IdenticalContents(x));
    CBackpack z(y);
    z.Add(CShoes());
    assert(!z . IdenticalContents(y));
    z = y;
    assert(z . IdenticalContents(y));


    return 0;
}
#endif /* __PROGTEST__ */