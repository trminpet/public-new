#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <algorithm>
#include <map>
#include <set>
#include <vector>
#include <list>
#include <deque>
#include <queue>
#include <stack>
using namespace std;
 
class CTrain{
    public:
    //default constructor
        CTrain(){};
 
    //default destructor
 
    //Add
    void Add(istringstream & is){
    
    
        string stat1,stat2;
        getline(is,stat1);
        m_Map[stat1];
        while(getline(is,stat2)){
        
            if(stat2.empty())
                continue;
            m_Map[stat1].insert(stat2);
            m_Map[stat2].insert(stat1);
            stat1=stat2;
        
        }
    
    
    
    
    
    
    
    
    
    };
    
    void print(const string & station)const{
    
        for (auto elem : m_Map) {
            cout << elem.first << endl;
        }

    
    
    }
    //Dest
    set<string> Dest (const string & from, int maxCost){
    
        set<string> visited;
        queue<string> que;
        map <string,string> prev;
        set<string> res;
        string current;
        int max = 0;
        que.push(from);
        visited.insert(from);
        if(max==maxCost){
            visited.insert(from);
           
         return visited;           
        }
        while(!que.empty()){
        
            current = que.front();
            que.pop();
            if(max==maxCost){
               
                return visited;
                

            
            }
            
            for (auto it = m_Map.at(current).begin();it!=m_Map.at(current).end(); ++it) {
                
                if(visited.count(*it)==0){
                    visited.insert(*it);
                    que.push(*it);
                    prev.insert(make_pair(*it,current));
                   
                
                }

            }
            max++;

        
        
        }
    
    
    
    
    
    
    
    
    
    
    }
 
    private:
    //todo
        map <string , set <string> > m_Map;
};

int main (void){



CTrain t0;
istringstream iss;
iss . clear ();
iss . str ("Forest Hill\nNational Museum\nWhite City\nChina Town\nWembley\nNew Cross\n");
t0 . Add ( iss );
iss . clear ();
iss . str ("Lady Palace\nKings Cross\nOldTown\nNew Cross\nNew Aley\n");
t0 . Add ( iss );
iss . clear ();
iss . str ("ZOO\nChina Town\nKings Cross\nFantom Bridge\n");
t0 . Add ( iss );

set<string> s = (t0.Dest("National Museum", 1));
/*Forest Hill
  National Museum
  White City*/
for(auto elem : s){
    cout << elem << endl;

}
cout << endl;
set<string> t = (t0.Dest("China Town", 0));
/*China Town*/
for(auto elem : t){
    cout << elem << endl;

}
cout << endl; 
set<string> u = (t0.Dest("China Town", 2));
/*China Town
  Fantom Bridge
  Kings Cross
  Lady Palace
  National Museum
  New Cross
  Old Town
  Wembley
  White City
  ZOO*/
for(auto elem : u){
    cout << elem << endl;

}
cout << endl; 
set<string> v = (t0.Dest("ZOO", 2));
/*China Town
  Kings Cross
  Wembley
  White City
  ZOO*/
for(auto elem : v){
    cout << elem << endl;

}
cout << endl;






}