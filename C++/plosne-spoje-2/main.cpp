/*  Author: Martin Szlauer  */

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <string>
#include <map>
#include <set>
#include <algorithm>
#include <list>
#include <queue>
#include <cassert>
#include <cctype>

using namespace std;

class CPCB {
public:

    CPCB(void) {}

    ~CPCB(void) {
        
    }
    list<string> Short(const string & from,
            const string & to);

    friend istream & operator>>(istream & is,
            CPCB & pcb);

    map <string,set <string> > m_Map;

};

list<string> CPCB::Short(const string & from,
        const string & to) {
      list<string> res;
    if(m_Map.find(from)==m_Map.end() || m_Map.find(to)==m_Map.end())
        return res;
    
    
    
  
    queue<string> que;
    set <string> visited;
    map <string, string > route;
    que.push(from);
    visited.insert(from);
    string current,node;
    while(!que.empty()){
    
        current = que.front();
        que.pop();
        if(current == to){
            node = to;
            while(node!=from)
            {
                res.push_front(node);
                node = route[node];
            }
            res.push_front(node);
            return res;
        
        
        
        
        
        }
        for (auto elem : m_Map.at(current)) {
            if(visited.count(elem)==0){
                visited.insert(elem);
                que.push(elem);
                route.insert(make_pair(elem,current));
            
            
            }
        }

    
    
    
    
    
    
    
    
    
    
    
    
    }
    

}

istream & operator>>(istream & is,
        CPCB & pcb) {
 #define SYNTAX_FAIL { is.setstate( ios_base::failbit ); return is; }
     char c;
     string nodeA,nodeB;
     
   if ( is >> skipws >> c, c != '{' )                    // read '{'
        SYNTAX_FAIL
 
    do{
        for ( is >> skipws >> c; isalnum(c); is.get(c)  ) // read 1. node
            nodeA.push_back(c);
        is.putback(c);
 
        if ( is >> skipws >> c, c != '-' )                // read '-'
            SYNTAX_FAIL
 
        for ( is >> skipws >> c; isalnum(c); is.get(c)  ) // read 2. node
            nodeB.push_back(c);
        is.putback(c);
 
        pcb.m_Map[nodeA].insert(nodeB);
         pcb.m_Map[nodeB].insert(nodeA);
         nodeA.clear();
         nodeB.clear();
    } while ( is >> skipws >> c, c == ',' );              // read ','
 
    if ( c != '}' )                                       // read '}'
        SYNTAX_FAIL
     
        return is;
       
}

void readlineX(istringstream & is){

    string a = "aaa\nbbb\nccc\nddd\n";
    string res;
    while(getline(is,res))
        cout << res << "xx"<<endl;










}

int main() {
    CPCB to;

    istringstream("  { 64U - 172 , 172 - ZZ , xx-ZZ,   X3 - ZZ, A24-ZZ, A73 - A24,B5-C7, A24 -B5, C7-f8 ,A73-B5} ")>>to;

    

    assert(to.Short("A73", "f8") == (list<string>({"A73", "B5", "C7", "f8"})));
    assert(to.Short("xx", "xx") == (list<string>({"xx"})));
    assert(to.Short("X3", "f8") == (list<string>({"X3", "ZZ", "A24", "B5", "C7", "f8"})));
    assert(!(istringstream("  { A3-VW,R} ") >> to));
    assert(!(istringstream("  { } ") >> to));
    assert(istringstream("{A3-VW}") >> to);
    
    assert(istringstream("  { A3-VW , 172-ZZ, 23-A3} ") >> to);
    assert(to.Short("23", "VW") == (list<string>({"23", "A3", "VW"})));
    assert(to.Short("xx", "23") == (list<string>({})));
    assert(istringstream("{64U- 172\t,172 - ZZ,xx-ZZ,X3 - ZZ, A24-ZZ, A73 - A24\n,B5-C7, A24 -B5, C7-f8 ,A73-B5, A3-VW , 172-ZZ, 23-A3 } ") >> to);
    assert(to.Short("23", "VW") == (list<string>({"23", "A3", "VW"})));
    assert(to.Short("X3", "f8") == (list<string>({"X3", "ZZ", "A24", "B5", "C7", "f8"})));
    assert(to.Short("xx", "23") == (list<string>({})));
    
    return 0;
}