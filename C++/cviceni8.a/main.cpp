/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: petr
 *
 * Created on April 11, 2017, 5:17 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

/*
 * 
 */
class Vozidlo {
public:
    Vozidlo(int kap,int spot):kapacita(kap),spotreba(spot){};
    virtual ~Vozidlo(){};
    virtual void Print(ostream & os) const {
        os << kapacita << " " << spotreba  ;
       
    }
protected:
    int kapacita;
    int spotreba;
};

class Autobus:public Vozidlo{
public:
    Autobus(int kap,int spot,string c):Vozidlo(kap,spot),cislo(c){
        a= new char [100 ];
    };
    virtual void Print(ostream & os) const {
       // os << kapacita << " " << spotreba << " " << cislo << endl ;
        Vozidlo::Print(os);
        os << cislo ;
        
    }
    virtual ~Autobus(){ delete [] a;}
private: 
    string cislo;
    char * a; 


};
class Auto:public Vozidlo{
public:
    Auto(int kap,int spot):Vozidlo(kap,spot){};
    //using Vozidlo::Vozidlo;
};

int main(int argc, char** argv) {

    Vozidlo ** pole = new Vozidlo *[3];
    pole[0] = new Auto(6,6);
    pole[1] = new Vozidlo(4,6);
   
    pole[2] = new Autobus(60,20,"xa");
    
    pole[0]->Print(cout);
    pole[2]->Print(cout);
    
    
    for (int i = 0; i < 3; i++) {
        delete pole[i];
       

    }
     delete [] pole;

    
    
    
    
    
    return 0;
}

