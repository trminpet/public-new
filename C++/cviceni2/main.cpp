/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: petr
 *
 * Created on February 28, 2017, 4:04 PM
 */

#include <cstdlib>
#include <iostream>


/*
 * 
 */


class CFrac{
  
private:
    int m_Num,m_Denom;
    void simplify();
    int gcd(int a,int b);
public:
    CFrac(int num,int denom);
    ~CFrac();
    void Print();
    CFrac add(const CFrac &b) const;
    CFrac mul(const CFrac &b) const;
    
};
CFrac::CFrac(int num,int denom){
    
    if(denom==0){
        std::cout<<"error"<<std::endl;
        exit(1);
    
    }
    m_Num=num;
    this->m_Denom=denom;
    this->simplify();
    

}

CFrac::~CFrac(){}
void CFrac::Print(){

   
    std::cout << m_Num << "/" << m_Denom << std::endl; 

}
int CFrac::gcd(int a,int b){

int c;
  while ( a != 0 ) {
     c = a; a = b%a;  b = c;
  }
  return b;


}
void CFrac::simplify(){

    int g = gcd(std::abs(m_Num),std::abs(m_Denom));
    m_Num/=g;
    m_Denom/=g;

}

CFrac CFrac::add(const CFrac & b) const{
    int x = m_Denom*b.m_Denom;
    int y = m_Num*b.m_Denom + b.m_Num*m_Denom;
    
    return CFrac (y,x);

}


CFrac CFrac::mul(const CFrac & b) const{
    
    
    return CFrac (m_Num* b.m_Num,m_Denom*b.m_Denom);

}
int main(int argc, char** argv) {

    
    
    CFrac a(1,2);
    CFrac b(1,2);
   CFrac c =  a.add(b);
   CFrac d = a.mul(b);

    a.Print();
    c.Print();
    d.Print();
    
    
    return 0;
}

