#include <iostream>
#include <cstdint>
#include <string>
#include <sstream>
using namespace std;

//FLAG for cycle
class CPart {
public:

    CPart() {
    };

    CPart(long t,long s,long i):m_time(t),m_status(s),m_id(i){
        m_longestProduction = 0;
        m_sizeDependencies=0;
        m_made = false;
        m_visited =false;


    };

    ~CPart(){
        //  cout << m_sizeDependencies<<endl;
        if(m_sizeDependencies>0)
            delete [] m_arrayDependencies;

    };

    void print(){
        cout << "--------------------------------------------------"<<endl;
        for (long i = 0; i <m_sizeDependencies ; ++i) {
            cout << m_arrayDependencies[i] << endl;
        }
        cout << "---------------------------------------------------"<<endl;
    }


    long m_time,m_sizeDependencies;
    long * m_arrayDependencies;
    //created = 2 ; visited = 1; nonvisited =0
    long m_status;
    //part is made
    bool m_made;
    //part is visited
    bool m_visited;
    long m_id;
    long m_longestProduction;






};
/*
 * Method for reading time required for making the part
 *
 * */
void readTime(string  l, CPart * array ){
    istringstream fin(l);
    long tmp = 0;
    long i = 0;

    while(fin>>tmp){

        array[i] = CPart(tmp,0,i);
        i++;

    }

    return;


}

/*
 * Reset function for program
 *
 * */
void reset(CPart * array,long count){
    for (long i = 0; i < count ; ++i) {
        array[i].m_longestProduction = 0;
        array[i].m_visited=false;
        array[i].m_made=false;
    }
}
/*
 * DFS function for program
 *
 * HLedani cyklu dle http://www.geeksforgeeks.org/detect-cycle-in-a-graph/
 * 21.10.2017
 * Petr Trminek
 *
 * */


long DFS(CPart & part,CPart * array,CPart & start) {

    long result = 0;



    if (!part.m_made && part.m_visited) {
       return -1;
    }

    if (part.m_sizeDependencies == 0) {


        part.m_made    = true;
        part.m_visited = true;
        return part.m_time;
    }
    if(part.m_made){
        return part.m_longestProduction + part.m_time;
    }



    part.m_visited = true;

    for (long i = 0; i < part.m_sizeDependencies; ++i) {

         long pos = array[part.m_id].m_arrayDependencies[i];


        result = DFS(array[pos], array, start);
        if(result == -1){
            return -1;
        }


        if (result >= part.m_longestProduction) {
            //cout << "AAA"<<endl;
            part.m_longestProduction = result;


        }
    }

    //   cout << part.m_longestProduction + part.m_time<<endl;

    part.m_made = true;
    return part.m_longestProduction + part.m_time;

}






/*
 * Solve function for program
 *
 * */
void solve(CPart * array,long count){


    long * results = new long [count];
    long final_time = 0;
    bool valid = true;


    //DFS for every part
    for (long i = 0; i < count ; ++i) {

        //   cout << "RESI SE VRCHOL " << i<<endl;
        //"Starting parts"

        if(array[i].m_sizeDependencies==0){
            results[i]=0;
        }
        else{

             long tmp =   DFS(array[i], array, array[i]);

           // cout << tmp <<endl;
            if(tmp==-1)
            {
                    valid = false;

                    break;
                }




            results[i] = array[i].m_longestProduction;

        }

        //tracking minimum time for parts

        if(final_time <=results[i]+array[i].m_time){

            final_time = results[i]+array[i].m_time;
            //    cout << "final time"<<endl;
            // cout << final_time<<endl;
        }
      //  reset(array,count);



    }
    //if not cycle

    if(valid){
        cout << final_time<<endl;
        for (long j = 0; j < count ; ++j) {
            if(j==count-1)
                cout <<results[j];
            else
                cout << results[j] <<" ";
        }
    }
        //if cycle
    else{
        cout << "No solution.";
    }

    cout << endl;
    delete [] results;

}




/*
 * Method for reading dependencies
 * Saves ID part into array
 *
 * */
void readDependencies(string & l, CPart & part){
    istringstream fin(l);
    long tmp = 0;
    fin >> tmp;
    part.m_sizeDependencies = tmp;

    if(tmp == 0) return;
    long i = 0;
    part.m_arrayDependencies = new long [tmp];
    while(fin >> tmp){
        part.m_arrayDependencies[i++] = tmp;
    }


}
/*
 * Main function for program
 *
 * */


int main() {
    //Count of parts
    long count = 0;
    cin>>count;


    CPart * arrayOfParts = new CPart[count];


    //Read input
    string line;
    getline(cin,line);
    //Fix for empty line
    getline(cin,line);
    readTime(line,arrayOfParts);



    //Read dependencies by line
    for (long j = 0; j < count; ++j) {
        getline(cin,line);
        readDependencies(line,arrayOfParts[j]);
    }
    //Solve problem





    solve(arrayOfParts,count);




    delete [] arrayOfParts;

    return 0;


}