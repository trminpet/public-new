#include <iostream>
#include <cstdint>
#include <string>
#include <sstream>
using namespace std;

//FLAG for cycle
int FLAG = 0;
class CPart {
public:

    CPart() {
    };

    CPart(long t,long s,long i):m_time(t),m_status(s),m_id(i){
        m_longestProduction = 0;
        m_sizeDependencies=0;


    };

    ~CPart(){
        //  cout << m_sizeDependencies<<endl;
        if(m_sizeDependencies>0)
            delete [] m_arrayDependencies;

    };

    void print(){
        cout << "--------------------------------------------------"<<endl;
        for (long i = 0; i <m_sizeDependencies ; ++i) {
            cout << m_arrayDependencies[i] << endl;
        }
        cout << "---------------------------------------------------"<<endl;
    }


    long m_time,m_sizeDependencies;
    long * m_arrayDependencies;
    long m_status;
    long m_id;
    long m_longestProduction;






};
/*
 * Method for reading time required for making the part
 *
 * */
void readTime(string  l, CPart * array ){
    istringstream fin(l);
    long tmp = 0;
    long i = 0;

    while(fin>>tmp){

        array[i] = CPart(tmp,0,i);
        i++;

    }

    return;


}

/*
 * Reset function for program
 *
 * */
void reset(CPart * array,long count){
    for (long i = 0; i < count ; ++i) {
        array[i].m_status=0;
        array[i].m_longestProduction = 0;
    }
}
/*
 * DFS function for program
 *
 * HLedani cyklu dle http://www.geeksforgeeks.org/detect-cycle-in-a-graph/
 * 21.10.2017
 * Petr Trminek
 *
 * */


long DFS(CPart & part,CPart * array,CPart & start,bool * visited, bool * stack){

    long result =0;


    if(part.m_sizeDependencies==0){


        return part.m_time;
    }
    if(visited[part.m_id] == false)
    {
        visited[part.m_id] = true;
        stack[part.m_id] = true;


        for(long i = 0; i < part.m_sizeDependencies ; ++i)
        {
            unsigned  long pos =array[part.m_id].m_arrayDependencies[i];


               if(!visited[pos]) {

                   result = DFS(array[pos], array, start, visited, stack);

                   if (result >= part.m_longestProduction) {
                     //cout << "AAA"<<endl;
                       part.m_longestProduction = result;
                       visited[pos]=false;
                   }
                   if(result==0){
                       FLAG=1;
                       //  cout << "visited"<<endl;
                       return 0;
                   };



               }
            else if (stack[pos]){

               //    cout << "stack"<<endl;
                FLAG=1;
                return 0;
            }


        }

    }

    stack[part.m_id] = false;
 //   cout << part.m_longestProduction + part.m_time<<endl;


    return  part.m_longestProduction + part.m_time;




}

long DFSVertex(CPart & start, CPart * array,bool * visited, bool * stack, long count){

    for (long k = 0; k < count ; ++k) {
        visited[k] = false;
        stack[k] = false;
    }

   // cout << start.m_sizeDependencies<<endl;

    long res = 0;
    for (long i = 0; i < start.m_sizeDependencies ; ++i) {


        for (long k = 0; k < count ; ++k) {
            visited[k] = false;
            stack[k] = false;
        }
        res=0;
        res = DFS(array[start.m_arrayDependencies[i]],array,start,visited,stack);
        if(FLAG==1) return 0;

    //    cout << "Res=" << res << ", lon=" << start.m_longestProduction<<endl;
        if(res>=start.m_longestProduction){
            start.m_longestProduction=res;
           // cout <<"BBB"<<endl;
        }




    }
    //  cout << res <<endl;
    return res;

}
/*
 * Solve function for program
 *
 * */
void solve(CPart * array,long count){


    long * results = new long [count];
    long final_time = 0;
    bool valid = true;
    bool *visited = new bool[count];
    bool *stack = new bool[count];


    //DFS for every part
    for (long i = 0; i < count ; ++i) {

        //   cout << "RESI SE VRCHOL " << i<<endl;
        //"Starting parts"

        if(array[i].m_sizeDependencies==0){
            results[i]=0;
        }
        else{
            DFSVertex(array[i],array,visited,stack,count);
            if(FLAG==1){
                valid = false;
                FLAG=0;
                break;
            }

            results[i] = array[i].m_longestProduction;

        }

        //tracking minimum time for parts

        if(final_time <=results[i]+array[i].m_time){

            final_time = results[i]+array[i].m_time;
            //    cout << "final time"<<endl;
            // cout << final_time<<endl;
        }

        reset(array,count);

    }
    //if not cycle

    if(valid){
        cout << final_time<<endl;
        for (long j = 0; j < count ; ++j) {
            if(j==count-1)
                cout <<results[j];
            else
                cout << results[j] <<" ";
        }
    }
        //if cycle
    else{
        cout << "No solution.";
    }

    cout << endl;
    delete [] results;
    delete [] stack;
    delete [] visited;
}




/*
 * Method for reading dependencies
 * Saves ID part into array
 *
 * */
void readDependencies(string & l, CPart & part){
    istringstream fin(l);
    long tmp = 0;
    fin >> tmp;
    part.m_sizeDependencies = tmp;

    if(tmp == 0) return;
    long i = 0;
    part.m_arrayDependencies = new long [tmp];
    while(fin >> tmp){
        part.m_arrayDependencies[i++] = tmp;
    }


}
/*
 * Main function for program
 *
 * */


int main() {
    //Count of parts
    long count = 0;
    cin>>count;


    CPart * arrayOfParts = new CPart[count];


    //Read input
    string line;
    getline(cin,line);
    //Fix for empty line
    getline(cin,line);
    readTime(line,arrayOfParts);



    //Read dependencies by line
    for (long j = 0; j < count; ++j) {
        getline(cin,line);
        readDependencies(line,arrayOfParts[j]);
    }
    //Solve problem





        solve(arrayOfParts,count);




    delete [] arrayOfParts;

    return 0;


}