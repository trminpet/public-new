<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method='html'/>
    <xsl:template match="/countries">
        <html>
            <head>
                <meta charset="utf-8"/>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <meta name="description" content="bi-xml-index"/>
                <meta name="author" content="trminpet"/>

                <title>BI-XML-INDEX</title>

                <!-- Bootstrap Core CSS -->
                <link href="css/bootstrap.min.css" rel="stylesheet"/>

                <!-- Custom CSS -->
                <link href="css/scrolling-nav.css" rel="stylesheet"/>

                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->
            </head>
            <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container">


                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav">
                                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                                <li class="hidden">
                                    <a class="page-scroll" href="#page-top"></a>
                                </li>
                                <li>
                                    <a class="" href="cameroon.html">Cameroon</a>
                                </li>
                                <li>
                                    <a class="" href="montenegro.html">Montenegro</a>
                                </li>
                                <li>
                                    <a class="" href="maldives.html">Maldives</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container -->
                </nav>

                <!-- Intro Section -->
                <section id="intro" class="intro-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>Index for countries</h1>
                                <h1>                    
                                    <xsl:apply-templates select="country" />
                                </h1>                    

                            </div>
                        </div>
                    </div>
                </section>

                <!-- About Section -->
                <section id="about" class="about-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>About Section</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Services Section -->
                <section id="services" class="services-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>Services Section</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Contact Section -->
                <section id="contact" class="contact-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>Contact Section</h1>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- jQuery -->
                <script src="js/jquery.js"></script>

                <!-- Bootstrap Core JavaScript -->
                <script src="js/bootstrap.min.js"></script>

                <!-- Scrolling Nav JavaScript -->
                <script src="js/jquery.easing.min.js"></script>
                <script src="js/scrolling-nav.js"></script>





            </body>
        </html>
    </xsl:template>
    <xsl:template match="country">
        <xsl:value-of select="@name" /> <xsl:text>&#xa;</xsl:text>
    </xsl:template>



</xsl:stylesheet>
