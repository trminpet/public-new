<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/countries">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-portrait"
                                       page-height="29.7cm" page-width="21.0cm" margin="2cm">
                    <fo:region-body/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="A4-portrait">
                <fo:flow flow-name="xsl-region-body"> 

                    <fo:block margin-top="250pt">

                        <fo:block space-before.optimum="1.5em" font-size="21pt" font-weight="bold" text-align="center">
                            Semestral work BI-XML
                        </fo:block>

                        <fo:block font-size="16pt" font-weight="bold" text-align="center">
                            Petr Trminek (trminpet)
                        </fo:block>
                        <fo:block font-size="14pt" font-weight="bold" text-align="center">
                            2016/2017 
                        </fo:block>
                        <fo:block color="black" font-size="24pt" font-weight="bold" text-align="center">
                            Montenegro
                        </fo:block>
                    </fo:block>

                    <fo:block page-break-before="always">

                        <fo:block margin-top="15pt" display-align="center" text-align="center">
                            <xsl:apply-templates select="montenegro" />
                        </fo:block>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template match="montenegro">
        <!-- Intro <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block>
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Background</fo:block>


                            <xsl:value-of select="introduction/background" />
                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

        <!-- Geogpraphy <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block>
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Geography </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Location:</fo:block>
                            <fo:block>
                                <xsl:value-of select="geography/location" />
                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt" >Geographic coordinates:</fo:block>
                            <fo:block>
                                <xsl:value-of select="geography/geographic-coordinates" />
                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Map references:</fo:block>
                            <fo:block>
                                <xsl:value-of select="geography/map-references" />
                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Area:</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <fo:table-row><fo:table-cell><fo:block>Total:  <xsl:value-of select="geography/area/total" /> 
                                                <xsl:value-of select="geography/area/total/@unit" /> 
                                            </fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>  Land:  <xsl:value-of select="geography/area/land" /> 
                                                <xsl:value-of select="geography/area/land/@unit" /> 
                                            </fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Water:  <xsl:value-of select="geography/area/water" /> 
                                                <xsl:value-of select="geography/area/water/@unit" /> 
                                            </fo:block></fo:table-cell></fo:table-row>
                                </fo:table-body></fo:table>
                            <fo:block>Country Comparison to the world: <xsl:value-of select="geography/area/country-comparison-to-the-world" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Area comparritive</fo:block> 
                            <fo:block>
                                <xsl:value-of select="geography/area-comparrative" /> 

                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Land boundaries:</fo:block>

                            <fo:block>Total:  <xsl:value-of select="geography/land-boundaries/total" /> 
                                <xsl:value-of select="geography/area/land-boundaries/total/@unit" /> 
                            </fo:block>


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Coastline </fo:block>
                            <fo:block>
                                <xsl:value-of select="geography/coastline" /> 
                                (<xsl:value-of select="geography/coastline/@unit" />) 
                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Maritime claims </fo:block>
                            <fo:block>
                                Terioarial sea
                                <xsl:value-of select="geography/maritime-claims/teritoarial-sea" /> 
                                (<xsl:value-of select="geography/maritime-claims/teritoarial-sea/@unit" />) 
                            </fo:block>
                            <fo:block>
                                Continental shelf
                                <xsl:value-of select="geography/maritime-claims/continental-shelf" /> 

                            </fo:block>



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Climate</fo:block>
                            <xsl:value-of select="geography/climate" /> 
                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Terrain</fo:block>
                            <xsl:value-of select="geography/terrain" /> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Elevation</fo:block>
                            <fo:block>Mean elevation:
                                <xsl:value-of select="geography/elevation/mean-elevation" />
                                ( <xsl:value-of select="geography/elevation/mean-elevation/@unit" />)
                            </fo:block>
                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Elevation extremes:

                                Lowest point: <xsl:value-of select="geography/elevation/elevation-extremes/lowest-point" />
                                Highest point: <xsl:value-of select="geography/elevation/elevation-extremes/highest-point" />
                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Natural resources</fo:block>
                            <fo:block><xsl:value-of select="geography/natural-resources" />
                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Land use:</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <fo:table-row><fo:table-cell><fo:block>Agricultural use:  <xsl:value-of select="geography/land-use/agricultural-land" /> 
                                                <xsl:value-of select="geography/land-use/agricultural-land/@unit" /> 
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block> <xsl:value-of select="geography/land-use/agricultural-land/value" /> </fo:block></fo:table-cell></fo:table-row>                             


                                    <fo:table-row><fo:table-cell><fo:block>Forest:  <xsl:value-of select="geography/land-use/forest" /> 
                                                <xsl:value-of select="geography/land-use/forest/@unit" /> 
                                            </fo:block></fo:table-cell></fo:table-row>

                                    <fo:table-row><fo:table-cell><fo:block>Other:  <xsl:value-of select="geography/land-use/other" /> 
                                                <xsl:value-of select="geography/land-use/other/@unit" /> 
                                            </fo:block></fo:table-cell></fo:table-row>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Iigated land</fo:block>
                            <fo:block> <xsl:value-of select="geography/iigated-land" /> 
                                <xsl:value-of select="geography/iigated-land//@unit" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Population distribution</fo:block>
                            <fo:block>  <xsl:value-of select="geography/population-distribution" /> 


                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Natural hazard</fo:block>
                            <fo:block>  <xsl:value-of select="geography/natural-hazard" /> 


                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Enviroment current issues </fo:block>
                            <fo:block><xsl:value-of select="geography/enviroment-current-issues" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Enviroment internation agreements </fo:block>
                            <fo:block><xsl:value-of select="geography/enviroment-internation-agreements/party-to" /></fo:block>
                            <fo:block><xsl:value-of select="geography/enviroment-internation-agreements/signed" /></fo:block>

                            <fo:block>Geography note </fo:block>
                            <fo:block><xsl:value-of select="geography/geography-note" /></fo:block>


                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>
        <!-- People-society <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block> 
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">People and society </fo:block>
                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Population</fo:block>
                            <fo:block>Population: <xsl:value-of select="people-society/population/value" /></fo:block>

                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/population/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Nationality</fo:block>
                            <fo:block>Noun: <xsl:value-of select="people-society/nationality/noun" /></fo:block>
                            <fo:block>Adjective: <xsl:value-of select="people-society/nationality/adjective" /></fo:block>


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Ethnic-group</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="people-society/ethnic-groups/group">
                                        <fo:table-row>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select ="@name"/>
                                                </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="."/>
                                                </fo:block></fo:table-cell>
                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Languages</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="people-society/languages/group">
                                        <fo:table-row>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select ="@name"/>
                                                </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="."/>
                                                </fo:block></fo:table-cell>
                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Religions</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="people-society/religions/group">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="@name"/> </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="."/>
                                                </fo:block></fo:table-cell>
                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Demographic profile</fo:block>
                            <fo:block><xsl:value-of select="people-society/demographic-profile" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Age structure</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>

                                    <fo:table-row><fo:table-cell><fo:block>Age 0-14
                                                <xsl:value-of select ="people-society/age-structure/age-0-14"/> : 
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Females: <xsl:value-of select="people-society/age-structure/age-0-14/@female"/>
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Males : <xsl:value-of select="people-society/age-structure/age-0-14/@male"/>
                                            </fo:block></fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 15-24
                                                <xsl:value-of select ="people-society/age-structure/age-15-24"/> 
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Females: <xsl:value-of select="people-society/age-structure/age-15-24/@female"/>
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Males : <xsl:value-of select="people-society/age-structure/age-15-24/@male"/>
                                            </fo:block></fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 25-54
                                                <xsl:value-of select ="people-society/age-structure/age-25-54"/> 
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Females: <xsl:value-of select="people-society/age-structure/age-25-54/@female"/>
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Males : <xsl:value-of select="people-society/age-structure/age-25-54/@male"/>
                                            </fo:block></fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 55-64
                                                <xsl:value-of select ="people-society/age-structure/age-55-64"/> 
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Females: <xsl:value-of select="people-society/age-structure/age-55-64/@female"/>
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Males : <xsl:value-of select="people-society/age-structure/age-55-64/@male"/>
                                            </fo:block></fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 65-older
                                                <xsl:value-of select ="people-society/age-structure/age-65-older"/> 
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Females: <xsl:value-of select="people-society/age-structure/age-65-older/@female"/>
                                            </fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Males : <xsl:value-of select="people-society/age-structure/age-65-older/@male"/>
                                            </fo:block></fo:table-cell>
                                    </fo:table-row>

                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Dependency ratios</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <fo:table-row><fo:table-cell><fo:block>Total:
                                                <xsl:value-of select ="people-society/dependency-ratios/total-dependency-ratio"/> 
                                            </fo:block></fo:table-cell> </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Youth:
                                                <xsl:value-of select ="people-society/dependency-ratios/youth-dependency-ratio"/> 
                                            </fo:block></fo:table-cell> </fo:table-row>    
                                    <fo:table-row><fo:table-cell><fo:block>Elderly:
                                                <xsl:value-of select ="people-society/dependency-ratios/elderly-dependency-ratio"/> 
                                            </fo:block></fo:table-cell> </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Potential:
                                                <xsl:value-of select ="people-society/dependency-ratios/potential-support-ratio"/> 
                                            </fo:block></fo:table-cell> </fo:table-row>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Median age </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/median-age/total" /></fo:block> 
                            <fo:block>Male:<xsl:value-of select="people-society/median-age/male" /></fo:block>
                            <fo:block>Female:<xsl:value-of select="people-society/median-age/female" /></fo:block>
                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/median-age/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Population growth rate </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/population-growth-age/@value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/population-growth-age/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Birth rate </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/birth-rate/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/birth-rate/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Death rate </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/death-rate/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/death-rate/country-comparison-to-the-world" /></fo:block> 

                            <fo:block >Population distribution</fo:block>
                            <fo:block>  <xsl:value-of select="people-society/population-distribution" /> 


                            </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Urbanization </fo:block>
                            <fo:block>Population:<xsl:value-of select="people-society/urbanization/urban-population"/></fo:block> 
                            <fo:block>Rate of urbanization<xsl:value-of select="people-society/urbanization/rate-of-urbanization" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Major urban areas</fo:block>
                            <fo:block><xsl:value-of select="people-society/major-urban-areas"/> : 
                                <xsl:value-of select="people-society/major-urban-areas/@count"/></fo:block>



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Sex ration </fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <fo:table-row><fo:table-cell><fo:block>Birth
                                                <xsl:value-of select ="people-society/sex-ratio/birth"/> : 
                                            </fo:block></fo:table-cell>

                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 0-14
                                                <xsl:value-of select ="people-society/sex-ratio/age-0-14"/>  
                                            </fo:block></fo:table-cell>

                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 15-24
                                                <xsl:value-of select ="people-society/asex-ratio/age-15-24"/> 
                                            </fo:block></fo:table-cell>

                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 25-54
                                                <xsl:value-of select ="people-society/asex-ratio/age-25-54"/> 
                                            </fo:block></fo:table-cell>
                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 55-64
                                                <xsl:value-of select ="people-society/sex-ratio/age-55-64"/> 
                                            </fo:block></fo:table-cell>

                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Age 65-older
                                                <xsl:value-of select ="people-society/sex-ratio/age-65-older"/> 
                                            </fo:block></fo:table-cell>

                                    </fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Total:
                                                <xsl:value-of select ="people-society/sex-ratio/total-population"/> 
                                            </fo:block></fo:table-cell>

                                    </fo:table-row>

                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Mothers age at first birth</fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/mothers-mean-age-at-first-birth"/></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Maternal mortality rate </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/maternal-mortality-rate/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/maternal-mortality-rate/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Health expanditures</fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/health-expanditures/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/health-expanditures/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Psychicians density</fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/physicians-density"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Hospital bed density</fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/hospital-bed-density"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Drinking water source </fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="people-society/drinking-water-source/urban">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                                </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                                </fo:block></fo:table-cell>

                                        </fo:table-row>
                                    </xsl:for-each>
                                    <xsl:for-each select="people-society/drinking-water-source/rural">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                                </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                                </fo:block></fo:table-cell>

                                        </fo:table-row>
                                    </xsl:for-each>
                                    <xsl:for-each select="people-society/drinking-water-source/total">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                                </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                                </fo:block></fo:table-cell>

                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Sanitation facility access </fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="people-society/sanitation-facility-access/urban">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                                </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                                </fo:block></fo:table-cell>

                                        </fo:table-row>
                                    </xsl:for-each>
                                    <xsl:for-each select="people-society/sanitation-facility-access/rural">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                                </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                                </fo:block></fo:table-cell>

                                        </fo:table-row>
                                    </xsl:for-each>
                                    <xsl:for-each select="people-society/sanitation-facility-access/total">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                                </fo:block></fo:table-cell>
                                            <fo:table-cell><fo:block>
                                                    <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                                </fo:block></fo:table-cell>

                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">HIV/AIDS adult prevence</fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/hiv-aids-adult-prevence"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">HIV/AIDS adult living</fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/hiv-aids-adult-living"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">HIV/AIDS adult death</fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/hiv-aids-death"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Major infection disease</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="people-society/major-infection-disaeses">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="name(.)"/> : <xsl:value-of select="."/> )
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Obesity </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/obesity/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/obesity/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Children Obesity </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/childen-obesity/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="people-society/childen-obesity/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Education expenditures </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/education-expenditures"/></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Literacy  </fo:block>
                            <fo:block>Definition:<xsl:value-of select="people-society/literacy/definition" /></fo:block> 
                            <fo:block>Total population:<xsl:value-of select="people-society/literacy/total-population" /></fo:block>
                            <fo:block>Male:<xsl:value-of select="people-society/literacy/@male" /></fo:block>
                            <fo:block>Female:<xsl:value-of select="people-society/literacy/@female" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">School life </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/school-life/total" /></fo:block> 
                            <fo:block>Male:<xsl:value-of select="people-society/school-life/male" /></fo:block>
                            <fo:block>Female:<xsl:value-of select="people-society/school-life/female" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Unemployment </fo:block>
                            <fo:block>Total:<xsl:value-of select="people-society/unemployment/total" /></fo:block> 
                            <fo:block>Male:<xsl:value-of select="people-society/unemployment/male" /></fo:block>
                            <fo:block>Female:<xsl:value-of select="people-society/unemployment/female" /></fo:block>



                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

        <!-- Goverment <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block> 
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Government </fo:block>                       <fo:block>Country name</fo:block>
                            <fo:block>Conventional long form:<xsl:value-of select="government/country-name/conventional-long-form"/></fo:block>
                            <fo:block>Conventional short form:<xsl:value-of select="government/country-name/conventional-short-form"/></fo:block>
                            <fo:block>Local long form:<xsl:value-of select="government/country-name/local-long-form"/></fo:block>
                            <fo:block>Local short form:<xsl:value-of select="government/country-name/local-short-form"/></fo:block>
                            <fo:block>Former:<xsl:value-of select="government/country-name/former"/></fo:block>
                            <fo:block>Etymology<xsl:value-of select="government/country-name/etymology"/></fo:block>

                            <fo:block>Goverment type</fo:block>
                            <fo:block><xsl:value-of select="government/government-type"/></fo:block>

                            <fo:block>Capital city</fo:block>
                            <fo:block>Name :<xsl:value-of select="government/capital/name"/></fo:block>
                            <fo:block>Geographic coordinates :<xsl:value-of select="government/capital/geographic-coordinates"/></fo:block>
                            <fo:block>Time difference :<xsl:value-of select="government/capital/time-difference"/></fo:block>
                            <fo:block>Daylight saving time:<xsl:value-of select="government/capital/daylight-saving-time"/></fo:block>

                            <fo:block>Administrative divisions </fo:block>
                            <fo:block><xsl:value-of select="government/administrative-divisions"/></fo:block>

                            <fo:block>Independence</fo:block>
                            <fo:block>Date:<xsl:value-of select="government/independence"/></fo:block>

                            <fo:block>National holiday</fo:block>
                            <fo:block>Date:<xsl:value-of select="government/national-holiday"/></fo:block>

                            <fo:block>Consititution</fo:block>
                            <fo:block>History:<xsl:value-of select="government/consititution/history"/></fo:block>
                            <fo:block>Amendments:<xsl:value-of select="government/consititution/amendments"/></fo:block>

                            <fo:block>Legal system</fo:block>
                            <fo:block><xsl:value-of select="government/legal-system"/></fo:block>

                            <fo:block>International lawyer organization </fo:block>
                            <fo:block><xsl:value-of select="government/international-law-org"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Citizenship</fo:block>
                            <fo:block>By Birth:<xsl:value-of select="government/citizenship/citizenship-by-birth"/></fo:block>
                            <fo:block>Descent:<xsl:value-of select="government/citizenship/citizenship-descent"/></fo:block>
                            <fo:block>Dual citizenship:<xsl:value-of select="government/citizenship/dual-citizenship"/></fo:block>
                            <fo:block>Residency requirement:<xsl:value-of select="government/citizenship/residency-requirement"/></fo:block>

                            <fo:block>Suffrage</fo:block>
                            <fo:block><xsl:value-of select="government/suffrage"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Executive branch </fo:block>
                            <fo:block>Chief of state:<xsl:value-of select="government/executive-branch/chief-of-state"/></fo:block>
                            <fo:block>Head of goverment:<xsl:value-of select="government/executive-branch/head-of-government"/></fo:block>
                            <fo:block>Cabinet:<xsl:value-of select="government/executive-branch/cabinet"/></fo:block>
                            <fo:block>Elections:<xsl:value-of select="government/executive-branch/elections"/></fo:block>
                            <fo:block>Election result:<xsl:value-of select="government/executive-branch/election-result"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Legislative branch </fo:block>
                            <fo:block>Description:<xsl:value-of select="government/legislative-branch/description"/></fo:block>
                            <fo:block>Elections:<xsl:value-of select="government/legislative-branch/elections"/></fo:block>
                            <fo:block>Election result:<xsl:value-of select="government/legislative-branch/election-result"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Judicial branch</fo:block>
                            <fo:block>Highest court:<xsl:value-of select="government/judicial-branch/highest-court"/></fo:block>
                            <fo:block>Judge selection and term:<xsl:value-of select="government/judicial-branch/judge-selection-and-term"/></fo:block>
                            <fo:block>Subordinate court<xsl:value-of select="government/judicial-branch/subordinate-court"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Political party</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="government/political-party/party">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> [ <xsl:value-of select="@leader"/> ]
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Political pressure groups </fo:block>
                            <fo:block><xsl:value-of select="government/political-pressure-group"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Internation org</fo:block>
                            <fo:block><xsl:value-of select="government/internation-org"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Diplomatic in USA </fo:block>
                            <fo:block>Chief of mission:<xsl:value-of select="government/diplomatic-in-US/chief-of-mission"/></fo:block>
                            <fo:block>Chancery:<xsl:value-of select="government/diplomatic-in-US/chancery"/></fo:block>
                            <fo:block>Telephone:<xsl:value-of select="government/diplomatic-in-US/telephone"/></fo:block>
                            <fo:block>FAX:<xsl:value-of select="government/diplomatic-in-US/FAX"/></fo:block>
                            <fo:block>Consulate-general:<xsl:value-of select="government/diplomatic-in-US/consulate-general"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Diplomatic from USA</fo:block>
                            <fo:block>Chief of mission:<xsl:value-of select="government/diplomatic-from-US/chief-of-mission"/></fo:block>
                            <fo:block>Chancery:<xsl:value-of select="government/diplomatic-from-US/chancery"/></fo:block>
                            <fo:block>Mailing address:<xsl:value-of select="government/diplomatic-from-US/mailling-address"/></fo:block>
                            <fo:block>Telephone:<xsl:value-of select="government/diplomatic-from-US/telephone"/></fo:block>
                            <fo:block>FAX:<xsl:value-of select="government/diplomatic-from-US/FAX"/></fo:block>

                            <fo:block>Flag description</fo:block>
                            <fo:block><xsl:value-of select="government/flag-description"/></fo:block>

                            <fo:block>National symbol</fo:block>
                            <fo:block><xsl:value-of select="government/national-symbol"/></fo:block>

                            <fo:block>National anthem</fo:block>
                            <fo:block>Name:<xsl:value-of select="government/national-anthem/name"/></fo:block>
                            <fo:block>Lyrics:<xsl:value-of select="government/national-anthem/lyrics"/></fo:block>
                            <fo:block>Note:<xsl:value-of select="government/national-anthem/note"/></fo:block>



                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

        <!-- Economy <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block> 
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Economy </fo:block>
                            <fo:block>Overview</fo:block>
                            <fo:block>Note:<xsl:value-of select="economy/economy-overview"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">GDP parity</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/gdp-parity/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/gdp-parity/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">GDP official</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/gdp-official/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/gdp-official/country-comparison-to-the-world" /></fo:block> 

                            <fo:block>GDP real</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/gdp-real/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/gdp-real/country-comparison-to-the-world" /></fo:block> 


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">GDP per capita</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/gdp-per-capita/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/gdp-per-capita/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Gross national saving</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/gross-national-saving/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/gross-national-saving/country-comparison-to-the-world" /></fo:block> 


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">GDP composition</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <fo:table-row><fo:table-cell><fo:block>Household:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-composition/household"/></fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Government:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-composition/government"/></fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Investment-fixed:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-composition/investment-fixed"/></fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Investment-inventories:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-composition/investment-in-inventories"/></fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Export:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-composition/export"/></fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Import:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-composition/import"/></fo:block></fo:table-cell></fo:table-row>
                                </fo:table-body></fo:table>


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">GDP sector</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <fo:table-row><fo:table-cell><fo:block>Agriculture:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-sector/agriculture"/></fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Industry:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-sector/industry"/></fo:block></fo:table-cell></fo:table-row>
                                    <fo:table-row><fo:table-cell><fo:block>Services:</fo:block></fo:table-cell><fo:table-cell><fo:block><xsl:value-of select="economy/gdp-sector/services"/></fo:block></fo:table-cell></fo:table-row>
                                </fo:table-body></fo:table>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Agriculture products</fo:block>
                            <fo:block><xsl:value-of select="economy/agriculture-products"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Industries</fo:block>
                            <fo:block><xsl:value-of select="economy/industries"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Industrial production </fo:block>
                            <fo:block>Total:<xsl:value-of select="economy/industrial-production/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/industrial-production/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Labor force </fo:block>
                            <fo:block>Total:<xsl:value-of select="economy/labor-force/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/labor-force/country-comparison-to-the-world" /><fo:block> 

                                    <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Unemployment rate </fo:block>
                                    <fo:block>Total:<xsl:value-of select="economy/unemployment-rate/value"/></fo:block> 
                                    <fo:block>Comparison to the world::<xsl:value-of select="economy/unemployment-rate/country-comparison-to-the-world" /></fo:block> 

                                    <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Population below poverty line </fo:block>
                                    <fo:block>Total:<xsl:value-of select="economy/population-below-poverty-line/value"/></fo:block> 

                                    <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Distribution family income </fo:block>
                                    <fo:block>Total:<xsl:value-of select="economy/distribution-family-income/value"/></fo:block> 
                                    <fo:block>Total:<xsl:value-of select="economy/distribution-family-income/value"/></fo:block>
                                    <fo:block>Comparison to the world::<xsl:value-of select="economy/distribution-family-income/country-comparison-to-the-world" /></fo:block> 

                                    <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Budget </fo:block>
                                </fo:block>Revenue:<xsl:value-of select="economy/budget/revenue"/></fo:block> 
                            <fo:block>Expenditures:<xsl:value-of select="economy/budget/expenditures"/></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Taxes </fo:block>
                            <fo:block>Total:<xsl:value-of select="economy/taxes/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/taxes/country-comparison-to-the-world" /></fo:block> 

                            <fo:block> font-size="16pt" font-weight="bold" margin-bottom="10pt"Budget surplus </fo:block>
                            <fo:block>Total:<xsl:value-of select="economy/budget-surplus/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/budget-surplus/country-comparison-to-the-world" /></fo:block> 


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Public debt</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/public-debt/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/public-debt/country-comparison-to-the-world" /></fo:block> 
                            <fo:block>Note:<xsl:value-of select="economy/public-debt/note" /></fo:block> 


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Fiscal year</fo:block>
                            <fo:block><xsl:value-of select="economy/fiscal-year" /></fo:block> 


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Inflation rate</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/inflation-rate/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/inflation-rate/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Commercial bank</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/commercial-bank/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/commercial-bank/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Stock narrow money</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/stock-narrow-money/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/stock-narrow-money/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Stock board money</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/stock-board-money/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/stock-board-money/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Stock domestic credit</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/stock-domestic-credit/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/stock-domestic-credit/country-comparison-to-the-world" /></fo:block> 



                            <fo:block>Market value</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/market-value/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/market-value/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Current balance</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/current-balance/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/current-balance/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Exports</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/exports/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Exports commodity:<xsl:value-of select="economy/exports-comm" /></fo:block> 
                            <fo:block>Exports partners:<xsl:value-of select="economy/exports-partners" /></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/exports/country-comparison-to-the-world" /></fo:block> 




                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Imports</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/imports/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Imports commodity:<xsl:value-of select="economy/imports-comm" /></fo:block> 
                            <fo:block>Imports partners:<xsl:value-of select="economy/imports-partners" /></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/imports/country-comparison-to-the-world" /></fo:block> 




                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Reserver gold</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/reserver-gold/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/reserver-gold/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Debt external</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/debt-external/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/debt-external/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Stock foreigh home</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/stock-foreign-home/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/stock-foreign-home/country-comparison-to-the-world" /></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Stock foreigh abroad</fo:block>
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/stock-foreign-abroad/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="economy/stock-foreign-abroad/country-comparison-to-the-world" /></fo:block> 




                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Exchange rates</fo:block>
                            <fo:block>Note:<xsl:value-of select="economy/exchange-rates/note" /></fo:block> 
                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="economy/exchange-rates/year">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> [ <xsl:value-of select ="@y"/> ]
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>






                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

        <!-- Energy <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block>
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Energy </fo:block>
                            <fo:block>Electricity access </fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-access/value" /></fo:block> 


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity production</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-production/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-production/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity consumption</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-consumption/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-consumption/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity export</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-export/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-export/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity import</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-import/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-import/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity capacity</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-capacity/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-capacity/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity fossil</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-fossil/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-fossil/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity nuclear</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-nuclear/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-nuclear/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity hydro</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-hydro/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-hydro/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Electricity other</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/electricity-other/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/electricity-other/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Crudeoil production</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/crudeoil-production/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/crudeoil-production/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Crudeoil export</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/crudeoil-export/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/crudeoil-export/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Crudeoil import</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/crudeoil-import/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/crudeoil-import/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Crudeoil reserves</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/crudeoil-reserves/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/crudeoil-reserves/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Petroleum production</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/petroleum-production/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/petroleum-production/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Petroleum export</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/petroleum-export/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/petroleum-export/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Petroleum import</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/petroleum-import/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/petroleum-import/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Petroleum consumption</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/petroleum-consumption/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/petroleum-consumption/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Gas production</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/gas-production/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/gas-production/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Gas export</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/gas-export/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/gas-export/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Gas import</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/gas-import/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/gas-import/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Gas consumption</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/gas-consumption/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/gas-consumption/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Gas reserves</fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/gas-reserves/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/gas-reserves/country-comparison-to-the-world" /></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Carbon emissions </fo:block>
                            <fo:block>Total:<xsl:value-of select="energy/gas-reserves/value"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="energy/gas-reserves/country-comparison-to-the-world" /></fo:block> 



                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

        <!-- Communication <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block>
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Communication </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Telephone lines</fo:block>
                            <fo:block>Total:<xsl:value-of select="communication/telephone-line/total"/></fo:block> 
                            <fo:block>Subs:<xsl:value-of select="communication/telephone-line/subs"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="communication/telephone-line/country-comparison-to-the-world" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Telephone mobile</fo:block>
                            <fo:block>Total:<xsl:value-of select="communication/telephone-mobile/total"/></fo:block> 
                            <fo:block>Subs:<xsl:value-of select="communication/telephone-mobile/subs"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="communication/telephone-mobile/country-comparison-to-the-world" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Telephone system</fo:block>
                            <fo:block>General:<xsl:value-of select="communication/telephone-system/general"/></fo:block> 
                            <fo:block>Domestic:<xsl:value-of select="communication/telephone-system/domestic"/></fo:block> 
                            <fo:block>International:<xsl:value-of select="communication/telephone-system/international" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Broadcast media</fo:block>
                            <fo:block><xsl:value-of select="communication/broadcast-media"/></fo:block> 


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Internet code</fo:block>
                            <fo:block><xsl:value-of select="communication/internet-code"/></fo:block> 


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Internet users</fo:block>
                            <fo:block>Total:<xsl:value-of select="communication/internet-users/total"/></fo:block> 
                            <fo:block>percent:<xsl:value-of select="communication/internet-users/percent"/></fo:block> 
                            <fo:block>Comparison to the world::<xsl:value-of select="communication/internet-users/country-comparison-to-the-world" /></fo:block>





                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

        <!-- Transportation <fo:block> -->
        <fo:block>
            <fo:block page-break-before="always">
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Transportation </fo:block>
                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Air system</fo:block>
                            <fo:block>Number of air carriers:<xsl:value-of select="transportation/air-system/number-of-air-carriers"/></fo:block> 
                            <fo:block>Inventory:<xsl:value-of select="transportation/air-system/number-of-air-carriers"/></fo:block> 
                            <fo:block>Annual passenger:<xsl:value-of select="transportation/air-system/annual-passenger"/></fo:block> 
                            <fo:block>Annual freight:<xsl:value-of select="transportation/air-system/annual-freight"/></fo:block>  

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Air prefix</fo:block>
                            <fo:block> <xsl:value-of select="transportation/air-prefix"/></fo:block>  

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Airport </fo:block>
                            <fo:block>Total: <xsl:value-of select="transportation/airport/value"/></fo:block>  
                            <fo:block>Comparison to the world::<xsl:value-of select="transportation/airport/country-comparison-to-the-world" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Heliport</fo:block>
                            <fo:block>Total: <xsl:value-of select="transportation/heliports/total"/></fo:block>


                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Railways</fo:block>
                            <fo:block>Total: <xsl:value-of select="transportation/railways/total"/></fo:block> 
                            <fo:block>Standart: <xsl:value-of select="transportation/railways/standart"/></fo:block> 

                            <fo:block>Comparison to the world::<xsl:value-of select="transportation/railways/country-comparison-to-the-world" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Roadways</fo:block>
                            <fo:block>Total: <xsl:value-of select="transportation/roadways/total"/></fo:block> 
                            <fo:block>Paved: <xsl:value-of select="transportation/roadways/paved"/></fo:block> 
                            <fo:block>Unpaved: <xsl:value-of select="transportation/roadways/unpaved"/></fo:block> 

                            <fo:block>Comparison to the world::<xsl:value-of select="transportation/roadways/country-comparison-to-the-world" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Marine</fo:block>
                            <fo:block>Total: <xsl:value-of select="transportation/marine/total"/></fo:block> 
                            <fo:block>Type: <xsl:value-of select="transportation/marine/type"/></fo:block> 
                            <fo:block>Foreign owner: <xsl:value-of select="transportation/marine/foreign-owner"/></fo:block>
                            <fo:block>Registred: <xsl:value-of select="transportation/marine/registred"/></fo:block>
                            <fo:block>Comparison to the world::<xsl:value-of select="transportation/marine/country-comparison-to-the-world" /></fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Port</fo:block>
                            <fo:block> <xsl:value-of select="transportation/port"/></fo:block> 



                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

        <!-- Military <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block>
                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Military </fo:block>
                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Military branches</fo:block>
                            <fo:block><xsl:value-of select="military/military-branches"/></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Service age</fo:block>
                            <fo:block><xsl:value-of select="military/service-age"/></fo:block> 



                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Expenditures</fo:block>



                            <fo:table table-layout="fixed" width="100%"><fo:table-body>
                                    <xsl:for-each select="military/expenditures/value">
                                        <fo:table-row><fo:table-cell><fo:block>
                                                    <xsl:value-of select ="."/> 
                                                </fo:block></fo:table-cell>


                                        </fo:table-row>
                                    </xsl:for-each>
                                </fo:table-body></fo:table>
                            <fo:block>Comparison to the world::<xsl:value-of select="military/expenditures/country-comparison-to-the-world" /></fo:block>





                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

        <!-- Issues <fo:block> -->
        <fo:block page-break-before="always">
            <fo:block>

                <fo:block>
                    <fo:block>
                        <fo:block>
                            <fo:block font-size="24pt" font-weight="bold" margin-bottom="10pt">Transnational-issues </fo:block>

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Disputes</fo:block>
                            <fo:block><xsl:value-of select="transnational-issues/disputes"/></fo:block> 

                            <fo:block font-size="16pt" font-weight="bold" margin-bottom="10pt">Refugees</fo:block>
                            <fo:block><xsl:value-of select="transnational-issues/refugees"/></fo:block> 



                        </fo:block>
                    </fo:block>
                </fo:block>
            </fo:block>
        </fo:block>

    </xsl:template>


</xsl:stylesheet>
