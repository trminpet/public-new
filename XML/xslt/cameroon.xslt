<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method='html'/>
    <xsl:template match="/countries">
        <html>
            <head>
                <meta charset="utf-8"/>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <meta name="description" content="bi-xml-index"/>
                <meta name="author" content="trminpet"/>

                <title>BI-XML-CAMEROON</title>

                <!-- Bootstrap Core CSS -->
                <link href="css/bootstrap.min.css" rel="stylesheet"/>

                <!-- Custom CSS -->
                <link href="css/scrolling-nav.css" rel="stylesheet"/>

                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->
            </head>
            <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

                <!-- Navigation -->
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container">


                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav">
                                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                                <li class="hidden">
                                    <a class="page-scroll" href="#page-top"></a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="index.html">Index</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#introduction">Introduction</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#geography">Geography</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#people-society">People and society</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#government">Government</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#economy">Economy</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#energy">Energy</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#communication">Communication</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#transportation">Transportation</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#military">Military</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#transnational-issues">Transnational issues</a>
                                </li>
                                <li>
                                    <a class="page-scroll" href="#gallery">Gallery</a>
                                </li>

                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container -->
                </nav>
            <xsl:apply-templates select="cameroon"/>




            <!-- jQuery -->
            <script src="js/jquery.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="js/bootstrap.min.js"></script>

            <!-- Scrolling Nav JavaScript -->
            <script src="js/jquery.easing.min.js"></script>
            <script src="js/scrolling-nav.js"></script>

            </body>

        </html>
    </xsl:template>
    <xsl:template match="cameroon">
        <!-- Intro Section -->
        <section id="introduction" class="intro-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Background</h1>

                        <p>
                        <xsl:value-of select="introduction/background" />
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <!-- Geography Section -->
        <section id="geography" class="geo">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Geography Section</h1>

                        <h2>Location:</h2>
                        <p>
                        <xsl:value-of select="geography/location" />
                        </p>
                        <hr />
                        <h2>Geographic coordinates:</h2>
                        <p>
                        <xsl:value-of select="geography/geographic-coordinates" />
                        </p>
                        <hr />
                        <h2>Map references:</h2>
                        <p>
                        <xsl:value-of select="geography/map-references" />
                        </p>
                        <hr />
                        <h2>Area:</h2>
                        <table>
                            <tr><td>Total:  <xsl:value-of select="geography/area/total" /> 
                            <xsl:value-of select="geography/area/total/@unit" /> 
                            </td></tr>
                            <tr><td>  Land:  <xsl:value-of select="geography/area/land" /> 
                            <xsl:value-of select="geography/area/land/@unit" /> 
                            </td></tr>
                            <tr><td>Water:  <xsl:value-of select="geography/area/water" /> 
                            <xsl:value-of select="geography/area/water/@unit" /> 
                            </td></tr>
                        </table>
                        <p>Country Comparison to the world:: <xsl:value-of select="geography/area/country-comparison-to-the-world" /></p>
                        <hr />
                        <h2>Area comparritive</h2> 
                        <p>
                        <xsl:value-of select="geography/area-comparrative" /> 

                        </p>
                        <hr />
                        <h2>Land boundaries</h2>

                        <p>Total:  <xsl:value-of select="geography/land-boundaries/total" /> 
                        <xsl:value-of select="geography/area/land-boundaries/total/@unit" /> 
                        </p>
                        <table>
                            <tr>
                                <th>Name</th>
                                <th>Size</th>
                            </tr>
                            <xsl:for-each select="geography/land-boundaries/border-countries/border-country">
                                <tr>
                                    <td><xsl:value-of select="@name"/></td>
                                <td>
                                <xsl:value-of select="@size"/>
                                (<xsl:value-of select="@unit"/>)

                                </td>
                                </tr>
                            </xsl:for-each>
                        </table>
                        <hr />
                        <h2>Coastline </h2>
                        <p>
                        <xsl:value-of select="geography/coastline" /> 
                        (<xsl:value-of select="geography/coastline/@unit" />) 
                        </p>
                        <hr />
                        <h2>Maritime claims </h2>
                        <p>
                            Terioarial sea
                        <xsl:value-of select="geography/maritime-claims/teritoarial-sea" /> 
                        (<xsl:value-of select="geography/maritime-claims/teritoarial-sea/@unit" />) 
                        </p>
                        <p>
                            Contiguos zone
                        <xsl:value-of select="geography/maritime-claims/contiguos-zone" /> 
                        (<xsl:value-of select="geography/maritime-claims/contiguos-zone/@unit" />) 
                        </p>
                        <hr />
                        <h2>Climate</h2>
                        <xsl:value-of select="geography/climate" /> 
                        <h2>Terrain</h2>
                        <xsl:value-of select="geography/terrain" /> 
                        <hr />
                        <h2>Elevation</h2>
                        <p>Mean elevation:
                        <xsl:value-of select="geography/elevation/mean-elevation" />
                        ( <xsl:value-of select="geography/elevation/mean-elevation/@unit" />)
                        </p>
                        <p>Elevation extremes:

                            Lowest point: <xsl:value-of select="geography/elevation/elevation-extremes/lowest-point" />
                        Highest point: <xsl:value-of select="geography/elevation/elevation-extremes/highest-point" />
                        </p>
                        <hr />
                        <h2>Natural resources</h2>
                        <p><xsl:value-of select="geography/natural-resources" />
                        </p>
                        <hr />
                        <h2>Land use:</h2>
                        <table>
                            <tr><td>Agricultural use:  <xsl:value-of select="geography/land-use/agricultural-land" /> 
                            <xsl:value-of select="geography/land-use/agricultural-land/@unit" /> 
                            </td>
                            <td> <xsl:value-of select="geography/land-use/agricultural-land/value" /> </td></tr>                             


                            <tr><td>Forest:  <xsl:value-of select="geography/land-use/forest" /> 
                            <xsl:value-of select="geography/land-use/forest/@unit" /> 
                            </td></tr>

                            <tr><td>Other:  <xsl:value-of select="geography/land-use/other" /> 
                            <xsl:value-of select="geography/land-use/other/@unit" /> 
                            </td></tr>
                        </table>
                        <hr />
                        <h2>Iigated land</h2>
                        <p> <xsl:value-of select="geography/iigated-land" /> 
                        <xsl:value-of select="geography/iigated-land//@unit" /> </p>
                        <hr />
                        <h2>Natural hazard</h2>
                        <p>  <xsl:value-of select="geography/natural-hazard" /> 

                        <h3>Volcanism</h3>
                        <p><xsl:value-of select="geography/natural-hazard/volcanism" /> 
                        </p>
                        </p>
                        <hr />
                        <h2>Enviroment current issues </h2>
                        <p><xsl:value-of select="geography/enviroment-current-issues" /></p>
                        <hr />
                        <h2>Enviroment internation agreements </h2>
                        <p><xsl:value-of select="geography/enviroment-internation-agreements/party-to" /></p>
                        <p><xsl:value-of select="geography/enviroment-internation-agreements/signed" /></p>
                        <hr />
                        <h2>Geography note </h2>
                        <p><xsl:value-of select="geography/geography-note" /></p>


                    </div>
                </div>
            </div>
        </section>
        <!-- People-society Section -->
        <section id="people-society" class="people">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>People and society section</h1>
                        <h2>Population</h2>
                        <p>Population: <xsl:value-of select="people-society/population/value" /></p>
                        <i>Note: <xsl:value-of select="people-society/population/note" /></i>  
                        <p>Comparison to the world:<xsl:value-of select="people-society/population/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Nationality</h2>
                        <p>Noun: <xsl:value-of select="people-society/nationality/noun" /></p>
                        <p>Adjective: <xsl:value-of select="people-society/nationality/adjective" /></p>

                        <hr />
                        <h2>Ethnic-group</h2>
                        <table>
                            <xsl:for-each select="people-society/ethnic-groups/group">
                                <tr>
                                    <td>
                                <xsl:value-of select ="@name"/>
                                </td>
                                <td>
                                <xsl:value-of select="."/>
                                </td>
                                </tr>
                            </xsl:for-each>
                        </table>
                        <hr />
                        <h2>Languages</h2>
                        <p><xsl:value-of select="people-society/languages/value" /></p> 
                        <hr />
                        <h2>Religions</h2>
                        <table>
                            <xsl:for-each select="people-society/religions/group">
                                <tr><td>
                                <xsl:value-of select ="@name"/> </td>
                                <td>
                                <xsl:value-of select="."/>
                                </td>
                                </tr>
                            </xsl:for-each>
                        </table>
                        <hr />
                        <h2>Demographic profile</h2>
                        <p><xsl:value-of select="people-society/demographic-profile" /></p> 
                        <hr />
                        <h2>Age structure</h2>
                        <table>

                            <tr><td>Age 0-14
                            <xsl:value-of select ="people-society/age-structure/age-0-14"/> : 
                            </td>
                            <td>Females: <xsl:value-of select="people-society/age-structure/age-0-14/@female"/>
                            </td>
                            <td>Males : <xsl:value-of select="people-society/age-structure/age-0-14/@male"/>
                            </td>
                            </tr>
                            <tr><td>Age 15-24
                            <xsl:value-of select ="people-society/age-structure/age-15-24"/> 
                            </td>
                            <td>Females: <xsl:value-of select="people-society/age-structure/age-15-24/@female"/>
                            </td>
                            <td>Males : <xsl:value-of select="people-society/age-structure/age-15-24/@male"/>
                            </td>
                            </tr>
                            <tr><td>Age 25-54
                            <xsl:value-of select ="people-society/age-structure/age-25-54"/> 
                            </td>
                            <td>Females: <xsl:value-of select="people-society/age-structure/age-25-54/@female"/>
                            </td>
                            <td>Males : <xsl:value-of select="people-society/age-structure/age-25-54/@male"/>
                            </td>
                            </tr>
                            <tr><td>Age 55-64
                            <xsl:value-of select ="people-society/age-structure/age-55-64"/> 
                            </td>
                            <td>Females: <xsl:value-of select="people-society/age-structure/age-55-64/@female"/>
                            </td>
                            <td>Males : <xsl:value-of select="people-society/age-structure/age-55-64/@male"/>
                            </td>
                            </tr>
                            <tr><td>Age 65-older
                            <xsl:value-of select ="people-society/age-structure/age-65-older"/> 
                            </td>
                            <td>Females: <xsl:value-of select="people-society/age-structure/age-65-older/@female"/>
                            </td>
                            <td>Males : <xsl:value-of select="people-society/age-structure/age-65-older/@male"/>
                            </td>
                            </tr>

                        </table>
                        <hr />
                        <h2>Median age </h2>
                        <p>Total:<xsl:value-of select="people-society/median-age/total" /></p> 
                        <p>Male:<xsl:value-of select="people-society/median-age/male" /></p>
                        <p>Female:<xsl:value-of select="people-society/median-age/female" /></p>
                        <p>Comparison to the world:<xsl:value-of select="people-society/median-age/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Population growth rate </h2>
                        <p>Total:<xsl:value-of select="people-society/population-growth-age/@value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/population-growth-age/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Birth rate </h2>
                        <p>Total:<xsl:value-of select="people-society/birth-rate/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/birth-rate/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Death rate </h2>
                        <p>Total:<xsl:value-of select="people-society/death-rate/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/death-rate/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Net migration rate </h2>
                        <p>Total:<xsl:value-of select="people-society/net-migration-rate/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/net-migration-rate/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Urbanization </h2>
                        <p>Population:<xsl:value-of select="people-society/urbanization/urban-population"/></p> 
                        <p>Rate of urbanization:<xsl:value-of select="people-society/urbanization/rate-of-urbanization" /></p> 
                        <hr />
                        <h2>Major urban areas</h2>
                        <p><xsl:value-of select="people-society/major-urban-areas[1]"/> : 
                        <xsl:value-of select="people-society/major-urban-areas/@count[1]"/> </p>

                        <p><xsl:value-of select="people-society/major-urban-areas[2]"/> : 
                        <xsl:value-of select="people-society/major-urban-areas/@count[2]"/> </p>
                        <hr />

                        <h2>Mothers age at first birth</h2>
                        <p>Total:<xsl:value-of select="people-society/mothers-mean-age-at-first-birth"/></p> 
                        <hr />
                        <h2>Maternal mortality rate </h2>
                        <p>Total:<xsl:value-of select="people-society/maternal-mortality-rate/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/maternal-mortality-rate/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Infant mortality </h2><p>Total:<xsl:value-of select="people-society/infant-mortality/total" /></p> 
                        <p>Male:<xsl:value-of select="people-society/infant-mortality/male" /></p>
                        <p>Female:<xsl:value-of select="people-society/infant-mortality/female" /></p>
                        <p>Comparison to the world:<xsl:value-of select="people-society/infant-mortality/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Life expectancy</h2>
                        <p>Total:<xsl:value-of select="people-society/life-expactancy/total" /></p> 
                        <p>Male:<xsl:value-of select="people-society/life-expactancy/male" /></p>
                        <p>Female:<xsl:value-of select="people-society/life-expactancy/female" /></p>
                        <p>Comparison to the world:<xsl:value-of select="people-society/life-expactancy/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Total fertility</h2>
                        <p>Total:<xsl:value-of select="people-society/total-fertility/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/total-fertility/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Health expanditures</h2>
                        <p>Total:<xsl:value-of select="people-society/health-expanditures/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/health-expanditures/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Psychicians density</h2>
                        <p>Total:<xsl:value-of select="people-society/physicians-density"/></p>
                        <hr />
                        <h2>Hospital bed density</h2>
                        <p>Total:<xsl:value-of select="people-society/hospital-bed-density"/></p>
                        <hr />
                        <h2>Drinking water source </h2>
                        <table>
                            <xsl:for-each select="people-society/drinking-water-source/urban">
                                <tr><td>
                                <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                </td>
                                <td>
                                <xsl:value-of select="@value"/> <xsl:value-of select="."/> 	
                                </td>

                                </tr>
                            </xsl:for-each>
                            <xsl:for-each select="people-society/drinking-water-source/rural">
                                <tr><td>
                                <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                </td>
                                <td>
                                <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                </td>

                                </tr>
                            </xsl:for-each>
                            <xsl:for-each select="people-society/drinking-water-source/total">
                                <tr><td>
                                <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                </td>
                                <td>
                                <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                </td>

                                </tr>
                            </xsl:for-each>
                        </table>
                        <hr />
                        <h2>Sanitation facility access </h2>
                        <table>
                            <xsl:for-each select="people-society/sanitation-facility-access/urban">
                                <tr><td>
                                <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                </td>
                                <td>
                                <xsl:value-of select="@value"/> <xsl:value-of select="."/> 	
                                </td>

                                </tr>
                            </xsl:for-each>
                            <xsl:for-each select="people-society/sanitation-facility-access/rural">
                                <tr><td>
                                <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                </td>
                                <td>
                                <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                </td>

                                </tr>
                            </xsl:for-each>
                            <xsl:for-each select="people-society/sanitation-facility-access/total">
                                <tr><td>
                                <xsl:value-of select ="name(.)"/> ( <xsl:value-of select="@type"/> )
                                </td>
                                <td>
                                <xsl:value-of select="@value"/> <xsl:value-of select="."/>  
                                </td>

                                </tr>
                            </xsl:for-each>
                        </table>
                        <hr />
                        <h2>HIV/AIDS adult prevence</h2>
                        <p>Total:<xsl:value-of select="people-society/hiv-aids-adult-prevence"/></p>
                        <hr />
                        <h2>HIV/AIDS adult living</h2>
                        <p>Total:<xsl:value-of select="people-society/hiv-aids-adult-living"/></p>
                        <hr />
                        <h2>HIV/AIDS adult death</h2>
                        <p>Total:<xsl:value-of select="people-society/hiv-aids-adult-death"/></p>
                        <hr />
                        <h2>Major infection disease</h2>
                        <table>
                            <xsl:for-each select="people-society/major-infection-disaeses">
                                <tr><td>
                                <xsl:value-of select ="local-name(.)"/> : <xsl:value-of select="."/> )
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <hr />
                        <h2>Obesity </h2>
                        <p>Total:<xsl:value-of select="people-society/obesity/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/obesity/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Children Obesity </h2>
                        <p>Total:<xsl:value-of select="people-society/childen-obesity/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="people-society/childen-obesity/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Education expenditures </h2>
                        <p>Total:<xsl:value-of select="people-society/education-expenditures"/></p> 
                        <hr />
                        <h2>School life </h2>
                        <p>Total:<xsl:value-of select="people-society/school-life/total" /></p> 
                        <p>Male:<xsl:value-of select="people-society/school-life/male" /></p>
                        <p>Female:<xsl:value-of select="people-society/school-life/female" /></p>
                        <hr />
                        <h2>Unemployment </h2>
                        <p>Total:<xsl:value-of select="people-society/unemployment/total" /></p> 
                        <p>Male:<xsl:value-of select="people-society/unemployment/male" /></p>
                        <p>Female:<xsl:value-of select="people-society/unemployment/female" /></p>



                    </div>
                </div>
            </div>
        </section>

        <!-- Government Section -->
        <section id="government" class="gov">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Government section</h1>
                        <h2>Country name</h2>
                        <p>Conventional long form:<xsl:value-of select="government/country-name/conventional-long-form"/></p>
                        <p>Conventional short form:<xsl:value-of select="government/country-name/conventional-short-form"/></p>
                        <p>Local long form:<xsl:value-of select="government/country-name/local-long-form"/></p>
                        <p>Local short form:<xsl:value-of select="government/country-name/local-short-form"/></p>
                        <p>Former:<xsl:value-of select="government/country-name/former"/></p>
                        <p>Etymology<xsl:value-of select="government/country-name/etymology"/></p>
                        <hr />
                        <h2>Goverment type</h2>
                        <p><xsl:value-of select="government/government-type"/></p>
                        <hr />
                        <h2>Capital city</h2>
                        <p>Name :<xsl:value-of select="government/capital/name"/></p>
                        <p>Geographic coordinates :<xsl:value-of select="government/capital/geographic-coordinates"/></p>
                        <p>Time difference :<xsl:value-of select="government/capital/time-difference"/></p>
                        <p>Daylight saving time:<xsl:value-of select="government/capital/daylight-saving-time"/></p>
                        <hr />
                        <h2>Administrative divisions </h2>
                        <p><xsl:value-of select="government/administrative-divisions"/></p>
                        <hr />
                        <h2>Independence</h2>
                        <p>Date:<xsl:value-of select="government/independence"/></p>
                        <hr />
                        <h2>National holiday</h2>
                        <p>Date:<xsl:value-of select="government/national-holiday"/></p>
                        <hr />
                        <h2>Consititution</h2>
                        <p><xsl:value-of select="government/consititution"/></p>
                        <hr />
                        <h2>Legal system</h2>
                        <p><xsl:value-of select="government/legal-system"/></p>
                        <hr />
                        <h2>International lawyer organization </h2>
                        <p><xsl:value-of select="government/international-law-org"/></p>
                        <hr />
                        <h2>Citizenship</h2>
                        <p>By Birth:<xsl:value-of select="government/citizenship/citizenship-by-birth"/></p>
                        <p>Descent:<xsl:value-of select="government/citizenship/citizenship-descent"/></p>
                        <p>Dual citizenship:<xsl:value-of select="government/citizenship/dual-citizenship"/></p>
                        <p>Residency requirement:<xsl:value-of select="government/citizenship/residency-requirement"/></p>
                        <hr />
                        <h2>Suffrage</h2>
                        <p><xsl:value-of select="government/suffrage"/></p>
                        <hr />
                        <h2>Executive branch </h2>
                        <p>Chief of state:<xsl:value-of select="government/executive-branch/chief-of-state"/></p>
                        <p>Head of goverment:<xsl:value-of select="government/executive-branch/head-of-government"/></p>
                        <p>Head of goverment:<xsl:value-of select="government/executive-branch/head-of-government"/></p>
                        <p>Cabinet:<xsl:value-of select="government/executive-branch/cabinet"/></p>
                        <p>Elections:<xsl:value-of select="government/executive-branch/elections"/></p>
                        <p>Election result:<xsl:value-of select="government/executive-branch/election-result"/></p>
                        <hr />
                        <h2>Legislative branch </h2>
                        <p>Description:<xsl:value-of select="government/legislative-branch/description"/></p>
                        <p>Elections:<xsl:value-of select="government/legislative-branch/elections"/></p>
                        <p>Election result:<xsl:value-of select="government/legislative-branch/election-result"/></p>
                        <hr />
                        <h2>Judicial branch</h2>
                        <p>Highest court:<xsl:value-of select="government/judicial-branch/highest-court"/></p>
                        <p>Judge selection and term:<xsl:value-of select="government/judicial-branch/judge-selection-and-term"/></p>
                        <p>Subordinate court<xsl:value-of select="government/judicial-branch/subordinate-court"/></p>
                        <hr />
                        <h2>Political party</h2>
                        <table>
                            <xsl:for-each select="government/political-party/party">
                                <tr><td>
                                <xsl:value-of select ="."/> [ <xsl:value-of select="@leader"/> ]
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <hr />
                        <h2>Political pressure groups </h2>
                        <p><xsl:value-of select="government/political-pressure-group"/></p>
                        <hr />
                        <h2>Internation org</h2>
                        <p><xsl:value-of select="government/internation-org"/></p>
                        <hr />
                        <h2>Diplomatic in USA </h2>
                        <p>Chief of mission:<xsl:value-of select="government/diplomatic-in-US/chief-of-mission"/></p>
                        <p>Chancery:<xsl:value-of select="government/diplomatic-in-US/chancery"/></p>
                        <p>Telephone:<xsl:value-of select="government/diplomatic-in-US/telephone"/></p>
                        <p>FAX:<xsl:value-of select="government/diplomatic-in-US/fax"/></p>
                        <hr />
                        <h2>Diplomatic from USA</h2>
                        <p>Chief of mission:<xsl:value-of select="government/diplomatic-from-US/chief-of-mission"/></p>
                        <p>Chancery:<xsl:value-of select="government/diplomatic-from-US/chancery"/></p>
                        <p>Mailing address:<xsl:value-of select="government/diplomatic-from-US/mailling-address"/></p>
                        <p>Telephone:<xsl:value-of select="government/diplomatic-from-US/telephone"/></p>
                        <p>FAX:<xsl:value-of select="government/diplomatic-from-US/FAX"/></p>
                        <hr />
                        <h2>Flag description</h2>
                        <p><xsl:value-of select="government/flag-description"/></p>
                        <hr />
                        <h2>National symbol</h2>
                        <p><xsl:value-of select="government/national-symbol"/></p>
                        <hr />
                        <h2>National anthem</h2>
                        <p>Name:<xsl:value-of select="government/national-anthem/name"/></p>
                        <p>Lyrics:<xsl:value-of select="government/national-anthem/lyrics"/></p>
                        <p>Note:<xsl:value-of select="government/national-anthem/note"/></p>
                        <audio id="audio-player-1" class="my-audio-player" src="anthem/CM.mp3" type="audio/mp3" controls="controls"></audio>

                    </div>
                </div>
            </div>
        </section>

        <!-- Economy Section -->
        <section id="economy" class="eco">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Economy Section</h1>
                        <h2>Overview</h2>
                        <p>Note:<xsl:value-of select="economy/economy-overview"/></p>
                        <hr />
                        <h2>GDP parity</h2>
                        <table>
                            <xsl:for-each select="economy/gdp-parity/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/gdp-parity/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>GDP official</h2>
                        <table>
                            <xsl:for-each select="economy/gdp-official/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/gdp-official/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>GDP real</h2>
                        <table>
                            <xsl:for-each select="economy/gdp-real/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                            <p>Comparison to the world:<xsl:value-of select="economy/gdp-real/country-comparison-to-the-world" /></p> 
                        </table>
                        <hr />
                        <h2>GDP per capita</h2>
                        <table>
                            <xsl:for-each select="economy/gdp-per-capita/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/gdp-per-capita/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Gross national saving</h2>
                        <table>
                            <xsl:for-each select="economy/gross-national-saving/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/gross-national-saving/country-comparison-to-the-world" /></p> 

                        <hr />
                        <h2>GDP composition</h2>
                        <table>
                            <tr><td>Household:</td><td><xsl:value-of select="economy/gdp-composition/household"/></td></tr>
                            <tr><td>Government:</td><td><xsl:value-of select="economy/gdp-composition/government"/></td></tr>
                            <tr><td>Investment-fixed:</td><td><xsl:value-of select="economy/gdp-composition/investment-fixed"/></td></tr>
                            <tr><td>Investment-inventories:</td><td><xsl:value-of select="economy/gdp-composition/investment-in-inventories"/></td></tr>
                            <tr><td>Export:</td><td><xsl:value-of select="economy/gdp-composition/export"/></td></tr>
                            <tr><td>Import:</td><td><xsl:value-of select="economy/gdp-composition/import"/></td></tr>
                        </table>


                        <hr />
                        <h2>GDP sector</h2>
                        <table>
                            <tr><td>Agriculture:</td><td><xsl:value-of select="economy/gdp-sector/agriculture"/></td></tr>
                            <tr><td>Industry:</td><td><xsl:value-of select="economy/gdp-sector/industry"/></td></tr>
                            <tr><td>Services:</td><td><xsl:value-of select="economy/gdp-sector/services"/></td></tr>
                        </table>


                        <hr />
                        <h2>Agriculture products</h2>
                        <p><xsl:value-of select="economy/agriculture-products"/></p>
                        <hr />
                        <h2>Industries</h2>
                        <p><xsl:value-of select="economy/industries"/></p>
                        <hr />
                        <h2>Industrial production </h2>
                        <p>Total:<xsl:value-of select="economy/industrial-production/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="economy/industrial-production/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Labor force </h2>
                        <p>Total:<xsl:value-of select="economy/labor-force/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="economy/labor-force/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Unemployment rate </h2>
                        <p>Total:<xsl:value-of select="economy/unemployment-rate/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="economy/unemployment-rate/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Population below poverty line </h2>
                        <p>Total:<xsl:value-of select="economy/population-below-poverty-line/value"/></p> 
                        <hr />
                        <h2>Distribution family income </h2>
                        <p>Total:<xsl:value-of select="economy/distribution-family-income/value"/></p> 
                        <p>Total:<xsl:value-of select="economy/distribution-family-income/value"/></p>
                        <p>Comparison to the world:<xsl:value-of select="economy/distribution-family-income/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Budget </h2>
                        <p>Revenue:<xsl:value-of select="economy/budget/revenue"/></p> 
                        <p>Expenditures:<xsl:value-of select="economy/budget/expenditures"/></p>
                        <hr />
                        <h2>Taxes </h2>
                        <p>Total:<xsl:value-of select="economy/taxes/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="economy/taxes/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Budget surplus </h2>
                        <p>Total:<xsl:value-of select="economy/budget-surplus/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="economy/budget-surplus/country-comparison-to-the-world" /></p> 
                        <hr />

                        <h2>Public debt</h2>
                        <table>
                            <xsl:for-each select="economy/public-debt/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/public-debt/country-comparison-to-the-world" /></p> 
                        <i>Note:<xsl:value-of select="economy/public-debt/note" /></i> 

                        <hr />
                        <h2>Fiscal year</h2>
                        <p><xsl:value-of select="economy/fiscal-year" /></p> 

                        <hr />
                        <h2>Inflation rate</h2>
                        <table>
                            <xsl:for-each select="economy/inflation-rate/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/inflation-rate/country-comparison-to-the-world" /></p> 


                        <hr />
                        <h2>Commercial bank</h2>
                        <table>
                            <xsl:for-each select="economy/commercial-bank/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/commercial-bank/country-comparison-to-the-world" /></p> 


                        <hr />
                        <h2>Stock narrow money</h2>
                        <table>
                            <xsl:for-each select="economy/stock-narrow-money/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/stock-narrow-money/country-comparison-to-the-world" /></p> 


                        <hr />
                        <h2>Stock board money</h2>
                        <table>
                            <xsl:for-each select="economy/stock-board-money/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/stock-board-money/country-comparison-to-the-world" /></p> 


                        <hr />
                        <h2>Stock domestic credit</h2>
                        <table>
                            <xsl:for-each select="economy/stock-domestic-credit/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/stock-domestic-credit/country-comparison-to-the-world" /></p> 


                        <hr />
                        <h2>Market value</h2>
                        <table>
                            <xsl:for-each select="economy/market-value/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/market-value/country-comparison-to-the-world" /></p> 


                        <hr />
                        <h2>Current balance</h2>
                        <table>
                            <xsl:for-each select="economy/current-balance/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/current-balance/country-comparison-to-the-world" /></p> 


                        <hr />
                        <h2>Exports</h2>
                        <table>
                            <xsl:for-each select="economy/exports/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Exports commodity:<xsl:value-of select="economy/exports-comm" /></p> 
                        <p>Exports partners:<xsl:value-of select="economy/exports-partners" /></p> 
                        <p>Comparison to the world:<xsl:value-of select="economy/exports/country-comparison-to-the-world" /></p> 



                        <hr />
                        <h2>Imports</h2>
                        <table>
                            <xsl:for-each select="economy/imports/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Exports commodity:<xsl:value-of select="economy/imports-comm" /></p> 
                        <p>Exports partners:<xsl:value-of select="economy/imports-partners" /></p> 
                        <p>Comparison to the world:<xsl:value-of select="economy/imports/country-comparison-to-the-world" /></p> 



                        <hr />
                        <h2>Reserver gold</h2>
                        <table>
                            <xsl:for-each select="economy/reserver-gold/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/reserver-gold/country-comparison-to-the-world" /></p> 


                        <hr />
                        <h2>Debt external</h2>
                        <table>
                            <xsl:for-each select="economy/debt-external/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="economy/debt-external/country-comparison-to-the-world" /></p> 


                        <hr />

                        <h2>Exchange rates</h2>
                        <i>Note:<xsl:value-of select="economy/exchange-rates/note" /></i> 
                        <table>
                            <xsl:for-each select="economy/exchange-rates/year">
                                <tr><td>
                                <xsl:value-of select ="."/> [ <xsl:value-of select ="@y"/> ]
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>






                    </div>
                </div>
            </div>
        </section>

        <!-- Energy Section -->
        <section id="energy" class="ener">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Energy Section</h1>
                        <h2>Electricity access </h2>
                        <p>Without:<xsl:value-of select="energy/electricity-access/without" /></p> 
                        <p>Total:<xsl:value-of select="energy/electricity-access/total" /></p>
                        <p>Urban:<xsl:value-of select="energy/electricity-access/urban" /></p>
                        <p>rural:<xsl:value-of select="energy/electricity-access/rural" /></p>
                        <hr />
                        <h2>Electricity production</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-production/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-production/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Electricity consumption</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-consumption/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-consumption/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Electricity export</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-export/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-export/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Electricity import</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-import/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-import/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Electricity capacity</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-capacity/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-capacity/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Electricity fossil</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-fossil/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-fossil/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Electricity nuclear</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-nuclear/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-nuclear/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Electricity hydro</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-hydro/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-hydro/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Electricity other</h2>
                        <p>Total:<xsl:value-of select="energy/electricity-other/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/electricity-other/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Crudeoil production</h2>
                        <p>Total:<xsl:value-of select="energy/crudeoil-production/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/crudeoil-production/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Crudeoil export</h2>
                        <p>Total:<xsl:value-of select="energy/crudeoil-export/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/crudeoil-export/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Crudeoil import</h2>
                        <p>Total:<xsl:value-of select="energy/crudeoil-import/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/crudeoil-import/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Crudeoil reserves</h2>
                        <p>Total:<xsl:value-of select="energy/crudeoil-reserves/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/crudeoil-reserves/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Petroleum production</h2>
                        <p>Total:<xsl:value-of select="energy/petroleum-production/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/petroleum-production/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Petroleum export</h2>
                        <p>Total:<xsl:value-of select="energy/petroleum-export/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/petroleum-export/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Petroleum import</h2>
                        <p>Total:<xsl:value-of select="energy/petroleum-import/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/petroleum-import/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Petroleum consumption</h2>
                        <p>Total:<xsl:value-of select="energy/petroleum-consumption/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/petroleum-consumption/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Gas production</h2>
                        <p>Total:<xsl:value-of select="energy/gas-production/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/gas-production/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Gas export</h2>
                        <p>Total:<xsl:value-of select="energy/gas-export/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/gas-export/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Gas import</h2>
                        <p>Total:<xsl:value-of select="energy/gas-import/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/gas-import/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Gas consumption</h2>
                        <p>Total:<xsl:value-of select="energy/gas-consumption/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/gas-consumption/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Gas reserves</h2>
                        <p>Total:<xsl:value-of select="energy/gas-reserves/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/gas-reserves/country-comparison-to-the-world" /></p> 
                        <hr />
                        <h2>Carbon emissions </h2>
                        <p>Total:<xsl:value-of select="energy/gas-reserves/value"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="energy/gas-reserves/country-comparison-to-the-world" /></p> 



                    </div>
                </div>
            </div>
        </section>

        <!-- Communication Section -->
        <section id="communication" class="com">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Communication Section</h1>

                        <h2>Telephone lines</h2>
                        <p>Total:<xsl:value-of select="communication/telephone-line/total"/></p> 
                        <p>Subs:<xsl:value-of select="communication/telephone-line/subs"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="communication/telephone-line/country-comparison-to-the-world" /></p>
                        <hr />
                        <h2>Telephone mobile</h2>
                        <p>Total:<xsl:value-of select="communication/telephone-mobile/total"/></p> 
                        <p>Subs:<xsl:value-of select="communication/telephone-mobile/subs"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="communication/telephone-mobile/country-comparison-to-the-world" /></p>
                        <hr />
                        <h2>Telephone system</h2>
                        <p>general:<xsl:value-of select="communication/telephone-system/general"/></p> 
                        <p>Subs:<xsl:value-of select="communication/telephone-system/domestic"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="communication/telephone-system/international" /></p>
                        <hr />
                        <h2>Broadcast media</h2>
                        <p><xsl:value-of select="communication/broadcast-media"/></p> 

                        <hr />
                        <h2>Internet code</h2>
                        <p><xsl:value-of select="communication/internet-code"/></p> 

                        <hr />
                        <h2>Internet users</h2>
                        <p>Total:<xsl:value-of select="communication/internet-users/total"/></p> 
                        <p>percent:<xsl:value-of select="communication/internet-users/percent"/></p> 
                        <p>Comparison to the world:<xsl:value-of select="communication/internet-users/country-comparison-to-the-world" /></p>





                    </div>
                </div>
            </div>
        </section>

        <!-- Transportation Section -->
        <section id="transportation" class="trans">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Transportation Section</h1>
                        <h2>Air system</h2>
                        <p>Number of air carriers:<xsl:value-of select="transportation/air-system/number-of-air-carriers"/></p> 
                        <p>Inventory:<xsl:value-of select="transportation/air-system/inventory"/></p> 
                        <p>Annual passenger:<xsl:value-of select="transportation/air-system/annual-passenger"/></p> 
                        <p>Annual freight:<xsl:value-of select="transportation/air-system/annual-freight"/></p>  
                        <hr />
                        <h2>Air prefix</h2>
                        <p> <xsl:value-of select="transportation/air-prefix"/></p>  
                        <hr />
                        <h2>Airport </h2>
                        <p>Total: <xsl:value-of select="transportation/airport/value"/></p>  
                        <p>Comparison to the world:<xsl:value-of select="transportation/airport/country-comparison-to-the-world" /></p>
                        <hr />
                        <h2>Pipelines</h2>
                        <p>Gas: <xsl:value-of select="transportation/pipelines/gas"/></p>
                        <p>Petroleum: <xsl:value-of select="transportation/pipelines/petroleum"/></p>
                        <p>Water: <xsl:value-of select="transportation/pipelines/water"/></p>
                        <p>Oil: <xsl:value-of select="transportation/pipelines/oil"/></p>
                        <hr />
                        <h2>Railways</h2>
                        <p>Total: <xsl:value-of select="transportation/railways/total"/></p> 
                        <p>Standart: <xsl:value-of select="transportation/railways/standart"/></p> 
                        <i>Note: <xsl:value-of select="transportation/railways/note"/></i> 
                        <p>Comparison to the world:<xsl:value-of select="transportation/railways/country-comparison-to-the-world" /></p>
                        <hr />
                        <h2>Roadways</h2>
                        <p>Total: <xsl:value-of select="transportation/roadways/total"/></p> 
                        <p>Paved: <xsl:value-of select="transportation/roadways/paved"/></p> 
                        <p>Unpaved: <xsl:value-of select="transportation/roadways/unpaved"/></p> 
                        <i>Note: <xsl:value-of select="transportation/roadways/note"/></i> 
                        <p>Comparison to the world:<xsl:value-of select="transportation/roadways/country-comparison-to-the-world" /></p>
                        <hr />
                        <h2>Waterways</h2>
                        <p> <xsl:value-of select="transportation/waterways"/></p> 
                        <hr />
                        <h2>Port</h2>
                        <p> River:<xsl:value-of select="transportation/port/river"/></p> 
                        <p> Oil:<xsl:value-of select="transportation/port/oil"/></p> 


                    </div>
                </div>
            </div>
        </section>

        <!-- Military Section -->
        <section id="military" class="mili">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Military Section</h1>
                        <h2>Military branches</h2>
                        <p><xsl:value-of select="military/military-branches"/></p> 
                        <hr />
                        <h2>Service age</h2>
                        <p><xsl:value-of select="military/service-age"/></p> 
                        <hr />


                        <h2>Expenditures</h2>



                        <table>
                            <xsl:for-each select="military/expenditures/value">
                                <tr><td>
                                <xsl:value-of select ="."/> 
                                </td>


                                </tr>
                            </xsl:for-each>
                        </table>
                        <p>Comparison to the world:<xsl:value-of select="military/expenditures/country-comparison-to-the-world" /></p>



                    </div>
                </div>
            </div>
        </section>

        <!-- issues Section -->
        <section id="transnational-issues" class="prob">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Transnational-issues Section</h1>

                        <h2>Disputes</h2>
                        <p><xsl:value-of select="transnational-issues/disputes"/></p> 
                        <hr />
                        <h2>Refugees</h2>
                        <p>Origin:<xsl:value-of select="transnational-issues/refugees/origin"/></p> 
                        <p>IDPS:<xsl:value-of select="transnational-issues/refugees/idps"/></p> 


                    </div>
                </div>
            </div>
        </section>
        <!-- issues Section -->
        <section id="gallery" class="gal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Gallery</h1>

                        <xsl:for-each select="images/image">

                            <img src= "{@source}" />
                            <p><xsl:value-of select="."/></p>
                            <hr />
                        </xsl:for-each>

                    </div>
                </div>
            </div>
        </section>
    </xsl:template>


</xsl:stylesheet>
