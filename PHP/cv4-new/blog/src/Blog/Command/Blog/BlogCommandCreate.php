<?php

/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 11:32 AM
 */
namespace  Blog\Command\Blog;
class BlogCommandCreate extends \Symfony\Component\Console\Command\Command
{

    const BLOG_TITLE = "TITLE";
    const BLOG_AUTHOR = "author";
    protected function configure()
    {
     $this->setName("blog:create title");
     $this->setDescription("Create title for blog");
        $this->addArgument(self::BLOG_TITLE,InputArgument::REQUIRED,"Title of the blog");
        $this->addArgument(self::BLOG_AUTHOR,InputArgument::OPTIONAL,"Author of blog");

    }

    protected function execute(InputInterface $input,OutputInterface $output)
    {
        $blog = new \Blog\Service\BlogService();
        $blog->create($input->getArgument,$this->getArgument);
    }
}