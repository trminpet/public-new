<?php

/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 11:54 AM
 */
namespace Blog\Command\Blog;
class BlogCommandWalk extends \Symfony\Component\Console\Command\Command
{


    protected function configure()
    {
        $this->setName("blog:walk");
        $this->setDescription("Walk through blogs");


    }

    protected function execute(InputInterface $input,OutputInterface $output)
    {
        $blog = new \Blog\Service\BlogService();
       print_r( $blog->walk());
    }
}