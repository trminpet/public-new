<?php

/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 11:51 AM
 */
namespace Blog\Command\Blog;
class BlogCommandFind extends \Symfony\Component\Console\Command\Command
{

    const BLOG_ID = "1";

    protected function configure()
    {
        $this->setName("blog:find id");
        $this->setDescription("Find id of blog");
        $this->addArgument(self::BLOG_ID,InputArgument::REQUIRED,"ID of the blog");


    }

    protected function execute(InputInterface $input,OutputInterface $output)
    {
        $blog = new \Blog\Service\BlogService();
        $blog->find($input.getArgument);
    }
}