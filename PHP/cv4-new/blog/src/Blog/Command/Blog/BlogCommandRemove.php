<?php

/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 12:00 PM
 */
namespace Blog\Command\Blog;
class BlogCommandRemove extends \Symfony\Component\Console\Command\Command
{

    const BLOG_ID = "blog";

    protected function configure()
    {
        $this->setName("blog:remove");
        $this->setDescription("Remove blog");
        $this->addArgument(self::BLOG_TITLE,InputArgument::REQUIRED,"Title of the blog");


    }

    protected function execute(InputInterface $input,OutputInterface $output)
    {
        $blog = new \Blog\Service\BlogService();
        $blog->remove($input->getArgument());
    }
}