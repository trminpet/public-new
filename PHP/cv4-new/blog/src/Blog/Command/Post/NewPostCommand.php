<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 11:01 AM
 */

namespace Blog\Command\Post;
use Blog\Model\Post;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;

class NewPostCommand extends Command
{

   const TITLE = "ADA";
    const blog = "blog";
  protected function configure()
  {
      $this->setName("post:create");
      $this->setDescription("Create a post");
      $this->addArgument(self::TITLE,InputArgument::REQUIRED,"Title of the post");
      $this->addArgument(self::blog,InputArgument::REQUIRED,"Content of post");
     }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
      $helper = $this->getHelper('question');
      $post = new Post();
      if($input->hasArgument(self::blog)){
          $post ->setBlog(Blog::find($input->getArgument(self::blog)));



      }







      if($input->hasArgument(self::TITLE)){
          $post->setTitle($input->getArgument(self::TITLE));

      }
      else{
          $question = new Question("What is title of post");
         $post->setTitle($helper->ask($input,$output,$question));
      }
  }
}