<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/5/16
 * Time: 12:43 PM
 */
namespace Blog\Service;

use Blog\Model\Blog;
use Blog\Model\User;
use Blog\Exception;

/**
 * Class BlogService
 * @package Blog\Service
 * A service responsible for managing blogs
 */
class BlogService
{

    use TestsTrait;


    /**
     * Returns a blog identified by its id
     * @param $id
     * @return Blog
     * @throws \OutOfBoundsException if no such blog exist
     */
    public function find($id)
    {
        try {
            $blog = Blog::find($id);
            return $blog;
        }
        catch (NoSuchBlogException $e ){
           new Exception\NoSuchBlogExpection($id);
        }
    }

    /**
     * Returns all blogs available
     * @return Blog[]
     */
    public function walk()
    {
        try {
            return Blog::walk();
        }
        catch (NoBlogsException $e){
            new Exception\NoBlogsException;

        }
    }

    /**
     * Creates a blog
     * @param $title
     * @param $author
     * @return Blog
     */
    public function create($title, User $author = null)
    {
        try{
        $blog = (new Blog())
            ->setTitle($title)
            ->setAuthor($author);
        $blog->save();
        return $blog;
    }
        catch (BlogNotSavedException $e){
            new Exception\BlogNotSavedException();
        }

    }

    /**
     * Removes a blog
     * @param $blog
     * @return $this
     */
    public function remove(Blog $blog)
    {
        try {
            $postService = new PostService();
            foreach ($postService->walk($blog) as $post) {
                $postService->remove($blog, $post);
            }
            $blog->delete();
            return $this;
        }
        catch(BlogNotRemovedException $e){

            new Exception\BlogNotRemovedException($blog);
        }
    }
}
