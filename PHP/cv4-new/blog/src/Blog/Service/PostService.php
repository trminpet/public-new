<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/5/16
 * Time: 12:43 PM
 */
namespace Blog\Service;

use Blog\Exception\NoPostsException;
use Blog\Exception\NoSuchPostExpection;
use Blog\Exception\PostNotRemovedException;
use Blog\Exception\PostNotSavedException;
use Blog\Model\Blog;
use Blog\Model\Post;

/**
 * Class PostService
 * @package Blog\Service
 * Operates on posts
 */
class PostService
{
    use TestsTrait;

    /**
     * @param Blog $blog
     * @param int $id
     * @return Post
     * @throws \OutOfBoundsException
     */
    public function find(Blog $blog, $id)
    {
        try{
        $post = Post::find($id);
        $this->isNotEmpty($post);
        return $post;
    }
    catch (NoSuchPostExpection $e){}
       new NoSuchPostExpection($id);
    }

    /**
     * Lists all posts
     * @param Blog $blog
     * @return Post[]
     */
    public function walk($blog)
    {
       try{
        return $blog->getPosts();
    }
    catch (NoPostsException $e)
    {
        new NoPostsException();
    }
    }

    /**
     * Creates a new post
     * @param Blog $blog
     * @param string $title
     * @param string $content
     * @param bool|\DateTime $publish
     * @param string $summary
     * @return Post
     */
    public function create(Blog $blog, $title, $content, $publish = false, $summary = '')
    {
        try {
            $post = (new Post())
                ->setTitle($title)
                ->setContent($content)
                ->setSummary($summary)
                ->setBlog($blog);
            if ($publish and $publish instanceof \DateTime) {
                $post->setPublished($publish);
            }
            $post->save();
            $blog->addPost($post);


            $blog->save();

            return $post;
        }
        catch (PostNotSavedException $e){
            new PostNotSavedException();

        }
    }

    /**
     * Removes a post
     * @param Blog $blog
     * @param Post $post
     * @return bool
     * @throws \UnexpectedValueException if blog or post are not instances of expected classes
     * @throws \OutOfBoundsException if post does not exist
     */
    public function remove(Blog $blog, Post $post)
    {
        try{
        $blog->removePost($post);
        $blog->save();
        $post->delete();
        return true;
    }
    catch (PostNotRemovedException $e){
        new PostNotRemovedException($post);

    }

    }

}