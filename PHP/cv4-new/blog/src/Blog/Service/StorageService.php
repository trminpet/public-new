<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/17/16
 * Time: 9:23 PM
 */

namespace Blog\Service;

use League\Flysystem\Filesystem;
use Symfony\Component\Console\Exception\LogicException;

/**
 * Class StorageService
 * @package Blog\Service
 *
 * A convenient way to access storage subsystem. It must be configured
 * first by calling setFilesystem.
 */
class StorageService
{
    use SingletonTrait;

    /** @var  Filesystem*/
    protected $filesystem;

    /**
     * Returns filesystem to those who need it
     * @return Filesystem
     * @throws LogicException if filesystem is empty
     */
    public function getFilesystem()
    {
        if (empty($this->filesystem))
            throw new \LogicException("Storage service not initialized with a filesystem.");
        return $this->filesystem;
    }

    /**
     * Sets filesystem - initializes the service
     * @param Filesystem $filesystem
     */
    public function setFilesystem(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }
}