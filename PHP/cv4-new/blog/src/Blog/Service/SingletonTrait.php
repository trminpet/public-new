<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/5/16
 * Time: 2:30 PM
 */

namespace Blog\Service;

/**
 * Class SingletonTrait
 * @package Blog\Service
 */
trait SingletonTrait
{
    protected static $instance;

    /**
     * Returns an instance
     * @return object
     */
    public static function getInstance()
    {
        if (empty(static::$instance))
            static::$instance = new static();
        return static::$instance;
    }
}