<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/5/16
 * Time: 3:54 PM
 */

namespace Blog\Service;

/**
 * Trait TestsTrait
 * @package Blog\Service
 * Basic checks
 */
trait TestsTrait
{

    /**
     * Checks if collection contains an element with given id
     * @param $id
     * @param $collection
     * @param string $message optional, %s will be replaced with id
     * @return bool
     */
    protected function exists($id, $collection, $message = "")
    {
        if (!isset($collection[$id]))
            throw new \OutOfBoundsException(sprintf($message, $id));
        return true;
    }

    /**
     * Checks if result is not empty
     * @param $result
     * @param string $message
     * @return bool
     * @throws \OutOfBoundsException
     */
    protected function isNotEmpty($result, $message = "No such result.")
    {
        if (empty($result))
            throw new \OutOfBoundsException($message);
        return true;
    }

    /**
     * Checks if an object is an instance of a given class
     * @param $object
     * @param $className
     * @param string $message optional, %s will be replaced with class name
     * @return bool
     */
    protected function isInstance($object, $className, $message = "Argument is expected to be an instance of %s")
    {
        if (!is_a($object, $className))
            throw new \UnexpectedValueException(sprintf($message, $className));
        return true;
    }
}