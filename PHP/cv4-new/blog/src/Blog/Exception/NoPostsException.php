<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:24 AM
 */

namespace Blog\Exception;


class NoPostsException extends PostException
{
  protected $message ;
    protected $code;

    function __construct(){
        $this->message="No posts saved";
        $this->code=554;

    }
}