<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:26 AM
 */

namespace Blog\Exception;


class PostNotRemovedException extends PostPersistenceException
{
    protected $message;
    protected $code ;
    function __construct(){
        $this->message   = "Something went wrong while removing post";
        $this->code=551;

    }
}