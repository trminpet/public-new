<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:19 AM
 */

namespace Blog\Exception;


class UnexpectedValueExpection extends Exception
{
   protected $message;
    protected $code;
    protected $value;

    function __construct($value){

       $this->message="Unexpected value expection";
        $this->code=400;
        $this->value = $value;
         }
}