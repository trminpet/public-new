<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:20 AM
 */

namespace Blog\Exception\BlogEx;


class BlogException extends UnexpectedValueExpection
{
    protected $message;
    protected $code ;
    protected $blog;
    function __construct(Blog $blog){
        $this->message="Blog exception";
        $this->code=500;
      $this->blog=$blog;


    }
}