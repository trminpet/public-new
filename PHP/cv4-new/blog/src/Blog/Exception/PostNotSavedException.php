<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:25 AM
 */

namespace Blog\Exception;


class PostNotSavedException extends PostPersistenceException
{
    protected $message = "Something went wrong while saving post";
    protected $code = 552;
    protected $post;
    function __construct(Post $post){
        $this->message   = "Something went wrong while saving post";
        $this->code=552;
        $this->post = $post;

    }

}