<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:23 AM
 */

namespace Blog\Exception;


class BlogNotSavedException extends BlogPersistenceException
{

    protected $message;
    protected $code ;
    protected $blog;
    function __construct(Blog $blog){
        $this->message== "Something went wrong while saving blog";
        $this->code=502;
        $this->blog=$blog;


    }
}