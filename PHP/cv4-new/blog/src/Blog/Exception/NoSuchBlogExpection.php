<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:21 AM
 */

namespace Blog\Exception;


class NoSuchBlogExpection extends BlogException
{
 protected $message;
    protected $code;
    protected $blogID;

    function __construct($blogID){
        $this->message="Blog with selected ID does not exist";
        $this->code=505;
        $this->blogID=$blogID;

    }
}