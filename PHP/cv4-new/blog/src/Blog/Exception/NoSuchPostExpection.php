<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:23 AM
 */

namespace Blog\Exception;


class NoSuchPostExpection extends PostException
{
    protected $message;
    protected $code;
    protected $postID;


    function __construct($postID){
        $this->message = "Post with selected ID does not exist";
        $this->code=505;
        $this->postID=$postID;

    }
}