<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:21 AM
 */

namespace Blog\Exception;


class NoBlogsException extends BlogException
{
   protected $message;
   protected $code ;
    function __construct(){
        $this->message="No blogs saved";
        $this->code=504;

    }
}