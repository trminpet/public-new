<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/3/16
 * Time: 10:32 PM
 */
namespace Blog\Model;

/**
 * Class User
 * @package Blog\Model
 * Represents a user of the application
 */
class User extends Model
{
    protected $email;

    protected $name;

    public function getId()
    {
        return $this->getEmail();
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }


}