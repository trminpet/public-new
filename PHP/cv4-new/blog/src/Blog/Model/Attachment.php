<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/5/16
 * Time: 11:51 AM
 */
namespace Blog\Model;


/**
 * Class Attachment
 * @package Blog\Model
 * Represents a generic attachment
 */
class Attachment
{

    const TYPE = 'application/octet-stream';

    /** @var int */
    protected static $sequence = 0;

    /** @var  int */
    protected $id;

    /** @var  string */
    protected $type;

    /** @var  string */
    protected $name;

    /** @var  string */
    protected $description;

    /** @var string */
    protected $location;

    /**
     * Attachment constructor.
     */
    public function __construct()
    {
        $this->id = ++self::$sequence;
        $this->type = static::TYPE;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Attachment
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Attachment
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     * @return Attachment
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }
}