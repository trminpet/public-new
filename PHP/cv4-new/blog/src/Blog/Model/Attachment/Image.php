<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/5/16
 * Time: 12:25 PM
 */
namespace Blog\Model\Attachment;

use Blog\Model\Attachment;

/**
 * Class Image
 * @package Blog\Model\Attachment
 * Generic image. Cannot be used directly - use a subclass.
 */
abstract class Image extends Attachment
{
}