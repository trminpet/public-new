<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/5/16
 * Time: 12:25 PM
 */
namespace Blog\Model\Attachment;

/**
 * Class Png
 * @package Blog\Model\Attachment
 * PNG image
 */
class Png extends Image
{
    const TYPE = 'image/png';
}