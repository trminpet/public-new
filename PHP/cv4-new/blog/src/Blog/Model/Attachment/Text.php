<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/5/16
 * Time: 12:25 PM
 */
namespace Blog\Model\Attachment;

use Blog\Model\Attachment;

/**
 * Class Text
 * @package Blog\Model\Attachment
 * Plain text attachment
 */
class Text extends Attachment
{
    const TYPE = 'text/plain';
}