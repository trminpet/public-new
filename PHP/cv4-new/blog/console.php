<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 11/25/16
 * Time: 9:40 AM
 */
require_once('vendor/autoload.php');

$adapter = new \League\Flysystem\Adapter\Local(__DIR__.'/var/data');
$filesystem = new \League\Flysystem\Filesystem($adapter);

/** @var \Blog\Service\StorageService storage */
$storage = \Blog\Service\StorageService::getInstance();
$storage->setFilesystem($filesystem);

$log = new \Monolog\Logger("application");
$log->pushHandler(new StreamHandler('__DIR__.\'/my_app.log\'', Logger::ERROR));
print_r(\Blog\Model\Blog::walk());
\Monolog\ErrorHandler::register($log);
ini_set('display_errors', '0');

$application = new \Symfony\Component\Console\Application();
$application->add(new \Blog\Command\Blog\BlogCommandFind());
$application->add(new \Blog\Command\Blog\BlogCommandCreate());
$application->add(new \Blog\Command\Blog\BlogCommandRemove());
$application->add(new \Blog\Command\Blog\BlogCommandWalk());
$application->run();