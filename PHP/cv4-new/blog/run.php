<?php
/**
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/3/16
 * Time: 11:01 PM
 */

require_once('vendor/autoload.php');

$adapter = new \League\Flysystem\Adapter\Local(__DIR__.'/var/data');
$filesystem = new \League\Flysystem\Filesystem($adapter);

/** @var \Blog\Service\StorageService storage */
$storage = \Blog\Service\StorageService::getInstance();
$storage->setFilesystem($filesystem);




$user = (new \Blog\Model\User())
    ->setEmail('user@example.org')
    ->setName('User User');

$blogService = new \Blog\Service\BlogService();
$postService = new \Blog\Service\PostService();

$blog = $blogService->find(1);
$blogService->remove($blog);

$blog = $blogService->find(2);
$postService->create($blog, "First post", "This is the first post in this blog. Hope you enjoyed it.");

print_r(\Blog\Model\Blog::walk());
