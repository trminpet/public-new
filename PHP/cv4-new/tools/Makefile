SHELL = /bin/bash
DOCKER_IMAGE_TAG = latest
DOCKER_IMAGE_NAME = "pandoc"
CWD := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

help:
	@cat README.rst
	@echo
	@echo "Current settings"
	@echo "----------------"
	@echo ""
	@echo "DOCKER_IMAGE_NAME = ${DOCKER_IMAGE_NAME}"
	@echo "DOCKER_IMAGE_TAG = ${DOCKER_IMAGE_TAG}"
	@echo ""

build: Dockerfile check_tag check_name
	docker build -t "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}" .

check_tag:
	@[ -n "${DOCKER_IMAGE_TAG}" ] || { echo "Missing DOCKER_IMAGE_TAG, please specify -e DOCKER_IMAGE_TAG=XX.Y" 1>&2; exit 1; }

check_name:
	@[ -n "${DOCKER_IMAGE_NAME}" ] || { echo "Missing DOCKER_IMAGE_NAME, please specify -e DOCKER_IMAGE_NAME=name" 1>&2; exit 1; }

clean: check_exists
	docker rmi "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}"

check_exists:
	@[ `docker images ${DOCKER_IMAGE_NAME}:latest | head -n-1 | wc -l` -eq 1 ] || { echo "Missing ${DOCKER_IMAGE_NAME}, please run make build" 1>&2; exit 1; }

clean_dangling:
	docker rmi `docker images --filter dangling=true --format='{{.ID}}'`

run: check_exists
	docker run -ti --rm \
		--volume="${CWD}../:/tmp/tutorials" \
		--volume="/etc/group:/etc/group:ro" \
		--volume="/etc/passwd:/etc/passwd:ro" \
		--workdir="/tmp/tutorials" \
		--user="$$UID:$$(id -g)" \
		${DOCKER_IMAGE_NAME}:latest \
		/bin/bash
