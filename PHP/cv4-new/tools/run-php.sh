#!/bin/bash
docker run -ti --rm \
	--volume="$HOME:$HOME" \
	--volume="$PWD/..:/tmp/tutorials" \
	--volume="/etc/group:/etc/group:ro" \
	--volume="/etc/passwd:/etc/passwd:ro" \
	-p 9000:9000 \
	--workdir="/tmp/tutorials" \
	--user="$UID:$(id -g)" \
	php:latest \
	/bin/bash
