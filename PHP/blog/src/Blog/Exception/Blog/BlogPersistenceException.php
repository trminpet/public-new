<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:19 PM
 */

namespace Blog\Exception\Blog;

use Blog\Exception\BlogException;
use Blog\Model\Blog;

/**
 * Class BlogPersistenceException
 * Indicates generic problem with persistence. Has specific subclasses.
 * @package Blog\Exception\Blog
 */
abstract class BlogPersistenceException extends BlogException
{
    protected $blog;

    /***
     * @return Blog|null
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * BlogNotSavedException constructor.
     * @param Blog|null $blog
     * @param \Exception $previous
     */
    public function __construct($blog, \Exception $previous = null)
    {
        $this->blog = $blog;
        parent::__construct(sprintf(static::MESSAGE, $blog), static::CODE, $previous);
    }

}