<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:00 PM
 */
namespace Blog\Exception\Blog;

use Blog\Exception\BlogException;

/**
 * Class NoSuchBlogException
 * Exception indicates that a blog with given ID does not exist.
 *
 * @package Blog\Exception\Blog
 */
class NoSuchBlogException extends BlogException
{
    const MESSAGE = "No such blog %s exists.";
    const CODE = 101;

    protected $id;

    /**
     * NoSuchBlogException constructor.
     * @param $id
     * @param \Exception $previous previous exception
     */
    public function __construct($id, \Exception $previous = null)
    {
        $this->id = $id;
        parent::__construct(sprintf(self::MESSAGE, $id), self::CODE, $previous);
    }

}