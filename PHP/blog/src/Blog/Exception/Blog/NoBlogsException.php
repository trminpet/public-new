<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:00 PM
 */
namespace Blog\Exception\Blog;

use Blog\Exception\BlogException;

/**
 * Class NoBlogsException
 * Exception indicates that there are no blogs stored (on a filesystem).
 *
 * @package Blog\Exception\Blog
 */
class NoBlogsException extends BlogException
{
    const MESSAGE = "No blogs exist.";
    const CODE = 102;

    /**
     * NoBlogsException constructor.
     * @param \Exception $previous
     */
    public function __construct(\Exception $previous = null)
    {
        parent::__construct(self::MESSAGE, self::CODE, $previous);
    }

}