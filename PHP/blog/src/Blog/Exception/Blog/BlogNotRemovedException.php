<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:01 PM
 */
namespace Blog\Exception\Blog;

/**
 * Class BlogNotRemovedException
 * Indicates a problem with removing a blog. A previous exception is
 * mostly set.
 * @package Blog\Exception\Blog
 */
class BlogNotRemovedException extends BlogPersistenceException
{
    const MESSAGE = "Failed to remove a blog %s";
    const CODE = 104;
}