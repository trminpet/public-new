<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:01 PM
 */
namespace Blog\Exception\Blog;

use Blog\Exception\BlogException;
use Blog\Model\Blog;

/**
 * Class BlogNotSavedException
 * Indicates a problem with saving a blog. A previous exception is
 * mostly set.
 * @package Blog\Exception\Blog
 */
class BlogNotSavedException extends BlogPersistenceException
{
    const MESSAGE = "Failed to save a blog %s";
    const CODE = 103;
}