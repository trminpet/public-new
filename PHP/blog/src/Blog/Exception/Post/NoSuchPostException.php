<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:00 PM
 */
namespace Blog\Exception\Post;

use Blog\Exception\PostException;

/**
 * Class NoSuchPostException
 * Exception indicates that a blog with given ID does not exist.
 *
 * @package Blog\Exception\Post
 */
class NoSuchPostException extends PostException
{
    const MESSAGE = "No such post %s exists.";
    const CODE = 111;

    protected $id;

    /**
     * NoSuchPostException constructor.
     * @param $id
     * @param \Exception $previous previous exception
     */
    public function __construct($id, \Exception $previous = null)
    {
        $this->id = $id;
        parent::__construct(sprintf(self::MESSAGE, $id), self::CODE, $previous);
    }

}