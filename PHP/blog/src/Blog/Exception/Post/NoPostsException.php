<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:00 PM
 */
namespace Blog\Exception\Post;

use Blog\Exception\PostException;

/**
 * Class NoPostsException
 * Exception indicates that there are no posts stored (on a filesystem).
 *
 * @package Blog\Exception\Post
 */
class NoPostsException extends PostException
{
    const MESSAGE = "No posts exist.";
    const CODE = 112;

    /**
     * NoPostsException constructor.
     * @param \Exception $previous
     */
    public function __construct(\Exception $previous = null)
    {
        parent::__construct(self::MESSAGE, self::CODE, $previous);
    }

}