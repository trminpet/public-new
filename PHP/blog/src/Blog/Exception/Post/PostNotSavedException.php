<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:01 PM
 */
namespace Blog\Exception\Post;

/**
 * Class PostNotSavedException
 * Indicates a problem with saving a post. A previous exception is
 * mostly set.
 * @package Post\Exception\Post
 */
class PostNotSavedException extends PostPersistenceException
{
    const MESSAGE = "Failed to save a blog %s";
    const CODE = 113;
}