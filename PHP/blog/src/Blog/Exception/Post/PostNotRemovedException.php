<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:01 PM
 */
namespace Blog\Exception\Post;

/**
 * Class PostNotRemovedException
 * Indicates a problem with removing a post. A previous exception is
 * mostly set.
 * @package Blog\Exception\Post
 */
class PostNotRemovedException extends PostPersistenceException
{
    const MESSAGE = "Failed to remove a post %s";
    const CODE = 114;
}