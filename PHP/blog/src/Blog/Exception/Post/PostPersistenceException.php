<?php
/*
 * BI-PHP.1/BIE-PHP.1 Post application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 6:19 PM
 */

namespace Blog\Exception\Post;

use Blog\Exception\PostException;
use Blog\Model\Post;

/**
 * Class PostPersistenceException
 * Indicates generic problem with persistence. Has specific subclasses.
 * @package Blog\Exception\Post
 */
abstract class PostPersistenceException extends PostException
{
    protected $post;

    /***
     * @return Post|null
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * PostNotSavedException constructor.
     * @param Post|null $post
     * @param \Exception $previous
     */
    public function __construct($post, \Exception $previous = null)
    {
        $this->post = $post;
        parent::__construct(sprintf(static::MESSAGE, $post), static::CODE, $previous);
    }

}