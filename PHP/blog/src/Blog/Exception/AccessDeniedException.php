<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 5:14 PM
 */

namespace Blog\Exception;

class AccessDeniedException extends BlogException
{
    const MESSAGE = "Access denied to %s";
    const CODE = 201;

    protected $path;

    /**
     * Returns path that access was denied to.
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * AccessDeniedException constructor.
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
        parent::__construct(sprintf(self::MESSAGE, $path), self::CODE);
    }
}