<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 5:12 PM
 */

namespace Blog\Exception;

/**
 * Class BlogException
 * Generic runtime error regarding blogs
 *
 * @package Blog\Exception
 */
class BlogException extends \UnexpectedValueException
{

}