<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/5/16
 * Time: 12:43 PM
 */
namespace Blog\Service;

use Blog\Exception\Blog\BlogNotRemovedException;
use Blog\Exception\Blog\BlogNotSavedException;
use Blog\Exception\Blog\NoBlogsException;
use Blog\Exception\Blog\NoSuchBlogException;
use Blog\Exception\Post\NoPostsException;
use Blog\Model\Blog;
use Blog\Model\User;
use League\Flysystem\Exception;

/**
 * Class BlogService
 * @package Blog\Service
 * A service responsible for managing blogs
 */
class BlogService
{
    /**
     * Returns a blog identified by its id
     * @param $id
     * @return Blog
     * @throws NoSuchBlogException if no such blog exist
     */
    public function find($id)
    {
        try {
            $blog = Blog::find($id);
            return $blog;
        } catch (\Exception $previous) {
            throw new NoSuchBlogException($id, $previous);
        }
    }

    /**
     * Returns all blogs available
     * @return Blog[]
     * @throws NoBlogsException if no blog exists at all
     */
    public function walk()
    {
        try {
            $blogs = Blog::walk();
            if (empty($blogs))
                throw new NoBlogsException();
            return $blogs;
        } catch (NoBlogsException $e) {
            throw $e;
        } catch (Exception $previous) {
            throw new NoBlogsException($previous);
        }
    }

    /**
     * Creates a blog
     * @param string $title
     * @param User $author
     * @return Blog
     * @throws BlogNotSavedException if save fails
     */
    public function create($title, User $author = null)
    {
        $blog = (new Blog())
            ->setTitle($title)
            ->setAuthor($author);
        try {
            $blog->save();
            return $blog;
        } catch (\Exception $previous) {
            throw new BlogNotSavedException($blog, $previous);
        }
    }

    /**
     * Saves a blog
     * @param Blog $blog
     */
    public function update(Blog $blog)
    {
        try {
            $blog->save();
        } catch (\Exception $previous) {
            throw new BlogNotSavedException($blog, $previous);
        }
    }

    /**
     * Removes a blog
     * @param Blog $blog
     * @throws BlogNotRemovedException if removal fails
     */
    public function remove(Blog $blog)
    {
        try {
            $postService = new PostService();
            try {
                foreach ($postService->walk($blog) as $post) {
                    $postService->remove($blog, $post);
                }
            } catch (NoPostsException $e) {}
            $blog->delete();
        } catch (\Exception $previous) {
            throw new BlogNotRemovedException($blog, $previous);
        }
    }
}
