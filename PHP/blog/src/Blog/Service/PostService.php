<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/5/16
 * Time: 12:43 PM
 */
namespace Blog\Service;

use Blog\Exception\Post\NoPostsException;
use Blog\Exception\Post\NoSuchPostException;
use Blog\Exception\Post\PostNotRemovedException;
use Blog\Exception\Post\PostNotSavedException;
use Blog\Model\Blog;
use Blog\Model\Post;
use League\Flysystem\Exception;

/**
 * Class PostService
 * Operates on posts
 * @package Blog\Service
 */
class PostService
{

    /**
     * Finds a post
     * @param Blog $blog
     * @param int $id
     * @return Post
     * @throws NoSuchPostException if post does not exist
     */
    public function find(Blog $blog, $id)
    {
        try {
            $post = Post::find($id);
            return $post;
        } catch (Exception $previous) {
            throw new NoSuchPostException($id, $previous);
        }
    }

    /**
     * Lists all posts
     * @param Blog $blog
     * @return Post[]
     */
    public function walk($blog)
    {
        try {
            $posts = $blog->getPosts();
            if (empty($posts))
                throw new NoPostsException();
            return $posts;
        } catch (NoPostsException $previous) {
            throw $previous;
        } catch (\Exception $previous) {
            throw new NoPostsException($previous);
        }
    }

    /**
     * Creates a new post
     * @param Blog $blog
     * @param string $title
     * @param string $content
     * @param bool|\DateTime $publish
     * @param string $summary
     * @return Post
     */
    public function create(Blog $blog, $title, $content, $publish = false, $summary = '')
    {

        $post = (new Post())
            ->setTitle($title)
            ->setContent($content)
            ->setSummary($summary)
            ->setBlog($blog);
        if ($publish and $publish instanceof \DateTime) {
            $post->setPublished($publish);
        }

        try {
            $post->save();
            $blog->addPost($post);
            (new BlogService())->update($blog);
            return $post;
        } catch (\Exception $previous) {
            throw new PostNotSavedException($post, $previous);
        }
    }

    /**
     * Removes a post
     * @param Blog $blog
     * @param Post $post
     * @throws PostNotRemovedException if removal fails
     */
    public function remove(Blog $blog, Post $post)
    {
        try {
            $blog->removePost($post);
            (new BlogService())->update($blog);
            $post->delete();
        } catch (\Exception $previous) {
            throw new PostNotRemovedException($post, $previous);
        }
    }

}