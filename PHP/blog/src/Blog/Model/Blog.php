<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/3/16
 * Time: 10:32 PM
 */
namespace Blog\Model;

/**
 * Class Blog
 * @package Blog\Model
 * Represents a blog. It has a title, an author and posts.
 */
class Blog extends Model implements \Serializable
{
    use SerializableTrait;

    /** @var int unique identifier */
    protected $id;

    /** @var  string title of a blog */
    protected $title;

    /** @var  User author of a blog */
    protected $author;

    /** @var Post[] array of post in a blog */
    protected $posts;

    /**
     * Blog constructor.
     */
    public function __construct()
    {
        // do not forget to initialize data structures
        $this->posts = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Blog
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return Blog
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return Post[]
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param $id
     * @return false|Post
     */
    public function getPost($id)
    {
        if (isset($this->posts[$id]))
            return $this->posts[$id];
        return false;
    }

    /**
     * @param $post
     * @return bool
     */
    public function addPost($post)
    {
        $this->posts[$post->getId()] = $post;
        return true;
    }

    /**
     * @param $post
     * @return bool
     */
    public function removePost($post)
    {
        if (isset($this->posts[$post->getId()])) {
            unset($this->posts[$post->getId()]);
            return true;
        }
        return false;
    }

    function __toString()
    {
        return sprintf("#%d %s", $this->getId(), $this->getTitle());
    }


    protected function preSerialize($properties)
    {
        if (!empty($properties['posts']))
            $properties['posts'] = array_map(function ($post) {
                return $post->getId();
            }, $properties['posts']);

        if (!empty($properties['author']))
            $properties['author'] = $properties['author']->getId();

        return $properties;
    }


    protected function postUnserialize()
    {
        parent::postUnserialize();
        if (!empty($this->posts))
            $this->posts = array_map(function ($post) {
                return Post::find($post);
            }, $this->posts);

        if (!empty(($this->author)))
            $this->author = User::find($this->author);
    }
}