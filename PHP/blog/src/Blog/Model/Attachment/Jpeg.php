<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/5/16
 * Time: 12:25 PM
 */
namespace Blog\Model\Attachment;

/**
 * Class Jpeg
 * @package Blog\Model\Attachment
 * JPEG image
 */
class Jpeg extends Image
{
    const TYPE = 'image/jpeg';
}