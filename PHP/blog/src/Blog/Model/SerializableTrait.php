<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/17/16
 * Time: 11:47 PM
 */

namespace Blog\Model;

/**
 * Class SerializableTrait
 * @package Blog\Model
 *
 * Implementation of methods declared in Serializable interface must be available
 * in each class. Otherwise private properties cannot be un/serialized successfully.
 */
trait SerializableTrait
{

    public function serialize()
    {
        $properties = get_object_vars($this);
        $properties = $this->preSerialize($properties);
        $properties['__parent'] = parent::serialize();
        return serialize($properties);
    }

    public function unserialize($serialized)
    {
        $properties = unserialize($serialized);
        parent::unserialize($properties['__parent']);
        foreach ($properties as $property => $value)
            $this->$property = $value;
    }

}