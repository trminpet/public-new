<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: tomas
 * Date: 11/3/16
 * Time: 10:32 PM
 */
namespace Blog\Model;

/**
 * Class Post
 * @package Blog\Model
 * Represents a single post. It has a title, content and it may have
 * a summary also.
 */
class Post extends Model
{
    use SerializableTrait;

    /** @var int unique identifier */
    protected $id;

    /** @var  string title of a post */
    protected $title;

    /** @var  string short summary (optional) */
    protected $summary;

    /** @var  string post content */
    protected $content;

    /** @var  Blog rerence to an owning block */
    protected $blog;

    /** @var  \DateTime last modified */
    protected $modified;

    /** @var  \DateTime to display (publish) must be set DateTime object in the past */
    protected $published;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        // modification time
        $this->modified = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->modified = new \DateTime();
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return Post
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
        $this->modified = new \DateTime();
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;
        $this->modified = new \DateTime();
        return $this;
    }

    /**
     * @return Blog
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * @param Blog $blog
     * @return Post
     */
    public function setBlog($blog)
    {
        $this->blog = $blog;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     * @return Post
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }

    function __toString()
    {
        return sprintf("%d/%d %s", $this->blog->getId(), $this->getId(), $this->getTitle());
    }

    protected function preSerialize($properties)
    {
        if (!empty($properties['blog']))
            $properties['blog'] = $properties['blog']->getId();

        return $properties;
    }

    protected function postUnserialize()
    {
        if (!empty(($this->blog)))
            $this->blog = Blog::find($this->blog);
    }

}