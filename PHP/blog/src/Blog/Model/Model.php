<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/17/16
 * Time: 10:06 PM
 */

namespace Blog\Model;

use Blog\Exception\AccessDeniedException;
use Blog\Service\StorageService;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;

/**
 * Class Model
 * @package Blog\Model
 *
 * Methods representing ActiveRecord pattern
 */
abstract class Model implements \Serializable
{

    /**
     * IdentityMap pattern (object is loaded just once)
     * @var array
     */
    protected static $map = [];

    /**
     * Returns an ID of an object
     * @return mixed
     */
    public abstract function getId();

    /**
     * Returns all stored objects
     * @return array
     */
    public static function walk()
    {
        /** @var Filesystem $filesystem */
        $filesystem = StorageService::getInstance()->getFilesystem();
        $objects = [];
        foreach ($filesystem->listContents(static::getShortName()) as $file) {
            if ($file['type'] == 'file') {
                $object = static::find($file['filename']);
                if ($object instanceof static)
                    $objects[$object->getId()] = $object;
            }
        }
        return $objects;
    }

    /**
     * Finds an object by its ID
     * @param $id
     * @return Model
     * @throws FileNotFoundException if file cannot be found
     * @throws \LogicException if deserialized object is not of expected type
     */
    public static function find($id)
    {

        if (!empty(self::$map[static::class . '_' .$id]))
            return self::$map[static::class . '_' .$id];

        /** @var Filesystem $filesystem */
        $filesystem = StorageService::getInstance()->getFilesystem();
        $path = static::getPath($id);
        $contents = $filesystem->read($path);
        if ($contents === false)
            throw new AccessDeniedException($path);
        $object = unserialize($contents);

        if (!$object instanceof static)
            throw new \LogicException("Unexpected type.");

        static::$map[static::class . '_' . $object->getId()] = $object;
        $object->postUnserialize();
        return $object;
    }

    /**
     * Returns path based on current object
     * @return string
     */
    protected static function getPath($id)
    {
        return static::getShortName() . '/' . $id . '.ser';
    }

    /**
     * Creates or updates a file with an object
     * @throws AccessDeniedException if a file cannot be created or updated
     */
    public function save()
    {
        if (empty($this->id)) {
            $this->id = $this->nextId();
        }

        /** @var Filesystem $filesystem */
        $filesystem = StorageService::getInstance()->getFilesystem();
        $path = static::getPath($this->getId());
        if (!$filesystem->put($path, serialize($this)))
            throw new AccessDeniedException(sprintf("Access denied to %s", $path));
        self::$map[static::class . '_' .$this->getId()]  = $this;
    }

    /**
     * Looks up unused ID
     * @return int
     */
    protected function nextId()
    {
        $filesystem = StorageService::getInstance()->getFilesystem();
        $id = 0;
        foreach ($filesystem->listContents(static::getShortName()) as $file)
            if ($id < $file['filename'])
                $id = $file['filename'];
        return $id + 1;
    }

    /**
     * Deletes a blog.
     * @throws AccessDeniedException if a file cannot be deleted
     */
    public function delete()
    {
        unset(self::$map[static::class . '_' .$this->getId()]);
        /** @var Filesystem $filesystem */
        $filesystem = StorageService::getInstance()->getFilesystem();
        $path = static::getPath($this->getId());
        if (!$filesystem->delete($path))
            throw new AccessDeniedException($path);
    }

    public function serialize()
    {
        return;
    }

    /**
     * Specifies properties that should be serialized. It might transform
     * objects to a serializable information (e. g. id).
     *
     * Do not call parent implementation!
     *
     * @param $properties
     * @return array
     */
    protected function preSerialize($properties)
    {
        return $properties;
    }

    public function unserialize($serialized)
    {
    }

    /**
     * Objects that were transformed using preSerialize must be resolved.
     * It is main duty of this method. It is executed after object is unserialized
     * and put into identity map. Thus it is not possible to stuck in an infinite
     * loop.
     *
     * Call parents!
     */
    protected function postUnserialize()
    {
    }

    /**
     * Returns unqualified name of the class
     * @return string
     */
    protected static function getShortName()
    {
        return (new \ReflectionClass(static::class))
            ->getShortName();
    }

}