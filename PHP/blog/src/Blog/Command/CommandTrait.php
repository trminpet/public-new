<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: kadleto2
 * Date: 11/23/16
 * Time: 6:15 PM
 */
namespace Blog\Command;

use Blog\Model\Blog;
use Blog\Service\BlogService;
use Blog\Service\PostService;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

trait CommandTrait
{

    /**
     * Returns a blog service ready to use
     * @return BlogService
     */
    protected function getBlogService()
    {
        return new BlogService();
    }

    /**
     * Returns a blog
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return Blog
     */
    protected function getBlog(InputInterface $input, OutputInterface $output)
    {
        if (!empty($input->getArgument('id')))
            return $this->getBlogById($input->getArgument('id'));
        else
            return $this->getBlogInteractively($input, $output);
    }

    /**
     * Returns a blog by its id
     * @param $id
     * @return Blog
     */
    protected function getBlogById($id)
    {
        return $this->getBlogService()->find($id);
    }

    /**
     * Returns a blog interactively
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return Blog
     */
    protected function getBlogInteractively(InputInterface $input, OutputInterface $output)
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $blogs = $this->getBlogService()->walk();
        $question = new ChoiceQuestion("Select a blog: ", $blogs);
        $response = $helper->ask($input, $output, $question);
        foreach ($blogs as $blog) {
            if ($blog == $response)
                break;
        }
        return $blog;
    }

    /**
     * Returns a post service ready to use
     * @return PostService
     */
    protected function getPostService()
    {
        return new PostService();
    }


    /**
     * Helper to display exceptions
     * @param \Exception $e
     * @param OutputInterface $output
     */
    protected function writelnException(\Exception $e, OutputInterface $output)
    {
        global $log;
        $output->writeln(sprintf("<error>%3d:</error> %s", $e->getCode(), $e->getMessage()));
        $log->error(sprintf("%3d: %s", $e->getCode(), $e->getMessage()));
        if (!empty($e->getPrevious()))
            $this->writelnException($e->getPrevious(), $output);
    }
}