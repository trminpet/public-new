<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/24/16
 * Time: 8:47 PM
 */

namespace Blog\Command\Post;

use Blog\Command\CommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Class CreateCommand
 * @package Blog\Command\Post
 */
class CreateCommand extends Command
{
    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('post:create')
            ->setDescription('Creates a new post')
            ->addArgument('id', InputArgument::OPTIONAL, 'Blog ID')
            ->addArgument('title', InputArgument::OPTIONAL, 'Name of a new post')
            ->addArgument('published', InputArgument::OPTIONAL, 'Published on a date and time')
            ->addArgument('summary', InputArgument::OPTIONAL, 'Short summary')
            ->addArgument('content', InputArgument::OPTIONAL, 'Content')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $blog = $this->getBlog($input, $output);
            $title = $this->getTitle($input, $output);
            $content = $this->getContent($input, $output);
            try {
                $published = new \DateTime($this->getPublished($input, $output));
            } catch (\Exception $e) {
                $published = null;
            }
            $summary = $this->getSummary($input, $output);
                $post = $this->getPostService()->create($blog, $title, $content, $published, $summary);
            print_r($post);
        } catch (\Exception $e) {
            $this->writelnException($e, $output);
            return $e->getCode();
        }
    }

    function __call($name, $arguments)
    {
        switch ($name) {
            case 'getTitle':
                return $this->getValue('title', ...$arguments);
            case 'getContent':
                return $this->getValue('content', ...$arguments);
            case 'getPublished':
                return $this->getValue('published', ...$arguments);
            case 'getSummary':
                return $this->getValue('summary', ...$arguments);
            default:
                throw new \LogicException("Unsupported method");
        }
    }

    protected function getValue($value, InputInterface $input, OutputInterface $output)
    {
        if (!empty($input->getArgument($value))){
            return $input->getArgument($value);
        } else {
            /** @var QuestionHelper $helper */
            $helper = $this->getHelper('question');
            return $helper->ask($input, $output, new Question("Please input $value: "));
        }
    }
}