<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/23/16
 * Time: 9:54 PM
 */
namespace Blog\Command\Blog;

use Blog\Command\CommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



class RemoveCommand extends Command
{
    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('blog:remove')
            ->setDescription('Removes a blog')
            ->addArgument('id', InputArgument::OPTIONAL, 'An ID of a blog to remove')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->getFormatter()->setStyle('error',
            new OutputFormatterStyle('white', 'red', array('bold')));
        try {
            $blog = $this->getBlog($input, $output);
            $this->getBlogService()->remove($blog);
        } catch (\Exception $e) {
            $this->writelnException($e, $output);
            return $e->getCode();
        }
    }

}
