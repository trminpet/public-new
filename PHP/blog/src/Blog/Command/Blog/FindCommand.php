<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 * 
 * User: tomas
 * Date: 11/23/16
 * Time: 8:41 PM
 */
namespace Blog\Command\Blog;

use Blog\Command\CommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindCommand extends Command
{
    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('blog:find')
            ->setDescription('Searches for a blog by its ID.')
            ->addArgument('id', InputArgument::REQUIRED, 'Blog ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $output->writeln(print_r($this->getBlogService()->find($id), true));
    }

}