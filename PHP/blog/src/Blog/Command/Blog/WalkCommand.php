<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: kadleto2
 * Date: 11/23/16
 * Time: 5:58 PM
 */
namespace Blog\Command\Blog;

use Blog\Command\CommandTrait;
use Blog\Service\BlogService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WalkCommand extends Command
{
    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('blog:walk')
            ->setDescription('Walks all available blogs');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->getBlogService()->walk() as $blog) {
            $output->writeln(sprintf('%3d: %s', $blog->getId(), $blog->getTitle()));
        }
    }


}