<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: kadleto2
 * Date: 11/23/16
 * Time: 6:04 PM
 */
namespace Blog\Command\Blog;

use Blog\Command\CommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCommand extends Command
{
    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('blog:create')
            ->setDescription('Creates a new blog')
            ->addArgument('title', InputArgument::REQUIRED, 'Title of the new blog')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $title = $input->getArgument('title');
        $blog = $this->getBlogService()->create($title);
        $output->writeln(print_r($blog, true));
    }

}