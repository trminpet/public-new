<?php
/*
 * BI-PHP.1/BIE-PHP.1 Blog application
 *
 * User: kadleto2
 * Date: 12/9/16
 * Time: 3:38 AM
 */

namespace Tests\Blog\Model;

use Blog\Model\Blog;
use Blog\Service\StorageService;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;


class ModelTest extends \PHPUnit_Framework_TestCase
{

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        $adapter = new Local(__DIR__.'/var/test');
        $filesystem = new Filesystem($adapter);
        foreach ($filesystem->listContents('Blog') as $file)
            if ($filesystem->has($file['path']))
                $filesystem->delete($file['path']);

        /** @var \Blog\Service\StorageService storage */
        $storage = StorageService::getInstance();
        $storage->setFilesystem($filesystem);
    }

    public function testSave()
    {
        $blog1 = new Blog();
        $blog1->save();
        $this->assertEquals($blog1->getId(), 1);

        $blog2 = new Blog();
        $blog2->save();
        $this->assertEquals($blog2->getId(), 2);

        $blog1->save();
        $this->assertEquals($blog1->getId(), 1);

        return $blog1;
    }

    /**
     * @depends testSave
     * @expectedException \League\Flysystem\FileNotFoundException
     */
    public function testDelete(Blog $blog)
    {
        $blog->delete();
        Blog::find(1);
    }

    /**
     * @depends testSave
     */
    public function testWalk()
    {
        $this->assertNotEmpty(Blog::walk());
    }


}
