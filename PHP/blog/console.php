<?php
/*
* This file is part of the reqtrack application.
*
* (c) 2016 Tomáš Kadlec <tomas@tomaskadlec.net>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace Blog;

require_once('vendor/autoload.php');

use Blog\Command\Blog\CreateCommand as BlogCreateCommand;
use Blog\Command\Blog\FindCommand as BlogFindCommand;
use Blog\Command\Blog\RemoveCommand as BlogRemoveCommand;
use Blog\Command\Blog\WalkCommand as BlogWalkCommand;
use Blog\Command\Post\CreateCommand as PostCreateCommand;
use Blog\Service\StorageService;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Monolog\ErrorHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Console\Application;

// filesystem
$adapter = new Local(__DIR__.'/var/data');
$filesystem = new Filesystem($adapter);

/** @var \Blog\Service\StorageService storage */
$storage = StorageService::getInstance();
$storage->setFilesystem($filesystem);

// logger
$log = new Logger('application');
$log->pushHandler(new StreamHandler('var/log/application.log', Logger::DEBUG));
ErrorHandler::register($log);
ini_set('log_errors', 'Off');

// create a console application
$application = new Application('Blog');

// register commands
$application->add(new BlogCreateCommand());
$application->add(new BlogRemoveCommand());
$application->add(new BlogFindCommand());
$application->add(new BlogWalkCommand());

$application->add(new PostCreateCommand());

// run application
$log->info("Application is about to run.");
$application->run();