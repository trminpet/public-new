/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: petr
 *
 * Created on December 3, 2016, 8:31 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define LINE_LENGHT 1024
/*
 * Definice struktury na uchovani dat
 */
typedef struct {
    char number [20];
    char * name;
    int size;
    char * t9code;





} TCONTACT;

/*
 * Funkce na prevod charu na t9 hodnotu
 */

char valueOfText(char x) {
    char tmp = '0';
    x = tolower(x);
    switch (x) {
        case 'a':case 'b':case 'c': tmp = '2';
            break;
        case 'd':case 'e':case 'f': tmp = '3';
            break;
        case 'g':case 'h':case 'i': tmp = '4';
            break;
        case 'j':case 'k':case 'l': tmp = '5';
            break;
        case 'm':case 'n':case 'o': tmp = '6';
            break;
        case 'p':case 'q':case 'r':case 's': tmp = '7';
            break;
        case 't':case 'u':case 'v': tmp = '8';
            break;
        case 'w':case 'x':case 'y':case 'z': tmp = '9';
            break;
        case ' ': tmp = '1';
            break;

        default: tmp = '#';
            break;

    }

    return tmp;
}

/*
 * Funkce na odstraneni NL z proseminare
 */

void removeNL(char *line) {
    int l = strlen(line);
    if (l > 0 && line[l - 1] == '\n')
        line[l - 1] = 0;
}

/*
 * Funkce na zjisteni zadaneho prikazu
 */

int getCommand(char *line) {

    if (line[0] == '+' && line[1] == ' ')
        return 1;
    else if (line[0] == '?' && line[1] == ' ')
        return 2;
    else
        return 0;

}

/*
 * Funkce na uvolneni pameti
 */
void FreeList(TCONTACT * list, int size) {

    int i;
    for (i = 0; i < size; i++) {
        free(list[i].name);
        free(list[i].t9code);

    }
    free(list);


}

/*
 * Funkce na zjisteni zda zadany kontakt jiz existuje, pokud vraci 0 kontakt neexistuje
 */
int validInsert(TCONTACT * list, int size, char *number, char *name) {

    int i;
    int counter = 0;
    int numCMP = 0;
    int nameCMP = 0;
    for (i = 0; i < size; i++) {
        numCMP = strcmp(list[i].number, number);
        nameCMP = strcmp(list[i].name, name);
        /*
         * Musi se shodovat jak cislo tak i jmeno
         */
        if (!numCMP && !nameCMP) {
            counter++;
        }

    }

    return counter;


}

/*
 * Funkce na vyhledavani cisla v seznamu
 */
int search(TCONTACT *list, int size, char *num) {



    int i = 0;
    int counter = 0;
    int valid = 1;
    int validD = 1;
    unsigned int b = 0;
    unsigned int siz = strlen(num);

    for (i = 0; i < size; i++) {
        valid = 1;
        validD = 1;
        for (b = 0; b < siz; b++) {
            if (list[i].number[b] == num[b] && valid) {
                valid = 1;


            } else {
                valid = 0;
            }
            if (list[i].t9code[b] == num[b] && validD) {
                validD = 1;


            } else {
                validD = 0;
            }
            if (!valid && !validD)
                break;



        }

        if (valid || validD) {
            counter++;


        }

    }

    if (counter <= 10) {

        for (i = 0; i < size; i++) {
            valid = 1;
            validD = 1;
            for (b = 0; b < siz; b++) {
                if (list[i].number[b] == num[b] && valid) {
                    valid = 1;


                } else {
                    valid = 0;
                }
                if (list[i].t9code[b] == num[b] && validD) {
                    validD = 1;


                } else {
                    validD = 0;
                }
                if (!valid && !validD)
                    break;

            }



            if (valid || validD) {


                int pos;
                int lenNumber = strlen(list[i].number);
                for (pos = 0; pos < lenNumber; pos++) {
                    printf("%c", list[i].number[pos]);

                }
                printf(" ");
                int lenName = strlen(list[i].name);
                for (pos = 0; pos < lenName; pos++) {
                    printf("%c", list[i].name[pos]);

                }
                printf("\n");

            }

        }


    }

    printf("Celkem: %d\n", counter);

    return 1;

}

int main(int argc, char** argv) {

    TCONTACT * list = NULL;
    char line[LINE_LENGHT];
    int cmd = 0;
    int valid = 0;
    int size = 0;
    int n = 0;
    int i = 0;
    int rep = 0;
    int sizeName = 0;
    /*
     * Nacitani radky ze vstupu
     */
    while (fgets(line, LINE_LENGHT - 1, stdin) != NULL) {

        /*
         * Vstup je validni
         */
        valid = 1;
        removeNL(line);
        cmd = getCommand(line);
        if (cmd != 0) {
            if (cmd == 1) {
                /*
                 * Alokace pameti pro seznam struktur
                 */
                if (n >= size) {

                    if (size < 1000)
                        size += 100;
                    else
                        size *= 1.5;
                    list = (TCONTACT *) realloc(list, size * sizeof ( *list));
                }
                /*
                 * Nacitani cisla a validace
                 */
                for (i = 2; i < 22 && line[i] != ' '; i++) {
                    if (isdigit(line[i]) && line[i] >= '0' && line[i] <= '9') {
                        list[n].number[i - 2] = line[i];

                    } else {

                        valid = 0;
                        break;


                    }

                }
                /*
                 * Umisteni ukoncovaniho znaku
                 */
                list[n].number[i - 2] = 0;
                /*
                 * Kontrola zadani prave jedne mezery mezi cislem a jmenem
                 */
                if (!(line[i] == ' ' && line[i + 1] != ' '))
                    valid = 0;
                if (valid) {
                    /*
                     * Nacitani jmena
                     */
                    list[n].size = 0;
                    sizeName = list[n].size;
                    list[n].name = NULL;
                    list[n].t9code = NULL;
                    rep = 0;
                    /*
                     * Dokud nenarazim na ukoncovani znak
                     */
                    while (line[++i] != '\0') {
                        /*
                         * Alokace pameti pro jmeno
                         */
                        if (rep >= sizeName) {

                            if (sizeName < 20)
                                sizeName += 10;
                            else
                                sizeName *= 1.5;

                            list[n].t9code = (char*) realloc(list[n].t9code, (sizeName + 1) * sizeof (char));
                            list[n].name = (char*) realloc(list[n].name, (sizeName + 1) * sizeof (char));

                        }
                        /*
                         * Validace jmena
                         */
                        if ((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z') || line[i] == ' ') {
                            list[n].name[rep] = line[i];
                            list[n].t9code[rep] = valueOfText(line[i]);
                            rep++;


                        } else {
                            free(list[n].name);
                            free(list[n].t9code);
                            valid = 0;
                            break;

                        }



                    }
                    if (valid) {
                        /*
                         * Pridani ukoncovaciho znaku za nactena data
                         */
                        list[n].name[rep] = 0;
                        list[n].t9code[rep] = 0;
                    }
                }



                if (valid) {
                    /*
                     * Vyhledavani zda kontakt jiz existuje ci nikoliv
                     */
                    if (n == 0 || !validInsert(list, n, list[n].number, list[n].name)) {
                        n++;

                        printf("OK\n");


                    } else {
                        printf("Kontakt jiz existuje.\n");
                        free(list[n].t9code);
                        free(list[n].name);


                    }


                }

            } else {
                /*
                 * Vyhledavani kontaktu dle cisla
                 */
                valid = 1;
                char lnumber[200];
                /*
                 * Nacitani vstupu do ukoncovaniho znaku
                 */
                for (i = 2; line[i] != 0; i++) {

                    if (isdigit(line[i]) && line[i] >= '0' && line[i] <= '9') {
                        lnumber[i - 2] = line[i];

                    } else {

                        valid = 0;
                        break;


                    }

                }
                /*
                 * Vyhledavani cisla a nasledny vypis
                 * 
                 *                  
                 */
                if (valid) {
                    lnumber[i - 2] = 0;
                    search(list, n, lnumber);
                }

            }

        }
        /*
         * Nevalidni prikaz nebo vstup zobrazeni chyboveho hlaseni
         */
        if (cmd == 0 || valid == 0) {

            printf("Nespravny vstup.\n");


        }

    }
    if (!feof(stdin)) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    FreeList(list, n);

    return (EXIT_SUCCESS);
}