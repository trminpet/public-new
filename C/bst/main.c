/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: petr
 *
 * Created on January 6, 2017, 7:37 AM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */

typedef struct Node {
    int value;
    struct Node * left;
    struct Node * right;

    
}TNODE;

typedef struct Tree {
    TNODE * root;
    
}TTREE;


TTREE * createEmptyTree(){
    //TNODE * tmp =(TNODE *) malloc (sizeof(*tmp));
    TTREE * tmp = NULL;
    tmp = (TTREE *)calloc(1,sizeof(*tmp));
    tmp->root=NULL;
    return tmp;

}
void burnBranch(TNODE * b){
    if(b==NULL)
        return;
    burnBranch(b->right);
    b->right=NULL;
    burnBranch(b->left);
    b->left=NULL;
    free(b);
    b=NULL;



}
void burnTree(TTREE * t){
    
    burnBranch(t->root);
    free(t);
    
}
//preorder pruchod
void printBranch(TNODE * b){
    if(b==NULL)
        return;
    printf("%d ",b->value);
    printBranch(b->left);
    printBranch(b->right);
    
    

}
void printTree(TTREE * t){
    printBranch(t->root);
    printf("\n");




}
void printAscendingBranch(TNODE * b){
   
    if(b==NULL)
        return;
    printAscendingBranch(b->left);
    printf("%d ",b->value);
  
    printAscendingBranch(b->right);

}
void printAscending(TTREE * t){
      
    printAscendingBranch(t->root);
    printf("\n");
    
}
void printDescendingBranch(TNODE * b){
   
    if(b==NULL)
        return;
    printDescendingBranch(b->right);
    printf("%d ",b->value);
  
    printDescendingBranch(b->left);

}
void printDescending(TTREE * t){
      
    printDescendingBranch(t->root);
    printf("\n");
    
}
TNODE * initNode ( int value ){
    TNODE * tmp = (TNODE * )calloc(1,sizeof(*tmp));
    tmp->value=value;
    tmp->right=NULL;
    tmp->left=NULL;
    
}



void insertNode (TNODE * insert, TNODE ** where){

  /*
    if(insert->value < where->value)
    {
    //vlozit doleveho podstromu
        if(where->left!=NULL){
            insertNode(insert,where->left);
        }
        where->left=insert;
            
    }
    else if(insert->value > where->value){
    //vlozit dopraveho podstromu
         if(where->right!=NULL){
            insertNode(insert,where->right);
        }
        where->right=insert;
    
    }else
    {
    // nic neni potreba jedna se o unikatni indexi
    }
*/
    if(*where==NULL){
        *where=insert;
    }
    else{
        if(insert->value < (*where)->value){
            insertNode(insert,&((*where)->left));
        }
        else if(insert->value > (*where)->value){
         insertNode(insert,&((*where)->right));
        }
    
    }



}
void insertValue (TTREE * tree, int value){
    TNODE * tmp = initNode(value);
    insertNode(tmp,&(tree->root));

}
TNODE * findValueBranch(TNODE * b,int value){
    if(b==NULL)
        return NULL;
    if(value==b->value)
        return b;
    if(value > b->value)
        findValueBranch(b->right,value);
    else if(value < b->value)
        findValueBranch(b->left,value);
    
    
    
    

    
}
TNODE * minValueNode ( TNODE * root){
    while(root->left!=NULL){
    
        root=root->left;
    }

    return root;
}

int findValue(TTREE * tree,int value){
    TNODE * tmp = tree->root;
    
    TNODE * res = findValueBranch(tmp,value);
    if(res==NULL)
        return 0;
    return res->value;
    
    
}
TNODE * deleteNode(TNODE * node,int value){
    if(node==NULL)
        return node;
    if(value < node->value)
        node->left=deleteNode(node->left,value);
    else if(value > node->value)
        node->right=deleteNode(node->right,value);
    else  {
        if(node->left==NULL){
            TNODE * tmp = node->right;
            free(node);
            return tmp;
        
        }
        else if(node->right==NULL){
            TNODE * tmp = node->left;
            free(node);
            return tmp;
        
        }
        
        TNODE * tmp = minValueNode(node->right);
        node->value= tmp->value;
        node->right=deleteNode(node->right,tmp->value);
    
    }
    return node;


}


TTREE * mergeTree(TTREE * a,TTREE * b){
      
    TNODE * tmp = b->root;
    insertValue(a,tmp->value);
    
    
    while(tmp->left!=NULL)
    {
       insertValue(a,tmp->value);
       tmp=tmp->left;
    
    }
    tmp=b->root;
      while(tmp->right!=NULL)
    {
       insertValue(a,tmp->value);
       tmp=tmp->right;
    
    }
    return a;


}






int main(int argc, char** argv) {

    TTREE * tree = createEmptyTree();
    TNODE * root=NULL;
    insertValue(tree,22);
    insertValue(tree,12);
    insertValue(tree,-1);
    insertValue(tree,15);
    insertValue(tree,13);
    insertValue(tree,18);
    insertValue(tree,55);
    insertValue(tree,33);
    insertValue(tree,34);
    printTree(tree);
   /* printAscending(tree);
    printDescending(tree);
    printf("Nalezeno %s\n",findValue(tree,14)!=0?"ano":"ne");
    printf("Nalezeno %s\n",findValue(tree,13)!=0?"ano":"ne");
    root= deleteNode(tree->root,22);
    printBranch(root);
    printf("\n");
    */ 
   TTREE * tree2 = createEmptyTree();
   insertValue(tree2,16);
    insertValue(tree2,1);
    insertValue(tree2,-14);
    insertValue(tree2,13);
    insertValue(tree2,10);
    insertValue(tree2,88);
    insertValue(tree2,12);
    insertValue(tree2,9);
    insertValue(tree2,3);
    printTree(tree2);
  TTREE * treemerge = mergeTree(tree,tree2);
printTree(tree);  
printf("\n");
  
    
    
    
    
    burnTree(tree);
    burnTree(tree2);
    
    return (EXIT_SUCCESS);
}

