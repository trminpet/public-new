/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: petr
 *
 * Created on November 5, 2016, 8:59 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

/*
 * Zmena charu na cislo
 */
int valueOfChar(char a) {

    switch (a) {
        case 'a': return 0;
        case 'b': return 1;
        case 'c': return 2;
        case 'd': return 3;
        case 'e': return 4;
        case 'f': return 5;
        case 'g': return 6;
        case 'h': return 7;
        case 'i': return 8;
        case 'j': return 9;
        case 'k': return 10;
        case 'l': return 11;
        case 'm': return 12;
        case 'n': return 13;
        case 'o': return 14;
        case 'p': return 15;
        case 'q': return 16;
        case 'r': return 17;
        case 's': return 18;
        case 't': return 19;
        case 'u': return 20;
        case 'v': return 21;
        case 'w': return 22;
        case 'x': return 23;
        case 'y': return 24;
        case 'z': return 25;
        case '|': return 30;
        case '\n': return 31;
        default: return -1;

    }

}

/*
 * Mocneni cisel
 */
int powA(int exp) {
    int base = 2;
    int tmp = 1;
    int i;
    for (i = 1; i <= exp; i++) {
        tmp *= base;
    }
    return tmp;


}

/*
 * Nacteni prefixu
 */
int readInputPrefix() {

    char prefix = 'x';
    int tmp = 0;
    while (scanf("%c", &prefix) == 1) {
        if (prefix == '|') {
       
            return tmp;
        }
        if (valueOfChar(prefix) == -1 || prefix == '\n') {
            return -1;
        }
        tmp += powA(valueOfChar(prefix));
    }

    return -1;
}

/*
 * Nacteni zbytku zpravy=zprava sama o sobe
 */
int readInputMs() {

    char ms = 'x';
    int tmp = 0;
    while (scanf("%c", &ms) == 1) {
        if (ms == '\n') {
            return tmp;
        }
        if (valueOfChar(ms) == -1 || ms == '|') {
            return -1;
        }
        tmp += powA(valueOfChar(ms));
    }
return -1;
}



int main(int argc, char** argv) {
    int prefixOne = 0;
    int prefixTwo = 0;
  int x=0;
   
    int y = 0;

    int msOne = 0;
    int msTwo = 0;

    /*
     * Kontrolova vstupu
     */
    printf("Zpravy:\n");
    if ((prefixOne = readInputPrefix()) == -1) {
        printf("Nespravny vstup.\n");
        return 1;
    }


    if ((msOne = readInputMs()) == -1) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    if ((prefixTwo = readInputPrefix()) == -1) {
        printf("Nespravny vstup.\n");
        return 1;
    }


    if ((msTwo = readInputMs()) == -1) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    if (prefixOne == 0 && msOne == 0) {
        printf("Nespravny vstup.\n");
        return 1;

    }
    if (prefixTwo == 0 && msTwo == 0) {
        printf("Nespravny vstup.\n");
        return 1;

    }
    /*
     *ostreni nulovych stavu 
     */
    if (prefixOne == 0 || prefixTwo == 0 || msOne == 0 || msTwo == 0) {

        printf("Synchronizace za: 0\n");
        return 0;
    }
    /*
     *Pokud jsou stejne prefixy zprava je synchronizovana 
     */
    if (prefixOne == prefixTwo) {
        printf("Synchronizace za: %d\n", prefixOne);
        return 0;

    }

  
    /*Pokud jsou shodne rovnice zvysim jednu stranu o jedna a pokracuji
     * 
     */
    if (prefixOne * x + (x - 1) * msOne == prefixTwo * y + (y - 1) * msTwo) {
        y+=prefixOne;
    }
    /*
     * Dokud se strany nerovnanji jede cyklus a y++
     */
    
    while (prefixOne * x + (x - 1) * msOne != y + (y/prefixTwo - 1) * msTwo) {
        y+=prefixTwo;
        x = (msOne + prefixTwo * (y/prefixTwo) + msTwo * (y/prefixTwo - 1)) / (prefixOne + msOne);
        
        if (x < 0 || y < 0 || (x/prefixTwo > INT_MAX / y)) {
            printf("Nelze dosahnout.\n");
            return 0;
        }



    }
    /*
     *  Osetreni specialnich vztahu kdy x a y = 0 a zaroven je zprava stejne dlouha 
     */
    if (x == 0 && y == 0 && msOne == msTwo) {

        printf("Synchronizace za: %d\n", msOne);
        return 0;

    }
    /*
     * Vypis vysledku
     */
    printf("Synchronizace za: %d\n", prefixOne * x + (x - 1) * msOne);

    return (EXIT_SUCCESS);
}
