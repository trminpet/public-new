/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: petr
 *
 * Created on November 12, 2016, 8:10 AM
 */

#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#endif /* __PROGTEST__ */
/*funkce ktera vraci 1 pokud je rok prestupny jinak vraci 0*/
int isLeapYear(int y) {

    if (y >= 4000) {
        if (y % 4 == 0 && (y % 100 != 0 || y % 400 == 0) && y % 4000 != 0)
            return 1;
        else return 0;
    } else {
        if (y % 4 == 0 && (y % 100 != 0 || y % 400 == 0))
            return 1;
        else return 0;

    }

}

/*funkce ktera vraci pocet dnu vzhledem k mesici/roku*/
int DaysOfMonth(int year, int month) {
    int days = 0;

    switch (month) {
        case 1: days = 31;
            break;
        case 2:
        {
            if (isLeapYear(year)) {
                days = 29;
            } else days = 28;
        }
            break;
        case 3: days = 31;
            break;
        case 4: days = 30;
            break;
        case 5: days = 31;
            break;
        case 6: days = 30;
            break;
        case 7: days = 31;
            break;
        case 8: days = 31;
            break;
        case 9: days = 30;
            break;
        case 10: days = 31;
            break;
        case 11: days = 30;
            break;
        case 12: days = 31;
            break;

    }
    return days;
}

/*Tomohiko Sakamoto's Algorithm
 *Tuto funkci jsem okopiroval od Tomohiko Sakamoto (rok vydani 1993)
 * Jedna se o algoritmus, ktery dle zadaneho datumu urci den v tydnu.
 * https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week
 * Petr Trminek 12.11.2016
 */
int dayofweek(int y, int m, int d) {
    static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    y -= m < 3;
    int a = (y + y / 4 - y / 100 + y / 400 + t[m - 1] + d) % 7;

    return a;
}



/*funkce, ktera kontroluje zda prvni zadane datum je mensi nez druhe.Pokud ano
 vraci 1 jinak vraci 0, rozepsane situace ktere mohou nastat*/
int validOrderDate(int y1, int m1, int d1,
        int y2, int m2, int d2) {

    if (y1 > y2)
        return 0;
    if (y1 < y2)
        return 1;
    else if (y1 == y2) {
        if (m1 < m2) {
            return 1;
        } else if (m1 == m2) {
            if (d1 <= d2)
                return 1;
            return 0;
        } else
            return 0;

    }



    return 0;
}
/*funkce ktera kontroluje zda pocet dnu se vejde do mesice a take prestupny rok*/
int validMonthAndDay(int y, int m, int d) {
    if (isLeapYear(y) && m == 2 && d >= 1 && d <= 29) {
        return 1;
    } else {
        switch (m) {
            case 1:if (d >= 1 && d <= 31)return 1;
                return 0;
            case 2:if (d >= 1 && d <= 28)return 1;
                return 0;
            case 3:if (d >= 1 && d <= 31)return 1;
                return 0;
            case 4:if (d >= 1 && d <= 30)return 1;
                return 0;
            case 5:if (d >= 1 && d <= 31)return 1;
                return 0;
            case 6:if (d >= 1 && d <= 30)return 1;
                return 0;
            case 7:if (d >= 1 && d <= 31)return 1;
                return 0;
            case 8:if (d >= 1 && d <= 31)return 1;
                return 0;
            case 9:if (d >= 1 && d <= 30)return 1;
                return 0;
            case 10:if (d >= 1 && d <= 31)return 1;
                return 0;
            case 11:if (d >= 1 && d <= 30)return 1;
                return 0;
            case 12:if (d >= 1 && d <= 31)return 1;
                return 0;
            default:return 0;
        }
        return 0;

    }
    return 0;
}


/*funkce ktera kontroluje zda datum je spravne rok a pocet mesicu*/
int validDate(int y, int m, int d) {
    if (y < 2000)
        return 0;

    if (!validMonthAndDay(y, m, d)) {
        return 0;
    }
    return 1;



}


/*funkce kontroluje zda zadane datum je svatek dle zadani krome Velikonec*/
int checkForHoliday(int m, int d) {

    switch (m) {
        case 1:if (d == 1)return 1;
            return 0;
        case 5:if (d == 1 || d == 8)return 1;
            return 0;
        case 7:if (d == 5 || d == 6)return 1;
            return 0;
        case 9:if (d == 28)return 1;
            return 0;
        case 10:if (d == 28)return 1;
            return 0;
        case 11:if (d == 17)return 1;
            return 0;
        case 12:if (d == 24 || d == 25 || d == 26)return 1;
            return 0;
        default:return 0;



    }



}
int manageBigDate(int y){
    int tmp = 0;
    if(y>4000){
       
        while(y>=4000){
            y-=4000;
            printf("%d\n",y);
            tmp++;
        
        
        
        
        
        }
      
        return y;    
            
            
    
    }
    else
       
        return y;












}
/*funkce ktera urcuje zda datum je pracovni den*/
int IsWorkDay(int y, int m, int d) {

    if (validDate(y, m, d)) {
        int day = dayofweek(y, m, d);
        /*pokud vyjde 6,0 jedna se o sobotu,ci nedeli*/
        if (day == 6 || day == 0) {

            return 0;
        }
        /*kontrola svatku*/
        if (checkForHoliday(m, d)) {

            return 0;
        }
        /*jedna se o pracovni den*/
        return 1;

    }
    return 0;


}

/*funkce ktera urcuje pocet pracovnich dni mezi dvemi daty*/
int CountWorkDays(int y1, int m1, int d1,
        int y2, int m2, int d2,
        int * cnt) {
    *cnt = 0;
    int diffOne =0;
    int diffTwo = 0;
    /*kontrola zda jsou datumy platne*/
    if (!validDate(y1, m1, d1) || !validDate(y2, m2, d2)) {

        return 0;
    }
    /*kontrola spravneho poradi*/
    if (!validOrderDate(y1, m1, d1, y2, m2, d2)) {
        return 0;
    }/*vypocet pracovnich dnu*/
    else {


        int tmp = 0;
        diffOne = y1;
        diffTwo = y2;
        printf("%d %d %d",y1,y2,diffTwo);
      
        y1=manageBigDate(y1);
        y2=manageBigDate(y2);
     
        printf("%d %d %d",y1,y2,diffTwo);
        
        while (1) {
            /*pokud se jedna o pracovni den pridam 1 k pracovnim dnu*/
            if (IsWorkDay(y1, m1, d1)) {
                tmp++;


            }

            /*pokud je den vetsi nez pocet dnu v mesici, resetuji pocet dnu a prictu mesic*/
            if (d1 > DaysOfMonth(y1, m1)) {

                d1 = 0;
                m1++;
            }
            /*pokud je pocet mesicu vetsi nez 12 resetuji den,mesic a prictu jeden rok*/
            if (m1 > 12) {
                d1 = 0;
                m1 = 1;
                y1++;

            }
            /*pokud je splnena tato podminka data se rovnaji*/
            if (y1 == y2 && m1 == m2 && d1 == d2) {
                break;
            }
            /*pricteni jednoho dnu*/
            d1++;

        }
           // printf("%d %d",y2,y1);
        if(diffTwo > 4000)
        {
           
            int tmp = y2 - diffOne;
            int count = diffTwo/tmp;
            tmp*=count;
        
            
        
        
        }
        
        
        
        
    //    printf("%d",tmp);
        /*predani tmp vystupnimu parametru*/
        *cnt = tmp;
        return 1;
    }

    return 0;

}

#ifndef __PROGTEST__

int main(int argc, char * argv []) {
    int cnt;
    CountWorkDays ( 2012, 4, 3, 32456, 1, 4, &cnt );
  

   // assert(CountWorkDays(2165, 11, 12, 5079507, 5, 18, &cnt) == 1 && cnt == 1284720613);
    /*
    assert(IsWorkDay(2016, 11, 11));
    assert(!IsWorkDay(2016, 11, 12));
    assert(!IsWorkDay(2016, 11, 17));
    assert(!IsWorkDay(2016, 11, 31));
    assert(!IsWorkDay(2004, 2, 29));

    assert(!IsWorkDay(2001, 2, 29));
    assert(IsWorkDay(2016, 2, 29));

    assert(!IsWorkDay(1996, 1, 1));
    assert(CountWorkDays(2016, 11, 1,
            2016, 11, 30, &cnt) == 1
            && cnt == 21);


    assert(CountWorkDays(2016, 11, 1,
            2016, 11, 17, &cnt) == 1
            && cnt == 12);

    assert(CountWorkDays(2016, 11, 1,
            2016, 11, 1, &cnt) == 1
            && cnt == 1);
    assert(CountWorkDays(2016, 11, 17,
            2016, 11, 17, &cnt) == 1
            && cnt == 0);


    assert(CountWorkDays(2016, 1, 1,
            2016, 12, 31, &cnt) == 1
            && cnt == 254);

    assert(CountWorkDays(2015, 1, 1,
            2015, 12, 31, &cnt) == 1
            && cnt == 252);
    assert(CountWorkDays(2000, 1, 1,
            2016, 12, 31, &cnt) == 1
            && cnt == 4302);

    assert(CountWorkDays(2001, 2, 3,
            2016, 7, 18, &cnt) == 1
            && cnt == 3911);
    assert(CountWorkDays(2014, 3, 27,
            2016, 11, 12, &cnt) == 1
            && cnt == 666);
    assert(CountWorkDays(2001, 1, 1,
            2000, 1, 1, &cnt) == 0);





    assert(CountWorkDays(2001, 1, 1,
            2015, 2, 29, &cnt) == 0);


     */
    return 0;
}
#endif /* __PROGTEST__ */