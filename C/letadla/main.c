/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: petr
 *
 * Created on November 26, 2016, 8:30 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Deklarace struktury
 */
typedef struct {
    /**
     * ID Struktury-zpravy
     */
    int id;
    /**
     * Pole zprav
     */
    int * list;
    /**
     * Velikost aktualni velikost pole
     */
    int listSize;
    /**
     * Velikost alokovaneho pole
     */
    int listAlSize;
    /**
     * Kontrolova zda zprava jiz byla pouzita ve vypisu
     */
    int used;



} TMESS;

/**
 * Funkce na uvolneni pameti
 */
void freeMess(TMESS * arr, int n) {


    if (n == 1) {
        free(arr[0].list);
        free(arr);
    } else {

        int i = 0;
        for (i = 0; i < n; i++) {
            free(arr[i].list);
        }

        free(arr);
    }

}

/**
 * Funkce na vytisknuti poli
 */
void printArray(TMESS * arr, int n) {

    int i;
    printf("%d\n", n);
    for (i = 0; i < n; i++) {


        printf("%d :", arr[i].id);
        int a;
        for (a = 0; a < arr[i].listSize; a++) {
            //   printf("%d ", arr[i].list[a]);

        }

        printf("\n");


    }

}

/**
 *Pomocna funkce pro QSORT ID
 */
int compare(const void * a, const void * b) {
    TMESS *tA = (TMESS *) a;
    TMESS *tB = (TMESS *) b;

    return tA->id - tB->id;

}

/**
 * QSORT struktur dle ID
 */
void sortTMESSList(TMESS * arr, int n) {
    qsort(arr, n, sizeof (TMESS), compare);



}

/**
 *Funkce na zjisteni zda se zadane ID naleze v poli Struktur, vraci pozici pri nalezeni
 */
int searchForIndex(TMESS * arr, int n, int x) {

    int i;
    for (i = 0; i < n; i++) {
        if (arr[i].id == x)
            return i;
    }
    return -1;

}

/**
 * Funcke na nacteni vstupu
 */
int readInput(TMESS ** arr, int * sizeOfArr) {


    TMESS * array = NULL;
    int id = 1;
    char dvojtecka = ':';
    char zavorka = '[';
    char znak = 'x';
    int value = 1;
    int n = 0;
    int size = 0;
    int rep = 0;
    int res = 0;
    int sizeAlArra = 0;
    int tmp = 0;
    int flag = 0;
    int index = 0;
    /**
     * Nacitani ID
     */
    while ((res = (scanf(" %d", &id))) == 1 && id >= 0) {

        /**
         * Alokace pole struktur
         */
        if (n >= size) {

            if (size < 1000)
                size += 100;
            else
                size *= 1.5;


            array = (TMESS *) realloc(array, size * sizeof ( *array));
        }

        /**
         * Ulozeni pozice pri nacitani, aby zustala spravna velikost pole
         */
        tmp = n;
        /**
         * Vyhledani indexu
         */
        index = searchForIndex(array, n, id);
        /**
         * Index se nasel, do pomocnych promennych se ulozi hodnoty ze struktury, nastavim FLAG kvuli spravnosti n
         * 
         */
        if (index != -1) {

            n = index;
            sizeAlArra = array[n].listAlSize;
            rep = array[n].listSize;

            flag = 0;

        } else {
            rep = 0;
            array[n].list = NULL;
            sizeAlArra = 0;


            flag = 1;
        }
        /**
         * Incializace hodnot ve strukture
         */
        array[n].id = id;
        array[n].used = 1;
        /**
         * Kontrola nacteni spravnych znaku : [
         */

        if (scanf(" %c %c", &dvojtecka, &zavorka) == 2 && dvojtecka == ':' && zavorka == '[') {
            /**
             * Nacitani cisel a ukoncovaciho znaku nebo carky
             */
            while (scanf("%d %c", &value, &znak) == 2 && (znak == ']' || znak == ',')) {

                /**
                 * Alokace pole ve strukture, dle poctu opakovani, kolik bylo zadano cisel
                 * Pokud je 0 prislo nove ID, jinak se pouzije hodnota ze struktury a je moznost
                 * ze se reallocuje
                 */
                if (rep >= sizeAlArra) {

                    if (sizeAlArra < 20)
                        sizeAlArra += 10;
                    else
                        sizeAlArra *= 1.5;


                    array[n].list = (int*) realloc(array[n].list, sizeAlArra * sizeof ( int));
                }
                /**
                 * Pridani opakovani
                 */

                rep++;



                array[n].list[rep - 1] = value;

                /**
                 * Prisel ukoncovani, znak ulozeni aktualni velikosti pole a alokovane velikosti
                 */

                if (znak == ']') {
                    array[n].listSize = rep;
                    array[n].listAlSize = sizeAlArra;
                    break;
                }

            }
            /**
             * Kontrola zda posledni znak je validni, jinak konec a free
             */
            if (!(znak == ',' || znak == ']')) {
                *sizeOfArr = n;
                *arr = array;

                return 0;
            }



        } else {
            *sizeOfArr = n;
            *arr = array;

            return 0;
        }
        /**
         * Nastaveni spravne hodnoty n a inkrementace pokud jsem nacetl nove ID, jinak zustava stejne
         */
        n = tmp;
        if (flag) {
            n++;
        }

    }
    /**
     * Po nacteni serazeni pole dle ID
     */
    sortTMESSList(array, n);
    /**
     * Kontrola EOF
     */
    if (res != EOF || znak == ',') {

        *sizeOfArr = n;
        *arr = array;

        return 0;
    }

    /**
     * Predani vystupnich parametru, pole struktur a velikost pole struktur
     */

    *sizeOfArr = n;
    *arr = array;

    return 1;


}

/**
 * Funkce na vyhledavani a porovnani poli
 */
void search(TMESS * arr, int n) {

    int i = 0;
    int flag = 0;
    int b = 0;
    int a = 0;
    int h = 0;
    int x = 0;
    int y = 0;
    int counter = 0;

    /**
     * Prochazeni pole od zacatku
     */
    for (i = 0; i < n; i++) {
        if (arr[i].used)
            printf("%d", arr[i].id);
        /**
         * Porovnavani s dalsimi poli
         */
        for (a = i + 1; a < n; a++) {


            /**
             * Pokud nesouhlasi velikost, preskakuji zpravu
             */
            if (arr[i].listSize != arr[a].listSize) {

                continue;
            }
            /**
             * Porovnavani poli dle cisel
             */

            flag = 0;


            for (h = 0; h < arr[i].listSize; h++) {

                counter = 0;
                for (b = 0; b < arr[a].listSize; b++) {
                    x = h;
                    y = b;
                    /**
                     * Porovnavani cisel a kontrola zda nalezana cisla jsou mensi nez celkovy pocet
                     */
                    while (counter < arr[i].listSize && arr[i].list[x] == arr[a].list[y]) {

                        if (y + 1 == arr[a].listSize) {
                            y = 0;
                            x++;

                        } else {
                            x++;
                            y++;

                        }
                        counter++;
                    }
                    if (x == 0 || y == 0)
                        continue;
                    /**
                     * Vyhodnoceni poctu zkontrolovanych znaku a kontrola znaku kde "stoji"
                     */
                    if (counter == arr[i].listSize && arr[i].list[x - 1] == arr[a].list[y - 1 ]) {
                        flag = 1;
                        break;

                    }

                }
                if (flag)
                    break;

            }


            /**
             * Je nastaven FLAG a pole nebylo pouzito
             */
            if (flag && arr[a].used) {
                printf(", %d", arr[a].id);
                arr[a].used = 0;
            }

        }
        if (arr[i].used) {
            arr[i].used = 0;
            printf("\n");
        }


    }

}

int main(int argc, char** argv) {

    TMESS * arr = NULL;

    int sizeOfArr = 0;
    printf("Zpravy:\n");
    if (!readInput(&arr, &sizeOfArr)) {
        freeMess(arr, sizeOfArr);
        printf("Nespravny vstup.\n");
        return 0;


    }

    printf("Unikatni zpravy:\n");
    search(arr, sizeOfArr);





    freeMess(arr, sizeOfArr);

    return (EXIT_SUCCESS);
}
