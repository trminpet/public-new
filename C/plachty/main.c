/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.c
 * Author: petr
 *
 * Created on October 29, 2016, 8:17 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define precision 0.00000001 
                 

/*
 *
 */

/*funkce na vypocet obsahu
 */
double foo(double wCloth, double hCloth,double overLap,double wSail,double hSail) {

    int countX = 0;
    int countY = 0;
   /*
    * vypocet velikosti po ose X
    */
    if (wCloth > overLap) {
        double a = wCloth;
        /*kontrola sirky plachty a pridavani latek 
         */
        while (a < wSail + (wCloth - overLap)&& (wSail + (wCloth - overLap))-a > precision) {
            countX++;
            a += (wCloth - overLap);
        }
     
         
         
    } else {
        countX = 1;
    }
    /*vypocet velikosti po ose Y
     */
    if (hCloth > overLap) {
        double b = hCloth;
          /*kontrola vysky plachty a pridavani latek 
           */
        while (b < hSail + (hCloth - overLap) && (hSail + (hCloth - overLap))-b  > precision) {
            countY++;
            b += (hCloth - overLap);
       
            
        }
      
    } else {
        countY = 1;
    }
    if (countX == 0) {
        countX++;
    } else if (countY == 0) {

        countY++;
    }
    /*
     Vracim obsah(pocet latek z X a Y)
     */
    return countX*countY;
}
int main(int argc, char** argv) {
    double wCloth = 0;
    double hCloth = 0;
    double wSail = 0;
    double hSail = 0;
    double overLap = 0;
    double res = 0;
    printf("Velikost latky:\n");
    if (scanf("%lf %lf", &hCloth, &wCloth) != 2 || wCloth <= 0 || hCloth <= 0) {
        printf("Nespravny vstup.\n");
        return 1;

    }
    printf("Velikost plachty:\n");
    if (scanf("%lf %lf", &hSail, &wSail) != 2 || wSail <= 0 || hSail <= 0) {
        printf("Nespravny vstup.\n");
        return 1;

    }
    /*
     Pokud je latka vetsi nez plachta
     */
    if ((wCloth >= wSail && hCloth >= hSail) || (wCloth >= hSail && hCloth >= wSail) ) {
       
  
        res = 1;
    } 
    else {
        printf("Prekryv:\n");
        if (scanf("%lf", &overLap) != 1 || overLap < 0) {
            printf("Nespravny vstup.\n");
            return 1;
        }
        /*
         Pokud je prekryv vetsi nez strany latky
         */
        if (overLap > wCloth && overLap > hCloth) {
            printf("Nelze vyrobit.\n");
            return 1;
        }
        /*
         Nulovy prekryv staci resit pomoci obsahu
         */
        if (overLap == 0) {
            res = (wSail * hSail) / (wCloth * hCloth);
        } else {
            /*
             Pokud je vyska mensi nez prekryv
             */
            if (hCloth <= overLap) {
                res = foo(hCloth, wCloth,overLap,wSail,hSail);
              
                
            } 
             /*
             Pokud je sirka mensi nez prekryv
             */            
            else if (wCloth <= overLap) {
                res = foo(wCloth, hCloth,overLap,wSail,hSail);
                
            }
            else {
                /*
                 Vraci nejmensi moznou hodnotu z funkce, zkousi se normalne a pak o 90 otoceno
                 */
                
                res = foo(hCloth, wCloth,overLap,wSail,hSail) > foo(wCloth, hCloth,overLap,wSail,hSail)
                      ? foo(wCloth, hCloth,overLap,wSail,hSail) : foo(hCloth, wCloth,overLap,wSail,hSail);
            
            }
        }
    }
    printf("Pocet kusu latky: %.0f\n", res);


    return (EXIT_SUCCESS);
}
