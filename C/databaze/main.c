#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

typedef struct TResult {
    TResult * m_Next;
    int m_ID;
    char * m_Name;
} TRESULT;
#endif /* __PROGTEST__ */

/*
 * Deklarace struktury pro uchovani osoby
 */
typedef struct TPerson {
    TPerson * m_Next;
    TPerson * m_Previous;
    int m_ID;
    char * m_Name;
    int m_ID1;
    int m_ID2;


} TPERSON;

/*
 * Deklarace seznamu osob
 */
typedef struct TList {
    TPerson * m_First;
    TPerson * m_Last;
    TPerson * m_Current;

} TLIST;

/*
 * Deklarace globalni promenne pro uchovani listu.
 */
TLIST * g_List;

/*
 * Funkce na overeni zda zadane ID existuje v seznamu
 */

int notUnique(int ID) {
    if (g_List->m_First == NULL)
        return 1;

    TPERSON * person;

    person = g_List->m_First;
    while (person != NULL) {
        if (person->m_ID == ID) {

            return 0;
        }
        if (person->m_Next == g_List->m_Last)
            break;
        person = person->m_Next;
    }
    if (g_List->m_Last->m_ID == ID) {

        return 0;
    }

    return 1;
}

/*
 * Funkce na vyhledani osoby dle ID
 */
TPERSON * findPerson(int ID) {
    TPERSON * person = NULL;


    person = g_List->m_First;
    while (person) {

        if (person->m_ID == ID)
            return person;
        person = person->m_Next;

    }

    return NULL;



}

/*
 * Odstraneni duplicit-prevzani kodu z webove stranky
 * http://www.geeksforgeeks.org/remove-duplicates-from-an-unsorted-linked-list/
 * 18.12.2016 Trminek 
 */

TRESULT * duplityResult(TRESULT *result) {
    TRESULT * p1, *p2, *duplicity;
    p1 = result;

    while (p1 != NULL && p1->m_Next != NULL) {
        p2 = p1;


        while (p2->m_Next != NULL) {
            /* Pokud je duplicita tak smazay */
            if (p1->m_ID == p2->m_Next->m_ID) {

                duplicity = p2->m_Next;
                p2->m_Next = p2->m_Next->m_Next;
                free(duplicity);
            } else {
                p2 = p2->m_Next;
            }
        }
        p1 = p1->m_Next;
    }
    return result;

}

/*
 * Funkce na smazani seznamu vysledku
 */
void FreeResult(TRESULT * res) {

    TRESULT * tmp;

    while (res) {

        tmp = res->m_Next;
        free(res);
        res = tmp;

    }


}

/*
 * Funkce na spojeni dvou seznamu dohromady
 */
TRESULT * unionResult(TRESULT *D1, TRESULT *D2) {


    if (D1 == NULL)
        return D2;
    if (D2 == NULL)
        return D1;


    TRESULT *tmp = D1;

    while (tmp->m_Next != NULL) {
        tmp = tmp->m_Next;
    }
    tmp->m_Next = D2;

    /*
     * Volani funkce ktera odstani ze seznamu duplicity
     */
    return duplityResult(D1);

}

/*
 * Funkce na vyhledani spolecnych zaznamu a vytvoreni noveho zaznamu
 */
TRESULT * commonResult(TRESULT *D1, TRESULT * D2) {
    TRESULT * result = NULL;
    TRESULT * p, *q, *start, *cur;
    p = D1;
    start = NULL;
    while (1) {

        q = D2;
        while (1) {

            if (p->m_ID == q->m_ID) {

                if (result == NULL) {
                    result = (TRESULT *) malloc(sizeof (*result));

                    result->m_ID = q->m_ID;

                    result->m_Name = q->m_Name;
                    result->m_Next = NULL;
                    start = result;
                    cur = result;
                } else {

                    result = (TRESULT *) malloc(sizeof (*result));
                    cur->m_Next = result;
                    result->m_ID = q->m_ID;


                    result->m_Name = q->m_Name;
                    result->m_Next = NULL;
                    cur = result;

                }

            }

            if (q->m_Next == NULL)
                break;
            q = q->m_Next;
        }

        if (p->m_Next == NULL)
            break;
        p = p->m_Next;

    }
    /*
     * Uvolneni nepotrebnych zaznamu
     */
    p = NULL;
    q = NULL;
    cur = NULL;
    result = NULL;


    FreeResult(D1);
    FreeResult(D2);
    D1 = NULL;
    D2 = NULL;



    return start;

}

/*
 * Funkce na incializaci databaze
 */
void Init(void) {
    g_List = (TLIST*) malloc(sizeof (*g_List));
    g_List->m_First = NULL;
    g_List->m_Current = NULL;
    g_List->m_Last = NULL;

}

/*
 * Funkce na smazani databaze, prevzato z proseminare - datove struktury
 */
void Done(void) {


    TPERSON * tmp;

    while (g_List -> m_First) {
        tmp = g_List -> m_First -> m_Next;
        free(g_List->m_First->m_Name);
        free(g_List -> m_First);
        g_List -> m_First = tmp;
    }
    g_List -> m_Last = NULL;
    g_List -> m_Current = NULL;

    free(g_List);


}

/*
 * Funkce na vlozeni nove osoby
 */
int Register(int ID,
        const char * name,
        int ID1,
        int ID2) {
    /*
     * Kontrola vstupnich parametru
     */
    if (!notUnique(ID) || ID <= 0)
        return 0;

    if (ID1 == ID2 && ID1 != 0)
        return 0;
    if (ID == ID1 || ID == ID2)
        return 0;
    /*
     * Kontrola rodicu zda existuji
     */
    if ((!notUnique(ID1) || ID1 == 0) && (!notUnique(ID2) || ID2 == 0)) {


        /*
         * Vytvoreni osoby
         */

        TPERSON * person;
        person = (TPERSON *) malloc(sizeof (*person));
        if (person == NULL) {

            return 0;
        }

        person->m_ID = ID;

        person->m_Name = (char*) malloc(101 * sizeof (char *));
        strncpy(person->m_Name, name, 101);
        person->m_ID1 = ID1;
        person->m_ID2 = ID2;
        person->m_Next = NULL;
        person->m_Previous = NULL;

        /*
         * Prazdna databaze
         */
        if (g_List->m_Current == NULL) {


            g_List->m_First = person;
            g_List->m_Last = person;
            g_List->m_Current = person;
            return 1;

        }
        /*
         * Databaze jiz neni prazdna
         */
        person->m_Previous = g_List->m_Current;
        g_List->m_Current->m_Next = person;
        g_List->m_Current = person;
        g_List->m_Last = person;


        return 1;
    } else
        return 0;




}

/*
 * Funkce na vyhledavani predku - rekurzivne
 */
TRESULT * Ancestors(int ID) {
    /*
     * Predek neexistuje
     */
    if (notUnique(ID))
        return NULL;

    TPERSON * person = NULL;
    person = findPerson(ID);

    TRESULT * result1;
    result1 = (TRESULT *) malloc(sizeof (*result1));
    TRESULT * result2;
    result2 = (TRESULT *) malloc(sizeof (*result2));
    /*
     * Ulozeni prvniho predka pokud je znam
     */
    if (person->m_ID1 != 0 && person != NULL) {

        result1->m_ID = person->m_ID1;
        result1->m_Name = findPerson(person->m_ID1)->m_Name;
        result1->m_Next = Ancestors(person->m_ID1);

    } else {

        free(result1);
        result1 = NULL;
    }

    /*
     * Ulozeni druheho predka pokud je znam
     */
    if (person->m_ID2 != 0 && person != NULL) {
        result2->m_ID = person->m_ID2;
        result2->m_Name = findPerson(person->m_ID2)->m_Name;
        result2->m_Next = Ancestors(person->m_ID2);

    } else {


        free(result2);
        result2 = NULL;
    }

    /*
     * Zavolani funkce na spojeni seznamu
     */
    return unionResult(result1, result2);

}

/*
 * Funkce na vyhledavani spolecnych predku
 */
TRESULT * CommonAncestors(int ID1,
        int ID2) {
    /*
     * Existuji oba predci
     */
    if (notUnique(ID1) || notUnique(ID2))
        return NULL;

    TRESULT* result1;

    TRESULT* result2;

    /*
     * Volani funkce pro zjisteni predku jedne osoby
     */

    result1 = Ancestors(ID1);
    result2 = Ancestors(ID2);

    if (result1 == NULL || result2 == NULL) {
        FreeResult(result1);
        FreeResult(result2);

        return NULL;
    }

    /*
     * Volani funkce na nalezeni spolecnych predku ze seznamu osob
     */


    return commonResult(result1, result2);

}





#ifndef __PROGTEST__

int main(int argc, char * argv []) {
    char name[100];
    TRESULT * l;
    Init();
    assert(Register(1, "John", 0, 0) == 1);
    strncpy(name, "Jane", sizeof ( name));
    assert(Register(2, name, 0, 0) == 1);
    assert(Register(3, "Caroline", 0, 0) == 1);
    assert(Register(4, "Peter", 0, 0) == 1);
    assert(Register(5, "George", 1, 2) == 1);
    assert(Register(6, "Martin", 1, 2) == 1);
    assert(Register(7, "John", 3, 4) == 1);
    assert(Register(8, "Sandra", 3, 4) == 1);
    assert(Register(9, "Eve", 1, 2) == 1);
    assert(Register(10, "Douglas", 1, 4) == 1);
    strncpy(name, "Phillipe", sizeof ( name));
    assert(Register(11, name, 6, 8) == 1);
    strncpy(name, "Maria", sizeof ( name));
    assert(Register(12, name, 5, 8) == 1);
    l = Ancestors(11);
    /*
     * ID=1, name="John"
     * ID=2, name="Jane"
     * ID=3, name="Caroline"
     * ID=4, name="Peter"
     * ID=6, name="Martin"
     * ID=8, name="Sandra"
     */
    FreeResult(l);
    assert(Ancestors(3) == NULL);
    assert(Ancestors(13) == NULL);
    l = CommonAncestors(11, 12);
    /*
     * ID=1, name="John"
     * ID=2, name="Jane"
     * ID=3, name="Caroline"
     * ID=4, name="Peter"
     * ID=8, name="Sandra"
     */
    FreeResult(l);
    l = CommonAncestors(10, 9);
    /*
     * ID=1, name="John"
     */
    FreeResult(l);
    assert(CommonAncestors(7, 6) == NULL);
    l = CommonAncestors(7, 10);
    /*
     * ID=4, name="Peter"
     */
    FreeResult(l);
    assert(Register(13, "Quido", 12, 11) == 1);
    l = Ancestors(13);
    /*
     * ID=1, name="John"
     * ID=2, name="Jane"
     * ID=3, name="Caroline"
     * ID=4, name="Peter"
     * ID=5, name="George"
     * ID=6, name="Martin"
     * ID=8, name="Sandra"
     * ID=11, name="Phillipe"
     * ID=12, name="Maria"
     */
    FreeResult(l);
    l = CommonAncestors(9, 12);
    /*
     * ID=1, name="John"
     * ID=2, name="Jane"
     */
    FreeResult(l);
    assert(Register(1, "Francois", 0, 0) == 0);
    Done();

    Init();
    assert(Register(10000, "John", 0, 0) == 1);
    assert(Register(10000, "Peter", 0, 0) == 0);
    assert(Register(20000, "Jane", 10000, 0) == 1);
    assert(Register(30000, "Maria", 10000, 10000) == 0);
    assert(Register(40000, "Joe", 10000, 30000) == 0);
    assert(Register(50000, "Carol", 50000, 20000) == 0);
    Done();
    return 0;
}
#endif /* __PROGTEST__ */