Úkolem je vytvořit program, který bude vyhledávat optimální investice do pozemků.

Na vstupu programu je zadání cen parcel. Předpokládáme, že parcely leží v pravoúhlém rastru, kde známe počet řádek a sloupců. Cena je známá pro každou parcelu, jedná se o celé číslo (kladné, nulové, záporné). Záporná (nulová) cena parcely je přípustná, může se např. jednat o parcelu s břemenem. Po zadání cen pozemků následuje seznam dotazů. Chceme investovat zadaný objem peněz a hledáme parcelu/parcely, které mají v součtu cenu přesně rovnou zadanému objemu peněz. Jsme ale omezeni tím, že můžeme nakupovat pouze sousední parcely. Zakoupené parcely navíc musí tvořit obdélník či čtverec v rastru. Program dokáže zpracovávat dotazy dvou typů: buď pouze zobrazí počet různých možností, jak investovat zadanou částku (dotaz count), nebo navíc vypíše i seznam parcel, které dotazu vyhovují (dotaz list).

Vstupem programu je:

velikost rastru (šířka, výška), velikost je omezena na 1 až 2000 v každém směru,
ceny jednotlivých parcel, ceny jsou zadané po řádcích,
seznam dotazů.
Dotaz je buď typu count x nebo list x, kde x je investovaná částka.

Výstupem programu je vyřešení dotazů:

na dotaz typu count x je odpovědí počet různých způsobů, kterými lze investovat částku x,
na dotaz typu list x je odpovědí seznam alokací parcel, následovaný počtem nalezených alokací (tedy číslo stejné, jaké by vrátil dotaz typu count x). Seznam alokací parcel má podobu:
    x @ (x1,y1) - (x2,y2) 
    
kde x1,y1 je souřadnice levého horního rohu alokace parcel a x2,y2 je souřadnice pravého dolního rohu alokace parcel.
Pokud je vstup neplatný, program to musí detekovat a zobrazit chybové hlášení. Chybové hlášení zobrazujte na standardní výstup (ne na chybový výstup). Za chybu považujte:

rozměr rastru je nečíselný, nulový, záporný nebo překračuje limit 2000,
zadaná cena parcel není číslo,
dotaz není typu count ani list,
číslo v dotazu chybí / není správně zadané.
Před implementací programu si rozmyslete, jakým způsobem budete reprezentovat ceny pozemků a jak budete v zadaném rastru vyhledávat. Velikost rastru je omezená na max. 2000 prvků v každém směru. Pro řešení tedy postačuje staticky alokovaná paměť.

Vyhledávání v cenách pozemků může trvat velmi dlouho. Naivní řešení má složitost n6, vylepšováním algoritmu se dá složitost výrazně snížit. Časové limity testovacího prostředí jsou nastavené tak, aby rozumná implementace naivního algoritmu prošla všemi testy mimo testů bonusových.


