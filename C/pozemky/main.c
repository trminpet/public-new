/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: petr
 *
 * Created on November 19, 2016, 9:10 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/*
 * Funkce na zjisteni velikosti mapy && osetreni validity parametru mapy
 */
int getSizeOfArray(int * column, int * row) {
    printf("Velikost mapy:\n");
    if (scanf("%d %d", column, row) != 2 || *row <= 0 || *column <= 0 || *row > 2000
            || *column > 2000) {
        return 1;
    }

    return 0;

}

/*
 * Funkce na alokovani velikosti zadaneho pole
 */
int ** alocationOfArray(int column, int row) {

    int ** tmp;
    int i;
    tmp = (int **) malloc(sizeof (*tmp) * row);

    for (i = 0; i < row; i++) {
        tmp[i] = (int*) malloc(sizeof (*tmp[i]) * column);

    }
    /*
     * Vracim ukazatel na naalokavane pole
     */
    return tmp;


}

/*
 * Funkce na uvolneni pole
 */
void freeArray(int ** arr, int row) {

    int i;
    /*
     * Uvolneni sloupcu
     */
    for (i = 0; i < row; i++) {
        free(arr[i]);
    }
    /*
     * Uvolneni radku
     */
    free(arr);

}

/*
 * Funkce na nacteni hodnot do pole && validace vstupu
 */
int getValuesIntoArray(int ** arr, int column, int row) {

    int i;
    int j;
    for (i = 0; i < row; i++) {

        for (j = 0; j < column; j++) {
            /*
             * Pokud byl nacten navalidni vstup uvolnim naalokavanou pamet a vracim 1
             */
            if (scanf("%d", &arr[i][j]) != 1) {
                freeArray(arr, row);
                return 1;
            }

        }


    }


    return 0;
}

/*
 * Pomocna funkce na vytisknuti pole
 */
void printArray(int ** arr, int column, int row) {

 int j;
    int i;
    for (i = 0; i < row; i++) {
       
        for (j = 0; j < column; j++) {
            printf("%d ", arr[i][j]);

        }
        printf("\n");


    }



}

/*
 * Hlavni funkce na vyhledava v poli
 */
void findInArray(int ** arr, int column, int row, int value, int * count, int flag) {

    int sum = 0;
    int counter = 0;
    int rowStart =0;
    int columnStart=0; 
    int rowEnd=0;
    int columnEnd=0;
    int r =0; 
    int c =0;




    /*
     * Hledani spravne sumy v poli row,column je velikost pole
     */

    /*
     * Cyklus ktery projde cele pole po radcich
     */
    for (rowStart = 0; rowStart < row; rowStart++) {
        /*
         * Cyklus ktery projde cele pole po sloupcich
         */
        for (columnStart = 0; columnStart < column; columnStart++) {
            /*
             * Cyklus ktery omezuje velikost radku pro vyhledavani
             */
            for (rowEnd = 0; rowEnd < row; rowEnd++) {
                /*
                 * Cyklus ktery omezuje velikost sloupce pro vyhledavni
                 */
                for (columnEnd = 0; columnEnd < column; columnEnd++) {
                    /*
                     * Cyklus ktery projizdi limitovane pole omezeno pomoci predeslych a nacita hodnoty bunek pro kazdy novy prubeh resetuje sumu
                     * Zacina na 0,0 a postupne roste do sirky a hloubky tim se zvetsuje prohledavane pole
                     * Po projeti se resetuje a posune se v radku. Dokud se neprojede cele puvodni pole a nenajdou se spravne sumy
                     */
                    sum = 0;
                    for (r = rowStart; r <= rowEnd; r++) {

                        for (c = columnStart; c <= columnEnd; c++) {
                            /*
                             * Nacitani hodnoty bunky do celkove sumy "maleho" pole
                             */
                            sum += arr[r][c];

                        }

                    }
                    /*
                     * Po projeti "maleho pole" vyhodnoceni stavu zdali se nactena sumu shoduje s hledanou hodnotou
                     * && osetreni hodnot, kdy se pocita s nulami
                     */
                    if (sum == value && (r != rowStart && c != columnStart)) {
                        /*
                         * Pokud FLAG=1 je volana funkce list tak vypisuji pozice
                         */
                        if (flag) {
                            /*
                             * Vypis pozic levy horni roh a pravy dolni roh -1
                             */
                            printf("%d @ (%d,%d) - (%d,%d)\n", value, columnStart, rowStart, c - 1, r - 1);
                        }
                        /*
                         * Pocitani vyskytu sum
                         */
                        counter++;
                        continue;

                    }

                }

            }

        }
    }
    /*
     * Predani promenne vystupnimu parametru
     */
    *count = counter;
}

int main(int argc, char** argv) {
    int row = 0;
    int column = 0;
    int **arr;
    char query[6] = "a";
    int value = 0;
    int count = 0;
    int flag = 0;
    /*
     * Zavolani funkce na nacteni velikosti pole, pokud vse v poradku alokuji pole
     */
    if (!getSizeOfArray(&column, &row)) {
        arr = alocationOfArray(column, row);
        printf("Cenova mapa:\n");
        if (!getValuesIntoArray(arr, column, row)) {
            /*
             * Nacitani prikazu list/count . Promenna FLAG slouzi k vypisu hodnot (ON|OFF)
             */
            printf("Dotazy:\n");
            while (scanf("%6s %d", query, &value) == 2) {

                /*
                 * Kontrola zadaneho prikazu a dle situaci s nim pracuji
                 */
                if (!strcmp(query, "list")) {
                    flag = 1;
                    findInArray(arr, column, row, value, &count, flag);
                    printf("Celkem: %d\n", count);
                } else if (!strcmp(query, "count")) {
                    flag = 0;
                    findInArray(arr, column, row, value, &count, flag);
                    printf("Celkem: %d\n", count);
                } else {
                    freeArray(arr, row);
                    printf("Nespravny vstup.\n");
                    return 0;

                }
            }
            /*
             * Kontrola ukonceni vstupu
             */
            if (!feof(stdin)) {
                freeArray(arr, row);
                printf("Nespravny vstup.\n");
                return 0;
            }
            /*
             * Konec programu, uvolnim pamet
             */
            freeArray(arr, row);
            return 0;




        }
        /*
         * Vypis hlasky pokud byla do pole zadana nevalidni hodnota-znak
         */
        printf("Nespravny vstup.\n");
        return 0;

        /*
         * Vypis hlasky pokud pri deklraci pole byla zadana nevalidni hodnota
         */
    } else {

        printf("Nespravny vstup.\n");
        return 0;
    }
    return (EXIT_SUCCESS);
}

